from main.models import *
from django import forms
from stellar_sdk.server import Server
from stellar_sdk.asset import Asset
from datetime import date
import re

class trasferenciaForm(forms.Form):
    username=forms.CharField(max_length=255,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white vali','placeholder':'Usuario Prontopagos, billetera o dirección de federación'}))
    nu_monto=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white vali','type':'number'}))
    nu_comision=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white vali','type':'number','onfocus':'this.blur()'}))
    nu_total=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white vali','type':'number','onfocus':'this.blur()'}))
    
    #co_moneda=forms.ModelChoiceField(queryset=WalletTb003Moneda.objects.none(), empty_label="Seleccione...", to_field_name="co_moneda",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    tx_balance=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','onfocus':'this.blur()'}))
    tx_limite=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','onfocus':'this.blur()'}))
    nu_balance=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'hidden'}))
    nu_limite=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'hidden'}))
    usernamedir = forms.ModelChoiceField(queryset=MainTb022Directorio.objects.none(), empty_label="Seleccione...", to_field_name="co_directorio",required=False,widget=forms.Select(attrs={'class': 'selectpicker','data-size': '7','data-style':'btn btn-primary vali','title':'Seleccione...'}))
    co_moneda=forms.ChoiceField(choices =(), label="Seleccione...",required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size': '7','data-style':'btn btn-primary','title':'Seleccione...'}))
    tx_apodo=forms.CharField(max_length=255,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    co_memo=forms.ChoiceField(choices =(),required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size': '7','data-style':'btn btn-primary'}))
    
    def clean_username(self):
        
        usernamedir=self.data.get('usernamedir') if self.data.get('usernamedir')!='' else 0
        username=self.cleaned_data['username']
        if WalletTb004Billetera.objects.filter(co_usuario__username=username).count()<=0 and WalletTb004Billetera.objects.filter(co_usuario=usernamedir).count()<=0:
            raise forms.ValidationError("el usuario no existe")

        return username

    def __init__(self,*args, **kwargs):  
        
        self.user=kwargs.pop('user')
        self.lista=kwargs.pop('lista')
        self.lista2=kwargs.pop('lista2')
        super(trasferenciaForm, self).__init__(*args, **kwargs)
        self.fields['usernamedir'].queryset=MainTb022Directorio.objects.filter(co_usuario=self.user.co_usuario)

        #tx_simbolo__in()
        self.fields['co_moneda'].choices =self.lista
        self.fields['co_memo'].choices =self.lista2

    def clean_nu_monto(self):
        balance=float(self.data.get('nu_balance'))
        nu_monto=float(self.cleaned_data['nu_monto'])

        if self.data.get('nu_limite') != 'Sin limite':
            if nu_monto>float(self.data.get('nu_limite')):     
                raise forms.ValidationError("El monto supera a tu limite disponible")

        if nu_monto > balance:
            raise forms.ValidationError("El monto supera a tu balance")

class depositoForm(forms.Form):
    co_moneda=forms.ChoiceField(choices =(), label="Seleccione...",required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size': '7','data-style':'btn btn-primary vali','title':'Seleccione...'}))

    def __init__(self,*args, **kwargs):  
        
        self.lista=kwargs.pop('lista')
        super(depositoForm, self).__init__(*args, **kwargs)
        self.fields['co_moneda'].choices =self.lista

class retiroForm(forms.Form):
    co_moneda=forms.ChoiceField(choices =(), label="Seleccione...",required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size': '7','data-style':'btn btn-primary vali','title':'Seleccione...'}))
    tx_cuenta=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control'}))
    nu_monto=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'number'}))
    nu_comision=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white vali','type':'text','onfocus':'this.blur()'}))
    nu_total=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white vali','type':'text','onfocus':'this.blur()'}))
    
    def __init__(self,*args, **kwargs):  
        
        self.lista=kwargs.pop('lista')
        super(retiroForm, self).__init__(*args, **kwargs)
        self.fields['co_moneda'].choices =self.lista



class comercioForm(forms.Form):
    co_moneda=forms.ChoiceField(choices =(), label="Seleccione...",required=True,widget=forms.Select(attrs={'class': 'form-control text-white vali'}))
    tx_cuenta=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control'}))
    nu_monto=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'number'}))
    def __init__(self,*args, **kwargs):  
        
        self.lista=kwargs.pop('lista')
        super(retiroForm, self).__init__(*args, **kwargs)
        self.fields['co_moneda'].choices =self.lista


class trasferenciaForm2(forms.Form):
    tx_public_key=forms.CharField(max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    nu_monto=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'number'}))


class recargaBSSForm(forms.Form):
    co_moneda=forms.ChoiceField(choices =(), label="Seleccione...",required=True,widget=forms.Select(attrs={'class': 'form-control text-white vali'}))
    co_banco=forms.ModelChoiceField(queryset=WalletTb007Banco.objects.none(), empty_label="Seleccione...", to_field_name="co_banco",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_cuenta=forms.ModelChoiceField(queryset=WalletTb008Cuenta.objects.none(), empty_label="Seleccione...", to_field_name="co_cuenta",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    #cedul=forms.ModelChoiceField(queryset=WalletTb008Cuenta.objects.all(), empty_label="Seleccione...", to_field_name="cedul",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    #nombre=forms.ModelChoiceField(queryset=WalletTb008Cuenta.objects.all(), empty_label="Seleccione...", to_field_name="nombre",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    #tx_comentario =forms.CharField(max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_codigo =forms.CharField(max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    nu_monto=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','type':'number'}))

    def __init__(self, *args, **kwargs):
        self.lista=kwargs.pop('lista')
        self.in_envio1=kwargs.pop('in_envio1')
        super(recargaBSSForm, self).__init__(*args, **kwargs)
      
        if self.in_envio1:
            #self.fields['co_billetera'].queryset = WalletTb004Billetera.objects.none()
            self.fields['co_cuenta'].queryset = WalletTb008Cuenta.objects.all()
            self.fields['co_banco'].queryset = WalletTb008Cuenta.objects.all()
            #self.fields['cedul'].queryset = WalletTb008Cuenta.objects.none()
            #self.fields['nombre'].queryset = WalletTb008Cuenta.objects.none()
     
        self.fields['co_moneda'].choices =self.lista

class bandejaServiciosForm(forms.Form):
    co_servicios=forms.ModelChoiceField(queryset=MainTb012Servicios.objects.all(), empty_label="Seleccione...", to_field_name="co_servicios",required=False,widget=forms.Select(attrs={'class': 'selectpicker','data-size': '7','data-style':'btn btn-primary','title':'Seleccione...'}))
    co_proveedor_servicios=forms.ModelChoiceField(queryset=MainTb011ProveedorServicios.objects.all(), empty_label="Seleccione...", to_field_name="co_proveedor_servicios",required=False,widget=forms.Select(attrs={'class': 'selectpicker','data-size': '7','data-style':'btn btn-primary','title':'Seleccione...'}))
    co_estatus = forms.ModelChoiceField(queryset=WalletTb009Estatus.objects.all(), empty_label="Seleccione...", to_field_name="co_estatus",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    
    def __init__(self, *args, **kwargs):
    
        self.in_envio=kwargs.pop('in_envio')
        super(bandejaServiciosForm, self).__init__(*args, **kwargs)
      
        if not self.in_envio:
            self.fields['co_servicios'].queryset = MainTb012Servicios.objects.none()
class pagoServiciosForm(forms.Form):
    nu_monto=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'number' , 'disabled':'True'}))
    #co_tipo_moneda=forms.ModelChoiceField(queryset=WalletTb002TipoMoneda.objects.all(), empty_label="Seleccione...", to_field_name="co_tipo_moneda",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    #co_moneda=forms.ModelChoiceField(queryset=WalletTb003Moneda.objects.none(), empty_label="Seleccione...", to_field_name="tx_simbolo",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    tx_balance=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','disabled':'disabled'}))
    tx_limite=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','disabled':'disabled'}))
    nu_balance=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'hidden'}))
    nu_limite=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'hidden'}))
    co_servicios=forms.ModelChoiceField(queryset=MainTb012Servicios.objects.all(), empty_label="Seleccione...", to_field_name="co_servicios",required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size': '7','data-style':'btn btn-primary','title':'Seleccione...', 'disabled':'True'}))
    co_proveedor_servicios=forms.ModelChoiceField(queryset=MainTb011ProveedorServicios.objects.all(), empty_label="Seleccione...", to_field_name="co_proveedor_servicios",required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size': '7','data-style':'btn btn-primary','title':'Seleccione...'}))
   
    """def clean_nu_monto(self):
        balance=float(self.data.get('nu_balance'))
        nu_monto=float(self.cleaned_data['nu_monto'])

        if self.data.get('nu_limite') != 'Sin limite':
            if nu_monto>float(self.data.get('nu_limite')):     
                raise forms.ValidationError("El monto supera a tu limite disponible")

        if nu_monto > balance:
            raise forms.ValidationError("El monto supera a tu balance")"""

    def __init__(self, *args, **kwargs):

        self.in_envio=kwargs.pop('in_envio')
        super(pagoServiciosForm, self).__init__(*args, **kwargs)
      
        if not self.in_envio:
            self.fields['co_servicios'].queryset = MainTb012Servicios.objects.none()
       

class newDirectorioForm(forms.Form):
    tx_apodo=forms.CharField(max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    
    
    """def __init__(self,*args, **kwargs):  
        
        self.user=kwargs.pop('user')
        super(trasferenciaForm, self).__init__(*args, **kwargs)
        self.fields['usernamedir'].queryset=MainTb022Directorio.objects.filter(co_usuario=self.user.co_usuario)
     """
class retirarFondosForm(forms.Form):
    co_moneda=forms.ModelChoiceField(queryset=WalletTb003Moneda.objects.filter(co_tipo_moneda=3), empty_label="Seleccione...", to_field_name="co_moneda",required=True,widget=forms.Select(attrs={'class':'form-control text-white'}))
    co_banco=forms.ModelChoiceField(queryset=MainTb016BancoMoneda.objects.none(), empty_label="Seleccione...", to_field_name="co_banco_moneda",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_cuenta = forms.ModelChoiceField(queryset=MainTb017Cuenta.objects.none(), empty_label="Seleccione...", to_field_name="co_cuenta",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    nu_monto=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'float'}))
    tx_balance=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','disabled':'disabled'}))
    nu_balance=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'hidden'}))
    
    def __init__(self, *args, **kwargs):
        
        self.in_envio=kwargs.pop('in_envio')
        #selft.arg=args
        super(retirarFondosForm, self).__init__(*args, **kwargs)
      
        if self.in_envio:
            self.fields['co_cuenta'].queryset = MainTb017Cuenta.objects.filter(co_banco_moneda=self.data.get('co_banco'))
            self.fields['co_banco'].queryset = MainTb016BancoMoneda.objects.filter(co_moneda=self.data.get('co_moneda'))



    def clean_nu_monto(self):
        balance=float(self.data.get('nu_balance'))
        nu_monto=float(self.cleaned_data['nu_monto'])

        if nu_monto <= 0:
            raise forms.ValidationError("El monto no puede ser menor o igual a 0")

        """if nu_monto > balance:
            raise forms.ValidationError("El monto supera a tu balance")"""

    

class retirosAprobarForm(forms.Form):
    hash_referencia=forms.CharField(max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))

class agentesMetodosPagosForm(forms.Form):
    co_metodo_pago=forms.ModelChoiceField(queryset=WalletTb025MetodoPago.objects.all(), empty_label="Seleccione...", to_field_name="co_metodo_pago",required=True,widget=forms.Select(attrs={'class': 'selectpicker','multiple':'multiple', 'title':'Seleccione...','data-size': '7',}))
    co_proveedor_servicios=forms.ModelChoiceField(queryset=MainTb011ProveedorServicios.objects.all(), empty_label="Seleccione...", to_field_name="co_proveedor_servicios",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_servicios=forms.ModelChoiceField(queryset=MainTb012Servicios.objects.all(), empty_label="Seleccione...", to_field_name="co_servicios",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    
    def __init__(self, *args, **kwargs):
    
        self.in_envio=kwargs.pop('in_envio')
        super(agentesMetodosPagosForm, self).__init__(*args, **kwargs)
      
        if not self.in_envio:
            self.fields['co_servicios'].queryset = MainTb012Servicios.objects.none()
    

class balanceForm(forms.Form):
    tx_asset_code=forms.CharField(max_length=12,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_asset_issuer=forms.CharField(max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))

class activosForm(forms.Form):
    tx_asset_code=forms.CharField(max_length=12,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_asset_issuer=forms.CharField(max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))

    def clean_tx_asset_issuer(self):

        asset_issuer=self.cleaned_data['tx_asset_issuer']
    
        if WalletTb029UsuarioActivos.objects.filter(co_activo__tx_asset_issuer=asset_issuer,co_usuario=self.usuario.co_usuario).count()>0:
            raise forms.ValidationError("Ya posees este activo registrado")

    def __init__(self, *args, **kwargs):
        self.usuario=kwargs.pop('usuario')
        
        super(activosForm, self).__init__(*args, **kwargs)


class historicoForm(forms.Form):
      #filtros 
    fecha_desde=forms.DateField(required=False,widget=forms.DateInput(attrs={'class': 'form-control text-white ', 'type':'date'}))
    fecha_hasta=forms.DateField(required=False,widget=forms.DateInput(attrs={'class': 'form-control text-white ', 'type':'date'}))
    acronimo=forms.ModelChoiceField(queryset=WalletTb003Moneda.objects.all(), empty_label="Seleccione...", to_field_name="co_moneda",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'})) 
    co_estatus = forms.ModelChoiceField(queryset=WalletTb009Estatus.objects.all(), empty_label="Seleccione...", to_field_name="co_estatus",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_tipo_transaccion=forms.ModelChoiceField(queryset=WalletTb013TipoTransaccion.objects.all(), empty_label="Seleccione...", to_field_name="co_tipo_transaccion",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'}))


class importarForm(forms.Form):
    tx_frase=forms.CharField(required=False,widget=forms.Textarea(attrs={'class': 'form-control text-white seg','placeholder':'Ingrese las 12 o 24 palabras de seguridad' }))
    tx_private_key=forms.CharField(max_length=56,min_length=56,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white seg','placeholder':'Ingrese los 56 caracteres de la llave privada que empieza por la "S"'}))
    tx_username=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_username2=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    
    def __init__(self, *args, **kwargs):
        if "tipo" in kwargs:
            self.tipo=kwargs.pop('tipo')
        else:
            self.tipo=None
        
        if "in_envio" in kwargs:
            self.in_envio=kwargs.pop('in_envio')
        else:
            self.in_envio=None

        if "in_envio2" in kwargs:
            self.in_envio2=kwargs.pop('in_envio2')
        else:
            self.in_envio2=None
        super(importarForm, self).__init__(*args, **kwargs)
        if self.in_envio is not None:
            self.fields['tx_username'].required = True
            
        if self.in_envio2 is not None:
            self.fields['tx_username2'].required = True

    def clean_tx_private_key(self):
        tx_private_key=self.cleaned_data['tx_private_key']
        if self.tipo=="1":
            if tx_private_key[0]!="S":
                raise forms.ValidationError("No es una llave privada")

    def clean_tx_frase(self):
        tx_frase=self.cleaned_data['tx_frase']
        numero = tx_frase.split(' ')
        if self.tipo=="2":
            print(len(numero))
            if len(numero)!=12 and len(numero)!=24:
                raise forms.ValidationError("Los numero de las palabras no coinciden")
        
class SolicitudCompraLumensForm(forms.Form):
    cantidad_depositar=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Monto a pagar','onfocus':'this.blur()'}))
    #moneda_depositar=forms.ModelChoiceField(queryset=WalletTb003Moneda.objects.all(), empty_label="Seleccione...", to_field_name="co_moneda",required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size':'7','data-style':'btn btn-primary','title':'Moneda'}))
    moneda_depositar=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','value':'BSS','disable':'disable','onfocus':'this.blur()'}))
    cantidad_recibido=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Monto a obtener'}))
    moneda_recibido=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','value':'XLM','disable':'disable','onfocus':'this.blur()'}))
    #co_referencia=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Codigo de Referencia'}))
    #fecha_transferencia=forms.DateField(required=True,widget=forms.DateInput(attrs={'class': 'form-control datepicker', 'type':'date'}))

class SolicitudCompraLumensReferenciaForm(forms.Form):
    cantidad_depositar=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Moneda a Depositar'}))
    moneda_depositar=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','value':'BSS','disable':'disable'}))
    cantidad_recibido=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Moneda a Recibir'}))
    moneda_recibido=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','value':'XLM','disable':'disable'}))
    
    co_banco=forms.ModelChoiceField(queryset=WalletTb007Banco.objects.all(), empty_label="Seleccione...", to_field_name="co_banco",required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size':'7','data-style':'btn btn-primary','title':'Banco'}))
    co_referencia=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Codigo de Referencia'}))
    #co_banco=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Banco donde hizo la transferencia'}))
    fecha_transferencia=forms.DateField(required=True,widget=forms.DateInput(attrs={'class': 'form-control datepicker', 'type':'date'}))

class CompraXLMParte1Form(forms.Form):
    nu_monto=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Monto'}))
    co_metodo_pago=forms.ModelChoiceField(queryset=WalletTb025MetodoPago.objects.all(), empty_label="Seleccione...", to_field_name="co_metodo_pago",required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size':'7','data-style':'btn btn-primary','title':'Banco'}))
    
class CompraXLMParte2Form(forms.Form):
    fe_transaccion=forms.DateField(required=True,widget=forms.DateInput(attrs={'class': 'form-control datepicker', 'type':'date'}))
    co_banco=forms.ModelChoiceField(queryset=WalletTb007Banco.objects.all(), empty_label="Seleccione...", to_field_name="co_banco",required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size':'7','data-style':'btn btn-primary','title':'Banco'}))
    nu_referencia=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Codigo de Referencia'}))
    
    def clean_fe_transaccion(self):
        fecha=self.cleaned_data['fe_transaccion']
        fecha_hoy = date.today()
        if(fecha>fecha_hoy):
            raise forms.ValidationError("La fecha debe ser menor o igual a hoy.")
        
        
        return fecha
    
    
    
    def clean_nu_referencia(self):
        banco=self.cleaned_data['co_banco']
        nu_referencia=self.cleaned_data['nu_referencia']
        recarga=WalletTb010Recarga.objects.filter(co_banco=banco,tx_codigo=nu_referencia)
        if recarga.exists():
            raise forms.ValidationError("La referencia ya existe")
        return nu_referencia

class SolicitudUSDVForm(forms.Form):
    nu_monto_usd=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','type':'text','placeholder':'Dolares Americanos (USD)'}))
    nu_monto_usdv=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','type':'text','placeholder':'Dolares ProntoPagos (USDV)','onfocus':'this.blur()'}))
    nu_fee=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','type':'text','placeholder':'Comisión','onfocus':'this.blur()'}))
    img_paypal= forms.ImageField(required=True,widget=forms.FileInput( attrs={'class': 'imagen_input','id':'img_paypal','name':'img_paypal','type':'file'}))

class SolicitudUsdvTdcForm(forms.Form):
    nu_monto_usdv=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','type':'text','placeholder':'Monto a Recibir en USDV'}))
    cvv = forms.CharField( max_length=3, required=True,widget=forms.TextInput(attrs={'class': 'form-control','type':'text'}))
    nu_tarjeta = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','type':'text'}))
    nu_identificacion = forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','type':'text','placeholder':'Número de Identificación'}))
    nu_monto_usd=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','onfocus':'this.blur()','placeholder':'Total a Cancelar USD'}))

    
  
      
        