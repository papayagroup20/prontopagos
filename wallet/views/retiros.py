from django.shortcuts import render,redirect
from rest_framework.authtoken.models import Token
from wallet.form import (retiroForm,retirarFondosForm,retirosAprobarForm)
from main.models import (
    WalletTb038MensajeStellar,WalletTb037Keystore,MainTb028Solicitud,
    WalletTb015Transaccion,WalletTb009Estatus,WalletTb003Moneda,WalletTb013TipoTransaccion,
    WalletTb018Retiro,MainTb017Cuenta,WalletTb027RetiroHistorico,MainTb029Ruta)
from django.db import IntegrityError, transaction

from apipago.serializers import MensajeStellarSerializer
from main.views.utils import (
    getContentBasicoL2,user2fa_required,confirmado_user_required)
from apipago.views.stellar import getAccountBalance
import json

@confirmado_user_required
@user2fa_required
def retiroVario(request):
    misAsset=getAccountBalance(request.session["public_key"])
    querymsjstellar=WalletTb038MensajeStellar.objects\
        .select_related("co_tipo_error_stellar")\
        .filter(co_tipo_error_stellar__in=[1,2,3,4])

    msjstellar=MensajeStellarSerializer(querymsjstellar,many=True)
    
    
    choice=[]
    choice=[("0","Seleccione...")]
    for a in misAsset["data"]:
        if a["tx_asset_issuer"]!="":
            choice.append((a["tx_asset_issuer"],a["tx_asset_code"]))

    content=getContentBasicoL2(request.user,None,retiroForm(lista=choice))
    #comision=WalletTb014Comision.objects.filter(co_tipo_transaccion=4,in_activo=True).only("nu_monto").first()
    #content['comision']=comision.nu_monto
    content['publico']=request.session["public_key"]
    content['misactivos']=misAsset["data"]
    content['misactivosjson']=json.dumps(content['misactivos'])
    content['msjstellarjson']=json.dumps(msjstellar.data)
    #content['publico']="GACXJ6ZKU3IX7RL5LZCVVAOFRLLMRLN4SC7W5IJGTTDJFYSRUKINUD6K"
    
    return render(request, '../templates/wallet/Retiros/retiro.html',content)


@confirmado_user_required
@user2fa_required
def retirofondos(request, co_tipo_solicitud):
    billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user.co_usuario)[0]
    content=getContentBasicoL2(request.user,co_tipo_solicitud,retirarFondosForm(in_envio=False))
    
    # token, _ =Token.objects.get_or_create(user = request.user)
    # content={
    #     'success':True,
    #     'data':'',
    #     'form':retirarFondosForm(),
    #     'token':token,
    #     'msg':''
    # }
    # content['msg']=""
    if  request.method=='POST':
        form=retirarFondosForm(request.POST,in_envio=True)
        
        
        
        if form.is_valid():
            
            try:
                with transaction.atomic():
                    #comision=WalletTb014Comision.objects.get(co_moneda=request.POST ['co_moneda'], co_tipo_transaccion=4)
                    solicitud=MainTb028Solicitud.objects.newSolicitud(co_tipo_solicitud,request.user)
                    solicitud.save()
                    #transferencia=transccionConComision(request.POST['co_moneda'],request.POST['nu_monto'],billetera,request.user,1,2,solicitud)

                    """transaccion=transccionConComision(
                        co_moneda=WalletTb003Moneda.objects.get(co_moneda=request.POST ['co_moneda']),
                        nu_monto=request.POST['nu_monto'],
                        destino=WalletTb037Keystore.objects.get(co_usuario__username='DISTRIBUIDORPPG'),                      
                        billetera=billetera,
                        usuario=request.user,
                        tipo_transaccion=4,
                        estatus=2,
                        co_solicitud=solicitud,)
                    transaccion.save()"""

                    transaccion=WalletTb015Transaccion(
                    nu_monto=request.POST ['nu_monto'],
                    co_emisor=request.user,
                    co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                    co_moneda=WalletTb003Moneda.objects.get(co_moneda=request.POST['co_moneda']),
                    co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=4),               
                    co_solicitud=solicitud
                    )
                    transaccion.save()

                    retiro=WalletTb018Retiro(
                        
                        #nu_monto=monto,
                        #nu_monto=request.POST['nu_monto'],
                        co_cuenta=MainTb017Cuenta.objects.get(co_cuenta=request.POST['co_cuenta']),
                        co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                        co_usuario=request.user,
                        co_transaccion=transaccion,
                        co_solicitud=solicitud
                    
                    )
                    retiro.save()
                    
                    retiro_historico=WalletTb027RetiroHistorico(
                        co_retiro=retiro,
                        co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                        co_usuario=request.user
                    )
                    retiro_historico.save()
                    
                    solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                    solicitud.in_finanza=True
                    solicitud.save()

                    ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                    ruta.in_activo=False
                    ruta.save()

                    
                    return redirect('bandejaInicioSuccess',success=1,codigo=6)
                         
            except IntegrityError:
                content['success']=False
                content['msg']="Ha habido un error en la operación"
                   
        #form=retirarFondosForm(request.POST,in_envio=False)
        content['form']=form
    return render(request, '../templates/wallet/Retiros/retirarfondos.html',content)


@confirmado_user_required
@user2fa_required
def bandejaRetiros(request):
    token, _ =Token.objects.get_or_create(user = request.user)
    content={
        'success':True,
        'data':'',
        'form':retirosAprobarForm(),
        'token':token,
        'msg':''
    }
    
    content['msg']=""
    

    historico=WalletTb018Retiro.objects.filter(in_activo=True, co_estatus=1, co_transaccion__co_tipo_transaccion=4)
    content['data']=historico

    #return render(request, '../templates/wallet/Retiros/bandejaretiros.html',content)




    form=retirosAprobarForm(request.POST)
    if  request.method=='POST':
        if 'boton' in request.POST:
            if request.POST['boton']=="3":
                
                try:
                    with transaction.atomic():
                        retiro_actualiza=WalletTb018Retiro.objects.get(co_retiro=request.POST['codigo'])
                        retiro_actualiza.co_estatus=WalletTb009Estatus.objects.get(co_estatus=5)
                        retiro_actualiza.save()
                    
                        retiro_historico=WalletTb027RetiroHistorico(
                            co_retiro=retiro_actualiza,
                            co_estatus=WalletTb009Estatus.objects.get(co_estatus=5)
                            )
                        retiro_historico.save()

                        hash_referencia=WalletTb015Transaccion(
                            tx_hash=request.POST['hash_referencia']
                        )
                        hash_referencia.save()

                        content['msg']="La operacion ha sido realizada sactifactoriamente"
                            
                except IntegrityError:
                    content['success']=False
                    content['msg']="La operacion ha sido realizada sactifactoriamente"
        
    return render(request, '../templates/wallet/Retiros/bandejaretiros.html',content)




