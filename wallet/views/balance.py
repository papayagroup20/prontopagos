from django.shortcuts import render,redirect
from main.models import (
    WalletTb038MensajeStellar,WalletTb014Comision,MainTb040Mensajes,MainTb028Solicitud,
    WalletTb028Activos,WalletTb029UsuarioActivos,WalletTb030SolicitudConfianza,
    MainTb029Ruta,WalletTb037Keystore,WalletTb009Estatus,WalletTb015Transaccion)
from stellar_sdk.asset import Asset
from django.db.models import Q
from wallet.form import (balanceForm,historicoForm,activosForm)
from django.db import IntegrityError, transaction
from wallet.views.operaciones import darConfianza
from main.views.utils import (
    getContentSuperBasico,confirmado_user_required,
    user2fa_required,getContentBasicoL)
from apipago.serializers.general import MensajeStellarSerializer
from apipago.views.stellar_utils import StellarAccount
import json
from pprint import pprint

"""
Esta es una funcion se le pasara un request 
llenara un directorio y retonara este mismo
"""
def balanceData(request)->dict:

    content=getContentSuperBasico(usuario=request.user,form=balanceForm())

    p=request.session["public_key"]
    cuenta=StellarAccount(p)
    cuenta.getActivosMioRecomendados()
    content["activos"]=cuenta.balance
    #pprint(content["activos"])
    i=0
    content["recomendados"]=[]
    for va in content["activos"]:
        #print(json.dumps(va,indent=4))
        if va["asset_type"]!="native" and not va["propio"] and i<3:
            content["recomendados"].append(va)
            i+=1

        
    content["publico"]=p

    msj=WalletTb038MensajeStellar.objects\
        .select_related("co_tipo_error_stellar")\
        .filter(co_tipo_error_stellar__in=[1,2,11])

    mensajes=MensajeStellarSerializer(msj,many=True)
    comision=WalletTb014Comision.objects.filter(co_tipo_transaccion=12,in_activo=True).only("nu_monto").first()
    content['comision']=comision.nu_monto
    content['co_comision']=comision.co_comision
    content['msjstellarjson']=json.dumps(mensajes.data)
    content['allactivosjson']=json.dumps(content["activos"])
    content['incremental']=0



    return content

@confirmado_user_required
@user2fa_required
def balance(request):

    content=balanceData(request)

    return render(request, '../templates/wallet/balance/balance.html',content)


@confirmado_user_required
@user2fa_required
def balanceSuccess(request,success,codigo):

    content=balanceData(request)
    mensaje=MainTb040Mensajes.objects.get(co_mensajes=codigo)
    content['success']=success
    content['msg']=mensaje.tx_mensajes

    return render(request, '../templates/wallet/balance/balance.html',content)



@confirmado_user_required
@user2fa_required
def historico(request):
    content=getContentBasicoL(request.user,None,historicoForm())




    return render(request, '../templates/wallet/balance/historico.html',content)





@confirmado_user_required
def newActivoSinConfianza(request,co_tipo_solicitud):
    
    if request.method=='POST':
        form=activosForm(request.POST,usuario=request.user)
        if form.is_valid():
            try:
                with transaction.atomic():
                    Asset(request.POST['tx_asset_code'],request.POST['tx_asset_issuer'])

                    solicitud=MainTb028Solicitud.objects.newSolicitud(co_tipo_solicitud,request.user)
                    solicitud.save()
                    activo, _=WalletTb028Activos.objects.get_or_create(
                        tx_asset_code=request.POST['tx_asset_code'],
                        tx_asset_issuer=request.POST['tx_asset_issuer']
                        )
                    if activo.co_solicitud is None:
                        activo.co_solicitud=solicitud
                    activo.save()

                    usuactivo=WalletTb029UsuarioActivos(
                        co_usuario=request.user,
                        co_activo=activo
                    )
                    usuactivo.save()

                    solicitudConfianza=WalletTb030SolicitudConfianza(
                        co_usuario_activos=usuactivo,
                        co_solicitud=solicitud,
                    )
                    solicitudConfianza.save()


                    ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                    ruta.in_activo=False
                    ruta.save()

                    return redirect('balanceSuccess',success=1,codigo=2)
            except IntegrityError:
                return redirect('balanceSuccess',success=0,codigo=1)

            
    return redirect('balanceSuccess',success=0,codigo=1)


@confirmado_user_required
def setConfianza(request,co_solicitud):
    
    if request.method=='GET':
        solicitudConfianza=WalletTb030SolicitudConfianza.objects.filter(
            co_solicitud=co_solicitud,
            in_activo=True
        )[0]
        usuactivo=WalletTb029UsuarioActivos.objects.get(co_usuario_activos=solicitudConfianza.co_usuario_activos.co_usuario_activos)

        
        try:
            with transaction.atomic():
                billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user.co_usuario)[0]

                response=darConfianza(billetera,usuactivo.co_activo,"9000000")

                solicitud=MainTb028Solicitud.objects.get(co_solicitud=co_solicitud)
                solicitud.in_activo=False
                solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=2)
                solicitud.save()
                
                usuactivo.in_confianza=True
                usuactivo.save()

                solicitudConfianza.in_activo=False
                solicitudConfianza.save()


                
                return redirect('balanceSuccess',success=1,codigo=2)

        except IntegrityError:
                return redirect('balanceSuccess',success=0,codigo=1)

    
    
    
    return redirect('balanceSuccess',success=0,codigo=1)

@confirmado_user_required
def deletContianza(request,co_tipo_solicitud):
    
    if request.method=='POST':
        try:
            with transaction.atomic():
                billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user.co_usuario)[0]

                usuarioActivo=WalletTb029UsuarioActivos.objects.get(co_usuario_activos=request.POST['codigo'])
                
                
                solicitud=MainTb028Solicitud.objects.newSolicitud(co_tipo_solicitud,request.user)
                solicitud.save()
        
                solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=2)
                solicitud.in_activo=False
                solicitud.save()


                if  WalletTb030SolicitudConfianza.objects.filter(
                    co_usuario_activos=usuarioActivo.co_usuario_activos,
                    in_activo=True
                    ).count()>0:
                    
                    return redirect('balanceSuccess',success=0,codigo=9)
                
                usuarioActivo.in_confianza=False
                usuarioActivo.save()

            

                solicitudConfianza=WalletTb030SolicitudConfianza(
                        co_usuario_activos=usuarioActivo,
                        co_solicitud=solicitud,
                        in_activo=False
                    )
                solicitudConfianza.save()

                

                response=darConfianza(billetera,usuarioActivo.co_activo,"0")


                
                return redirect('balanceSuccess',success=1,codigo=2)

        except IntegrityError:
            return redirect('balanceSuccess',success=0,codigo=1)


                
    
    return redirect('balanceSuccess',success=0,codigo=1)

@confirmado_user_required
def setConfianza2(request,co_tipo_solicitud):
    
    if request.method=='POST':
        usuactivo=WalletTb029UsuarioActivos.objects.get(co_usuario_activos=request.POST['codigo'])

        
        try:
            with transaction.atomic():
                billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user.co_usuario)[0]

                response=darConfianza(billetera,usuactivo.co_activo,"9000000")

                solicitud=MainTb028Solicitud.objects.newSolicitud(co_tipo_solicitud,request.user)
                solicitud.save()

                ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                ruta.in_activo=False
                ruta.save()

                solicitud.in_activo=False
                solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=2)
                solicitud.save()
                
                usuactivo.in_confianza=True
                usuactivo.save()

                solicitudConfianza=WalletTb030SolicitudConfianza(
                        co_usuario_activos=usuactivo,
                        co_solicitud=solicitud,
                        in_activo=False
                    )
                solicitudConfianza.save()


                
                return redirect('balanceSuccess',success=1,codigo=2)

        except IntegrityError:
                return redirect('balanceSuccess',success=0,codigo=1)

    
    
    
    return redirect('balanceSuccess',success=0,codigo=1)


@confirmado_user_required
@user2fa_required
def filtro_historico(request):
    content={
        'success':True,
        'data':'',
        'user':'',
        'form': historicoForm(),
    }
    #filtro por fechas
    if (request.POST['acronimo']=='' and request.POST['co_estatus']=='' and request.POST['co_tipo_transaccion']=='' ):
        fe_inicio=request.POST['fecha_desde']
        fe_fin=request.POST['fecha_hasta']
        #historico=WalletTb015Transaccion.objects.filter(co_transaccion_emisor__isnull=True, update_at__range=(request.POST['fecha_desde'],request.POST['fecha_hasta']), co_emisor=request.user, co_receptor=request.user)
        historico=WalletTb015Transaccion.objects.filter(Q(updated_at__range=(fe_inicio,fe_fin)), Q(co_transaccion_emisor__isnull=True), Q(co_receptor=request.user) | Q(co_emisor=request.user) )
        content['user']=request.user
        content['data']=historico

    #filtro por monedas
    elif(request.POST['fecha_desde']=='' and request.POST['fecha_hasta']=='' and request.POST['co_tipo_transaccion']=='' and request.POST['co_estatus']==''):
        historico=WalletTb015Transaccion.objects.filter(Q(co_moneda=request.POST['acronimo']), Q(co_transaccion_emisor__isnull=True), Q(co_receptor=request.user) | Q(co_emisor=request.user) )
        content['user']=request.user
        content['data']=historico

    #filtro por estatus
    elif(request.POST['fecha_desde']=='' and request.POST['fecha_hasta']=='' and request.POST['acronimo']=='' and request.POST['co_tipo_transaccion']==''):
        historico=WalletTb015Transaccion.objects.filter(Q(co_estatus=request.POST['co_estatus']), Q(co_transaccion_emisor__isnull=True), Q(co_receptor=request.user) | Q(co_emisor=request.user) )
        content['user']=request.user
        content['data']=historico

    #filtro por tipo de transaccion
    elif(request.POST['fecha_desde']=='' and request.POST['fecha_hasta']=='' and request.POST['acronimo']=='' and request.POST['co_estatus']==''):
        historico=WalletTb015Transaccion.objects.filter(Q(co_tipo_transaccion=request.POST['co_tipo_transaccion']), Q(co_transaccion_emisor__isnull=True), Q(co_receptor=request.user) | Q(co_emisor=request.user) )
        content['user']=request.user
        content['data']=historico


    return render(request, '../templates/wallet/balance/historico.html',content)
    