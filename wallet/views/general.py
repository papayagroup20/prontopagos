from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required,user_passes_test
from django.http import HttpResponse
from main.models import *
from stellar_sdk.server import Server
from stellar_sdk.asset import Asset
from django.db.models import Q
from wallet.form import *
from django.db import IntegrityError, transaction
from main.views.utils import  getContentBasicoL,getContentBasicoL2,user2fa_required,confirmado_user_required
from wallet.views.operaciones import darConfianza
from stellar_sdk.keypair import Keypair
from django_otp.decorators import otp_required
import requests


@login_required
def getHeaderFederacion(request):
    header={
        "Authorization": 'Token bf00448f9159c3c3f1122212692b0b7103fb6d1f'
    }
    return header



@confirmado_user_required
def billeteraContainer(request):
    content=getContentBasicoL(request.user,None,importarForm())
    billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user.co_usuario).first()
    
    content['billetera']=billetera
    init={
        "federacion":'',
        "federacion2":'',
    }
    #if request.user.co_federation is not None and request.user.co_federation2 is not None and billetera is not None:
    if request.user.co_federation is not None and billetera is not None:
        gett=buscarFederacion(request,request.user.co_federation)
        #print(gett.json())
        init['federacion']=gett.json()
        #gett2=buscarFederacion(request,request.user.co_federation2)
        #init['federacion2']=gett2.json()

        content['form']=importarForm(initial=init)

        content['federacion']=init['federacion']
        #content['federacion2']=init['federacion2']
    
    else:
        content['form']=importarForm()
   
    
    return content



@confirmado_user_required
def billetera(request):
    content=billeteraContainer(request)

    #secreto=Keypair.from_secret("SCMQKWYCUHUYMD4TGA7KFJFUK5VNNRCM2ZFO4HQF62BL44USRZ43LJVQ")
    #frase=Keypair.generate_mnemonic_phrase('english',128)

    #secreto=Keypair.from_mnemonic_phrase("rice congress impulse salad media drastic chalk arch wage nut response piano")
    #print(frase)
    
    #url = 'https://friendbot.stellar.org'
    #response = requests.get(url, params={'addr': secreto.public_key})
    #print(secreto.public_key)
    return render(request, '../templates/wallet/billetera/billetera.html',content)



@confirmado_user_required
def billeteraSuccess(request,success,codigo):
    mensaje=MainTb040Mensajes.objects.get(co_mensajes=codigo)
    content=billeteraContainer(request)
    content['success']=success
    content['msg']=mensaje.tx_mensajes

    return render(request, '../templates/wallet/billetera/billetera.html',content)




@confirmado_user_required
def DeletBilletera(request,co_tipo_solicitud):
    
    content=getContentBasicoL2(request.user,co_tipo_solicitud,None)
    billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user.co_usuario)[0]
    try:
        with transaction.atomic():
            usuario=request.user
            billetera.save()


            

            solicitud=MainTb028Solicitud.objects.unPaso(co_tipo_solicitud,request.user)
                

            
            
            return redirect('billeterasu',success=1,codigo=12)

    except IntegrityError:
        
        return redirect('billeterasu',success=0,codigo=1)

@confirmado_user_required
def deleteFederacion(request,codigo):
    url="https://stellarid.io/api/addresses/"+str(codigo)+"/"
    delete=requests.delete(url,data=None,headers=getHeaderFederacion(request))

    return delete

def crearFederacion(request,username,public_key):
    url="https://stellarid.io/api/addresses/"
    postdata={
        "username":username,
        "domain":"gobelx.io",
        "account_id":public_key,
    }
    post=requests.post(url,data=postdata,headers=getHeaderFederacion(request))
   
    jsonpo1=post.json()
    return jsonpo1["id"]

def crearFede(request,username,public_key,domain):
    url="https://stellarid.io/api/addresses/"
    postdata={
        "username":username,
        "domain":domain,
        "account_id":public_key,
    }
    post=requests.post(url,data=postdata,headers=getHeaderFederacion(request))
   
    jsonpo1=post.json()
    return jsonpo1["id"]

@confirmado_user_required
def usernameFederacion(request,codigo,username):

    url="https://stellarid.io/api/addresses/"+str(codigo)+"/"
    patchdata={
        "username":username
    }
    patch=requests.patch(url,data=patchdata,headers=getHeaderFederacion(request))

    return patch


@confirmado_user_required
def publicKeyFederacion(request,codigo,public_key):
    url="https://stellarid.io/api/addresses/"+str(codigo)+"/"
    patchdata={
        "account_id":public_key
    }
    patch=requests.patch(url,data=patchdata,headers=getHeaderFederacion(request))
    return patch

def buscarFederacion(request,codigo):
    url="https://stellarid.io/api/addresses/"+str(codigo)+"/"
    get=requests.get(url,data=None,headers=getHeaderFederacion(request))
    return get

@confirmado_user_required
def createUpdateFederation(request,public_key):
    
    usuario=request.user
    if request.user.co_federation is not None:
        patch=publicKeyFederacion(request,request.user.co_federation,public_key)
    else:
        usuario.co_federation=crearFederacion(request,request.user.username,public_key)
    if request.user.co_federation2 is not None:
        patch=publicKeyFederacion(request,request.user.co_federation2,public_key)
    else:
        usuario.co_federation2=crearFederacion(request,request.user.email,public_key)
    usuario.save()

    return usuario

@confirmado_user_required
def NewBilletera(request,co_tipo_solicitud):
    
    content=getContentBasicoL2(request.user,co_tipo_solicitud,None)
    #billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user.co_usuario,in_activo=True)
    
    if WalletTb037Keystore.objects.filter(co_usuario=request.user.co_usuario).exists():
        return redirect('billeterasu',success=0,codigo=1)
    if  request.method=='POST':
        try:
            with transaction.atomic():
                
                solicitud=MainTb028Solicitud.objects.unPaso(co_tipo_solicitud,request.user)
                nuevaBillereta=WalletTb037Keystore(
                    co_usuario=request.user,
                    address=request.POST['tx_public_key']
                )
                nuevaBillereta.save()
                usuarioFederacion=createUpdateFederation(request,request.POST['tx_public_key'])

                
                
                
                return redirect('billeterasu',success=1,codigo=2)

        except IntegrityError:
            return redirect('billeterasu',success=0,codigo=1)
    else:
        return redirect('billeterasu',success=0,codigo=1)

@confirmado_user_required
def ImportarBilletera(request,co_tipo_solicitud):
    
    content=getContentBasicoL2(request.user,co_tipo_solicitud,None)

    form=importarForm(request.POST,tipo=request.POST['tipoimportacion'])
    if  request.method=='POST':
        if form.is_valid():
            
            if request.POST['tipoimportacion']=="1":
                secreto=Keypair.from_secret(request.POST['tx_private_key'])
            else:
                secreto=Keypair.from_mnemonic_phrase(request.POST['tx_frase'])

            usuarioFederacion=createUpdateFederation(request,secreto.public_key)

            nuevaBillereta=WalletTb037Keystore(
                    co_usuario=request.user,
                    address=secreto.public_key,
                )
            

            if request.POST['tipoimportacion']=="1":
                nuevaBilletera.in_frase=False

            nuevaBillereta.save()


            solicitud=MainTb028Solicitud.objects.unPaso(co_tipo_solicitud,request.user)
                

            return redirect('billeterasu',success=1,codigo=2)
        else:
            return redirect('billeterasu',success=0,codigo=1)
    else:
        return redirect('billeterasu',success=0,codigo=1)


def editarFederacion(request,username,co_tipo_solicitud):
    url="https://stellarid.io/api/addresses/"+str(request.user.co_federation)+"/"
    patchdata={
        "username":username
    }
    patch=requests.patch(url,data=patchdata,headers=getHeaderFederacion(request))

    solicitud=MainTb028Solicitud.objects.unPaso(co_tipo_solicitud,request.user)
                

@confirmado_user_required
def UserFederation(request,co_tipo_solicitud):
    form=importarForm(request.POST,in_envio=True)
    if  request.method=='POST':
        if form.is_valid():
            try:
                with transaction.atomic(): 
                    patch=usernameFederacion(request,request.user.co_federation,request.POST['tx_username'])
                    solicitud=MainTb028Solicitud.objects.unPaso(co_tipo_solicitud,request.user)
                
                    
                    return redirect('billeterasu',success=1,codigo=2)
            except IntegrityError:
                return redirect('billeterasu',success=0,codigo=1)
        else:
            return redirect('billeterasu',success=0,codigo=1)
    else:
        return redirect('billeterasu',success=0,codigo=1)

@confirmado_user_required
def UserFederation2(request,co_tipo_solicitud):
    form=importarForm(request.POST,in_envio2=True)
    if  request.method=='POST':
        if form.is_valid():
            try:
                with transaction.atomic(): 
                    patch=usernameFederacion(request,request.user.co_federation2,request.POST['tx_username2'])
                    solicitud=MainTb028Solicitud.objects.unPaso(co_tipo_solicitud,request.user)
                
                    
                    return redirect('billeterasu',success=1,codigo=2)
            except IntegrityError:
                return redirect('billeterasu',success=0,codigo=1)
        else:
            return redirect('billeterasu',success=0,codigo=1)
    else:
        return redirect('billeterasu',success=0,codigo=1)


@confirmado_user_required
def EstatusUserFederation(request,co_tipo_solicitud):
    if  request.method=='POST':
        try:
            with transaction.atomic(): 
                usuario=request.user
                usuario.in_aceptar=not request.user.in_aceptar
                usuario.save()

                solicitud=MainTb028Solicitud.objects.unPaso(co_tipo_solicitud,request.user)
                
                return redirect('billeterasu',success=1,codigo=2)
        except IntegrityError:
            return redirect('billeterasu',success=0,codigo=1)
    else:
        return redirect('billeterasu',success=0,codigo=1)

