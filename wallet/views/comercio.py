from django.shortcuts import render
from main.models import WalletTb038MensajeStellar,WalletTb014Comision
from apipago.serializers import MensajeStellarSerializer
from main.views.utils import (
    getContentSuperBasico,user2fa_required,confirmado_user_required
    )
from apipago.views.stellar_utils import StellarAccount
import json

@confirmado_user_required
@user2fa_required
def comercioVario(request,code1:str=None,issuer1:str=None,code2:str=None,issuer2:str=None)->render:
    content=getContentSuperBasico(request.user)
    content['publico']=request.session["public_key"]

    

    cuenta=StellarAccount(content['publico'])
    cuenta.getActivosMioRecomendados()

    querymsjstellar=WalletTb038MensajeStellar.objects\
        .select_related("co_tipo_error_stellar")\
        .filter(co_tipo_error_stellar__in=[1,2,7,8])

    msjstellar=MensajeStellarSerializer(querymsjstellar,many=True)
    content['msjstellarjson']=json.dumps(msjstellar.data)  
    #actrec=getActivoMioReco2(content['publico'],True)
    content['activosrecoment']=cuenta.balance
    #content['publico']="GBY6ZDFNONM26OMQCT4VWWMFSI64WSMGOGJ7OMN7D2CAIGHZMDPT6ARX"
    #misAsset=getAssetAccountToml(content['publico'])
    misAsset=[]
    
    #if "data" in getOrdenBook
    #content[""]
    content['actinit']=""
    content['actinitbuy']=""
    content['actinitsell']=""

    for val in cuenta.balance:

        if val["propio"] or val["asset_type"] == "native":
            misAsset.append(val)

        if val["asset"]["code"]=="USDC" and val["asset"]["code"]=="GA5ZSEJYB37JRC5AVCIA5MOP4RHTM335X2KGX3IHOJAPP5RE34K4KZVN":
            content['actinitbuy']=val

    content['asset1']={
        "code":code1 if code1 is not None else "0",
        "issuer":issuer1 if issuer1 is not None else "0"
    }
    content['asset2']={
        "code":code2 if code2 is not None else "0",
        "issuer":issuer2 if issuer2 is not None else "0"
    }


    i=True
    j=True
    k=True
    
    if len(misAsset)>0:
        for va in misAsset:

            if va["asset"]["code"]=="USDC" and va["asset"]["issuer"]=="GA5ZSEJYB37JRC5AVCIA5MOP4RHTM335X2KGX3IHOJAPP5RE34K4KZVN":
                content['actinitbuy']=va
                i=False
            elif va["asset_type"] == "native":
                content['actinitsell']=va

        if code1 is not None and issuer1 is not None:
            #print(code1)
            for val in content['activosrecoment']:
                if j:
                    if code1=="XLM" and val["asset"]["code"]=="XLM":
                        content['actinitsell']=val
                        j=False

                    if val["asset"]["code"]==code1 and val["asset"]["issuer"]==issuer1:
                        content['actinitsell']=val
                        j=False
                    elif issuer1=="0":
                        if code2=="XLM":
                            content['actinitsell']=val
                            j=False
                        elif val["asset"]["code"]=="XLM":
                            content['actinitsell']=val
                            j=False

        if code2 is not None and issuer2 is not None:
            print("quui")
            for val in content['activosrecoment']:
                if k:
                    if code2=="XLM" and val["asset"]["code"]=="XLM":
                        content['actinitbuy']=val
                        k=False

                    elif val["asset"]["code"]==code2 and val["asset"]["issuer"]==issuer2:
                        content['actinitbuy']=val
                        k=False
                    elif issuer2=="0":
                        if code1=="XLM":
                            content['actinitbuy']=val
                            k=False
                        elif val["asset"]["code"]=="XLM":
                            content['actinitbuy']=val
                            k=False

        if content['actinitbuy']!="":
            content['asset2']['code']=content['actinitbuy']["asset"]["code"] if content['asset2']['code'] == "0" else content['asset2']['code']
            content['asset2']['issuer']=content['actinitbuy']["asset"]["issuer"] if content['asset2']['issuer']== "0" else content['asset2']['issuer']
            if content['asset2']['issuer']=="":
                content['asset2']['issuer']="0"
        else:
            activoref=cuenta.getAssetRaw("USDC","GA5ZSEJYB37JRC5AVCIA5MOP4RHTM335X2KGX3IHOJAPP5RE34K4KZVN")
            activoref=cuenta.getDetalleActivoPPG(asset=activoref,tipo_return=2)
            content['actinitbuy']=activoref[0]

            if content['actinitbuy']!="":
                content['asset2']['code']=content['actinitbuy']["asset"]["code"] if content['asset2']['code'] == "0" else content['asset2']['code']
                content['asset2']['issuer']=content['actinitbuy']["asset"]["issuer"] if content['asset2']['issuer']== "0" else content['asset2']['issuer']
                if content['asset2']['issuer']=="":
                    content['asset2']['issuer']="0"

    #asset_sell=Asset(content['asset1']['code'],issuer1) if code1 != "XLM" else Asset.native()
    #asset_buy=Asset(code2,issuer2) if code2 != "XLM" else Asset.native()
    asset_base=cuenta.getAssetRaw(
        code=content['asset1']['code'] if content['asset1']['code']!="0" and content['asset1']['code']!="" else "XLM",
        issuer=content['asset1']['issuer'] if content['asset1']['issuer']!="0" and content['asset1']['issuer']!="" else None,
        )
    asset_request=cuenta.getAssetRaw(
        code=content['asset2']['code'] if content['asset2']['code']!="0" and content['asset2']['code']!="" else "XLM",
        issuer=content['asset2']['issuer'] if content['asset2']['issuer']!="0" and content['asset2']['issuer']!="" else "XLM",
        )



                

    content['misactivos']=misAsset


    comision=WalletTb014Comision.objects.select_related('co_tipo_transaccion').filter(co_tipo_transaccion__in=[13,14],in_activo=True).only("nu_monto","co_tipo_transaccion__co_tipo_transaccion")
    for valco in comision:
        if valco.co_tipo_transaccion.co_tipo_transaccion==13:
            content['comision1']=valco.nu_monto
            content['co_comision1']=valco.co_comision

        else:
            content['comision2']=valco.nu_monto
            content['co_comision2']=valco.co_comision
            
    content['actinitbuyjson']=json.dumps(content['actinitbuy'])
    content['actinitselljson']=json.dumps(content['actinitsell'])
    content['activosrecomentjson']=json.dumps(content['activosrecoment'])
    content['jsonasset1']=json.dumps(content['asset1'])
    content['jsonasset2']=json.dumps(content['asset2'])
    #content['tradesjson']=json.dumps(content['trades'])

    #print(content['actinitbuy'])

    







    return render(request, '../templates/wallet/comercio/trade.html',content)