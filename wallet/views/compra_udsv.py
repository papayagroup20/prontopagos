from decimal import Decimal
from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required,user_passes_test
from main.models import *
from stellar_sdk.server import Server
from stellar_sdk import TransactionBuilder, Network, Keypair, Account,Asset,Operation,AllowTrust
from wallet.form import SolicitudCompraLumensForm,SolicitudCompraLumensReferenciaForm,SolicitudUSDVForm, SolicitudUsdvTdcForm
from apipago.views.operaciones import datos_email, getComisionCompraLumens, getOferta, getOfertaBSS, getOferta_XLM_BSS
from main.views.utils import getContentBasicoL2,getContentBasicoL,iplocalizacion
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action,api_view,renderer_classes

from django.db import IntegrityError, transaction
from rest_framework.response import Response
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse



from ipware import get_client_ip
from django_user_agents.utils import get_user_agent
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
from base64 import b64encode
from Crypto.Util.Padding import pad
import requests
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from rest_framework.decorators import action,api_view,renderer_classes,permission_classes,parser_classes

def facturacion(request):
    nu_factura = WalletTb051FacturacionTdc(
        in_activo = False
    )
    nu_factura.save()

    return nu_factura

def comprarUsdvTdc(request, co_tipo_solicitud=None):
    content = getContentBasicoL2(request.user,None, SolicitudUsdvTdcForm)

    # nu_factura = WalletTb049FacturacionTdc(
    #     in_activo = False
    # )
    # nu_factura.save()
    content["invoice_number"]=facturacion(request)
    content["ip"]=get_client_ip(request)[0]
    content["navegador"]=request.user_agent.browser.family
     
    

    # if request.method == 'POST':
    #     try:
    #         with transaction.atomic():

    # return render(request,'../templates/wallet/recargas/compra_usdv_tdc.html',content)

    
    if request.method=='POST':
        form=SolicitudUsdvTdcForm(request.POST)
        print("segun se creo la solicitud")
        # print(request.POST['monto'])
        
        
        # if form.is_valid():
        
        try:
            with transaction.atomic():
                test = WalletTb051FacturacionTdc.objects.get(co_facturacion=request.POST["facturacion"],in_activo = False)
                # print(test)

                # test.tx_persona = request.POST["nombre"]
                # test.save()
                # solicitud=MainTb028Solicitud.objects.newSolicitud(request.POST['tp_solicitud'],request.user)
                # solicitud.save()
                
            
                # transaccion=WalletTb015Transaccion(
                #     nu_monto=request.POST['monto'],
                #     in_activo=True,
                #     co_emisor=request.user,
                #     co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                #     # co_receptor=Tb001Usuario.objects.get(co_usuario=350),
                #     co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=6),
                #     co_moneda=WalletTb003Moneda.objects.get(co_moneda=4),
                #     co_solicitud=solicitud,
                #     # tx_hash_stellar=request.POST['hash'],
                #     nu_monto2=request.POST['monto2'],
                    
                # )
                # transaccion.save()
                # if transaccion:
                #     nu_factura.tx_persona = request.POST['persona']
                #     nu_factura.tx_referencia_pago = request.POST['referencia_pago']
                #     nu_factura.tx_estatus = request.POST['estatus']
                #     nu_factura.tx_concepto = request.POST['concepto']
                #     nu_factura.in_activo = True
                #     nu_factura.co_solicitud = solicitud
                #     nu_factura.co_transaccion = transaccion

                #     nu_factura.save()

                    
                #     solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                #     solicitud.in_finanza=True
                #     solicitud.save()

                #     ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                #     ruta.in_activo=False
                #     ruta.save()

                    
                # else:
                #     content['success']=False
                #     content['msg']="Hubo un problema con la operación"





                


        except IntegrityError:
                
                content['success']=False
                content['msg']="Hubo un problema en los datos"

        content['form']=form
                
            
            

    return render(request, '../templates/wallet/recargas/compra_usdv_tdc.html',content)




@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def cifradotdc(request):
    content={
        'success':True,
        'data':''
    }
    h = SHA256.new()
    h.update(b'A9279120481620090622AA30')
    print (h.hexdigest()[:16])

    key = h.digest()[:16]
    cipher = AES.new(key, AES.MODE_ECB)

    print(cipher)

    l = request.POST["cvv"]
    print(l)
    arr = bytes(l, 'utf-8')
    data = b'752'
    data2 = b'1234'
    
    ct_bytes = cipher.encrypt(pad(arr, AES.block_size))
    ct_bytes2 = cipher.encrypt(pad(data2, AES.block_size))
    # iv = b64encode(cipher.iv).decode('utf-8')
    ct = b64encode(ct_bytes).decode('utf-8')
    # ct2 = b64encode(ct_bytes2).decode('utf-8')
    # result = json.dumps({'iv':iv, 'ciphertext':ct})
    # result = json.dumps({'cvv':ct2})
    
    # print(result)
    content['data'] = ct
    return JsonResponse(content)