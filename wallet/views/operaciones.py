from main.models import *
from stellar_sdk.server import Server
from stellar_sdk import TransactionBuilder, Network, Keypair
from wallet.form import *
from django.shortcuts import render
from django.db import IntegrityError, transaction
from main.views.utils import user2fa_required,confirmado_user_required



def transacion(moneda,destino,billetera,monto,public):
    receptor = destino.tx_public_key if public is None else public
    
    root_keypair = Keypair.from_secret(billetera.tx_private_key)
    #Create an Account object from an address and sequence number.
    server = Server(horizon_url="https://horizon-testnet.stellar.org")
    source_account = server.load_account(root_keypair.public_key)
    base_fee = server.fetch_base_fee()

    if moneda.tx_simbolo=="XLM":
        
        transactions = (
            TransactionBuilder(
            source_account=source_account,
             # If you want to submit to pubnet, you need to change `network_passphrase` to `Network.PUBLIC_NETWORK_PASSPHRASE`
            network_passphrase=Network.TESTNET_NETWORK_PASSPHRASE,
            base_fee=base_fee,
            )
            .add_text_memo('transferencia de prueba')
            .append_payment_op(
                receptor,
                str(monto),
                "XLM"
            )
            .set_timeout(300) 
            .build()
        )
    else:
        transactions = (
            TransactionBuilder(
            source_account=source_account,
            # If you want to submit to pubnet, you need to change `network_passphrase` to `Network.PUBLIC_NETWORK_PASSPHRASE`
            network_passphrase=Network.TESTNET_NETWORK_PASSPHRASE,
            base_fee=base_fee,
            )
            .add_text_memo('transferencia de prueba')
            .append_payment_op(
                receptor,
                str(monto),
                moneda.tx_simbolo,
                #'GB4U7HVKRGTTHJUGNQ2CK2B5QMCA2RQBC6BFGVDCJDBRWUKE3JLHDMEV'
                'GDNYGQC6464OGM77WSS44H4DK5L4PG4C7E76BYUQHMVVJYQLUXLFCOGD'
                #'GBNH2N2WFJVNWQBSCY4XWVCJG37OO6SGZBF3EP2T74A5KGVRVJY6SPKB'
            )
            .set_timeout(300) 
            .build()
        )

    transactions.sign(root_keypair)
    print(transactions.to_xdr())
    print("sisi")
    response = server.submit_transaction(transactions)
    print("dola")
    print(response)

    return response

def darConfianza(billetera,activo,limite):
    root_keypair = Keypair.from_secret(billetera.tx_private_key)                   
    server = Server(horizon_url="https://horizon-testnet.stellar.org")
    source_account = server.load_account(root_keypair.public_key)
    base_fee = server.fetch_base_fee()

    transactions = (
        TransactionBuilder(
             source_account=source_account,
             # If you want to submit to pubnet, you need to change `network_passphrase` to `Network.PUBLIC_NETWORK_PASSPHRASE`
             network_passphrase=Network.TESTNET_NETWORK_PASSPHRASE,
             base_fee=base_fee,
        ).add_text_memo("confianza a"+activo.tx_asset_code)
        .append_change_trust_op(
             asset_code=activo.tx_asset_code, 
             asset_issuer=activo.tx_asset_issuer, 
             limit=limite)
        .set_timeout(300) 
        .build()
    )
    transactions.sign(root_keypair)
    print(transactions.to_xdr())
    response = server.submit_transaction(transactions)
    print(response)
    return response


def transccionConComision(co_moneda,nu_monto,destino,billetera,usuario,tipo_transaccion,estatus,co_solicitud):  
    try:
        with transaction.atomic():

            response={
                'hash':''
            }
            response2={
                'hash':''
            }
            destinatario= destino.co_usuario if destino is not None else None
            comision=WalletTb014Comision.objects.filter(co_moneda=co_moneda)[0]
            monto_comision=float(nu_monto)*float(comision.nu_monto)
            monto=float(nu_monto)-float(monto_comision)

            if estatus==2 and destino is not None:
                response=transacion(comision.co_moneda,destino,billetera,monto,None)


            transferencia=WalletTb015Transaccion(
                tx_hash=response['hash'],
                nu_monto=monto,
                co_comision=comision,
                co_emisor=usuario,
                co_estatus=WalletTb009Estatus.objects.get(co_estatus=estatus),
                co_moneda=comision.co_moneda,
                co_receptor=destinatario,
                co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=tipo_transaccion),
                nu_telegram=usuario.nu_telegram,
                co_solicitud=co_solicitud

            )
            transferencia.save()
            print(transferencia)
            #p="GAXBRDCCFNGND7UQMN2GT55GD2NRSQBRKS5MQDIMHHBXXNBLMMO4BSLK"
            p="GBSPKSKODEPEAI4L67NQNHLNWLHSBPNFJTELM2HMWJCVFBBL6QGD6RZG"
            if estatus==2 and destino is not None:
                response2=transacion(comision.co_moneda,destino,billetera,monto_comision,p)

            transferencia2=WalletTb015Transaccion(
                tx_hash=response2['hash'],
                nu_monto=monto_comision,
                co_comision=comision,
                co_emisor=usuario,
                co_estatus=WalletTb009Estatus.objects.get(co_estatus=estatus),
                co_moneda=comision.co_moneda,
                co_transaccion_emisor=transferencia,
                co_receptor=Tb001Usuario.objects.get(co_usuario=122),
                co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=tipo_transaccion),
                nu_telegram=usuario.nu_telegram,
                co_solicitud=co_solicitud

            )
            transferencia2.save()
            
            print(transferencia2)

            return transferencia

    except IntegrityError:
        return False
  
def transccionSinComision(co_moneda,nu_monto,destino,billetera,usuario,tipo_transaccion,estatus):  
    try:
        with transaction.atomic():

            response={
                'hash':''
            }
            destinatario= destino.co_usuario if destino is not None else None
            monto=float(nu_monto)

            if estatus==2 and destino is not None:
                response=transacion(co_moneda,destino,billetera,monto,None)


            transferencia=WalletTb015Transaccion(
                tx_hash=response['hash'],
                nu_monto=monto,
                co_comision=None,
                co_emisor=usuario,
                co_estatus=WalletTb009Estatus.objects.get(co_estatus=estatus),
                co_moneda=co_moneda,
                co_receptor=destinatario,
                co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=tipo_transaccion),
                nu_telegram=usuario.nu_telegram

            )
            transferencia.save()

            return transferencia

    except IntegrityError:
        return False

def transccionPrueba(co_moneda,nu_monto,destino,billetera,usuario,tipo_transaccion,estatus,co_solicitud):  
    try:
        with transaction.atomic():

            destinatario= destino.co_usuario if destino is not None else None
            comision=WalletTb014Comision.objects.filter(co_moneda=co_moneda)[0]
            monto_comision=float(nu_monto)*float(comision.nu_monto)
            monto=float(nu_monto)-float(monto_comision)


            transferencia=WalletTb015Transaccion(
                tx_hash='prueba',
                nu_monto=monto,
                co_comision=comision,
                co_emisor=usuario,
                co_estatus=WalletTb009Estatus.objects.get(co_estatus=estatus),
                co_moneda=comision.co_moneda,
                co_receptor=destinatario,
                co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=tipo_transaccion),
                nu_telegram=usuario.nu_telegram,
                co_solicitud=co_solicitud

            )
            transferencia.save()
            print(transferencia)
            #p="GAXBRDCCFNGND7UQMN2GT55GD2NRSQBRKS5MQDIMHHBXXNBLMMO4BSLK"
            p="GBSPKSKODEPEAI4L67NQNHLNWLHSBPNFJTELM2HMWJCVFBBL6QGD6RZG"
            
            transferencia2=WalletTb015Transaccion(
                tx_hash='pruebacomision',
                nu_monto=monto_comision,
                co_comision=comision,
                co_emisor=usuario,
                co_estatus=WalletTb009Estatus.objects.get(co_estatus=estatus),
                co_moneda=comision.co_moneda,
                co_receptor=Tb001Usuario.objects.get(co_usuario=122),
                co_transaccion_emisor=transferencia,
                co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=tipo_transaccion),
                nu_telegram=usuario.nu_telegram,
                co_solicitud=co_solicitud

            )
            transferencia2.save()
            
            print(transferencia2)

            return transferencia

    except IntegrityError:
        return False

  
def transccionPrueba2(co_moneda,nu_monto,destino,billetera,usuario,tipo_transaccion,estatus,co_solicitud):  
    try:
        with transaction.atomic():

            destinatario= destino.co_usuario if destino is not None else None
            monto=float(nu_monto)

            transferencia=WalletTb015Transaccion(
                tx_hash='prueba2',
                nu_monto=monto,
                co_comision=None,
                co_emisor=usuario,
                co_estatus=WalletTb009Estatus.objects.get(co_estatus=estatus),
                co_moneda=co_moneda,
                co_receptor=destinatario,
                co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=tipo_transaccion),
                nu_telegram=usuario.nu_telegram,
                co_solicitud=co_solicitud


            )
            transferencia.save()

            return transferencia

    except IntegrityError:
        return False


@confirmado_user_required
@user2fa_required

def trasferencia2(request):
    content={
        'success':True,
        'data':'',
        'form': trasferenciaForm()
    }
    algo=''
    if request.method=='POST':
          form=trasferenciaForm(request.POST)
          if form.is_valid():
              billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user)[0]
              root_keypair = Keypair.from_secret(billetera.tx_private_key)

              # Create an Account object from an address and sequence number.
              

              server = Server(horizon_url="https://horizon-testnet.stellar.org")
              source_account = server.load_account(account_id=root_keypair.public_key)
              base_fee = server.fetch_base_fee()

              


              
              path = [    ]
              
              transaction = (
                  
                TransactionBuilder(
                source_account=source_account,
                # If you want to submit to pubnet, you need to change `network_passphrase` to `Network.PUBLIC_NETWORK_PASSPHRASE`
                network_passphrase=Network.TESTNET_NETWORK_PASSPHRASE,
                base_fee=base_fee,
                )
                .add_text_memo('transferencia de tok')
                .append_path_payment_op(
                    destination=request.POST['tx_public_key'],
                    send_code="XLM",
                    send_issuer=None,
                    send_max="1000",
                    dest_code="PPG",
                    dest_issuer="GB4U7HVKRGTTHJUGNQ2CK2B5QMCA2RQBC6BFGVDCJDBRWUKE3JLHDMEV",
                    dest_amount=request.POST['nu_monto'],
                    path=path) 
                .set_timeout(300) 
                .build()
              )


              transaction.sign(root_keypair)
              print(transaction.to_xdr())
              response = server.submit_transaction(transaction)
              print(response)

          content['form']=form
            

    return render(request, '../templates/wallet/transferencias/transfInterno2.html',content)


#-------------------------------------------------------
# @confirmado_user_required
# @user2fa_required
# def retirofondos(request):
#     billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user)[0]
    
    
#     token, _ =Token.objects.get_or_create(user = request.user)
#     content={
#         'success':True,
#         'data':'',
#         'form':retirarFondosForm(),
#         'token':token,
#         'msg':''
#     }
#     content['msg']=""
#     if  request.method=='POST':
#         form=retirarFondosForm(request.POST)
        
        
        
#         if form.is_valid():
            
#             try:
#                 with transaction.atomic():
#                     comision=WalletTb014Comision.objects.get(co_moneda=request.POST ['co_moneda'], co_tipo_transaccion=4)
                   
#                     transaccion=transccionConComision(
#                         co_moneda=WalletTb003Moneda.objects.get(co_moneda=request.POST ['co_moneda']),
#                         nu_monto=request.POST['nu_monto'],
#                          destino=WalletTb037Keystore.objects.get(co_usuario__username='LDOS97'),                      
#                         billetera=billetera,
#                         usuario=request.user,
#                         tipo_transaccion=4,
#                         estatus=2)
#                     transaccion.save()

#                     retiro=WalletTb018Retiro(
                        
#                         #nu_monto=monto,
#                         #nu_monto=request.POST['nu_monto'],
#                         co_cuenta=MaintTb017Cuenta.objects.get(co_cuenta=request.POST['co_cuenta']),
#                         co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
#                         co_usuario=request.user,
#                         co_transaccion=transaccion,
#                         #co_moneda=WalletTb003Moneda.objects.get(co_moneda=request.POST ['co_moneda']),
#                         #co_comision=comision
                    
#                     )
#                     retiro.save()
                    
#                     retiro_historico=WalletTb027RetiroHistorico(
#                         co_retiro=retiro,
#                         co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
#                     )
#                     retiro_historico.save()
#                     #messages.success(request,"proceso complettado")
#                     content['msg']="La operacion ha sido realizada sactifactoriamente"
                         
#             except IntegrityError:
#                 content['success']=False
#                 content['msg']="La operacion ha sido realizada sactifactoriamente"
                   

#         content['form']=form
#     return render(request, '../templates/wallet/Retiros/retirarfondos.html',content)