from django.shortcuts import render
from main.models import (
    WalletTb038MensajeStellar,WalletTb036Activos,MainTb028Solicitud,
    WalletTb015Transaccion,WalletTb009Estatus,WalletTb013TipoTransaccion,
    WalletTb010Recarga,WalletTb008Cuenta,WalletTb011HistoricoRecarga,
    MainTb029Ruta
    )
from wallet.form import (depositoForm,recargaBSSForm)
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view,renderer_classes

from django.db import IntegrityError, transaction
from rest_framework.response import Response
from apipago.serializers import MensajeStellarSerializer,RecargaSerializer
from main.views.utils import (
    getContentBasicoL2,user2fa_required,confirmado_user_required,user_staff_required)
from apipago.views.stellar import getAssetAccountToml,getAccountBalance
import json
@confirmado_user_required
@user2fa_required
def depositoVario(request):
    misAsset=getAccountBalance(request.session["public_key"])

    choice=[("0","Seleccione...")]
    for a in misAsset["data"]:
        choice.append((a["tx_asset_issuer"],a["tx_asset_code"]))

    content=getContentBasicoL2(request.user,None,depositoForm(lista=choice))

    querymsjstellar=WalletTb038MensajeStellar.objects\
        .select_related("co_tipo_error_stellar")\
        .filter(co_tipo_error_stellar__in=[1,2,3,4])

    msjstellar=MensajeStellarSerializer(querymsjstellar,many=True)
    #comision=WalletTb014Comision.objects.filter(co_tipo_transaccion=9,in_activo=True).only("nu_monto").first()
    #content['comision']=comision.nu_monto
    content['msjstellarjson']=json.dumps(msjstellar.data)
    content['publico']=request.session["public_key"]
    #content['publico']="GACXJ6ZKU3IX7RL5LZCVVAOFRLLMRLN4SC7W5IJGTTDJFYSRUKINUD6K"
    
    return render(request, '../templates/wallet/recargas/deposito.html',content)

@confirmado_user_required
@user2fa_required
def recargaBSS(request,co_tipo_solicitud):
    misAsset=getAssetAccountToml(request.user.getPublic())
    choice=[("0","Seleccione...")]
    activosppg=WalletTb036Activos.objects.filter(tx_asset_issuer="GDKCWJB7N3HW2MD7O5QLBE2BAVVCQCP5VYH75S4YI3HLOUY2JRYYDQ4S")
    #Descomentar cuando se pase a real
    """for mio in misAsset:
        for ppg in activosppg:
            if mio["tx_asset_issuer"]==ppg.tx_asset_issuer and mio["tx_asset_code"]==ppg.tx_asset_code:
                choice.append((ppg.co_activo,ppg.tx_asset_code))"""
    for ppg in activosppg:
        choice.append((ppg.co_activo,ppg.tx_asset_code))





    content=getContentBasicoL2(request.user,co_tipo_solicitud,recargaBSSForm(in_envio1=False,lista=choice))
    
    
    #content['msg']="Debe depositar desde una cuenta de su propiedad no nos haremos responsables de un deposito de terceros"
    if  request.method=='POST':
        form=recargaBSSForm(request.POST,in_envio1=True,lista=choice)
        if form.is_valid():
            form=recargaBSSForm(in_envio1=False,lista=choice)

            try:
                with transaction.atomic():
                    
                    solicitud=MainTb028Solicitud.objects.newSolicitud(co_tipo_solicitud,request.user)
                    solicitud.save()

                    transaccion=WalletTb015Transaccion(
                    nu_monto=request.POST ['nu_monto'],
                    co_emisor=request.user,
                    co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                    co_activo=WalletTb036Activos.objects.get(co_activo=request.POST["co_moneda"]),
                    co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=6),               
                    co_solicitud=solicitud
                    )
                    transaccion.save()

                    recarga=WalletTb010Recarga(
                        co_usuario=request.user,
                        #co_banco=WalletTb007Banco.objects.get(co_banco=request.POST['co_banco']),
                        co_cuenta=WalletTb008Cuenta.objects.get(co_cuenta=request.POST['co_cuenta']),
                        tx_codigo=request.POST['tx_codigo'],
                        co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                        co_transaccion=transaccion,
                        co_solicitud=solicitud
                        #co_moneda=WalletTb003Moneda.objects.get(co_moneda=request.POST ['co_moneda']),
                    )
                     
                    recarga.save()

                    recarga_historico=WalletTb011HistoricoRecarga(
                        co_recarga=recarga,
                        co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                        co_usuario=request.user
                    )
                    recarga_historico.save()


                    solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                    solicitud.in_finanza=True
                    solicitud.save()

                    ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                    ruta.in_activo=False
                    ruta.save()
                    content["msg"]="La operación ha sido realizada sactifactoriamente"
                    content["form"]=form
                    
                    
                    #return redirect('bandejaInicioSuccess',success=1,codigo=6)
                    return render(request, '../templates/wallet/recargas/recargaBSS.html',content)
                    
                         
            except IntegrityError:
                content['success']=False
                content['msg']="Ha habido un error en la operación"
                   

        content['form']=form
    return render(request, '../templates/wallet/recargas/recargaBSS.html',content)


@confirmado_user_required
@user2fa_required
@user_staff_required
def bandeja(request):
    content={
        'success':True,
        'data':'',
        'msg':'',
    }
    bandeja=WalletTb010Recarga.objects.filter(co_estatus=1)
    content['data']=bandeja
    return render(request, '../templates/wallet/recargas/bandeja.html',content)


@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
def validarBSS(request):
    content={
        'success':False,
        'data':'',
        'msg':'',
    }
    if(request.method=='POST'):
        if 'co_recarga' in request.POST and request.POST['co_recarga']!='':
            if request.user.is_staff and request.user.in_confirmado:
                try:
                    with transaction.atomic():

                        validarbandeja=WalletTb010Recarga.objects.get(co_recarga=request.POST['co_recarga'])
                        validarbandeja.co_estatus=WalletTb009Estatus.objects.get(co_estatus=request.POST['co_estatus'])
                        validarbandeja.save()

                        if WalletTb011HistoricoRecarga.objects.filter(co_recarga=validarbandeja).count()>0:
                            oldHistorico=WalletTb011HistoricoRecarga.objects.filter(co_recarga=validarbandeja)
                            oldHistorico.in_activo=False
                            oldHistorico.save()

                        historico=WalletTb011HistoricoRecarga(
                            co_recarga=validarbandeja,
                            co_usuario=request.user,
                            co_estatus=WalletTb009Estatus.objects.get(co_estatus=request.POST['co_estatus'])
                        )
                        historico.save()

                        # Create an Account object from an address and sequence number.
                        
                        
                        

                        


                        content['success']=True 
                        if request.POST['co_estatus'] == '2':
                            content['msg']='Ha sido aprobado Sactifactoriamente'
                            """root_keypair = Keypair.from_secret('SAJ67NRTYFCLFWILJVBA6MATE7SBVODYI7RDJRKYHTYWOB5E57BQ4QVX')
                        
                            billetera=WalletTb037Keystore.objects.filter(co_usuario=validarbandeja.co_usuario)[0]

                            server = Server(horizon_url="https://horizon-testnet.stellar.org")
                            source_account = server.load_account(root_keypair.public_key)
                            base_fee = server.fetch_base_fee()

                            transactions = (
                                TransactionBuilder(
                                source_account=source_account,
                                # If you want to submit to pubnet, you need to change `network_passphrase` to `Network.PUBLIC_NETWORK_PASSPHRASE`
                                network_passphrase=Network.TESTNET_NETWORK_PASSPHRASE,
                                base_fee=base_fee,
                                ).add_text_memo('transferencia bss')
                                .append_payment_op(
                                    billetera.tx_public_key,
                                    validarbandeja.nu_monto,
                                    "BSS"
                                    ,'GB4U7HVKRGTTHJUGNQ2CK2B5QMCA2RQBC6BFGVDCJDBRWUKE3JLHDMEV')
                                .set_timeout(300) 
                                .build()
                            )
                            transactions.sign(root_keypair)
                            print(transactions.to_xdr())
                            response = server.submit_transaction(transactions)
                            print(response)"""

                        else:
                            content['msg']='Ha sido rechazado Sactifactoriamente'


                except IntegrityError:
                    content['msg']='Hubo un error en la operación'

                queryset =WalletTb010Recarga.objects.filter(co_estatus=1)
                serializer = RecargaSerializer(queryset , many=True)
                    
                content['data']=serializer.data
    return Response(content)