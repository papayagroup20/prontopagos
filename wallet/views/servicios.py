from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required,user_passes_test
from main.models import *

from stellar_sdk.server import Server
from stellar_sdk import TransactionBuilder, Network, Keypair, Account,Asset,Operation,AllowTrust
from wallet.form import *
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action,api_view,renderer_classes,permission_classes

from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated

from django.db.models import Q
from main.form import TipoSolicitudForm
from rest_framework.authtoken.models import Token
from django.db import IntegrityError, transaction
from rest_framework.response import Response
from django.views import View
from apipago.serializers import *
from wallet.views import transacion,transccionConComision,transccionSinComision,transccionPrueba,transccionPrueba2
from apipago.views.stellar import getAssetAccountToml,getAccountBalance,getActivoMio,getOrdenBook
from decimal import getcontext, Decimal, ROUND_HALF_UP

from main.views.agentes import user_agente_required
import json

from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required


from main.models import WalletTb006DetallePagoServicios, Tb001Usuario, MainTb012Servicios, WalletTb005PagoServicios, MainTb028Solicitud, WalletTb015Transaccion,WalletTb009Estatus,WalletTb013TipoTransaccion,WalletTb036Activos
@confirmado_user_required
def banSer(request):
    content=getContentBasicoL(request.user,None,bandejaServiciosForm(in_envio=False))

    rolsolicitud=MainTb037RolSolicitud.objects.filter(co_rol=request.user.co_rol.co_rol,in_activo=True).values_list('co_tipo_solicitud')
    if request.method=='POST':
        if 'boton' in request.POST:
            form=bandejaServiciosForm(in_envio=True)
            #content['data']=MainTb028Solicitud.objects.filter(Q(co_tipo_solicitud=request.POST['co_tipo_solicitud']),Q(co_estatus=request.POST['co_estatus']),Q(co_usuario=request.user) | Q(co_usuario2=request.user) )
            #if request.POST['co_servicios'] is not None:
            ban=WalletTb005PagoServicios.objects.filter(
                co_solicitud__co_usuario=request.user,
                co_solicitud__co_tipo_solicitud=15)
            if request.POST['co_estatus']!='':
                ban=ban.filter(co_estatus=request.POST['co_estatus'])

            if request.POST['co_servicios']!='':
                ban=ban.filter(co_servicios=request.POST['co_servicios'])
            elif request.POST['co_proveedor_servicios']!='':
                ban=ban.filter(co_servicios__co_proveedor_servicios=request.POST['co_proveedor_servicios'])
                
            content['data']=ban
    elif request.method=='GET':

            content['data']=WalletTb005PagoServicios.objects.filter(co_solicitud__co_usuario=request.user,co_solicitud__co_tipo_solicitud=15,co_solicitud__in_activo=True)
            #content['data']=MainTb028Solicitud.objects.filter(co_tipo_solicitud__co_rol_solicitud__co_rol__co_usuario=request.user.co_usuario)
    

    
    
    return content


@confirmado_user_required
@user2fa_required
def bandejaServicios(request):
    content=banSer(request)
    return render(request, '../templates/wallet/servicios/bandejaservicio.html',content)

@confirmado_user_required
@user2fa_required
def bandejaServiciosSuccess(request,success,codigo):

    content=banSer(request)
    mensaje=MainTb040Mensajes.objects.get(co_mensajes=codigo)
    content['success']=success
    content['msg']=mensaje.tx_mensajes

    return render(request, '../templates/wallet/servicios/bandejaservicio.html',content)

@confirmado_user_required
@user2fa_required
def pagoServicios(request,co_tipo_solicitud=None):
    init={}
    billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user)[0]
    server = Server("https://horizon.stellar.org")
    account = server.accounts().account_id(billetera.address).call()
    
   
    #billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user)[0]
    content=getContentBasicoL2(request.user,co_tipo_solicitud,pagoServiciosForm(initial=init,in_envio=False))
    content['proveedor'] = MainTb011ProveedorServicios.objects.filter(in_activo=True)


    publico=request.session["public_key"]
    misAsset=getActivoMio(publico)

    content['activos']=misAsset["data"]
    # print(content['activos'])
    content['publico']=publico
    content['misactivosjson']=json.dumps(misAsset["data"])





    if request.method=='POST':
        
        form=pagoServiciosForm(request.POST,initial=init,in_envio=True)
        # if form.is_valid():
        form=pagoServiciosForm(initial=init,in_envio=False)
        try:
            with transaction.atomic():
                solicitud=MainTb028Solicitud.objects.newSolicitud(request.POST['tp_solicitud'],request.user)
                solicitud.save()

               
                transferencia=WalletTb015Transaccion(
                    nu_monto=request.POST['monto'],
                    in_activo=True,
                    co_emisor=request.user,
                    co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                    # co_receptor=Tb001Usuario.objects.get(co_usuario=350),
                    co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=5),
                    co_activo=WalletTb036Activos.objects.get(tx_asset_code=request.POST['activo_code'], tx_asset_issuer=request.POST['activo_issuer'], in_activo=True),
                    co_solicitud=solicitud,
                    # tx_hash_stellar=request.POST['hash'],
                    nu_monto2=request.POST['monto2'],
                    
                )
                transferencia.save()
                print(transferencia, solicitud)
                if transferencia:
                    pagoservicios=WalletTb005PagoServicios(
                        co_servicios=MainTb012Servicios.objects.get(co_servicios=request.POST['co_servicios']),
                        co_usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario),   
                        co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                        co_solicitud=solicitud
                    )
                    pagoservicios.save()
                    print("pagoservicios aca",pagoservicios)

                    pagohistorico=WalletTb020HistoricoPagoServicios(
                        co_transaccion=transferencia,
                        co_pago_servicios=pagoservicios,
                        co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                        
                    )
                    pagohistorico.save()
                    # for i in range(int(request.POST['count'])):
                    requisitos=WalletTb006DetallePagoServicios(
                        # tx_respuesta_requisito=request.POST["req"+str(i)],
                        tx_respuesta_requisito=request.POST["tx_respuesta_requisito"],
                        co_pago_servicios=pagoservicios,
                        co_requisitos_servicios=MainTb013RequisitosServicios.objects.get(co_requisitos_servicios=request.POST["co_requisitos_servicios"])
                        # co_requisitos_servicios=MainTb013RequisitosServicios.objects.get(co_requisitos_servicios=request.POST["idreq"+str(i)])
                    )
                    requisitos.save()

                    solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                    solicitud.in_finanza=True
                    solicitud.save()

                    ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                    ruta.in_activo=False
                    ruta.save()

                    # return redirect('bandejaInicioSuccess',success=1,codigo=6)
                else:
                    content['success']=False
                    content['msg']="Hubo un problema con la operación"





                


        except IntegrityError:
                
                content['success']=False
                content['msg']="Hubo un problema en los datos"

        content['form']=form
                
            
            

    return render(request, '../templates/wallet/servicios/pagoservicios.html',content)

@confirmado_user_required
@user2fa_required
def CalcelarPagoServicios(request,co_solicitud):
    try:
        with transaction.atomic():
            solicitud=MainTb028Solicitud.objects.get(co_solicitud=co_solicitud)
            solicitud.cancelar()

            billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user)[0]
            pagoservicios=WalletTb005PagoServicios.objects.filter(co_solicitud=co_solicitud)[0]
            pagoservicios.co_estatus=WalletTb009Estatus.objects.get(co_estatus=3)
            pagoservicios.in_activo=False
            pagoservicios.save()

            histpagoserviciosold=WalletTb020HistoricoPagoServicios.objects.filter(
                co_pago_servicios=pagoservicios.co_pago_servicios
                ).update(in_activo=False)

            hist=pagoservicios.get_hist_transaccion()

            transaccion=transccionPrueba2(
                co_moneda=WalletTb003Moneda.objects.get(co_moneda=3),
                nu_monto=hist.co_transaccion.nu_monto,
                destino=WalletTb037Keystore.objects.get(co_usuario=request.user.co_usuario),
                billetera=billetera,
                usuario=Tb001Usuario.objects.get(username='RECARGAPPG'),
                tipo_transaccion=7,
                estatus=2,
                co_solicitud=solicitud
            )
            if transaccion:
                histpagoservicios=WalletTb020HistoricoPagoServicios(
                    co_transaccion=transaccion,
                    co_pago_servicios=pagoservicios,
                    co_estatus=WalletTb009Estatus.objects.get(co_estatus=3),
                    in_activo=False
                    )
                histpagoservicios.save()
            
            return redirect('bandejaInicioSuccess',success=1,codigo=7)

    except IntegrityError:

        return redirect('bandejaInicioSuccess',success=0,codigo=1)


@confirmado_user_required
@user2fa_required
def bandejaServiciosGlobal(request):
    
    token, _ = Token.objects.get_or_create(user = request.user)
    content={
        'success':True,
        'data':'',
        'token':token,
        'algo':''

    }
    agente=MainTb036AgenteMetodoPago.objects.filter(co_agente_servicios__co_usuario__co_usuario=request.user.co_usuario)
    metodo=[]
    for value in agente:
        metodo.append(value.co_metodo_pago.co_metodo_pago)
    mepago=WalletTb026DetalleMetodoPago.objects.filter(co_metodo_pago__in=metodo)
    metodo2=[]
    for val in mepago:
        metodo2.append(val.co_tipo_servicios)

    

    
    historico=WalletTb005PagoServicios.objects.filter(co_estatus=1,in_activo=True,co_servicios__co_tipo_servicios__in=metodo2)
    
    content['data']=historico


    return render(request, '../templates/wallet/servicios/bandejaglobal.html',content)


@confirmado_user_required
@user2fa_required
def bandejaServiciosPendientes(request):
    
    token, _ = Token.objects.get_or_create(user = request.user)
    content={
        'success':True,
        'data':'',
        'token':token.key
    }

    historico=WalletTb005PagoServicios.objects.filter(
        in_activo=True,
        co_usuario=request.user.co_usuario)
    
    content['data']=historico




    return render(request, '../templates/wallet/servicios/bandejapendiente.html',content)

@confirmado_user_required
@user2fa_required
def procesoServicios(request):
    content={
        'success':True,
        'data':'',
        'msg':''
    }
    
    return render(request, '../templates/wallet/servicios/procesando.html',content)

@confirmado_user_required
@user2fa_required
def peticionAgente(request,co_tipo_solicitud):  
    
    content=getContentBasicoL2(request.user,co_tipo_solicitud,agentesMetodosPagosForm(in_envio=False))
    
    """if  request.user.co_estatus_img.co_estatus != 2 and request.user.co_estatus_persona.co_estatus != 2:
        return redirect('bandejaInicioSuccess',success=0,codigo=5)"""

    
    if  request.method=='POST':
        form=agentesMetodosPagosForm(request.POST,in_envio=True)
        if form.is_valid():
            try:
                with transaction.atomic():

                    if 'servicios' not in request.POST:
                        content['success']=False
                        content['msg']="No ha seleccionado ningun servicio"
                        return render(request, '../templates/wallet/servicios/agentepeticion.html',content)
                        


                    if MainTb014AgenteServicios.objects.filter(co_usuario=request.user.co_usuario,in_activo=True).count()<=0:

                        agente=MainTb014AgenteServicios(
                            co_usuario=request.user,
                            in_activo=False
                        )
                        agente.save()



                        for val in request.POST.getlist('co_metodo_pago', []):
                            if val!='':
                                agente_metodo_pago= MainTb036AgenteMetodoPago(
                                co_agente_servicios= agente,
                                co_metodo_pago= WalletTb025MetodoPago.objects.get(co_metodo_pago=val)
                                )
                                agente_metodo_pago.save()
                        
                        for value in request.POST.getlist('servicios', []):
                            if value!='':
                                agente_detalle_servicios= MainTb041AgenteDetalleServicios(
                                co_agente_servicios= agente,
                                co_servicios= MainTb012Servicios.objects.get(co_servicios=value)
                                )
                                agente_detalle_servicios.save()

                        solicitud=MainTb028Solicitud.objects.newSolicitud(co_tipo_solicitud,request.user)
                        solicitud.save()
                        ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                        ruta.in_activo=False
                        ruta.save()

                        #content['msg']="La operacion ha sido realizada sactifactoriamente"
                        return redirect('bandejaInicioSuccess',success=1,codigo=2)




                    else:
                        agente=MainTb014AgenteServicios.objects.filter(co_usuario=request.user)
                        if agente.in_activo:
                            content['success']=False
                            content['msg']="Error, Ya eres agente"



            except IntegrityError:
                    content['success']=False
                    content['msg']="Ha habido un error"
        else:
            content['success']=False
            content['msg']="Ha habido un error en los datos"
            
        content['form']=form         
    return render(request, '../templates/wallet/servicios/agentepeticion.html',content)


@confirmado_user_required
@user_agente_required
@user2fa_required
def tomarServicio(request,co_solicitud):  
    content=getContentBasicoL(request.user,co_solicitud,None)

    solicitud=MainTb028Solicitud.objects.get(co_solicitud=co_solicitud)
    pagoServicios=WalletTb005PagoServicios.objects.filter(co_solicitud=co_solicitud)[0]
    ruta=MainTb029Ruta.objects.filter(co_solicitud=co_solicitud,in_activo=True)[0]
    try:
        with transaction.atomic():
            solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=5)
            solicitud.co_usuario2=request.user
            solicitud.save()

            ruta.in_activo=False
            ruta.save()
            bandeja=WalletTb012BandejaServicios(
                co_estatus=WalletTb009Estatus.objects.get(co_estatus=5),
                co_usuario=request.user,
                co_pago_servicios=pagoServicios
            )
            bandeja.save()
            return redirect('agentebandejasuccess',success=1,codigo=8)

    except IntegrityError:
        return redirect('agentebandejasuccess',success=0,codigo=1)

@confirmado_user_required
@user_agente_required
@user2fa_required
def CancelarTransServicio(request,co_solicitud):  
    content=getContentBasicoL(request.user,co_solicitud,None)

    solicitud=MainTb028Solicitud.objects.get(co_solicitud=co_solicitud,co_usuario2=request.user.co_usuario)
    pagoServicios=WalletTb005PagoServicios.objects.filter(co_solicitud=co_solicitud)[0]
    ruta=MainTb029Ruta.objects.filter(co_solicitud=co_solicitud,in_activo=True)[0]
    try:
        with transaction.atomic():
            solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
            solicitud.co_usuario2=None
            solicitud.save()

            ruta.delete()
            rutaold=MainTb029Ruta.objects.filter(co_solicitud=co_solicitud,in_activo=False,co_detalle_ruta=21)[0]
            rutaold.in_activo=True
            rutaold.save()

            bandeja=WalletTb012BandejaServicios.objects.filter(co_pago_servicios=pagoServicios.co_pago_servicios)[0]
            bandeja.in_activo=False
            bandeja.co_estatus=WalletTb009Estatus.objects.get(co_estatus=6)
            bandeja.save()
            
            return redirect('agentebandejasuccess',success=1,codigo=7)

    except IntegrityError:
        return redirect('agentebandejasuccess',success=0,codigo=1)

@confirmado_user_required
@user_agente_required
@user2fa_required
def AprobarTransServicio(request,co_solicitud):  
    content=getContentBasicoL(request.user,co_solicitud,None)

    solicitud=MainTb028Solicitud.objects.get(co_solicitud=co_solicitud,co_usuario2=request.user.co_usuario)
    pagoServicios=WalletTb005PagoServicios.objects.filter(co_solicitud=co_solicitud)[0]
    ruta=MainTb029Ruta.objects.filter(co_solicitud=co_solicitud,in_activo=True)[0]
    try:
        with transaction.atomic():

            solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
            solicitud.save()

            ruta.in_activo=False
            ruta.save()

            bandeja=WalletTb012BandejaServicios.objects.filter(co_pago_servicios=pagoServicios.co_pago_servicios)[0]
            bandeja.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
            bandeja.save()
            
            return redirect('agentebandejasuccess',success=1,codigo=2)

    except IntegrityError:
        return redirect('agentebandejasuccess',success=0,codigo=1)

@confirmado_user_required
@user2fa_required
def CulminarTransServicio(request,co_solicitud):  
    content=getContentBasicoL(request.user,co_solicitud,None)

    solicitud=MainTb028Solicitud.objects.get(co_solicitud=co_solicitud,co_usuario=request.user.co_usuario)
    pagoServicios=WalletTb005PagoServicios.objects.filter(co_solicitud=co_solicitud)[0]
    ruta=MainTb029Ruta.objects.filter(co_solicitud=co_solicitud,in_activo=True)[0]
    #print(request.POST['boton'])
    try:
        with transaction.atomic():

            """solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
            solicitud.save()

            ruta.in_activo=False
            ruta.save()

            bandeja=WalletTb012BandejaServicios.objects.filter(co_pago_servicios=pagoServicios.co_pago_servicios)[0]
            bandeja.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
            bandeja.save()"""
            
            return redirect('bandejaInicioSuccess',success=1,codigo=2)

    except IntegrityError:
        return redirect('bandejaInicioSuccess',success=0,codigo=1)