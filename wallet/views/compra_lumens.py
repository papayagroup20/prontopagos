from decimal import Decimal
from django.shortcuts import render
from django.contrib.auth.decorators import login_required,user_passes_test
#from main.models import *
from main.models import (
    OtpTotpTotpdevice,WalletTb015Transaccion,WalletTb040Limite,WalletTb041LimiteUsuario,
    MainTb028Solicitud,WalletTb009Estatus,WalletTb003Moneda,WalletTb013TipoTransaccion,
    WalletTb010Recarga,WalletTb007Banco,MainTb029Ruta,WalletTb014Comision,WalletTb047PaypalOperacion)
from wallet.form import SolicitudCompraLumensForm,SolicitudCompraLumensReferenciaForm,SolicitudUSDVForm
from apipago.views.operaciones import datos_email, getOferta, getOfertaBSS, getOferta_XLM_BSS

from django.db import IntegrityError, transaction

#from apipago.serializers import *
from main.views.utils import getContentBasicoL2
from datetime import datetime
from django.utils import timezone

user_login_required = user_passes_test(lambda user: user.in_confirmado, login_url='/usuario/verificar')
user_staf = user_passes_test(lambda user: user.is_staff, login_url='/inicio')

user_login2fa_required=user_passes_test(
    lambda user: True if OtpTotpTotpdevice.objects.filter(user_id=user.co_usuario).count()==0 else user.is_verified(),
    login_url='/login2FA')

def user2fa_required(view_func):
    decorated_view_func = login_required(user_login2fa_required(view_func),login_url='/login')
    return decorated_view_func

def confirmado_user_required(view_func):
    decorated_view_func = login_required(user_login_required(view_func),login_url='/login')
    return decorated_view_func

def user_staff_required(view_func):
      decorated_view_func = login_required(user_staf(view_func),login_url='/login')
      return decorated_view_func


def compraLumens(request,co_tipo_solicitud):
    print("wasabu")
    
    content=getContentBasicoL2(request.user,co_tipo_solicitud,SolicitudCompraLumensForm())
    content['ofertBSS']=getOfertaBSS()
    content['ofert']=getOferta()
    content['ofert_XLM_BSS']=getOferta_XLM_BSS()
    content['data1']=WalletTb015Transaccion.objects.filter(co_emisor=request.user, co_tipo_transaccion=6)
    content['mostrar']=True
    #content['fee']=getComisionCompraLumens()
    if  'procesar' in request.POST:
        form=SolicitudCompraLumensForm(request.POST)
        if form.is_valid():
            try:
                with transaction.atomic():
                    litppg=WalletTb040Limite.objects.latest('created_at')
                    limiteTemp,created=WalletTb041LimiteUsuario.objects.get_or_create(
                        co_limite=litppg,
                        co_usuario=request.user,
                        in_activo=True)

                    fecha_hoy = timezone.now()  
                    dif_hora=(fecha_hoy - limiteTemp.created_at)
                    days, seconds = dif_hora.days, dif_hora.seconds
                    hours = days * 24 + seconds // 3600

                    if dif_hora.days>=1:
                        limiteTemp.in_activo=False
                        limiteTemp.save()
                        limite=WalletTb041LimiteUsuario(
                            co_limite=litppg,
                            co_usuario=request.user)
                        limite.save()
                    else:
                        limite=limiteTemp

                    if limite.checkLimite(0):
                        return comprarLumens2(request,co_tipo_solicitud)
                    else:
                        content['success']=False
                        content['msg']="Ha excedido el monto maximo diario permitido de compra de lumens por nuestro sistema"
                                    
            except IntegrityError:
                content['success']=False
                content['msg']="Ha habido un error en la operación"
    elif 'validar' in request.POST:
        return comprarLumens2(request,co_tipo_solicitud)

    return render(request, '../templates/wallet/recargas/compralumens.html',content)

def comprarLumens2 (request,co_tipo_solicitud):
    content=getContentBasicoL2(request.user,co_tipo_solicitud,SolicitudCompraLumensReferenciaForm())
    content["data"]=request.POST
    content['mostrar']=True
    if 'validar' in request.POST:
        form=SolicitudCompraLumensReferenciaForm(request.POST)
        if form.is_valid():
            try:
                with transaction.atomic():
                    litppg=WalletTb040Limite.objects.latest('created_at')
                    limiteTemp,created=WalletTb041LimiteUsuario.objects.get_or_create(
                        co_limite=litppg,
                        co_usuario=request.user,
                        in_activo=True)
                        
                    #fecha_hoy=datetime.now()
                    fecha_hoy = timezone.now()
                    #print(str(naive))
                    #print(str(fecha_hoy)+"-"+str(limiteTemp.created_at))
                    dif_hora=(fecha_hoy - limiteTemp.created_at)

                    days, seconds = dif_hora.days, dif_hora.seconds
                    hours = days * 24 + seconds // 3600
                    #print(str(dif_hora.days))
                    #print(str(hours))
                    #print(str(seconds))
                    if dif_hora.days>=1:
                        limiteTemp.in_activo=False
                        limiteTemp.save()
                        limite=WalletTb041LimiteUsuario(
                            co_limite=litppg,
                            co_usuario=request.user)
                        limite.save()
                    else:
                        limite=limiteTemp
    
                    if limite.checkLimite(Decimal(request.POST['cantidad_recibido'])):
                        
                        limite.nu_cantidad=Decimal(limite.nu_cantidad)+Decimal(request.POST['cantidad_recibido'])
                        limite.save()

                        solicitud=MainTb028Solicitud.objects.newSolicitud(co_tipo_solicitud,request.user)
                        solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                        solicitud.in_finanza=True
                        solicitud.save()

                        transaccion=WalletTb015Transaccion(
                        nu_monto=request.POST['cantidad_depositar'],
                        co_emisor=request.user,
                        co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                        #co_moneda=WalletTb003Moneda.objects.get(co_moneda=request.POST['moneda_depositar']),
                        co_moneda=WalletTb003Moneda.objects.get(co_moneda=3),
                        co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=6),
                        nu_monto2=request.POST['cantidad_recibido'],
                        fe_transaccion=request.POST['fecha_transferencia'],
                        co_solicitud=solicitud
                        )
                        transaccion.save()

                        referencia=WalletTb010Recarga(
                        tx_codigo=request.POST['co_referencia'],
                        co_banco=WalletTb007Banco.objects.get(co_banco=request.POST['co_banco']),
                        co_transaccion=transaccion,
                        co_solicitud=solicitud
                        )
                        referencia.save()
                    
                        ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                        ruta.in_activo=False
                        ruta.save()
                        
                        content['data1']=WalletTb015Transaccion.objects.filter(co_emisor=request.user, co_tipo_transaccion=6)
                        content['success']=True
                        content['mostrar']=False
                        content['msg']="Su solicitud de compra ha sido enviada con exito. Tendra respuesta en aproximadamente 30 min y se depositaran los lumens en su billetera"

                        nota=''
                        estatus='enviada'
                        datos_email(referencia,nota,estatus)
                        
                        return render(request, '../templates/wallet/recargas/compralumens.html',content)
                    else:
                        content['success']=False
                        content['msg']="Ha excedido el monto maximo diario permitido de compra de lumens por nuestro sistema"

                        return render(request, '../templates/wallet/recargas/compralumens.html',content)

                         
            except IntegrityError:
                content['success']=False
                content['msg']="Ha habido un error en la operación"
        content['form']=form

    return render(request, '../templates/wallet/recargas/compralumensreferencia.html',content)

def comprar_usdv(request,co_tipo_solicitud):
    content=getContentBasicoL2(request.user,co_tipo_solicitud,SolicitudUSDVForm())
    if  'aprobar' in request.POST:
        print(request.POST)
        print('entro al post')
        if request.POST['p_estatus']=='COMPLETED':
            print('funciono el completado')
            form=SolicitudUSDVForm(request.POST, request.FILES)
            print(form)
            if form.is_valid():
                try:
                    with transaction.atomic():
                        print('si atomic')
                        solicitud=MainTb028Solicitud.objects.newSolicitud(28,request.user)
                        solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                        solicitud.in_finanza=True
                        solicitud.save()

                        fecha= datetime.now()
                        transaccion=WalletTb015Transaccion(
                        nu_monto=request.POST['nu_monto_usd'],#monto en usd
                        nu_monto2=request.POST['nu_monto_usdv'],#monto en usdv
                        co_emisor=request.user,
                        co_estatus=WalletTb009Estatus.objects.get(co_estatus=2),
                        co_moneda=WalletTb003Moneda.objects.get(co_moneda=4),
                        co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=6),
                        fe_transaccion=fecha,
                        co_comision=WalletTb014Comision.objects.get(co_comision=16),
                        co_solicitud=solicitud,
                        )
                        transaccion.save()

                        paypal=WalletTb047PaypalOperacion(
                            tx_nombres=request.POST['p_nombre'],#nombre
                            tx_apellidos=request.POST['p_apellido'],#apellidos
                            tx_correo=request.POST['p_correo'],#correo del usuario que pago
                            tx_estatus=request.POST['p_estatus'],#estado de al transapcion(COMPLETED)
                            img_paypal = form.cleaned_data["img_paypal"],
                            in_activo=True,
                            co_solicitud=solicitud,
                            co_transaccion=transaccion
                        )
                        paypal.save()

                        ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                        ruta.in_activo=False
                        ruta.save()
                        #print('termino') 

                        content['success']=True
                        content['msg']="Proceso realizado con éxito. Puede revisar la acreditación de los USDV en su balance."
                except IntegrityError:
                    content['success']=False
                    content['msg']="Ha habido un error en la operación"
            else:
                content['success']=False
                content['msg']="Error en el formulario"
        else:
            content['success']=False
            content['msg']="La Transapcion tubo error en el pago"
    

    return render(request, '../templates/wallet/recargas/compra_usdv.html',content)