from django.views.generic.base import TemplateView
from stellar_sdk.sep.stellar_uri import TransactionStellarUri,PayStellarUri
from django.utils.decorators import method_decorator
from main.views.utils import confirmado_user_required,user2fa_required,getTokenUsuario
from apipago.views.stellar import getAccountBalance
from stellar_sdk.server import Server
from stellar_sdk import (
     TransactionBuilder, Network, Keypair, 
     Account,Asset,Operation,AllowTrust,memo as sdkmemo
     )
from typing import List, Dict, Optional, Tuple
from decimal import Decimal,getcontext
from apipago.serializers import MensajeStellarSerializer
from main.models import (
    WalletTb038MensajeStellar,WalletTb042StellarToml,
    WalletTb045AplicationSEP07,WalletTb046AplicationKeystore,
    WalletTb037Keystore,MainTb022Directorio
    )
from stellar_sdk.sep.stellar_toml import fetch_stellar_toml
from django.db.models import QuerySet,Prefetch,Q
from django.shortcuts import render,redirect
from main.views.login import login3
from django_otp import user_has_device
import json
import base64

#@method_decorator(decorators, name='dispatch')
class Sep07Views(TemplateView):
    
    template_name = "../templates/wallet/sep07/payment.html"

    context={}

        


    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            if  self.request.method=="GET":
                if "operation" not in self.request.GET or self.request.GET["operation"]=="":
                    self.template_name="../templates/wallet/sep07/payment_error.html"
                    return super(Sep07Views, self).dispatch(request, *args, **kwargs)
                else:
                    return redirect('login-sep07',operation=self.request.GET["operation"])
        
        dosfa=user_has_device(request.user) if request.user.in_2fa else True
        if not dosfa or not request.user.in_confirmado:
            return redirect('login-sep07',operation=self.request.GET["operation"])

        return super(Sep07Views, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        self.context = super().get_context_data(**kwargs)

        #PRIMERA VALIDACION, SOLO SE ACCEPTA METODO GET
        self.context["success"]=True
        self.context["token"]=getTokenUsuario(self.request.user)
        
        #BUSCAR LOS MENSAJES DE STELLAR DE LA BASE DE DATOS
        msjstellar=MensajeStellarSerializer(WalletTb038MensajeStellar.objects.all(),many=True)
        self.context['msjstellarjson']=json.dumps(msjstellar.data)

        
        return self.context

    
    def post(self,*args,**kwargs):
        resp = super().get(*args, **kwargs)
        operation=self.request.POST["operation"] if "operation" in self.request.POST else None
        if operation is None:
            operation=kwargs["operation"] if "operation" in kwargs else operation

        #print(operation)
        self.inicializacion(operation,resp)
        return resp


    def get(self,*args,**kwargs):
        resp = super().get(*args, **kwargs)
        operation=self.request.GET["operation"] if "operation" in self.request.GET else None
        if operation is None:
            operation=kwargs["operation"] if "operation" in kwargs else operation
        self.inicializacion(operation,resp)
        return resp

    #PROCEDIMIENTO PRINCIPAL
    def inicializacion(self,operation,resp)->object:
        self.context["xdr"]=None
        if self.context["success"]==False:
            return resp

        #VALIDAR SI EXISTE LA VARIABLE OPERACION
        if operation is None or operation=="":
            self.context["success"]=False
            self.context["error"]="No hay datos externos para construir una transaccion."
            return resp

        
        uri=self.validateGetUri(operation)
        
        #VALIDAR EL CUSTOM SCHEMA URL
        if uri is None:
            self.context["success"]=False
            self.context["error"]="Los datos externos son invalidos o estan errados"
            return resp

        #OBTENER EL TIPO DEL URI DE STELLAR SEGUN SEAN LOS DATOS RECIVIDOS
        if isinstance(uri,PayStellarUri):
            self.context["tipo"]="pay"
            if uri.asset_code is None:
                uri.asset_code="XLM"

            #ERROR EN CASO DE CALLBACK POR FALTA DE FUNCIONALIDAD
            if uri.callback is not None:
                self.context["success"]=False
                self.context["error"]="La operaciones payment con callback aun se encuentra en desarrollo"
                return resp

            payment=self.buildPayment(uri)
            if not payment["success"]:
                self.context["success"]=False
                self.context["error"]=payment["error"]
                return resp
            self.context["xdr"]=payment["xdr"]
            self.context["operation"]={
                "source":payment["source"],
                "fee":payment["fee"],
                "in_monto":payment["in_monto"],
                "balance":payment["balance"],
                "balance_native":payment["balance_native"]
            }


        else:
            self.context["tipo"]="tx"
            self.context["success"]=False
            self.context["error"]="Esta plataforma aun no posee un soporte para operaciones tipo TX"
            return resp



        #PASO DONDE SE VALIDARA EL DOMINIO Y LA FIRMA DEL MISMO
        validarDominio=self.validateDomainSign(uri,operation)
        if not validarDominio["success"]:
            self.context["success"]=False
            self.context["error"]=validarDominio["error"]
            return resp
        else:
            self.context["in_directorio"]=validarDominio["in_directorio"]
            self.context["in_gobelx"]=validarDominio["in_gobelx"]


        serialize=self.sarializerURI(uri)
        serialize["tipo"]=self.context["tipo"]

        jsonuri=json.dumps(serialize)
        jsonoperation=json.dumps(self.context["operation"])

        self.context["adv"]=validarDominio["adv"]
        self.context["data"]=uri
        self.context["jsonoperation"]=jsonoperation
        self.context["jsondata"]=jsonuri
    


        return resp

    #SERIALIZAR A DIRECTORIO LOS DATOS DE LA OPERACION SEP07 SEGUN SU TIPO
    def sarializerURI(self,stellarUri:object)->dict:
        
        if isinstance(stellarUri,PayStellarUri):
            memo=stellarUri.memo.decode("utf-8") if stellarUri.memo_type=="MEMO_TEXT" else stellarUri.memo
            content={
                "destination":stellarUri.destination if stellarUri.destination is not None else None , 
                "amount":stellarUri.amount if stellarUri.amount is not None else None , 
                "asset_code":stellarUri.asset_code if stellarUri.asset_code is not None else None , 
                "asset_issuer":stellarUri.asset_issuer if stellarUri.asset_issuer is not None else None , 
                "memo":str(memo) if stellarUri.memo is not None else None, 
                "memo_type":stellarUri.memo_type if stellarUri.memo_type is not None else None, 
                "callback":stellarUri.callback if stellarUri.callback is not None else None , 
                "msg":stellarUri.msg if stellarUri.msg is not None else None , 
                "network_passphrase":stellarUri.network_passphrase if stellarUri.network_passphrase is not None else None , 
                "origin_domain":stellarUri.origin_domain if stellarUri.origin_domain is not None else None , 
                "signature":stellarUri.signature if stellarUri.signature is not None else None 
            }
        else:
            content={
                "xdr":str(stellarUri.transaction_envelope.to_xdr()) if stellarUri.transaction_envelope is not None else None,
                "replace":stellarUri.replace if stellarUri.replace is not None else None,
                "callback":stellarUri.callback if stellarUri.callback is not None else None,
                "pubkey":stellarUri.pubkey if stellarUri.pubkey is not None else None,
                "msg":stellarUri.msg if stellarUri.msg is not None else None,
                "network_passphrase":stellarUri.network_passphrase if stellarUri.network_passphrase is not None else None,
                "origin_domain":stellarUri.origin_domain if stellarUri.origin_domain is not None else None,
                "signature":stellarUri.signature if stellarUri.signature is not None else None,

            }   

        return content 

    def buildMemo(
        self,transaction:TransactionBuilder,stellarUri:PayStellarUri
    )->TransactionBuilder:
        if stellarUri.memo_type=="MEMO_TEXT":
            transaction=transaction.add_text_memo(stellarUri.memo)
        elif stellarUri.memo_type=="MEMO_ID":
            try:
    
                transaction=transaction.add_id_memo(stellarUri.memo)
            except:
    
                transaction=transaction.add_text_memo(stellarUri.memo)
        elif stellarUri.memo_type=="MEMO_HASH":
            try:
    
                transaction=transaction.add_hash_memo(stellarUri.memo)
            except:
    
                transaction=transaction.add_text_memo(stellarUri.memo)
        elif stellarUri.memo_type=="MEMO_RETURN":
            try:
    
                transaction=transaction.add_return_memo(stellarUri.memo)
            except:
    
                transaction=transaction.add_text_memo(stellarUri.memo)
        else:
             transaction=transaction.add_text_memo(stellarUri.memo)

        return transaction


    #AQUI SE CONSTRUIRA UNA TRANSACCION PAYMENT EN CASO DE QUE EL URI SEA PAY
    def buildPayment(self,stellarUri:PayStellarUri)->dict:

        #------------------INICIALIZACION DE VARIABLES PRIMARIAS------------
        #print(self.request.session["merma"])
        public=self.request.session["public_key"]
        content={
            'success':False,
            'xdr':None,
            'balance':None,
            'balance_native':None,
            'source':public,   
            }

        receptor = stellarUri.destination
        server = Server(horizon_url="https://horizon.stellar.org")

        try:
            source_account = server.load_account(public)
        except:
            content["error"]="Su dirección stellar no es valida o no esta activa"
            return content

        try:
            source_receptor=server.load_account(receptor)
        except:
            content["error"]="La dirección stellar de destino no es valida o no esta activa"
            return content

        base_fee = server.fetch_base_fee()+1000
        raw_fee=round(float(base_fee)*float(0.0000001),7)    

        content["fee"]={
            "normal":base_fee,
            "raw":raw_fee
        }
        network_passphrase=Network.PUBLIC_NETWORK_PASSPHRASE

        #----------------COMPROBACIONES DE REQUISITOS SEGUN LOS DATOS RECIVIDOS---------------------
        
        #------------------------CUENTA
        #Comprobar si la cuenta existe
        cuenta=getAccountBalance(public)
        if len(cuenta["data"])<=0:
            content["error"]="Su dirección stellar no es valida o no esta activa"
            return content


        #------------------------NETWORK_PASSPHARASE
        #Comprobar si el network_passphrase es minimamente valido
        if stellarUri.network_passphrase is not None and stellarUri.network_passphrase!="":
            if stellarUri.network_passphrase==Network.TESTNET_NETWORK_PASSPHRASE:
                content["error"]="La operacion no puede ser de prueba"
                return content
        
            if stellarUri.network_passphrase!=network_passphrase:
                network_passphrase=stellarUri.network_passphrase


        #------------------------MEMO
        #Comprobar como va ser el memo si es que va a tener
        memo_content={
            "in_memo":False,
            "memo":None,
            "memo_type":None
        }
        if stellarUri.memo is not None:
            memo_content["in_memo"]=True


        #--------------------------ACTIVO/ASSET
        #Comprobar si el activo es una cuenta activa o es nativo
        tx_asset_code="XLM"
        tx_asset=tx_asset_code
        if stellarUri.asset_code is not None and stellarUri.asset_code!="XLM":

            #Sercioar si el supuesto token tiene un emisor
            if stellarUri.asset_issuer is None:
                content["error"]="El activo debe llevar un emisor representativo"
                return content

          
            tx_asset_code=stellarUri.asset_code
            tx_asset=tx_asset_code+":"+stellarUri.asset_issuer
            
            #Comprobar si el usuario tiene tiene confianza
            #----nota: si tiene confianza significa que el activo existe 
            #----y no hace falta comprobarlo
            confianza=False
            for value in cuenta["data"]:
                if value["tx_asset"]==tx_asset:
                    content["balance"]=value
                    confianza=True
                if value["tx_asset_code"]=="XLM":
                    content["balance_native"]=value["nu_balance"]
            
            if not confianza:
                content["error"]="El usuario no tiene confianza con este activo"
                return content

        else:
            for value in cuenta["data"]:
                if value["tx_asset"]==tx_asset:
                    content["balance"]=value
                if value["tx_asset_code"]=="XLM":
                    content["balance_native"]=value["nu_balance"]


        #--------------------------MONTO
        #Comprobar si es necesario monto
        content["in_monto"]=False
        if stellarUri.amount is not None:
            content["in_monto"]=True
            #-1. Comprobar si hay recerva de XLM suficientes     
            if float(content["balance_native"])<=0:
                content["error"]="No tienes ninguna recerva de XLM para realizar alguna operación"
                return content

            
            #-2. Comprobar si tu recerva cubre el costo de la comision y o monto en XLM
            #----Si es el caso
            monto_native=round(float(stellarUri.amount),7) if stellarUri.asset_code=="XLM" else 0.00 
            monto=round(float(stellarUri.amount),7)

            if float(content["balance_native"])<raw_fee+monto_native:
                content["error"]="No tienes la suficiente recerva de XLM para realizar esta operación"
                return content
                

            #-3. Comprobar si tienes suficientes token para realizar la transaccion 
            #----si es el caso de usar token en esta operacion.
            #print((float(content["balance"]["nu_balance"]),monto))
            if stellarUri.asset_code!="XLM" and float(content["balance"]["nu_balance"])<monto:
                
                content["error"]="No tienes la suficiente recerva de "+stellarUri.asset_code+" para realizar esta operación"
                return content


        #---------------------CONSTRUCCION DE LA TRANSACCION------------------- 
        #Construir la base la transaccion mas basica supuestamente permitida
        transactions=TransactionBuilder(
            source_account=source_account,
            network_passphrase=network_passphrase,
            base_fee=base_fee,
            ).set_timeout(300)
        

        #--------ASIGNAR EL MEMO
        if memo_content["in_memo"]:
                transactions=self.buildMemo(transactions,stellarUri)
        #--------CONSTRUIR LA OPERACION EN CASO DE SER NECESARIO
        if content["in_monto"]:
            transactions=transactions.append_payment_op(
                receptor,
                str(stellarUri.amount),
                tx_asset_code,
                stellarUri.asset_issuer if stellarUri.asset_issuer is not None else None
            )
        
        #--------CONSTRUIR LA TRANSACCION
        transactions=transactions.build()
        
        content["xdr"]=transactions.to_xdr()

        content["success"]=True
        return content
    
    #AQUI SE VALIDARA LA FIABILIDAD DEL DOMINIO Y EL CAMPO DE FIRMA
    def validateDomainSign(self,stellarUri:PayStellarUri,uri:str)->dict:
        content={
            "success":True,
            "adv":[]
        }
        #------PASO 1 COMPROBAR SI EXISTE LA VARIABLE ORIFIN_DOMAIN
        if stellarUri.origin_domain is None:
            msj="Esta operación no posee un dominio vinculado,\
                por lo cual es una transaccion anonima y se recomianda precaución"
            content["adv"].append(msj)
            return content

        #------PASO 2 COMPROBAR SI EXISTE LA VARIABLE SIGNARUTE
        if stellarUri.signature is None:
            msj="El link de esta operación no posee ninguna firma con la cual verificar\
                la veracidad del dominio de la operacion por lo cual se considera muy inseguro\
                y por lo tal no se podra efectuar la operación"
            content["success"]=False  
            content["error"]=msj
            return content


        #------PASO 3 COMPROBAR SI EL DOMINIO ES VALIDO Y TIENE TOML
        try:
            toml=fetch_stellar_toml(stellarUri.origin_domain,use_http=True)
            
        except:
            msj="El dominio de esta operación es invalido o no posee TOML por lo cual se considera muy inseguro\
                y por lo tal no se podra efectuar la operación"
            content["success"]=False  
            content["error"]=msj
            return content

        #------PASO 4 COMPROBAR SI EL TOML POSEE URI_REQUEST_SIGNING_KEY
        if "URI_REQUEST_SIGNING_KEY" not in toml:
            msj="El TOML del dominio de esta operacíon no tiene registrado una llave firmante \
                por lo cual se considera muy inseguro\
                y por lo tal no se podra efectuar la operación"
            content["success"]=False  
            content["error"]=msj
            return content


        #------PASO 5 MANEJAR LOS DATOS DEL TOML Y URI_REQUEST_SIGNING_KEY
        toml_docu=toml["DOCUMENTATION"] if "DOCUMENTATION" in toml else {}

        if "ORG_URL" not in toml_docu:
            toml_docu["ORG_URL"]=stellarUri.origin_domain
            
        aplicacion_sep07,_=WalletTb045AplicationSEP07.objects\
            .select_related('co_stellar_toml')\
            .get_or_create(tx_origin_domain=stellarUri.origin_domain)
        

        #------PASO 5.1 GUARDAR LOS DATOS EN DB PARA COMPROBAR SI HAY 
        # --------------CAMBIOS EN OPERACIONES POSTERIOES

        if aplicacion_sep07.co_stellar_toml is None:

            stellar_toml=WalletTb042StellarToml(
                tx_org_url=toml_docu["ORG_URL"],
                tx_org_desc=toml_docu["ORG_DESCRIPTION"] if "ORG_DESCRIPTION" in toml_docu else None,
                tx_uri_key=toml["URI_REQUEST_SIGNING_KEY"]
            )
            stellar_toml.save()
            aplicacion_sep07.co_stellar_toml=stellar_toml
            aplicacion_sep07.save()
        else:
            if aplicacion_sep07.co_stellar_toml.tx_uri_key is None:
                aplicacion_sep07.co_stellar_toml.tx_uri_key=toml["URI_REQUEST_SIGNING_KEY"]
                aplicacion_sep07.co_stellar_toml.save()

            #------PASO 5.2 COMPROBAR SI HAY ALGUNA DIFERENCIA 
            # --------------EN LA KEY DE LA BD Y LA DEL TOML
            elif aplicacion_sep07.co_stellar_toml.tx_uri_key != toml["URI_REQUEST_SIGNING_KEY"]:
                msj="La llave fimante de este dominio, no coincide con nuestros registros, \
                    por lo cual se considera que ha sido actualizado por su dominio\
                    y se recomienda precausión."
                content["adv"].append(msj)
                aplicacion_sep07.co_stellar_toml.tx_uri_key=toml["URI_REQUEST_SIGNING_KEY"]
                aplicacion_sep07.co_stellar_toml.save()


        #------PASO 6 VERIFICAR LA FIRMA
        key_pair=Keypair.from_public_key(aplicacion_sep07.co_stellar_toml.tx_uri_key)
        firma=base64.b64decode(stellarUri.signature)
        try:
            key_pair.verify(stellarUri._signature_payload,firma)
        except:
            msj="La firma suministrada para la operación es invalida \
                por lo cual se considera muy inseguro\
                y por lo tal no se podra efectuar la operación"
            content["success"]=False  
            content["error"]=msj
            return content


        
        #------PASO 7 VERIFICAR SI HAY UN HISTORAL CON EL USUARIO Y EL DOMINIO EXTERNO 
        #------NOTA ESTO PUEDE OBVIARSE
        content["in_directorio"]=False
        content["in_gobelx"]=False

        if "ORG_DBA" in toml_docu and toml_docu["ORG_DBA"]=="Gobelx" :
            content["in_gobelx"]=True

        if not content["in_gobelx"]:
            keystore,_=WalletTb037Keystore.objects.get_or_create(
                address=stellarUri.destination
            )

            aplicacion_keystore,_=WalletTb046AplicationKeystore.objects\
                .select_related('co_aplication_sep07','co_keystore')\
                .prefetch_related(
                Prefetch("co_keystore__billetera",
                    queryset=MainTb022Directorio.objects.filter(co_usuario=self.request.user.co_usuario))
                )\
                .get_or_create(
                    co_aplication_sep07=aplicacion_sep07,
                    co_keystore=keystore)
            directorio=aplicacion_keystore.co_keystore.billetera.all()

            if not directorio.exists():
                msj="Querido usuario, esta es la primera vez que usted realiza \
                esta transferencia a este usuario, se recomienda cautela."
                content["adv"].append(msj)
                content["in_directorio"]=True
                


        

        return content

    #VALIDAR SI EL CUSTOM SCHEMA URI ES UNA URI SEP07 VALIDA
    def validateGetUri(self,operation:str)->object:
        try:
            uri=PayStellarUri.from_uri(operation)
        except:
            try:
                uri=TransactionStellarUri.from_uri(operation,"Public Global Stellar Network ; September 2015")
            except Exception as e:
                uri=None

        return uri