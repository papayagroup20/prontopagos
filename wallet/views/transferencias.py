from django.shortcuts import render,redirect
from main.models import (WalletTb038MensajeStellar,WalletTb014Comision,Tb001Usuario,MainTb022Directorio)
from wallet.form import trasferenciaForm
from django.db import IntegrityError, transaction
from apipago.serializers import MensajeStellarSerializer
from apipago.views.stellar import getAccountBalance
from main.views.utils import (getContentBasicoL2,user2fa_required,confirmado_user_required)
import json

@confirmado_user_required
@user2fa_required
def trasferencia(request):
    
    #billetera=WalletTb037Keystore.objects.filter(co_usuario=94)[0] if WalletTb037Keystore.objects.filter(co_usuario=94).count()>0 is not None else None
    publico=request.session["public_key"]
    misAsset=getAccountBalance(publico)
    querymsjstellar=WalletTb038MensajeStellar.objects\
        .select_related("co_tipo_error_stellar")\
        .filter(co_tipo_error_stellar__in=[1,2,3,4])

    msjstellar=MensajeStellarSerializer(querymsjstellar,many=True)
    comision=WalletTb014Comision.objects.filter(co_tipo_transaccion=1,in_activo=True).only("nu_monto").first()
    choice=[("0","Seleccione...")]
    memo=[
        ("1","TEXTO"),
        ("2","ID"),
        ("3","HASH"),
        ("4","RETURN")

    ]
    for a in misAsset["data"]:
        choice.append((a["tx_asset_issuer"],a["tx_asset_code"]))
    content=getContentBasicoL2(request.user,None,trasferenciaForm(initial={'co_moneda':'0'},lista=choice,user=request.user,lista2=memo))
    content['nuevo']=False
    content['comision']=comision.nu_monto
    content['co_comision']=comision.co_comision
    content['activos']=misAsset
    content['publico']=publico
    content['msjstellarjson']=json.dumps(msjstellar.data)
    content['misactivosjson']=json.dumps(misAsset["data"])
            

    return render(request, '../templates/wallet/transferencias/transfInterno1.html',content)

@confirmado_user_required
@user2fa_required
def trasferenciaSuccess(request,co_tipo_solicitud):

    content=getContentBasicoL2(request.user,co_tipo_solicitud,None)
    content['nuevo']=False
    
    if request.method=='POST':
        if 'usernamedir' in request.POST or 'username' in request.POST:

            if Tb001Usuario.objects.filter(co_usuario__username=request.POST['username']) is None or Tb001Usuario.objects.filter(co_usuario=request.POST['usernamedir']) is None:
                return redirect('bandejaInicioSuccess',success=1,codigo=2)

          
              
        elif 'co_usuario' in request.POST:
            if 'check' in request.POST:
                if  request.POST['check']=='on':
                    try:
                        with transaction.atomic():
                            usu=Tb001Usuario.objects.get(co_usuario=request.POST['co_usuario'])
                            apodo=usu.co_persona.get_nombre_corto if usu.co_persona is not None else usu.username

                            directorio=MainTb022Directorio(
                                tx_apodo=apodo,
                                co_usuario=request.user,
                                co_usuario_guardado=usu


                            )
                            directorio.save()
                            
                            content['msg']='todo bien'
                    except IntegrityError:
                        content['msg']='Hubo un error en la operación'
                    
    
            return redirect('bandejaInicioSuccess',success=1,codigo=2)

        else:
            return redirect('bandejaInicioSuccess',success=1,codigo=2) 
    
    else:
        return redirect('bandejaInicioSuccess',success=0,codigo=1)
            

