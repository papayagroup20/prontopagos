from rest_framework import serializers
from main.models import WalletTb036Activos,WalletTb042StellarToml


"""class assetSerializer():
    issuer
    
    class Meta:
        model=WalletTb036Activos
        fields=[
            'code',
            'issuer',
        ]
        read_only_fields=[
            'co_solicitud_pago',
            'created_at',
            'updated_at',
        ]"""
"""class TomlSerializer(serializers.ModelSerializer):
    class Meta:
        model=WalletTb042StellarToml
        fields=[
            'tx_org_url'
            'tx_org_desc'   
        ]"""
class ActivoSerializer(serializers.ModelSerializer):
    tx_asset = serializers.SerializerMethodField()
    asset = serializers.SerializerMethodField()
    tx_moneda = serializers.SerializerMethodField()
    asset_desc=serializers.CharField(source='toml_asset_desc',default='')
    asset_conditions=serializers.CharField(source='toml_asset_conditions',default='',)
    tx_moneda=serializers.CharField(source='toml_name',default='')
    img=serializers.CharField(source='toml_img',default='')
    asset_type=serializers.CharField(source='toml_asset_type',default='')
    org_url=serializers.ReadOnlyField(source='co_stellar_toml.tx_org_url',default='')
    org_desc=serializers.ReadOnlyField(source='co_stellar_toml.tx_org_desc',default='')
    class Meta:
        model=WalletTb036Activos
        fields=[
            'asset',
            'tx_asset',
            'tx_moneda',
            'asset_desc',
            'asset_conditions',
            'org_url',
            'org_desc',
            'img',
            'asset_type',
            'in_activo',
            'in_dep_ret'
                 
        ]
    
    def get_tx_asset(self, obj):
        tx_asset=obj.tx_asset_code+":"+obj.tx_asset_issuer if obj.tx_asset_code!="XLM" else "XLM"   
        return tx_asset
    
    def get_asset(self,obj):
        asset={
            'code':obj.tx_asset_code,
            'issuer':obj.tx_asset_issuer if obj.tx_asset_code!="XLM" else ''
            }
        return asset
    
    def get_tx_moneda(self,obj):
        tx_moneda=''
        if obj.tx_alias is not None:
            tx_moneda=obj.tx_alias
        elif obj.toml_name is not None:
            tx_moneda=obj.toml_name
        return tx_moneda

"""asset
tx_asset
asset_desc
asset_conditions
tx_moneda
nu_balance
nu_balance2
nu_limit
img
org_url
org_desc
in_activo
asset_type
propio"""