from rest_framework import serializers
from main.models import *




class ServiciosSerializer(serializers.ModelSerializer):
    tx_servicios=serializers.ReadOnlyField(source='co_tipo_servicios.tx_servicios')
    class Meta:
        model=MainTb012Servicios
        
        fields=['co_servicios','tx_servicios','co_tipo_servicios']

class TipoServiciosSerializer(serializers.ModelSerializer):
    servicios_tpser=ServiciosSerializer(many=True, read_only=True)
    class Meta:
        model=MainTb010TipoServicios
        
        fields=['co_tipo_servicios','tx_servicios','servicios_tpser']

class ProveedorServiciosSerializer(serializers.ModelSerializer):
    serviciosp=ServiciosSerializer(many=True, read_only=True)
    class Meta:
        model=MainTb011ProveedorServicios
        
        fields=['co_proveedor_servicios','tx_proveedor_servicios','serviciosp']


class TipoServiciosBasicoSerializer(serializers.ModelSerializer):
    class Meta:
        model=MainTb010TipoServicios
        
        fields=['co_tipo_servicios','tx_servicios']

class ProveedorServiciosBasicoSerializer(serializers.ModelSerializer):
    class Meta:
        model=MainTb011ProveedorServicios
        
        fields=['co_proveedor_servicios','tx_proveedor_servicios']

class RequisitosServiciosSerializer(serializers.ModelSerializer):

    class Meta:
        model=MainTb013RequisitosServicios
        
        fields=['co_requisitos_servicios','tx_requisitos_servicios']


class AllServiciosSerializer(serializers.ModelSerializer):
    proveedor=ProveedorServiciosBasicoSerializer(source='co_proveedor_servicios', read_only=True)
    servicio=TipoServiciosBasicoSerializer(source='co_tipo_servicios', read_only=True)
    requisitos=RequisitosServiciosSerializer(source='reqser_servicios',many=True, read_only=True)
    class Meta:
        model=MainTb012Servicios
        
        fields=['co_servicios','proveedor','servicio','requisitos']



class RequestRequisitosSerializer(serializers.ModelSerializer):
    
    tx_requisito=serializers.CharField()
    class Meta:
        model=WalletTb006DetallePagoServicios
        
        fields=['co_detalle_pago_servicios','tx_respuesta_requisito','tx_requisito']

class StringListField(serializers.ListField):
    campo = serializers.CharField()

class RequestServicioSerializer(serializers.ModelSerializer):
    tx_hash=serializers.CharField(max_length=255)
    lista_operaciones=StringListField()
    requisitos=RequestRequisitosSerializer(source='reqser_servicios',many=True, read_only=True)
    class Meta:
        model=MainTb012Servicios
        fields=['co_servicios','tx_hash','lista_operaciones','requisitos']
        read_only_fields=['co_servicios','tx_hash','lista_operaciones','requisitos']
class DetallePagosServiciosSerializer(serializers.ModelSerializer):
    
    tx_requisito=serializers.ReadOnlyField(source='co_requisitos_servicios.tx_requisitos_servicios')
    class Meta:
        model=WalletTb006DetallePagoServicios
        
        fields=['co_detalle_pago_servicios','tx_respuesta_requisito','tx_requisito']

class ComisionServiciosSerializer(serializers.ModelSerializer):
    class Meta:
        model=WalletTb015Transaccion
        
        fields=['co_transaccion','tx_hash','tx_hash_stellar','co_transaccion_emisor']

class pagoServiciosProceso(serializers.ModelSerializer):
    tx_servicio=serializers.ReadOnlyField(source='co_servicios.co_tipo_servicios.tx_servicios')
    tx_proveedor=serializers.ReadOnlyField(source='co_servicios.co_proveedor_servicios.tx_proveedor_servicios')
    username=serializers.ReadOnlyField(source='get_hist_transaccion.co_transaccion.co_emisor.username')
    requisitos=DetallePagosServiciosSerializer(many=True, read_only=True)
    id_solicitud=serializers.ReadOnlyField(source='co_solicitud.co_solicitud')
    tx_hash=serializers.ReadOnlyField(source='get_hist_transaccion.co_transaccion.tx_hash_stellar')
    tx_operacion=serializers.ReadOnlyField(source='get_hist_transaccion.co_transaccion.tx_hash')
    comisiones=ComisionServiciosSerializer(source='get_hist_transaccion.co_transaccion.subtran_tran',many=True, read_only=True)    
    tx_estatus=serializers.ReadOnlyField(source='co_estatus.tx_estatus')
    co_tipo_servicio=serializers.ReadOnlyField(source='co_servicios.co_tipo_servicios.co_tipo_servicios')
   
    
    class Meta:
        model=WalletTb005PagoServicios
        fields=[
            'co_pago_servicios','tx_servicio','tx_proveedor',
            'username','comisiones','tx_hash','tx_operacion',
            'requisitos','id_solicitud','co_estatus','tx_estatus','co_tipo_servicio'
        ]

class DataServiciosProceso(serializers.ModelSerializer):
    tx_servicio=serializers.ReadOnlyField(source='co_servicios.co_tipo_servicios.tx_servicios')
    tx_proveedor=serializers.ReadOnlyField(source='co_servicios.co_proveedor_servicios.tx_proveedor_servicios')
    username=serializers.ReadOnlyField(source='get_hist_transaccion.co_transaccion.co_emisor.username')
    tx_moneda=serializers.ReadOnlyField(source='get_hist_transaccion.co_transaccion.co_moneda.tx_moneda')
    tx_simbolo=serializers.ReadOnlyField(source='get_hist_transaccion.co_transaccion.co_moneda.tx_simbolo')
    nu_monto=serializers.ReadOnlyField(source='get_hist_transaccion.co_transaccion.nu_monto')
    nu_comision=serializers.ReadOnlyField(source='get_hist_transaccion.co_transaccion.co_comision.nu_monto')
    nu_monto_comision=serializers.ReadOnlyField(source='get_hist_transaccion.co_transaccion.get_comision.nu_monto')
    requisitos=DetallePagosServiciosSerializer(many=True, read_only=True)
    id_solicitud=serializers.ReadOnlyField(source='co_solicitud.co_solicitud')
    
    class Meta:
        model=WalletTb005PagoServicios
        fields=[
            'co_pago_servicios','tx_servicio','tx_proveedor',
            'username','tx_moneda','tx_simbolo',
            'nu_monto','nu_comision','nu_monto_comision',
            'requisitos','id_solicitud'
        ]