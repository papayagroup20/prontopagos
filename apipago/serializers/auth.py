from rest_framework import serializers
from main.models import Tb001Usuario,WalletTb045AplicationSEP07

class UsuarioBasicSerializer(serializers.ModelSerializer):
    #tx_primer_nombre=serializers.ReadOnlyField(source='co_persona.tx_primer_nombre')
    class Meta:
        model=Tb001Usuario
        fields=[
            'username',
            'email'
        ]