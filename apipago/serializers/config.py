from rest_framework import serializers
from main.models import *

class rolSerializer(serializers.ModelSerializer):
    class Meta:
        
        model=MainTb023Rol
        fields=['co_rol','tx_rol','in_activo']

class institutoSerializer(serializers.ModelSerializer):
    class Meta:
        
        model=MainTb024Instituto
        fields=['co_instituto','tx_instituto','in_activo']

class procesoSerializer(serializers.ModelSerializer):
    class Meta:
        
        model=MainTb025Proceso
        fields=['co_proceso','tx_proceso','in_activo']

class menuSerializer(serializers.ModelSerializer):
    class Meta:
        
        model=MainTb030Menu
        fields=['co_menu','tx_menu','in_activo','nu_padre','co_icono','tx_url']

class RutaSerializer(serializers.ModelSerializer):
    
    tx_instituto=serializers.ReadOnlyField(source='co_instituto.tx_instituto')
    tx_proceso=serializers.ReadOnlyField(source='co_proceso.tx_proceso')
    class Meta:
        
        model=MainTb026DetalleRuta
        fields=['co_detalle_ruta','nu_orden','tx_instituto','tx_proceso']
class tipoSolicitudSerializer(serializers.ModelSerializer):
    
    tx_instituto=serializers.ReadOnlyField(source='co_instituto.tx_instituto')
    
    ruta = RutaSerializer(many=True, read_only=True)
    
    class Meta:
        
        model=MainTb027TipoSolicitud
        fields=['co_tipo_solicitud','tx_tipo_solicitud','in_activo','tx_instituto','ruta']

    
    def get_ruta(self, instance):
        ruta = instance.ruta.all().order_by('nu_orden')
        return RutaSerializer(ruta, many=True).data

class UsuarioConfigSerializer(serializers.ModelSerializer):
    tx_primer_nombre=serializers.ReadOnlyField(source='co_persona.tx_primer_nombre')
    tx_segundo_nombre=serializers.ReadOnlyField(source='co_persona.tx_segundo_nombre')
    tx_primer_apellido=serializers.ReadOnlyField(source='co_persona.tx_primer_apellido')
    tx_segundo_apellido=serializers.ReadOnlyField(source='co_persona.tx_segundo_apellido')
    tx_tipo_documento=serializers.ReadOnlyField(source='co_persona.co_tipo_documento.tx_tipo_documento')
    tx_nombre=serializers.ReadOnlyField(source='co_persona.get_nombre_completo')
    tx_abreviacion=serializers.ReadOnlyField(source='co_persona.co_tipo_documento.tx_abreviacion')
    
    nu_documento=serializers.ReadOnlyField(source='co_persona.nu_documento')
    
    

    class Meta:
        model=Tb001Usuario
        fields=[
            'co_usuario',
            'is_active',
            'username',
            'tx_nombre',
            'tx_primer_nombre',
            'tx_segundo_nombre',
            'tx_primer_apellido',
            'tx_segundo_apellido',
            'tx_tipo_documento',
            'nu_documento',
            'tx_abreviacion',
            'co_rol',
            'co_instituto',
        ]


class UserSigninSerializer(serializers.Serializer):
    username = serializers.CharField(required = True)
    password = serializers.CharField(required = True)