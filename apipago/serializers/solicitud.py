from rest_framework import serializers
from main.models import *

class SolicitudSerialiser(serializers.ModelSerializer):
    tx_solicitud=serializers.ReadOnlyField(source='co_tipo_solicitud.tx_tipo_solicitud')

    class Meta:
        model=WalletTb005PagoServicios
        fields=[
            'co_solicitud','tx_solicitud',
        ]