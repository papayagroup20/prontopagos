from rest_framework import serializers
from main.models import (
    WalletTb015Transaccion,WalletTb047PaypalOperacion,
    WalletTb055SolicitudPago,Tb001Usuario,WalletTb037Keystore,
    WalletTb056InfoMetodoPago,WalletTb025MetodoPago)
from rest_framework.settings import api_settings
from private_storage.models import PrivateFile
from private_storage.storage import private_storage
from apipago.serializers.stellar import ActivoSerializer
from apipago.views.stellar_utils import BaseStellar 
import base64
from pprint import pprint

class TransaccionSerializer(serializers.ModelSerializer):
    
    nu_comision=serializers.ReadOnlyField(source='co_comision.nu_monto')
    tx_moneda=serializers.ReadOnlyField(source='co_moneda.tx_moneda')
    tx_simbolo=serializers.ReadOnlyField(source='co_moneda.tx_simbolo')
    
    tx_receptor=serializers.ReadOnlyField(source='co_receptor.username')
    tx_estatus=serializers.ReadOnlyField(source='co_estatus.tx_estatus')
    tx_tipo_transaccion=serializers.ReadOnlyField(source='co_tipo_transaccion.tx_tipo_transaccion')
    updated_at = serializers.DateTimeField(format=api_settings.DATETIME_FORMAT,input_formats=None, default_timezone=None)
    class Meta:
        model=WalletTb015Transaccion
        
        fields=['co_transaccion','nu_monto','nu_monto2','nu_comision','tx_moneda',
                'tx_simbolo','tx_receptor','co_transaccion_emisor',
                'tx_estatus','tx_tipo_transaccion','updated_at','fe_transaccion']


class InfoMetodoPagoSerializers(serializers.ModelSerializer):
    class Meta:
        model=WalletTb056InfoMetodoPago
        fields=[
            'co_info_metodo_pago','tx_titulo','tx_info'
        ]
        
        
class MetodoPagoSerializers(serializers.ModelSerializer):
    info_metodo_pago=InfoMetodoPagoSerializers(source='inmepa_mepa', read_only=True,many=True)
    class Meta:
        model=WalletTb025MetodoPago
        fields=[
            'co_metodo_pago','tx_metodo_pago','info_metodo_pago'
        ]
        
        read_only_fields=[
            'co_metodo_pago','tx_metodo_pago','info_metodo_pago'
        ]

class PaypalOperacionSerializer(serializers.ModelSerializer):
    imagen=serializers.SerializerMethodField()

    class Meta:
        model=WalletTb047PaypalOperacion
        fields=[
            "co_paypal_operacion","tx_nombres","tx_apellidos",
            "tx_correo","tx_estatus","imagen","co_solicitud",
            "co_transaccion","created_at","updated_at","in_activo"]
        read_only_fields=[
            "co_paypal_operacion","co_solicitud","co_transaccion",
            "created_at","updated_at","in_activo"]

    def tenerImagen(self,request,imagen):
        storage = private_storage
        privateFile=PrivateFile(
            request=request,
            storage=storage,
            relative_name=str(imagen)
        )

        ope=privateFile.open()
        encoded = base64.b64encode(ope.read())
        return encoded

    def get_imagen(self, obj):
        request=self.context.get("request")
        return self.tenerImagen(request,obj.img_paypal)

class publicKey(serializers.ModelSerializer):
    class Meta:
        model=WalletTb037Keystore
        fields=[
            'address',
        ]
        

class usuarioCortoSerializer(serializers.ModelSerializer):
    tx_nombre_completo=serializers.ReadOnlyField(source='co_persona.get_nombre_completo')
    tx_nombre_corto=serializers.ReadOnlyField(source='co_persona.get_nombre_corto')
    keystore=publicKey(source='keystore_usuario', read_only=True,many=True)
    class Meta:
        model=Tb001Usuario
        fields=[
            'co_usuario',
            'username',
            'keystore',
            'tx_nombre_completo',
            'tx_nombre_corto',
        ]
        
class SolicitudPagoSerializer(serializers.ModelSerializer):
    destino=usuarioCortoSerializer(source='co_usuario', read_only=True)
    solicitante=usuarioCortoSerializer(source='co_usuario_solicita', read_only=True)
    activo=ActivoSerializer(source='co_activo', read_only=True)
    tx_estatus=serializers.ReadOnlyField(source='co_estatus.tx_estatus')
    precioXLM= serializers.SerializerMethodField()
    class Meta:
        model=WalletTb055SolicitudPago
        fields=[
            'co_solicitud_pago',
            'destino',
            'solicitante',
            'activo',
            'nu_cantidad',
            'precioXLM',
            'tx_hash',
            'descripcion',
            'tx_estatus',
            'in_activo_emisor',
            'in_activo_receptor',
            'in_asset_base',
            'created_at',
            'updated_at',
        ]
        read_only_fields=[
            'co_solicitud_pago',
            'created_at',
            'updated_at',
        ]
        
    def get_precioXLM(self, obj):
        try:
            if obj.co_activo.tx_asset_code!="XLM":
                baseStellar=BaseStellar()
                base={
                    "code":obj.co_activo.tx_asset_code,
                    "issuer":obj.co_activo.tx_asset_issuer
                }
                precio=baseStellar.getReferenciaPath(_asset=base)
                #pprint(precio)
                return float(precio['precio'])
            else:
                return 0
        except Exception as e:
            print(e,'<---lala')
            return 0 