from rest_framework import serializers
from main.models import *
from main.views.utils import getPublicRecortado

class CiudadSerializer(serializers.ModelSerializer):
    class Meta:
        model=MainTb004Ciudad
        
        fields=['co_ciudad','tx_ciudad','in_activo']


class EstadoSerializer(serializers.ModelSerializer):
    
    ciudad = CiudadSerializer(many=True, read_only=True)
    class Meta:
        model=MainTb003Estado
        fields=['co_estado','tx_estado','ciudad']

class PaisSerializer(serializers.ModelSerializer):
    
    class Meta:
        model=MainTb002Pais
        fields=['co_pais','tx_pais','tx_nacionalidad','in_activo']

class PersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model=MainTb005Persona
        fields=['tx_primer_nombre','tx_segundo_nombre','tx_primer_apellido','tx_segundo_apellido']


class CuentaSerializer(serializers.ModelSerializer):
    class Meta:
        model=WalletTb008Cuenta
        fields=['co_cuenta','tx_cuenta','cedul','nombre']

class bancoSerializer(serializers.ModelSerializer):
    class Meta:
        model=WalletTb007Banco

        fields=['co_banco','tx_banco']

class stelSerializer(serializers.ModelSerializer):
        class Meta:
            model=WalletTb004Billetera
            fields=['co_billetera','tx_public_key','co_usuario']

# class limdeperializer(serializers.ModelSerializer):
#         class Meta:
#             model=
#             fields=['co_billetera','tx_public_key','co_usuario']

class RecargaSerializer(serializers.ModelSerializer):
    usuario = serializers.ReadOnlyField(source='co_usuario.username')
    nombre = serializers.ReadOnlyField(source='co_usuario.co_persona.get_nombre_corto')

    moneda=serializers.ReadOnlyField(source='co_banco.co_moneda.tx_simbolo')
    banco=serializers.ReadOnlyField(source='co_banco.tx_banco')

    banco=serializers.ReadOnlyField(source='co_cuenta.co_banco.tx_siglas')
    cuenta=serializers.ReadOnlyField(source='co_cuenta.tx_cuenta')

    class Meta:
        model= WalletTb010Recarga
        fields=['co_recargabs','tx_codigo','nu_monto','usuario','nombre','banco','cuenta']




class MonedaSerializer(serializers.ModelSerializer):
    class Meta:
        model=WalletTb003Moneda
        
        fields=['co_moneda','tx_moneda','tx_simbolo']


class IndentificacionSerializer(serializers.ModelSerializer):
    
    
    class Meta:
        model=MainTb021Indentificacion
        
        fields=['co_indentificacion','img_uno','img_dos','img_selfie']

class UsuarioPersonaSerializer(serializers.ModelSerializer):
    persona = serializers.ReadOnlyField(source='co_persona.co_persona')
    tx_primer_nombre=serializers.ReadOnlyField(source='co_persona.tx_primer_nombre')
    tx_segundo_nombre=serializers.ReadOnlyField(source='co_persona.tx_segundo_nombre')
    tx_primer_apellido=serializers.ReadOnlyField(source='co_persona.tx_primer_apellido')
    tx_segundo_apellido=serializers.ReadOnlyField(source='co_persona.tx_segundo_apellido')
    tx_tipo_documento=serializers.ReadOnlyField(source='co_persona.co_tipo_documento.tx_tipo_documento')
    tx_nombre=serializers.ReadOnlyField(source='co_persona.get_nombre_completo')
    tx_abreviacion=serializers.ReadOnlyField(source='co_persona.co_tipo_documento.tx_abreviacion')
    
    nu_documento=serializers.ReadOnlyField(source='co_persona.nu_documento')
    
    
    tx_sexo=serializers.ReadOnlyField(source='co_persona.co_sexo.tx_sexo')
    nu_telefono=serializers.ReadOnlyField(source='co_persona.nu_telefono')
    fe_nacimiento=serializers.ReadOnlyField(source='co_persona.fe_nacimiento')
    
    
    tx_ciudad=serializers.ReadOnlyField(source='co_persona.co_direccion.co_ciudad.tx_ciudad')
    tx_estado=serializers.ReadOnlyField(source='co_persona.co_direccion.co_ciudad.co_estado.tx_estado')
    tx_pais=serializers.ReadOnlyField(source='co_persona.co_direccion.co_ciudad.co_estado.co_pais.tx_pais')
    tx_codigo_postal=serializers.ReadOnlyField(source='co_persona.co_direccion.tx_codigo_postal')
    tx_direccion=serializers.ReadOnlyField(source='co_persona.co_direccion.tx_direccion')
 
    #cosa de indentificacion
    #identificacion=serializers.ReadOnlyField(source='co_indentificacion.co_indentificacion')
    #img_uno=serializers.ReadOnlyField(source='co_indentificacion.img_uno')
    #img_dos=serializers.ReadOnlyField(source='co_indentificacion.img_dos')
    #img_selfie=serializers.ReadOnlyField(source='co_indentificacion.img_selfie')
    class Meta:
        model=Tb001Usuario
        fields=[
            'co_usuario',
            'username',
            'persona',
            'tx_nombre',
            'tx_primer_nombre',
            'tx_segundo_nombre',
            'tx_primer_apellido',
            'tx_segundo_apellido',
            'tx_tipo_documento',
            'nu_documento',
            'tx_abreviacion',
            'tx_sexo',
            'nu_telefono',
            'fe_nacimiento',
            'tx_direccion',
            'tx_codigo_postal',
            'tx_ciudad',
            'tx_estado',
            'tx_pais',
            'co_indentificacion'

        ]

class DireccionSerializer(serializers.ModelSerializer):
    
    tx_ciudad=serializers.ReadOnlyField(source='co_ciudad.tx_ciudad')
    tx_estado=serializers.ReadOnlyField(source='co_ciudad.co_estado.tx_estado')
    tx_pais=serializers.ReadOnlyField(source='co_ciudad.co_estado.co_pais.tx_pais')

    class Meta:
        model=MainTb008Direccion
        fields=[
            'co_direccion','tx_direccion','tx_codigo_postal','tx_ciudad','tx_estado','tx_pais'
        ]




class bancoRetiroSerializer(serializers.ModelSerializer):
   
    tx_banco=serializers.ReadOnlyField(source='co_banco.tx_banco')
    

    class Meta:
        
        model=MainTb016BancoMoneda
        fields=['tx_banco','co_banco', 'co_banco_moneda']



class cuentaRetiroSerializer(serializers.ModelSerializer):
   
    #tx_cuenta=serializers.ReadOnlyField(source='co_banco_moneda.co_cuenta.tx_cuenta')
    #co_usuario=serializers.ReadOnlyField(source='co_banco_moneda.co_usuario')
    #co_cuenta = serializers.ReadOnlyField(source='co_banco_moneda.co_cuenta')
    

    class Meta:
        
        model=MainTb017Cuenta
        fields=['co_banco_moneda','tx_cuenta', 'co_cuenta']


class tipoDocumentoSerializer(serializers.ModelSerializer):
    class Meta:
        
        model=MainTb006TipoDocumento
        fields=['co_tipo_documento','tx_tipo_documento', 'tx_abreviacion']

class RetirosAprobarSerializer(serializers.ModelSerializer):
    tx_nombre=serializers.ReadOnlyField(source='co_usuario.co_persona.get_nombre_completo')
    co_tipo_documento=serializers.ReadOnlyField(source='co_usuario.co_persona.co_tipo_documento.tx_abreviacion')
    nu_documento=serializers.ReadOnlyField(source='co_usuario.co_persona.nu_documento')
    tx_simbolo=serializers.ReadOnlyField(source='co_transaccion.co_moneda.tx_simbolo')
    tx_banco=serializers.ReadOnlyField(source='co_cuenta.co_banco_moneda.co_banco.tx_banco')
    tx_cuenta=serializers.ReadOnlyField(source='co_cuenta.tx_cuenta')
    nu_monto=serializers.ReadOnlyField(source='co_transaccion.nu_monto')

    class Meta:

        model=WalletTb018Retiro
        fields=[
            'co_retiro', 'tx_nombre', 'co_tipo_documento',
            'nu_documento', 'tx_simbolo', 'tx_banco',
            'tx_cuenta','nu_monto'
        ]

class MensajeStellarSerializer(serializers.ModelSerializer):
    co_tipo=serializers.ReadOnlyField(source='co_tipo_error_stellar.co_tipo_error_stellar')
    tx_tipo=serializers.ReadOnlyField(source='co_tipo_error_stellar.tx_tipo_error_stellar')
    class Meta:
        model=WalletTb038MensajeStellar
        
        fields=['co_mensaje_stellar','tx_codigo','tx_mensaje_original','tx_mensaje_traduccion','co_tipo','tx_tipo']



class DirectorioSerializer(serializers.ModelSerializer):
    #co_tipo=serializers.ReadOnlyField(source='co_tipo_error_stellar.co_tipo_error_stellar')
    #tx_tipo=serializers.ReadOnlyField(source='co_tipo_error_stellar.tx_tipo_error_stellar')
    tx_public=serializers.ReadOnlyField(source='co_keystore.address')
    tx_public_corto=serializers.SerializerMethodField()
    tx_username=serializers.ReadOnlyField(source='co_keystore.co_usuario.username')
    tx_nombre_corto=serializers.ReadOnlyField(source='co_keystore.co_usuario.co_persona.get_nombre_corto')
    tx_nombre_completo=serializers.ReadOnlyField(source='co_keystore.co_usuario.co_persona.get_nombre_completo')
    
    class Meta:
        model=MainTb022Directorio
        
        fields=[
            'co_directorio','tx_apodo','created_at','updated_at',
            'tx_memo','tx_public','tx_public_corto','tx_username','tx_nombre_corto','co_memo',
            'tx_nombre_completo']

        read_only_fields=['co_directorio','created_at','updated_at']

    def get_tx_public_corto(self, obj):
         
        return getPublicRecortado(obj.co_keystore.address)

class KeystoreSerializer(serializers.ModelSerializer):

    class Meta:
        model=WalletTb037Keystore
        
        fields=['co_keystore','address','version','ciphertext','nonce','salt','n','r','p','dklen','encoding']

        read_only_fields=['co_keystore','address','version','ciphertext','nonce','salt','n','r','p','dklen','encoding']


class ComisionSerializer(serializers.ModelSerializer):
    class Meta:
        model=WalletTb014Comision
        
        fields=['co_comision','nu_monto','in_activo','co_tipo_transaccion','is_porcentaje']