from django.apps import AppConfig


class ApipagoConfig(AppConfig):
    name = 'apipago'
