from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required,user_passes_test
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import *
from main.models import *
from main.form import *
from django.utils.crypto import get_random_string
from django.contrib.auth.hashers import make_password,check_password
from django.core.mail import EmailMessage
from django.conf import settings
from stellar_sdk import TransactionBuilder, Network, Keypair, Account,Asset,Operation,AllowTrust
from stellar_sdk.server import Server
import requests
from django.db import IntegrityError, transaction
from apipago.serializers import *
from django.core import serializers
import qrcode
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action,api_view,renderer_classes,api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from django.http import Http404
from django_otp.forms import OTPAuthenticationForm
from django.urls import reverse

from django.views.generic.base import View

from django.utils.module_loading import import_string

from django.contrib.sites.shortcuts import get_current_site


from django.views.decorators.cache import never_cache

import qrcode
import qrcode.image.svg
from base64 import b32encode
from binascii import unhexlify
from django_otp import login as otp_login,match_token


from stellar_sdk.server import Server

from django.template.loader import render_to_string
from apipago.views.servicios import ApiPagoServicios,ApiCulPagoServicios
from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required



@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@user2fa_required
@permission_classes((IsAuthenticated,))
def apiSolicitudController(request):
    content={
        'success':True,
        'data':'',
    }
    
    if 'co_solicitud' in request.POST:

        ruta=MainTb029Ruta.objects.filter(co_solicitud=request.POST['co_solicitud'],in_activo=True)[0]
        if request.user == ruta.co_solicitud.co_usuario:
            formulario=MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None)[0] if MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None).exists() else None
        else:
            formulario=MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=request.user.co_rol.co_rol)[0] if MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=request.user.co_rol.co_rol).exists() else None

        if formulario is not None:
            if formulario.tx_api_funcion is not None:
                return globals()[formulario.tx_api_funcion](request,formulario)
            content['success']=False
            return Response(content)
        else:
            content['success']=False
            return Response(content)
    else:
        content['success']=False
        return Response(content)