from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAuthenticated
from fcm_django.models import FCMDevice

class DeviceViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]
    
    def create(self, request):
        content={
            "success":False
        }
        
        data=request.data
        try:
            if "token" not in data and data["token"]=='':
                return Response(content)
            name=data["device"]["client"]["name"]+'-'+data["device"]["os"]["name"]+'-'+data["device"]["device"]["type"]
            
            if data["device"]["os"]["name"]=="Android":
                tipo="android"
            elif data["device"]["os"]["name"]=="iOS":
                tipo="ios"
            else:
                tipo="web"
            
            
            device,_=FCMDevice.objects.get_or_create(
                name=name,
                type=tipo,
                registration_id=data["token"],
                user_id=request.user.co_usuario
            )
            if device.device_id is None:
                device.device_id=device.id
                device.save()
            content["success"]=True
            
            
        except Exception as e:
            print(e)
        
        
        return Response(content)