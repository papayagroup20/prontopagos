from main.models import MainTb022Directorio,WalletTb037Keystore,MainTb046Memo
from apipago.serializers.general import DirectorioSerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser,IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Prefetch,Q
from django.db import transaction
from stellar_sdk import memo as stellar_memo
from stellar_sdk.server import Server

class DirectorioViewSet(viewsets.ModelViewSet):
    queryset = MainTb022Directorio.objects.\
    select_related(
      'co_keystore','co_keystore__co_usuario',
      'co_keystore__co_usuario__co_persona').all()
    serializer_class = DirectorioSerializer
    permission_classes = [IsAdminUser]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['co_usuario']


    def checkMemo(self,_data:object)->object:
      print(_data)
      if (
          "co_memo" in _data and _data["co_memo"]!="" and 
          "tx_memo" in _data and _data["tx_memo"]!=""
        ):
        try:
            if _data["co_memo"]==1:
              stellar_memo.TextMemo(_data["tx_memo"])
            elif _data["co_memo"]==2:
              stellar_memo.IdMemo(int(_data["tx_memo"]))
            elif _data["co_memo"]==3:
              stellar_memo.HashMemo(_data["tx_memo"])
            elif _data["co_memo"]==4:
              stellar_memo.ReturnHashMemo(_data["tx_memo"])
            else:
              stellar_memo.TextMemo(_data["tx_memo"])
            return {"success":True,"is_memo":True}
        except Exception as e:
            print(e)
            return {"success":False,"is_memo":True,"error":"El tipo de memo es incorrecto"}

      else:
        return {"success":True,"is_memo":False}


    @action(detail=False, methods=['GET'],url_path='propio', url_name='propio',permission_classes=(IsAuthenticated, ))
    def contactos(self, request)->Response:
      midirectorio=self.queryset.filter(co_usuario=request.user.co_usuario)
      serializer=DirectorioSerializer(midirectorio,many=True)
      return Response(serializer.data)
    
    @contactos.mapping.post
    def newContacto(self, request)->Response:
      datos=request.data
      if "tx_identidad" not in datos or "tx_apodo" not in datos or datos["tx_identidad"]=="" or datos["tx_apodo"]=="":
        return Response({"success":False,"error":"No se suministraron los datos necesarios"})

      #billetera
      keystore=WalletTb037Keystore.objects\
        .select_related("co_usuario")\
        .prefetch_related(
            Prefetch("billetera"),
        )\
        .filter(Q(co_usuario__username=datos["tx_identidad"])|Q(address=datos["tx_identidad"]))\
        .first()

        
      if keystore is not None:
        print(keystore)
        if keystore.billetera.exists():
          for item in keystore.billetera.all():
            if item.co_usuario==request.user:
              return Response({"success":False,"error":"Este usuario ya lo tienes registrado"})
        if keystore.co_usuario==request.user:
          return Response({"success":False,"error":"No te puedes agregarte a ti mismo"})
        
        try:
          with transaction.atomic():
            newdirectorio=MainTb022Directorio(
              tx_apodo=datos["tx_apodo"],
              co_usuario=request.user,
              co_keystore=keystore,
            )
            check_memo=self.checkMemo(datos)
            if check_memo["success"] and check_memo["is_memo"]:
                newdirectorio.tx_memo=datos["tx_memo"]
                m=MainTb046Memo.objects.get(co_memo=datos["co_memo"])
                if m is not None:
                  newdirectorio.co_memo=m
                else:
                  newdirectorio.co_memo=MainTb046Memo.objects.get(co_memo=1)

            elif not check_memo["success"]:
              return Response(check_memo)
            
            newdirectorio.save()

        except Exception as e:
          print(e)
          return Response({"success":False,"error":"Ha ocurrido un error vuelve a intentarlo"})

      else:
        server=Server(horizon_url="https://horizon.stellar.org")
        try:
          with transaction.atomic():
            #prueba GAJ7NKB4KSRPAQJ6WKWSTVZRPMFRPHDHQX734IICR2XJUCU33EL5W5AZ
            source_account = server.load_account(datos["tx_identidad"])
            newkeystore=WalletTb037Keystore(
              address=datos["tx_identidad"],
              version="gobelx-10-09-2020"
            )
            newkeystore.save()

            newdirectorio=MainTb022Directorio(
              tx_apodo=datos["tx_apodo"],
              co_usuario=request.user,
              co_keystore=newkeystore,
            )

            check_memo=self.checkMemo(datos)
            if check_memo["success"] and check_memo["is_memo"]:
                newdirectorio.tx_memo=datos["tx_memo"]
                m=MainTb046Memo.objects.get(co_memo=datos["co_memo"])
                if m is not None:
                  newdirectorio.co_memo=m
                else:
                  newdirectorio.co_memo=MainTb046Memo.objects.get(co_memo=1)        
            elif not check_memo["success"]:
                return Response(check_memo)

            newdirectorio.save()
        except Exception as e:
          print(e)
          return Response({"success":False,"error":"El usuario no existe o llave publica invalido"})

      
      return Response({"success":True})



    @action(detail=True, methods=['PATCH'],url_path='propio', url_name='propio',permission_classes=(IsAuthenticated, ))
    def contactoDetalle(self,request,pk=None)->Response:
      midirectorio=self.queryset.filter(co_usuario=request.user.co_usuario)
      directorio=midirectorio.get(co_directorio=pk)
      if directorio.tx_apodo==request.data["tx_apodo"]:
        return Response({"success":False,"error":"El apodo es identico al que tiene actualmente"})

      for item in midirectorio:
        if item.tx_apodo==request.data["tx_apodo"]:
          return Response({"success":False,"error":"Este apodo ya lo posee otro contacto"})
      
      try:
        with transaction.atomic():
          directorio.tx_apodo=request.data["tx_apodo"]
          check_memo=self.checkMemo(request.data)
          print(check_memo)
          if check_memo["success"] and check_memo["is_memo"]:
              print("aqui")
              directorio.tx_memo=request.data["tx_memo"]
              m=MainTb046Memo.objects.get(co_memo=request.data["co_memo"])
              if m is not None:
                tp_memo=m
              else:
                tp_memo=MainTb046Memo.objects.get(co_memo=1)                
              directorio.co_memo=tp_memo
          elif not check_memo["success"]:
              return Response(check_memo)
         
          directorio.save()
      except Exception as e:
          print(e)
          return Response({"success":False,"error":"Ha ocurrido un error, vuelve a intentar"})


      return Response({"success":True})
    
    @contactoDetalle.mapping.delete
    def deleteContacto(self, request,pk=None)->Response:
      cadaver=self.queryset.get(co_directorio=pk)
      try:
        with transaction.atomic():
          cadaver.delete()
      except Exception as e:
          print(e)
          return Response({"success":False,"error":"Ha ocurrido un error, vuelve a intentar"})

      return Response({"success":True})





