from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action,api_view,renderer_classes,api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from base64 import b32encode,b32decode
from binascii import unhexlify
from django_otp.plugins.otp_totp.models import TOTPDevice
from django_otp.plugins.otp_static.models import StaticDevice,StaticToken
from django_otp.oath import TOTP
from django_otp import devices_for_user
import pyotp
import secrets
from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required
from django.conf import settings

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def getTemplateLink(request):
    content={
        'success':True,
        'data':'',
    }
    rendered = render_to_string('../templates/usuario/seguridad/google2fa/paso1-link.html')
    content['data']=rendered

    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def getTemplatePaso2(request):
    content={
        'success':True,
        'data':'',
    }
    olddevice=TOTPDevice.objects.filter(user_id=request.user.co_usuario)
    olddevice.delete()

    device=TOTPDevice(
          user_id=request.user.co_usuario,
          name="client-device",
          confirmed=False
     )
    device.save()

    b32key = b32encode(bytes(device.key,"ascii")).decode("ascii")
    link=pyotp.totp.TOTP(b32key).provisioning_uri(name=request.user.email, issuer_name=settings.OTP_TOTP_ISSUER)
    content2={
        'img':link
    }

    rendered = render_to_string('../templates/usuario/seguridad/google2fa/paso2-qr.html',content2)
    content['data']=rendered

    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def getTemplatePaso3(request):
    content={
        'success':True,
        'data':'',
    }
    if StaticDevice.objects.filter(user_id=request.user.co_usuario).exists():
        estaticoDevice=StaticDevice.objects.filter(user_id=request.user.co_usuario)[0]
    else:
        estaticoDevice=StaticDevice(
            name="client-static-device",
            user_id=request.user.co_usuario,
            confirmed=False
        )
        estaticoDevice.save()
    
    oldestaticoToken=StaticToken.objects.filter(device_id=estaticoDevice.id)
    oldestaticoToken.delete()
    
    token=pyotp.random_base32()
    estaticoToken=estaticoDevice.token_set.create(
          token=token
     )


    content2={
        'token':token
    }
    content['pass']=True
    rendered = render_to_string('../templates/usuario/seguridad/google2fa/paso3-backup.html',content2)
    content['data']=rendered

    return Response(content)
