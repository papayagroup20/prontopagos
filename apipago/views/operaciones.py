from main.models import *
from django.http import JsonResponse
from django.core import serializers
from django.template.loader import render_to_string
from rest_framework.response import Response
from django.http import HttpResponse
from apipago.serializers import *
from rest_framework.decorators import action,api_view,renderer_classes,permission_classes
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from rest_framework.renderers import JSONRenderer,TemplateHTMLRenderer
from rest_framework import viewsets, status
from django.core.mail import EmailMessage

from stellar_sdk.call_builder.call_builder_sync import OrderbookCallBuilder
from stellar_sdk.asset import Asset
from stellar_sdk.server import Server
import requests

from datetime import datetime, timedelta
import pytz
from django.utils import timezone
import calendar;
import time;
from django.db import IntegrityError, transaction
from main.views.utils import getContentBasicoL,getContentBasicoL2
from wallet.form import SolicitudUSDVForm
from decimal import getcontext, Decimal, ROUND_HALF_UP

from wallet.form import SolicitudCompraLumensForm

from django.views.decorators.csrf import csrf_exempt

import PIL.Image as Image
from django.core.files.storage import default_storage as storage
import io
from io import BytesIO
import base64
from rest_framework import renderers

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def getTransaccion(request):
    content={
        'success':True,
        'data':''
    }
    if request.POST['co_solicitud']!='':
        queryset =WalletTb015Transaccion.objects.filter(co_emisor=request.user.co_usuario,co_solicitud=request.POST['co_solicitud'])
        serializer = TransaccionSerializer(queryset , many=True)
        content['data']=serializer.data
    else:
        content['success']=False

    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def getTransaccion2(request):
    content={
        'success':True,
        'data':''
    }
    if request.POST['co_solicitud']!='':
        queryset =WalletTb015Transaccion.objects.filter(co_solicitud=request.POST['co_solicitud'])
        serializer = TransaccionSerializer(queryset , many=True)
        content['data']=serializer.data
    else:
        content['success']=False

    return Response(content)

def getOferta():#obtiene oferta 1 Lumens en USD
    content={
        'success':True,
        'data':''
    }
    ts = calendar.timegm(time.gmtime())
    precio=requests.get("https://api.blockchain.info/price/index?api_code=1770d5d9-bcea-4d28-ad21-6cbd5be018a8&base=XLM&quote=USD&time="+str(ts))
    ofertablock=precio.json()
    
    #server=Server("https://horizon.stellar.org/")
    #selasset=Asset.native()
    #buyasset=Asset("USD","GDUKMGUGDZQK6YHYA5Z6AY2G4XDSZPSZ3SW5UN3ARVMO6QSRDWP5YLEX")
    #orderbook=server.orderbook(selasset,buyasset)
    
    #ofertas=orderbook.call()
    #price_mayor=0
    #for val in ofertas['bids']:
    #    if Decimal(val['price']) > Decimal(price_mayor):
    #        price_mayor = val['price']

    #content['data']=price_mayor
    content['data']=ofertablock['price']

    return content

def getOfertaBSS():#obtiene oferta 1 dolares en bolivares
    content={
        'success':True,
        'data':''
    }
    fecha_hoy = timezone.now()
    precioppg=WalletTb039PrecioPPG.objects.filter(
        in_activo=True
        )[0]
    dif_hora=(fecha_hoy - precioppg.created_at)
    days, seconds = dif_hora.days, dif_hora.seconds
    hours = days * 24 + seconds // 3600

    if dif_hora.days>=1:
        oferta= requests.get('https://s3.amazonaws.com/dolartoday/data.json')
        list_ofertas=oferta.json()
        dolartoday_USD= list_ofertas['USD']['dolartoday']
        content['data']=dolartoday_USD*1.05
    
    else:
        ofertappg= str(precioppg)
        content['data']=Decimal(ofertappg)

    #oferta= requests.get('https://s3.amazonaws.com/dolartoday/data.json')
    #list_ofertas=oferta.json()
    #dolartoday_USD= list_ofertas['USD']['dolartoday']
    #content['data']=dolartoday_USD

    return content

def getOferta_XLM_BSS():
    content={
        'data':''
    }

    oferta=getOferta()#valor 1 Lumens en USD
    ofertaBSS=getOfertaBSS()#valor 1 USD en Bolivares por dolartoday
    subtotal=Decimal(oferta['data'])*Decimal(ofertaBSS['data'])
    total=Decimal(subtotal.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))

    content['data']=total
    #print(oferta)
    #print(ofertaBSS)
    #print(subtotal)
    #print(total)
    
    return content

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def resultLumens(request):#Transforma de Bolivares a Lumens
    content={
        'success':True,
        'data':'',
        'msg':''
    }
    limitemax=WalletTb040Limite.objects.latest('created_at')
    num_max=str(limitemax)
    cantidad=request.POST['cantidad']#monto de entrada en bolivares
    if cantidad.isalpha() == False:
        
        oferta= getOferta()#valor 1 Lumens en USD
        ofertaBSS= getOfertaBSS()#valor 1 USD en Bolivares por dolartoday
        ofertaTotal= Decimal(oferta['data'])*Decimal(ofertaBSS['data'])#monto resultado en lumens
        subtotal=Decimal(request.POST['cantidad'])/Decimal(ofertaTotal)#monto a recibir en lumens con la oferta

        if float(subtotal) <= float(num_max) and float(subtotal) >= 1:        
            total=Decimal(subtotal.quantize(Decimal('.000000001'), rounding=ROUND_HALF_UP))
            content['data']=Decimal(total)

        else:
            total=0
            content['success']=False
            content['data']=Decimal(total)

            if float(subtotal) <= 1:
                content['msg']="El limite mínimo es de 1 lumens"
                
            elif float(subtotal) >= float(num_max):
                content['msg']="El limite máximo es de "+num_max+" lumens"

    else:
        total=0
        content['success']=False
        content['msg']="Solo puede usar numeros"
        content['data']=Decimal(total)

    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def resultBSS(request):#Transforma de Lumens a Bolivares
    content={
        'success':True,
        'data':''
    }
    
    limitemax=WalletTb040Limite.objects.latest('created_at')
    num_max=str(limitemax)
    #fee=getComisionCompraLumens()
    cantidad=request.POST['cantidad']#monto de entrada en lumens
    if cantidad.isalpha() == False:
        if cantidad == '':
            total=0 
            content['data']=Decimal(total)
        
        elif float(cantidad) <= float(num_max) and float(cantidad) >= 1:
            oferta= getOferta()#valor 1 Lumens en USD
            ofertaBSS= getOfertaBSS()#valor 1 USD en Bolivares por dolartoday
            ofertaTotal= Decimal(oferta['data'])*Decimal(ofertaBSS['data'])#monto resultado en Bolivares
            subtotal=Decimal(ofertaTotal)*Decimal(request.POST['cantidad'])
            #subtotal2=Decimal(ofertaTotal)*Decimal(request.POST['cantidad'])
            total=Decimal(subtotal.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))
            content['data']=Decimal(total)
            #content['fee']=fee['data']

        else:
            total=0
            content['data']=Decimal(total)
            content['success']=False

            if float(cantidad) <= 1:
                content['msg']="El limite mínimo es de 1 lumens"
                
            elif float(cantidad) >= float(num_max):
                content['msg']="El limite máximo es de "+num_max+" lumens"

    else:
        total=0
        content['success']=False
        content['msg']="Solo puede usar numeros"

    return Response(content)

def getComisionCompraLumens():
    content={
        'data':''
    }
    feeDB=WalletTb014Comision.objects.filter(co_tipo_transaccion=15, in_activo=True)[0]
    fee=feeDB.nu_monto
    ofertCambio=getOfertaBSS()

    feeTotal=Decimal(fee)*Decimal(ofertCambio['data'])
    total=Decimal(feeTotal.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))

    content['data']=total
    return content

def fechaGet(date):
    months = ("Enero", "Febrero", "Marzo", "Abri", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
    day = date.day
    month = months[date.month - 1]
    year = date.year
    messsage = "{} de {} de {}".format(day, month, year)

    return messsage

@csrf_exempt
def datos_email(solicitud,nota,estatus):
    content={
         'success':True,
         'msg':[],
         'data':'',
    }
 
    data={
    "fecha":"",
    "username":"",
    "monto":"",
    "code":"",
    "tipo_t":"",
    "nota":"",
    "estado":"",
    }
    
    usuario=solicitud.co_transaccion.co_emisor
    if solicitud:
        fe = solicitud.created_at
        fecha = fechaGet(fe)
        data["fecha"]= fecha
        data["username"] = usuario
        data["monto"]=solicitud.co_transaccion.nu_monto
        data["code"]=solicitud.co_transaccion
        data["tipo_t"]=solicitud.co_solicitud.co_tipo_solicitud
        if nota:
            data["nota"]=nota
        data["estado"]=estatus

        if estatus == 'rechazada':
            solicitud.co_transaccion.co_solicitud.tx_motivo_rechazo= nota

        notificacion_email(usuario,data,'notificaciones/email_notif.html')

        data["estado"]='compra de lumens'
        notificacion_email_PP(usuario,data,'notificaciones/email_notif_prontopagos.html')

        content['success']=True
    
    else:
        content['success']=False
        content['error']="Error en la solicitud"

    return JsonResponse(content)


def notificacion_email(user, detalles,template):

     texto="../templates/usuario/"+template
     body = render_to_string(texto,
          {'codigo':detalles}
     )

     email=EmailMessage(
          subject='Notificación de ProntoPagos',
          body=body,
          from_email=settings.EMAIL_HOST_USER,
          to=[user.email]
     )
     email.content_subtype='html'
     email.send()

def notificacion_email_PP(user, detalles,template):

     texto="../templates/usuario/"+template
     body = render_to_string(texto,
          {'codigo':detalles}
     )

     email=EmailMessage(
          subject='Notificación de ProntoPagos',
          body=body,
          from_email=settings.EMAIL_HOST_USER,
          to=['info@prontopagos.com','info@gobelx.com']
     )
     email.content_subtype='html'
     email.send()

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def total_usdv(request):
    content={
        'success':True,
        'data_monto':'',
        'data_fee':''
    }

    montouser=request.POST['nu_monto_usd']
    if montouser == '':
        montoUSD=0
    else:
        montoUSD=request.POST['nu_monto_usd']
    fee=WalletTb014Comision.objects.filter(co_tipo_transaccion=16, in_activo=True)[0]
    feeMonto=fee.nu_monto

    if feeMonto == 0:
        subtotalMonto=Decimal(montoUSD)
        subfeeMonto=Decimal(feeMonto)
    else:
        
        subtotalMonto=Decimal(montoUSD)+(Decimal(montoUSD)*Decimal(feeMonto))
        subfeeMonto=subtotalMonto-Decimal(montoUSD)

    totalfee=Decimal(subfeeMonto.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))
    content['data_fee']=Decimal(totalfee)

    totalMonto=Decimal(subtotalMonto.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))
    content['data_monto']=Decimal(totalMonto)
    
    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer,TemplateHTMLRenderer])
def paypal_solicitud(request):
    content={
        "success":True
    }
    print(request.POST)
    #print(request.POST['cantidad'])
    #print(request.POST['monto_usd'])
    #print(request.POST['nombre'])
    #print(request.POST['apellido'])
    #print(request.POST['correo'])
    #print(request.POST['estatus'])
    #print(request.POST['co_moneda'])
    #print(request.POST['correo_remitente'])
    print(request.POST['img_paypal'])

    if request.method=='POST':
        print('entro al post')
        if request.POST['estatus']=='COMPLETED':
            print('funciono el completado')
        #form=SolicitudUSDVForm(request.POST)
        #if form.is_valid():
            try:
                with transaction.atomic():
                    print('si atomic')
                    solicitud=MainTb028Solicitud.objects.newSolicitud(28,request.user)
                    solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                    solicitud.in_finanza=True
                    solicitud.save()

                    #fechahoy=request.POST['fecha']
                    #fecha= datetime.strptime(fechahoy, '%Y-%m-%d').date()
                    fecha= datetime.now()

                    transaccion=WalletTb015Transaccion(
                    nu_monto=request.POST['monto_usd'],#monto en usd
                    nu_monto2=request.POST['cantidad'],#monto en usdv
                    co_emisor=request.user,
                    co_estatus=WalletTb009Estatus.objects.get(co_estatus=2),
                    co_moneda=WalletTb003Moneda.objects.get(co_moneda=4),
                    co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=6),
                    fe_transaccion=fecha,
                    co_comision=WalletTb014Comision.objects.get(co_comision=16),
                    co_solicitud=solicitud,
                    )
                    transaccion.save()

                    paypal=WalletTb047PaypalOperacion(
                        tx_nombres=request.POST['nombre'],#nombre
                        tx_apellidos=request.POST['apellido'],#apellidos
                        tx_correo=request.POST['correo'],#correo del usuario que pago
                        tx_estatus=request.POST['estatus'],#estado de al transapcion(COMPLETED)
                        img_paypal = request.cleaned_data['img_uno_file'],
                        in_activo=True,
                        co_solicitud=solicitud,
                        co_transaccion=transaccion
                    )
                    paypal.save()

                    referencia=WalletTb010Recarga(
                    #tx_codigo=request.POST['co_referencia'],
                    #co_banco=WalletTb007Banco.objects.get(co_banco=request.POST['co_banco']),
                    co_transaccion=transaccion,
                    co_solicitud=solicitud
                    )
                    referencia.save()
                
                    ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                    ruta.in_activo=False
                    ruta.save()  
            except IntegrityError:
                content['success']=False
                content['msg']="Ha habido un error en la operación"
        else:
            content['success']=False
            content['msg']="La Transapcion tubo error en el pago"
    return Response(content)

class CustomRenderer(renderers.BaseRenderer):
    media_type = 'image/png'
    format = 'png'
    charset = None
    render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        return data

@api_view(['POST','GET'])
@renderer_classes ((CustomRenderer,))
def imagen_prontoamigo(request):
    base=settings.STATIC_ROOT
    fondo=base+"img/ProntoamiGO.png"
    #base1='C:/Users/struv/Documents/desarrollo_papaya/prontopago/static/img/ProntoamiGO.png'
    #print(base1)
    bufer=io.BytesIO()
    fp=fondo
    img= Image.open(fp, mode='r')
    print(img.format, img.size, img.mode)
    #img=Image.frombytes('RGBA', (1098,750) ,'PNG')
    print(img)
    
    #img.save(fp)
    print(img)
    #print(img)
    #buffered = buffered.getvalue()
    return HttpResponse(img,content_type="image/png")