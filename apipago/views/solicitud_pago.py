from main.models import WalletTb055SolicitudPago,WalletTb009Estatus,MainTb022Directorio,WalletTb036Activos
from apipago.serializers.operacion import SolicitudPagoSerializer
from rest_framework import viewsets
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Prefetch,Q
from django.db import  transaction
from firebase_admin.messaging import (
    Message,Notification,WebpushConfig,WebpushFCMOptions,
    WebpushNotification)
from fcm_django.models import FCMDevice
from  pprint import pprint

class SolicitudPagoViewSet(viewsets.ModelViewSet):
    queryset = WalletTb055SolicitudPago.objects\
        .select_related(
            'co_usuario','co_usuario_solicita',
            'co_usuario__co_persona','co_usuario_solicita__co_persona',
            'co_estatus','co_activo','co_activo__co_stellar_toml',
            )\
        .prefetch_related(
            Prefetch('co_usuario__keystore_usuario'),
            Prefetch('co_usuario_solicita__keystore_usuario')
            )\
        .order_by('-created_at')\
        .all()
    serializer_class = SolicitudPagoSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['created_at']
    
    
    """def list(self, request, pk=None):
        raise Http404"""
        
    
    
    def list(self, request, *args, **kwargs)->Response:
        queryset = self.filter_queryset(self.get_queryset())
        queryset=queryset.filter(Q(co_estatus=1),Q(co_usuario=request.user.co_usuario) | Q(co_usuario_solicita=request.user.co_usuario))
        #print(request.user)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
    
    def create(self, request)->Response:
        #usuario=request.user
        data=request.data
        content={
            'success':True,
            'msg':'',
            'error':'',
            'errorForm':{}
        }
        #---------------VALIDACIONES----------
        #nota, se hacen aqui ya que son casi todos son select y cosas raras, y es mucho
        #trauma hacer una clase form o usar serializer para comprobarlo.
        
        #----------VALIDAR CONTACTO
        if 'contactoSelect' not in data:
            content['success']=False
            content['errorForm']={'contactoSelect':['Debe seleccionar un contacto']}

            
        directorio=MainTb022Directorio.objects\
            .select_related('co_keystore','co_keystore__co_usuario')\
            .get(co_directorio=data['contactoSelect'])
        
        if directorio is None:
            content['success']=False
            content['errorForm']={'contactoSelect':['El contacto no existe']}
        
        #--------VALIDAR ACTIVO
        if 'assetSelect' not in data:
            content['success']=False
            content['errorForm']={'assetSelect':['Debe seleccionar un activo']}
        
        activo=WalletTb036Activos.objects
        if data['assetSelect'] is not None:
            if data['assetSelect']['asset_type']=='native':
                activo=activo.get(co_activo=7)
            else:
                activo=activo.get(tx_asset_code=data['assetSelect']['asset']['code'],tx_asset_issuer=data['assetSelect']['asset']['issuer'])
        
        if activo is None:
            content['success']=False
            content['errorForm']={'assetSelect':['El activo no existe']}
        
        
        #--------VALIDAR CANTIDAD
        if 'nu_monto' not in data:
            content['success']=False
            content['errorForm']={'assetSelect':['Debe ingresar alguna cantidad']}
        try:
            if float(data['nu_monto'])<=0:
                content['success']=False
                content['errorForm']={'assetSelect':['El monto debe ser mayor a 0']}
        except Exception as e:
            content['success']=False
            content['errorForm']={'assetSelect':['El monto debe ser un numero']}
            print(e)
        
        description=data['tx_descripcion'] if 'tx_descripcion' in data else '' 
        #-------AHORA SI NUEVA SOLICITUD
        try:
            with transaction.atomic():
                solicitudPago=WalletTb055SolicitudPago(
                    co_usuario_solicita=request.user,
                    co_usuario=directorio.co_keystore.co_usuario,
                    co_activo=activo,
                    nu_cantidad=data['nu_monto'],
                    descripcion=description,
                    in_asset_base=True if 'in_asset_base' in data and data['in_asset_base']==True else False
                )
                
                solicitudPago.save()
                content['msg']='La solicitud de pago ha sido enviada sactifactoriamente.'
                self.notificar(solicitudPago)
                
        except Exception as e:
            content['success']=False
            content['error']='Ha ocurrido un error'
            print(e)
        
        return Response(content)

    def update(self, request, pk=None, project_pk=None)->Response:
        solicitud=self.queryset.get(co_solicitud_pago=pk,co_usuario=request.user.co_usuario)
        data=request.data
        content={
            'success':True,
            'msg':'',
            'error':'',
            'errorForm':{}
        }
        
        if solicitud is None:
            content['success']=False
            content['error']='El codigo de la solicitud no ha sido suministrado, o no existe.'
            return Response(content)
        
        if 'tx_hash' not in data:
            content['success']=False
            content['error']='El hash no ha sido suministrado.'
            return Response(content)
            
        try:
            with transaction.atomic():
                solicitud.in_activo_receptor=False
                solicitud.tx_hash=data['tx_hash']
                solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=2)
                solicitud.save()
                
        except Exception as e:
            print(e)
            content['success']=False
            content['error']='Ha ocurrido un error, vuelve a intentar'
        
        
        return Response(content)

    def destroy(self, request, pk=None)->Response:
        cadaver=self.queryset.get(Q(co_solicitud_pago=pk),Q(co_usuario=request.user.co_usuario) | Q(co_usuario_solicita=request.user.co_usuario))
        if cadaver is None:
            return Response({"success":False,"error":"Usuario o codigo incorrecto"})
        try:
            with transaction.atomic():
                if request.user==cadaver.co_usuario_solicita:
                    cadaver.in_activo_emisor=False
                else:
                    cadaver.in_activo_receptor=False
                cadaver.co_estatus=WalletTb009Estatus.objects.get(co_estatus=3)
                cadaver.save()
        except Exception as e:
            print(e)
            return Response({"success":False,"error":"Ha ocurrido un error, vuelve a intentar"})

            
        return Response({'success':True})
    
    
    
    
    def notificar(self,solicitud:WalletTb055SolicitudPago)->None:
        try:
            webpush_op=WebpushFCMOptions(link="https://pay.prontopagos.io")
            data={
                "tipo_ppg":"1",
                "usuario":solicitud.co_usuario_solicita.username,
                "monto":solicitud.nu_cantidad,
                "moneda":"$" if not solicitud.in_asset_base else solicitud.co_activo.tx_asset_code
            }
            webpush_notification=WebpushNotification(
                icon="http://localhost/static/logoppg6.png",
                title="Solicitud de pago",
                body=solicitud.co_usuario_solicita.username+" te ha solicitado un pago por "+solicitud.nu_cantidad+" "+data["moneda"]
                )
            webpush=WebpushConfig(data=data,notification=webpush_notification,fcm_options=webpush_op)
            msg=Message(webpush=webpush)
            
            
            
            device = FCMDevice.objects.filter(user_id=solicitud.co_usuario.co_usuario)
            device.send_message(message=msg)
        except Exception as e:
            print(e)
    