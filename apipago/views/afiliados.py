from rest_framework.authentication import (
    TokenAuthentication,SessionAuthentication,BasicAuthentication)
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from rest_framework.views import APIView
from main.models import (
    WalletTb048TasaAfiliado,WalletTb050ComisionAfiliado,WalletTb014Comision,
    WalletTb009Estatus,WalletTb013TipoTransaccion,WalletTb037Keystore,
    WalletTb036Activos,MainTb045Referidos,WalletTb053IntegranteComision,
    Evento01Tb001RecompensaAfiliado,WalletTb037Keystore,Tb001Usuario)
from rest_framework.response import Response
from django.db import IntegrityError, transaction
from decimal import Decimal
from django.db.models import QuerySet,Prefetch,Q
import json
from stellar_sdk import TransactionBuilder, Network, Keypair, Account
from stellar_sdk.transaction_envelope import TransactionEnvelope
from stellar_sdk.call_builder.call_builder_sync import OperationsCallBuilder
from stellar_sdk.server import Server
from pprint import pprint

class AfiliacionApiView(APIView):
    authentication_classes = [
        TokenAuthentication,BasicAuthentication,
        SessionAuthentication]
    permission_classes = [IsAuthenticated]




    def post(self,request,format=None):
        usuario=request.user
        paso=2 if request.data["tipo"]=='1' else 1
        
        
        referido_emisor=MainTb045Referidos.objects\
            .filter(Q(nu_telegram=usuario.nu_telegram))

        if not referido_emisor.exists():
            paso=paso-1
            #return Response({'success':False,'msg':'el emisor no es referido ni referenciado'})

         
        #MainTb045Referidos
        elreceptor=None
        if request.data["tipo"]=='1':

            receptor=WalletTb037Keystore.objects\
                .select_related('co_usuario')\
                .prefetch_related(
                    Prefetch(
                        "co_usuario__usuario_afiliador",
                        queryset=MainTb045Referidos.objects.filter(in_validado=True))
                )\
                .filter(address=request.data["destino"])


            if not receptor.exists():
                return Response({'success':False,'msg':'el receptor no es usuario ppg'})


            receptor=receptor.first()

            if  receptor.co_usuario is None:
                return Response({'success':False,'msg':'el receptor no es usuario ppg'})

            else: 
                elreceptor=receptor.co_usuario
                if not receptor.co_usuario.usuario_afiliador.exists():
                    paso=paso-1
                    #return Response({'success':False,'msg':'el receptor no es un afiliador'})

                else:
                    referido_receptor=MainTb045Referidos.objects\
                        .filter(nu_telegram=receptor.co_usuario.nu_telegram)
                    if not referido_receptor.exists():
                        paso=paso-1
                

        if paso<=0:
            return Response({'success':False,'msg':'el receptor y el emisor no son afiliados o afiliadores'})
            
        if usuario.co_tasa_afiliados is None:
            usuario.co_tasa_afiliados=WalletTb048TasaAfiliado.objects.get(co_tasa_afiliados=1)
            usuario.save()

        monto=Decimal(usuario.co_tasa_afiliados.nu_tasa)*Decimal(request.data["nu_monto"])

        try:
            with transaction.atomic():
                transaccion=WalletTb050ComisionAfiliado(
                    tx_hash=request.data["tx_codigo"],
                    co_comision=WalletTb014Comision.objects.get(co_comision=request.data["co_comision"]),
                    co_tasa_afiliados=usuario.co_tasa_afiliados,
                    nu_monto=monto,
                    co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=request.data["tipo"]),
                  
                )

                transaccion.save()
                
                integrante1=WalletTb053IntegranteComision(
                    co_comision_afiliado=transaccion,
                    co_referido=usuario
                )
                integrante1.save()

                if elreceptor is not None:
                    integrante2=WalletTb053IntegranteComision(
                        co_comision_afiliado=transaccion,
                        co_referido=elreceptor,
                        in_emisor=False
                    )
                    integrante2.save()

        except IntegrityError as e:
            print(e)
            return Response({'success':False})
        return Response({'success':True})

class AfiliacionRecompensaApiView(APIView):
    authentication_classes = [
        TokenAuthentication,BasicAuthentication,
        SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self,request,format=None):

        #------------------------PARTE 1------------------
        usuario=request.user
        usuario=Tb001Usuario.objects\
            .prefetch_related(
                Prefetch(
                    'usuario_afiliador',
                    queryset=MainTb045Referidos.objects.select_related('raref_referido')),
                Prefetch('keystore_usuario'))\
            .get(co_usuario=request.user.co_usuario)

        if not usuario.usuario_afiliador.exists():
            return Response({'success':False,'msg':'el destinatario no es afiliador'})


        #Evento01Tb001RecompensaAfiliado
        
         #------------------------PARTE 2------------------
        server2 = Server(horizon_url="https://horizon-testnet.stellar.org")
        server = Server(horizon_url="https://horizon.stellar.org")
        cuenta=server2.accounts()\
            .account_id("GAX4EJBXK7GQVVCV26UKPPYW7MJ3KQZZ7GXQAZUJR7YJABUTSV5D4QKD")\
            .call()

        #comprobar si hay balance en la cuenta evento
        for balance in cuenta["balances"]:
            if balance["asset_type"]=="native" and float(balance["balance"])<=2.6:
                return Response({'success':False,'msg':'Evento ha culminado'})



        
        #------------------------PARTE 3------------------


        receptor=WalletTb037Keystore.objects\
                .select_related('co_usuario')\
                .filter(address=request.data["destino"])

        if not receptor.exists():
                return Response({'success':False,'msg':'el receptor no es usuario ppg'})

        receptor=receptor.first()
        if  receptor.co_usuario is None:
            return Response({'success':False,'msg':'el receptor no es usuario ppg'})
        
        pase=False
        referido=None
        
        for value in usuario.usuario_afiliador.all():
            if value.nu_telegram==receptor.co_usuario.nu_telegram:
                pase=True
                referido=value

                if Evento01Tb001RecompensaAfiliado.objects.filter(co_referido=referido.co_referidos).exists():
                    return Response({'success':False,'msg':'ya se le dio recompensa'})

        if not pase:
            return Response({'success':False,'msg':'no hay conexion entre ambos o ya hay un pago registrado'})

        #print(keystore)

        #------------------------PARTE 4------------------
        ope=server.operations().for_transaction(request.data["hash"]).call()
        transa=server.transactions().transaction(request.data["hash"]).call()
        keystore=usuario.keystore_usuario.first()
        if transa['successful'] == True:

            response=None
            #pprint(ope['_embedded']['records'][0])
            if ope['_embedded']['records'][0]['asset_type']!="native":
                laope=ope['_embedded']['records'][0]
                if laope['asset_code']=="BSS" and laope['asset_issuer']=="GA6E6YDYXXAXG4VZAOGIX3N3HETLMGVBLQKAAIZQ3PIMQGHAQ63RA6EO" \
                    and float(laope['amount'])>=0.1:
                        try:
                            root_keypair = Keypair.from_secret("SDQKOT4QWJRAKGIZUCAXTH5UQ3FGHEEVCHTWQGEUM55TWOXGBGY2INTY")
                            source_account = server2.load_account(root_keypair.public_key)

                
                            base_fee = server2.fetch_base_fee()
                            transactions = (
                                TransactionBuilder(
                                source_account=source_account,
                                 # If you want to submit to pubnet, you need to change `network_passphrase` to `Network.PUBLIC_NETWORK_PASSPHRASE`
                                network_passphrase=Network.TESTNET_NETWORK_PASSPHRASE,
                                base_fee=base_fee,
                                )
                                .add_text_memo("Recompensa de ppg")
                                .append_payment_op(
                                    keystore.address,
                                    "1.6",
                                    "XLM"
                                )
                                .set_timeout(300) 
                                .build()
                            )

                            transactions.sign(root_keypair)
                            response = server2.submit_transaction(transactions)
            
                        except Exception as e:
                            print(e) 
                            return Response({'success':False,'msg':'Error en la transaccion'})

                        #------------------------PARTE 5------------------   
                        if response["successful"]==True:
                            try:
                                with transaction.atomic():
                                    eventorecompensa=Evento01Tb001RecompensaAfiliado(
                                        tx_hash=request.data["hash"],
                                        nu_monto=float(laope['amount']),
                                        tx_hash_recompensa=response["hash"],
                                        nu_monto_recompensa=1.6,
                                        co_referido=referido,
                                        co_usuario_referido=receptor.co_usuario,
                                    )
                                    eventorecompensa.save()
                            except Exception as e:
                                print(e)
                else:
                    return Response({'success':False,'msg':'Solo se acepta BSS'})
            else:
                return Response({'success':False,'msg':'Solo se acepta TOKEN'})

            #------------------------PARTE 5------------------   
             
            

        return Response({'success':True})