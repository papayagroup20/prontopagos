from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAuthenticated
from main.form.general import BaseLoginForm
from django.db.models import Q
from main.models import (WalletTb037Keystore)
from fcm_django.models import FCMDevice
from apipago.serializers.general import KeystoreSerializer
from main.views.sep10 import SEP10Auth
from main.views.usuario import emailMessage
from  pprint import pprint
class LoginViewSet(viewsets.ViewSet):
    permission_classes = [AllowAny]
    
    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == 'list':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]

    def list(self, request):
        
        content={
            "success":False
        }
        keystore=WalletTb037Keystore.objects\
            .select_related("co_usuario")\
            .filter(co_usuario=request.user.co_usuario)\
            .first()
            
        if keystore is not None:
            keystoreseri=KeystoreSerializer(keystore)
            content["keystore"]=keystoreseri.data
            content["success"]=True
        
        return Response(content)
    
    def create(self, request):
        content={
            "success":True,
            "2fa":{
                "email":False,
                "code":False
            },
            "ip":False,
            "is_need_device":True
        }
        form=BaseLoginForm(request.data)
        
        if not form.is_valid():
            content["success"]=False
            content["error"]=form.errors
            return Response(content)

        keystore=WalletTb037Keystore.objects\
            .select_related("co_usuario")\
            .filter(
               (Q(co_usuario__username=form.cleaned_data["username"])|
                Q(co_usuario__email=form.cleaned_data["username"])|
                Q(address=form.cleaned_data["username"])),co_usuario__isnull=False)\
            .first()
        
        
            
        if keystore is not None:
            if "device" in request.data and request.data["device"] is not None:
                device=FCMDevice.objects.filter(name=request.data["device"],user_id=keystore.co_usuario.co_usuario).first()
                if device is not None:
                    content["is_need_device"]=False
                
            keystoreseri=KeystoreSerializer(keystore)
            content["keystore"]=keystoreseri.data
            content["2fa"]["email"]=keystore.co_usuario.in_2fa_email
            content["2fa"]["code"]=keystore.co_usuario.in_2fa
            content["ip"]=keystore.co_usuario.in_ip
            content["in_confirmado"]=keystore.co_usuario.in_confirmado

        challenge=SEP10Auth._challenge_transaction(content["keystore"]["address"],'https://prontopagos.io',None,None)
        content["challenge"]={
            "transaction": challenge,
            "network_passphrase": "Public Global Stellar Network ; September 2015",
        }
        return Response(content)
    
    
    
class Validation2FAViewSet(viewsets.ViewSet):
    permission_classes = [AllowAny]
    #Este metodo solo envia el correo con el codigo de autenticacion.
    def create(self, request):
        content={
            "success":True
        }
        data=request.data
        print(data)
        form=BaseLoginForm(data["form"])
        
        if not form.is_valid():
            content["success"]=False
            content["error"]="Usuario invalido"
        
        envelope_xdr = data['transaction']
        valido,error=SEP10Auth.validar_challenge_xdr(envelope_xdr)
        
        if valido==False:
            content["success"]=False
            content["error"]="codigo de autenticación invalida"
        
        
        keystore=WalletTb037Keystore.objects\
            .select_related("co_usuario")\
            .filter(
               (Q(co_usuario__username=form.cleaned_data["username"])|
                Q(co_usuario__email=form.cleaned_data["username"])|
                Q(address=form.cleaned_data["username"])),co_usuario__isnull=False)\
            .first()
        if keystore is not None:
            if(data['tipo']=='email'):  
                emailMessage(keystore.co_usuario,'emailclave2fa.html')
        else:
            content["success"]=False
            content["error"]="Usuario invalido"

        
        
        
        return Response(content)