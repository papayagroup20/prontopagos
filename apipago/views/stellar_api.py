from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions,viewsets
from django.db.models import Prefetch,Q
from django.contrib.auth.models import User
from apipago.views.stellar_utils import StellarAccount,BaseStellar 
from apipago.serializers.general import MensajeStellarSerializer
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend
from django.http import Http404
from stellar_sdk.asset import Asset
from typing import Union,Coroutine,Dict,Any,List,Tuple
from main.models import WalletTb037Keystore,WalletTb038MensajeStellar,WalletTb039PrecioPPG
from stellar_sdk.server import Server
from django.utils import timezone
import requests
from pprint import pprint


"""from django.db import connection
        print('# of Queries: {}'.format(len(connection.queries)))
        for query in connection.queries:
            print(query['sql'])"""

#--------CLASE BASE DE CUENTA
# Esta clase intentara suministras funciones comunes para las demas.
class AccountBase(APIView):
    authentication_classes = [
        authentication.TokenAuthentication,authentication.BasicAuthentication,
        authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def getInicial(self,request)->Tuple[dict,StellarAccount]:
        public=None
        data={
            "success":True
        }
        try:
            keystore=request.user.keystore_usuario.first()
            public=keystore.address
        except:
            data['success']=False
            data['error']='Este usuario no posee una cuenta stellar'
        #GDQXEH77VGOG7LQ5OWKNKUSA3DZ2KODVKK3GI362GERGJYN3TKZE6B3T ---CUENTA DE PRUEBA NO ACTIVA
        return data,StellarAccount(public)



#-------------------------------SOLO BALANCE-------------------------------------------
#---------CUENTA BALANCE PPG
# Una clase que solo tiene el metodo get y debuelbe al usuario su balance y los activos de ppg
class AccountBalancePPG(AccountBase):
    def get(self, request, format=None):
        """
        Retorna el balance y los activos ppg
        """
        
        data,cuenta=self.getInicial(request)
        cuenta.getActivosMioRecomendados()
        data["datos"]=cuenta.balance
        return Response(data)

#---------CUENTA BALANCE DETALLE
# Una clase para retorna el detalle del balance segun formato ppg
class AccountBalanceDetalle(AccountBase):

    def get(self, request, format=None):
        """
        Retorna el balance detallado del usuario logueado
        """
        data,cuenta=self.getInicial(request)
        cuenta.getDetalleActivo()
        data["datos"]=cuenta.balance
        return Response(data)

    def post(self,request,format=None):
        """
        Retorna el balance detalle de un tercero
        """
        if 'publico' not in request.data:
            return Response({
                'success':False,
                'error':'No se paso una llave publica'
            })

        cuenta=StellarAccount(request.data['publico'])
        cuenta.getDetalleActivo()

        return Response({
            'success':True,
            'datos':cuenta.balance
        })

#---------CUENTA BALANCE DETALLE REFERENCIA
# Una clase que debuelve el datalle del balance junto a una referencia de algun token
class AccountBalanceDetalleReferencia(AccountBase):

    def get(self, request, format=None):
        """
        Retorna el balance detallado con referencia del usuario solicitante
        """
        

        data,cuenta=self.getInicial(request)
        cuenta.getDetalleActivo()
        try:
            asset=Asset.native()
            
            if ('code' in request.GET and 'issuer' in request.GET):
                asset={
                    'code':request.GET['code'],
                    'issuer':request.GET['issuer']
                }
                
            if 'in_path' in request.GET and request.GET["in_path"]=="true":
                if 'in_usd_ref' in request.GET and request.GET["in_usd_ref"]=="true":
                    cuenta.getReferenciaActivosPath(asset)
                    cuenta.setExternalReferenciaUSD(asset)
                else:
                    cuenta.getReferenciaActivosPath(asset)
            else: 
                activo_referencia=cuenta.getAssetFormat(asset)
                cuenta.getReferenciaActivos(activo_referencia)
            
            
            data["datos"]=cuenta.balance
            return Response(data)
        except Exception as e:
            
            print('----')
            print(e)
            print('----')
            
            return Response({
                'success':False,
                'error':'Activo invalido'
            })


    def post(self,request,format=None):
        """
        Retorna el balance con referencia de un tercero
        """
        if 'publico' not in request.data:
            return Response({
                'success':False,
                'error':'No se paso una llave publica'
            })

        if 'code' not in request.data or 'issuer' not in request.data:
            return Response({
                'success':False,
                'error':'No activo invalido'
            })

        cuenta=StellarAccount(request.data['publico'])
        cuenta.getDetalleActivo()

        try:
            activo_referencia=cuenta.getAsset(request.GET['code'],request.GET['issuer'])
            cuenta.getReferenciaActivos(activo_referencia)
            return Response({
                'success':True,
                'datos':cuenta.balance
            })
        except Exception as e:
            return Response({
                'success':False,
                'error':'No activo invalido'
            })

#---------CUENTA BALANCE DETALLE REFERENCIA CON RECOMENDADO PPG
# Una clase que debuelve el datalle del balance junto a una referencia de algun token y activos recomendados
class AccountBalanceDetalleReferenciaPPG(AccountBase):

    def get(self, request, format=None):
        """
        Retorna el balance detallado con referencia del usuario solicitante
        """
        data,cuenta=self.getInicial(request)
        cuenta.getActivosMioRecomendados()
        try:
            asset=Asset.native()
            
            if ('code' in request.GET and 'issuer' in request.GET):
                asset={
                    'code':request.GET['code'],
                    'issuer':request.GET['issuer']
                }
            
            if 'in_path' in request.GET and request.GET["in_path"]:
                if 'in_usd_ref' in request.GET and request.GET["in_usd_ref"]:
                    cuenta.getReferenciaActivosPath(asset)
                    cuenta.setExternalReferenciaUSD(asset)
                else:
                    cuenta.getReferenciaActivosPath(asset)
            else: 
                activo_referencia=cuenta.getAssetFormat(asset)
                cuenta.getReferenciaActivos(activo_referencia)
            
            data["datos"]=cuenta.balance
            return Response(data)
        except Exception as e:
            print(e)
            return Response({
                'success':False,
                'error':'No activo invalido'
            })


    def post(self,request,format=None):
        """
        Retorna el balance con referencia de un tercero
        """
        if 'publico' not in request.data:
            return Response({
                'success':False,
                'error':'No se paso una llave publica'
            })

        if 'code' not in request.data or 'issuer' not in request.data:
            return Response({
                'success':False,
                'error':'No activo invalido'
            })

        cuenta=StellarAccount(request.data['publico'])
        cuenta.getActivosMioRecomendados()

        try:
            activo_referencia=cuenta.getAsset(request.GET['code'],request.GET['issuer'])
            cuenta.getReferenciaActivos(activo_referencia)
            return Response({
                'success':True,
                'datos':cuenta.balance
            })
        except Exception as e:
            return Response({
                'success':False,
                'error':'No activo invalido'
            })


#---------HISTORICO TRANSACCIONES
# Una clase para retornar el historico de las transacciones
class AccountHistorico(AccountBase):

    def get(self, request, format=None):
        """
        Retorna el balance detallado del usuario logueado
        """
        data,cuenta=self.getInicial(request)
        
        if data['success']==False:
            return Response(data)
        try:
            limite=request.GET["limite"] if "limite" in request.GET else 20
            if "page" in request.GET:
                cuenta.getHistorico(request.GET['page'],limite)
            else:
                cuenta.getHistorico(limite=limite)

            data.update(cuenta.historico)
        except Exception as e:
            print(e)
            data['success']=False
            data['error']='Ha ocurrido un error'
        
        return Response(data)

#---------HISTORICO TRANSACCIONES CON DETALLES
# Una clase para retornar el historico de las transacciones
class AccountHistoricoDetalle(AccountBase):

    def get(self, request, format=None):
        """
        Retorna el balance detallado del usuario logueado
        """
        data,cuenta=self.getInicial(request)
        
        if data['success']==False:
            return Response(data)
        try:
            limite=request.GET["limite"] if "limite" in request.GET else 20
            if "page" in request.GET:
                cuenta.getHistoricoDetalle(request.GET['page'],limite)
            else:  
                cuenta.getHistoricoDetalle(limite=limite)

            data.update(cuenta.historico)
            
        except Exception as e:
            print(e)
            data['success']=False
            data['error']='Ha ocurrido un error'
        
        return Response(data)

#---------CUENTA USUARIO
# Una clase que solo tiene el metodo get y devuelbe al usuario y su publica
class AccountUser(APIView):
    authentication_classes = [
        authentication.TokenAuthentication,authentication.BasicAuthentication,
        authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        content={
            'success':False,
        }

        
        if 'publica' not in request.GET:
            content['error']='No se suministro la llave publica'
            return Response(content)

        content['public']=request.GET['publica']

        keystore=WalletTb037Keystore.objects\
            .select_related('co_usuario')\
            .filter(address=request.GET['publica'])\
            .first()
        
        if keystore is None:
            content['username']='Anonimo'
            return Response(content)

        if keystore.co_usuario is None:
            content['username']='Anonimo'
            return Response(content)
        content['success']=True
        content['username']=keystore.co_usuario.username
        return Response(content)

    def post(self, request, format=None):
        content={
            'success':False,
        }
        datos=request.data
        if "tx_identidad" not in datos or datos["tx_identidad"]=="":
            return Response({"success":False,"error":"No se suministraron los datos necesarios"})
        
        keystore=WalletTb037Keystore.objects\
            .select_related("co_usuario")\
            .filter(Q(co_usuario__username=datos["tx_identidad"])|Q(address=datos["tx_identidad"]))\
            .first()

        if keystore is not None:
            content['public']=keystore.address
            if keystore.co_usuario is not None:
                if keystore.co_usuario==request.user:
                    return Response({"success":False,"error":"No te puedes ingresar tu mismo usuario"})
                content['username']=keystore.co_usuario.username
            else:
                content['username']='Anonimo'
        else:
            try:
                server=Server(horizon_url="https://horizon.stellar.org")
                source_account = server.load_account(datos["tx_identidad"])
                content["public"]=datos["tx_identidad"]
                content['username']='Anonimo'
            except Exception as e:
                print(e)
                return Response({"success":False,"error":"El usuario no existe o llave publica invalido"})


        content['success']=True
        return Response(content)

        

#---------CUENTA OFERTAS
# Una clase que solo tiene el metodo get y debuelbe al usuario su ofertas actuales
class AccountOfertas(AccountBase):

    def get(self, request, format=None):
        """
        Retorna las ofertas actuales
        """
        data,cuenta=self.getInicial(request)
        cuenta.getOfertas()
        data["datos"]=cuenta.oferta
        return Response(data)

#---------CUENTA TRADES
# Una clase que solo tiene el metodo get y debuelbe al usuario su historico de trades
class AccountTrades(AccountBase):
    
    def get(self, request, format=None):
        """
        Retorna el historico de trades
        """
        data,cuenta=self.getInicial(request)
        cuenta.getTrades()
        data["datos"]=cuenta.trade
        return Response(data)

class MensajeStellarViewSet(viewsets.ModelViewSet):
    queryset = WalletTb038MensajeStellar.objects\
        .select_related(
            'co_tipo_error_stellar'
            )\
        .all()
    serializer_class = MensajeStellarSerializer
    permission_classes = [AllowAny]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['created_at','co_tipo_error_stellar','tx_codigo']
    
    def create(self, request):
        raise Http404

    def destroy(self, request, pk=None):
        raise Http404
    
    def update(self, request, pk=None):
        raise Http404
    
    def update(self, request, pk=None):
        raise Http404
    
    def partial_update(self, request, pk=None):
        raise Http404

class StellarPPGViewSet(viewsets.ViewSet):
    permission_classes = [AllowAny]
    def list(self, request):
        return Response(StellarAccount.getDetalleActivoPPG(tipo_return=2))
class PreciosViewSet(viewsets.ViewSet):
    permission_classes = [AllowAny]
    
    def list(self, request):
        return Response(BaseStellar.getUSDExternalPrice())
    
    @action(detail=False, methods=['get'],url_path='bss-prices', url_name='bss-prices',permission_classes=[AllowAny])
    def getBssGeneralPriceResponse(self, request):
        fecha_hoy = timezone.now()
        precioppg=WalletTb039PrecioPPG.objects.filter(
            in_activo=True
            )[0]
        dif_hora=(fecha_hoy - precioppg.created_at)
        #days, seconds = dif_hora.days, dif_hora.seconds
        #hours = days * 24 + seconds // 3600

        if dif_hora.days>=1:
            oferta= requests.get('https://s3.amazonaws.com/dolartoday/data.json')
            list_ofertas=oferta.json()
            return Response(list_ofertas)

        else:
            ofertappg= str(precioppg)
            return Response(ofertappg)
    
    
    @staticmethod
    def getBssPriceXLM()->float:
        USDprice=BaseStellar.getUSDExternalPrice()
        fecha_hoy = timezone.now()
        precioppg=WalletTb039PrecioPPG.objects.filter(
            in_activo=True
            )[0]
        dif_hora=(fecha_hoy - precioppg.created_at)
        #days, seconds = dif_hora.days, dif_hora.seconds
        #hours = days * 24 + seconds // 3600
        ofertappg=precioppg
        
        if dif_hora.days>=1:
            oferta= requests.get('https://s3.amazonaws.com/dolartoday/data.json')
            list_ofertas=oferta.json()
            ofertappg=list_ofertas["USD"]["dolartoday"]
        
        ofertappg=round(ofertappg*USDprice["price"]*1.05,2)
        return ofertappg
        
            
    