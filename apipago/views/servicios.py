from main.models import *
from django.http import JsonResponse
from django.core import serializers
from rest_framework.response import Response
from django.http import HttpResponse
from apipago.serializers import *
from rest_framework.decorators import action,api_view,renderer_classes,permission_classes
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework import viewsets, status
import requests
from stellar_sdk.server import Server
from apipago.views.stellar import getAssetAccountToml,getAccountBalance,getActivoMio,getOrdenBook
from decimal import getcontext, Decimal, ROUND_HALF_UP

from django.contrib.auth.decorators import login_required,user_passes_test
from django.db import IntegrityError, transaction

from wallet.views.operaciones import transccionSinComision
from django.views.decorators.http import require_http_methods


from django.template.loader import render_to_string
from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required


@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def getTipoServiciosP(request):
    content={
        'success':True,
        'data':''
    }
    if(request.method=='POST'):
        if request.POST['co_proveedor_servicios']!='':
            queryset =MainTb012Servicios.objects.filter(co_proveedor_servicios=request.POST['co_proveedor_servicios'])
            serializer = ServiciosSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False
    else:
        queryset =MainTb012Servicios.objects.all()
        serializer = ServiciosSerializer(queryset , many=True)
        content['data']=serializer.data   

    return Response(content)

# aca
@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def getRequisitosServicios(request):
    content={
        'success':True,
        'data':''
    }
    if(request.method=='POST'):
        if request.POST['co_servicios']!='':
            queryset =MainTb013RequisitosServicios.objects.filter(co_servicios=request.POST['co_servicios'],in_activo=True)
            serializer = RequisitosServiciosSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False
    else:
        queryset =MainTb013RequisitosServicios.objects.all()
        serializer = RequisitosServiciosSerializer(queryset , many=True)
        content['data']=serializer.data   

    return Response(content)


@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def getDataServicios(request):
    content={
        'success':True,
        'data':''
    }
    if(request.method=='POST'):
        if request.POST['co_pago_servicios']!='':
            
            queryset =WalletTb005PagoServicios.objects.filter(co_pago_servicios=request.POST['co_pago_servicios'])
            serializer = DataServiciosProceso(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False  

    return Response(content)


@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
#@permission_classes((AllowAny,))
def ServiciosProgresoUpdate(request):
    content={
        'success':False,
        'data':'',
        'msg':'',
    }
    
    if(request.method=='POST'):
        if 'co_pago_servicios' in request.POST and request.POST['co_pago_servicios']!='':
            if  request.user.in_confirmado:
                try:
                    with transaction.atomic():
                        
                        billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user)[0]
                        pagoservicios=WalletTb005PagoServicios.objects.get(co_pago_servicios=request.POST['co_pago_servicios'])
                        pagoservicios.co_estatus=WalletTb009Estatus.objects.get(co_estatus=3)
                        pagoservicios.in_activo=False
                        pagoservicios.save()

                        histpagoserviciosold=WalletTb020HistoricoPagoServicios.objects.filter(
                            co_pago_servicios=request.POST['co_pago_servicios']
                        ).update(in_activo=False)

                        hist=pagoservicios.get_hist_transaccion()

                        transaccion=transccionSinComision(
                        co_moneda=WalletTb003Moneda.objects.get(co_moneda=3),
                        nu_monto=hist.co_transaccion.nu_monto,
                        destino=WalletTb037Keystore.objects.get(co_usuario=request.user.co_usuario),
                        billetera=billetera,
                        usuario=Tb001Usuario.objects.get(username='avaricia'),
                        tipo_transaccion=7,
                        estatus=2
                        )

                        if transaccion:
                            histpagoservicios=WalletTb020HistoricoPagoServicios(
                                co_transaccion=transaccion,
                                co_pago_servicios=pagoservicios,
                                co_estatus=WalletTb009Estatus.objects.get(co_estatus=3),
                                in_activo=False
                                )
                            histpagoservicios.save()
                        
                            


                        content['success']=True
                        content['msg']='Ha sido rechazado Sactifactoriamente'


                except IntegrityError:
                    content['msg']='Hubo un error en la operación'
    return Response(content)


@require_http_methods(["POST"])
@confirmado_user_required
def ApiPagoServicios(request,formulario):
    
    content=getContentBasicoL(request.user,request.POST['co_solicitud'],None)

   

    pago=WalletTb005PagoServicios.objects.filter(co_solicitud=request.POST['co_solicitud'])[0]
    #serializer = DataServiciosProceso(queryset , many=True)
    requisitos=WalletTb006DetallePagoServicios.objects.filter(co_pago_servicios=pago.co_pago_servicios)
    content2={
        'data':pago,
        'requisitos':requisitos,
    }

    rendered = render_to_string("../templates/solicitudes/"+formulario.tx_url_template,content2)
    content['data']=rendered
    return Response(content)


@confirmado_user_required
@user2fa_required
def ApiCulPagoServicios(request,formulario):
    content=getContentBasicoL(request.user,request.POST['co_solicitud'],None)

   

    pago=WalletTb005PagoServicios.objects.filter(co_solicitud=request.POST['co_solicitud'])[0]

    requisitos=WalletTb006DetallePagoServicios.objects.filter(co_pago_servicios=pago.co_pago_servicios)
    content2={
        'data':pago,
        'requisitos':requisitos,
    }

    rendered = render_to_string("../templates/solicitudes/"+formulario.tx_url_template,content2)
    content['data']=rendered
    return Response(content)


def ApiconvercionActivo(request):
    content={
        'success':True,
        'data':''
    }

    publico=request.session["public_key"]
    misAsset=getActivoMio(publico)

    # content['activos']=misAsset["data"]
    # content['publico']=publico
    # content['misactivosjson']=json.dumps(misAsset["data"])



    listaPrecios=[]
    if(request.method=='POST'):
        datos = re
        if "data" in misAsset and len(misAsset["data"])>0:

            content['data1']=misAsset["data"]
            #print(balance)
            
            for val in misAsset["data"]:
                if val['tx_asset_code'] != 'USDC' and val['tx_asset_issuer']!="GA5ZSEJYB37JRC5AVCIA5MOP4RHTM335X2KGX3IHOJAPP5RE34K4KZVN":
                    orderbook=getOrdenBook(
                        code1=val['tx_asset_code'],
                        code2="USDC",
                        issuer1=val['tx_asset_issuer'],
                        issuer2="GA5ZSEJYB37JRC5AVCIA5MOP4RHTM335X2KGX3IHOJAPP5RE34K4KZVN",
                    )
                    price=0
                    balance_activo=Decimal(0)
                    try:
                        price=orderbook['data']["asks"][0]["price"]
                        # nu_monto en decimal
                        total=Decimal(price)*Decimal(5)
                        balance_activo=Decimal(total.quantize(Decimal('.0000001'), rounding=ROUND_HALF_UP))
                        

                    except Exception as e:
                        pass
                        #print(e)
                    siete=f"{balance_activo.normalize():.7f}"
                    data={
                        "asset_code_sell":val['tx_asset_code'],
                        "asset_issuer_sell":val['tx_asset_issuer'],
                        "asset_code_buy":"USDC",
                        "asset_issuer_buy":"GA5ZSEJYB37JRC5AVCIA5MOP4RHTM335X2KGX3IHOJAPP5RE34K4KZVN",
                        "balance":val['nu_balance'],
                        "precio":price,
                        "ref_USD":str(siete)
                    }
                    #print(data)
                    listaPrecios.append(data)
            print(listaPrecios)
            #comprobar los que no tienen oferta y revisar si tienen oferta con xlm->USDC
            # for valores in listaPrecios:
            #     if Decimal(valores["precio"])<=0 and Decimal(valores["balance"])>0:
            #         bookxlm=getOrdenBook(
            #             code1=valores['asset_code_sell'],
            #             code2="XLM",
            #             issuer1=valores['asset_issuer_sell'],
            #             issuer2=""
            #         )
            #         if len(bookxlm['data']["asks"])>0:
            #             pricexlm=bookxlm['data']["asks"][0]["price"]
            #             totalxlm=Decimal(pricexlm)*Decimal(valores['balance'])
            #             price_usd=0
            #             for valusd in listaPrecios:
            #                 if valusd["asset_code_sell"]=="XLM":
            #                     price_usd=valusd["precio"]

            #             totalusd=Decimal(price_usd)*Decimal(totalxlm)    
            #             balance_activo_xlm=Decimal(totalusd.quantize(Decimal('.0000001'), rounding=ROUND_HALF_UP))
            #             valores["ref_USD"]=str(balance_activo_xlm.normalize())
                        
            #print(listaPrecios)
            content['data3']=listaPrecios

        #     for va in misAsset["data"]:

        #         if va["asset_type"] == "native":
        #             lista=va
        #             monto=Decimal(lista['nu_balance'])
        #             montol=str(monto.quantize(Decimal('.0000001'), rounding=ROUND_HALF_UP))
        #             lista['nu_balance']=montol
        #             content['data2']=lista

        # else:
        #     misAsset=[{
        #         'nu_balance': '0.0000000', 
        #         'tx_asset_code': 'XLM', 
        #         'tx_moneda': 'Lumens',  
        #     }]
            
        #     content['data1']=misAsset
        
        #     for val in misAsset:
        #         if val["tx_asset_code"] == "XLM":
        #             content['data2']=val

        # historico=GetHistoricoBasic(content['publico'])
        # content['historico']=historico['data']
        content['orderbookjson']=json.dumps(content['data3'])
        # print(content['orderbookjson'])
        return content
