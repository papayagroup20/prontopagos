from main.models import *
from django.http import JsonResponse
from django.core import serializers
from rest_framework.response import Response
from django.http import HttpResponse
from apipago.serializers import *
from rest_framework.decorators import action,api_view,renderer_classes,permission_classes
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework import viewsets, status
from apipago.views.stellar_utils import StellarAccount 
import requests

from stellar_sdk.server import Server
#"<tr><td>"+value.usuario+"</td><td>"+value.nombre+"</td><td>"+value.nu_monto+"</td><td>"+value.tx_codigo+"</td><td>"+value.banco+"</td><td>"+value.cuenta+"</td></tr>"
                    
@api_view(['POST','GET'])
@permission_classes((AllowAny,))
@renderer_classes([JSONRenderer])
def getEstado(request):
    content={
        'success':True,
        'data':''
    }
    if(request.method=='POST'):
        if request.POST['co_pais']!='':
            queryset =MainTb003Estado.objects.filter(co_pais=request.POST['co_pais'])
            serializer = EstadoSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False
    else:
        queryset =MainTb003Estado.objects.all()
        serializer = EstadoSerializer(queryset , many=True)
        content['data']=serializer.data
    
    return Response(content)

@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((AllowAny,))
def getCiudad(request):
    content={
        'success':True,
        'data':''
    }
    if(request.method=='POST'):
        if request.POST['co_estado']!='':
            queryset =MainTb004Ciudad.objects.filter(co_estado=request.POST['co_estado'])
            serializer = CiudadSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False
    else:
        queryset =MainTb004Ciudad.objects.all()
        serializer = CiudadSerializer(queryset , many=True)
        content['data']=serializer.data   

    return Response(content)

    

@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((AllowAny,))
def getbanco(request):
    content={
        'success':True,
        'data':''
    }
  
    if(request.method=='POST'):
        if request.POST['co_moneda']!='':
            queryset =WalletTb007Banco.objects.filter(co_activo=request.POST['co_moneda'],in_activo=True)
            serializer = bancoSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False
    else:
        queryset =WalletTb007Banco.objects.all()
        serializer = bancoSerializer(queryset , many=True)
        content['data']=serializer.data   

    return Response(content)

@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((AllowAny,))
def getstel(request):
    content={
        'success':True,
        'data':''
    }

    if(request.method=='POST'):
        if request.POST['co_moneda']!=1:
            queryset=WalletTb037Keystore.objects.filter(co_usuario=request.user.co_usuario)
            serializer = stelSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False
    else:
        queryset =WalletTb037Keystore.objects.all()
        serializer = stelSerializer(queryset , many=True)
        content['data']=serializer.data   

    return Response(content)

@api_view(['POST','GET'])
@permission_classes((IsAuthenticated,))
@renderer_classes([JSONRenderer])
def getCuenta(request):
    content={
        'success':True,
        'data':''
    }
    if(request.method=='POST'):
        if request.POST['co_banco']!='':
            queryset =WalletTb008Cuenta.objects.filter(co_banco=request.POST['co_banco'],in_staf=True)
            serializer = CuentaSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False
    else:
        queryset =WalletTb008Cuenta.objects.all()
        serializer = CuentaSerializer(queryset , many=True)
        content['data']=serializer.data   

    return Response(content)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@renderer_classes([JSONRenderer])
def getBancoMoneda(request):
    content={
        'success':True,
        'data':''
    }
    if request.POST['co_moneda']!='':
        queryset=MainTb016BancoMoneda.objects.filter(co_moneda=request.POST['co_moneda'],maintb017cuenta__co_usuario=request.user.co_usuario)
        serializer = bancoRetiroSerializer(queryset , many=True)
        content['data']=serializer.data
    else:
        content['success']=False

    return Response(content)

@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def getBalanceTokenDisp(request):
    
    content={
        'success':False,
        'data':{
            'balance':'',
            'limit':''
        }

    }
    if(request.method=='POST'):
        if request.user.in_confirmado:
            billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user)[0]
            server = Server("https://horizon-testnet.stellar.org")
            account = server.accounts().account_id(billetera.tx_public_key).call()
            moneda = WalletTb003Moneda.objects.get(co_moneda=request.POST['co_moneda'])
            for value in account['balances']:

                if moneda.tx_simbolo == 'XLM':
                    if value['asset_type']=='native':
                        content['data']['balance']=value['balance']                
                        content['data']['limit']='Sin limite'

                else:
                    if 'asset_code' in value:
                        if  moneda.tx_simbolo == value['asset_code']:        
                            content['data']['balance']=value['balance'] 
                            if 'limit' in value:
                                content['data']['limit']=value['limit']
        content['success']=True        
    
    return Response(content)

@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((AllowAny,))
def getmoneda(request):
    content={
        'success':True,
        'data':''
    }
    if(request.method=='POST'):
        if request.POST['co_tipo_moneda']!='':
            if request.user.in_confirmado:
                billetera=WalletTb037Keystore.objects.filter(co_usuario=request.user)[0]
                if request.POST['co_tipo_moneda']!=3:
                    queryset =WalletTb003Moneda.objects.filter(co_tipo_moneda=request.POST['co_tipo_moneda'],tx_simbolo__in=StellarAccount.getTokenDisponibles(billetera))
                else:
                    queryset =WalletTb003Moneda.objects.filter(co_tipo_moneda=request.POST['co_tipo_moneda'])
                
                serializer = MonedaSerializer(queryset , many=True)
                content['data']=serializer.data
        else:
            content['success']=False
    else:
        queryset =WalletTb003Moneda.objects.all()
        serializer = MonedaSerializer(queryset , many=True)
        content['data']=serializer.data   

    return Response(content)


@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((AllowAny,))
def getbancoRetiro(request):
    content={
        'success':True,
        'data':''
    }
    if(request.method=='POST'):
        if request.POST['co_moneda']!='':
            #queryset=MaintTb016BancoMoneda.objects.filter(co_moneda=3, in_activo=True)
            queryset=MaintTb016BancoMoneda.objects.filter(co_moneda=request.POST['co_moneda'])
            serializer = bancoRetiroSerializer(queryset , many=True)
            content['data']=serializer.data

        else:
            content['success']=False
    else:
      
        queryset=MaintTb016BancoMoneda.objects.all()
        serializer = bancoRetiroSerializer(queryset , many=True)
        content['data']=serializer.data   

    return Response(content)




@api_view(['POST'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def getcuentaRetiro(request):
    content={
        'success':True,
        'data':''
    }
    if request.POST['co_banco_moneda']!='':
        #queryset =MaintTb017Cuenta.objects.filter(co_banco_moneda=7, in_activo=True, co_usuario=request.user.co_usuario)
        queryset =MainTb017Cuenta.objects.filter(co_banco_moneda=request.POST['co_banco_moneda'], in_activo=True, co_usuario=request.user.co_usuario)
        serializer = cuentaRetiroSerializer(queryset , many=True)
        content['data']=serializer.data
    else:
        content['success']=False 
    return Response(content)

    
@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def getRetiro(request):
    content={
        'success':True,
        'data':'',
    }
    if(request.method=='POST'):
        if 'co_retiro' in request.POST and request.POST['co_retiro']!='':
       
            queryset =WalletTb018Retiro.objects.filter(co_retiro=request.POST['co_retiro'], in_activo=True, co_estatus=1)
            serializer = RetirosAprobarSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False  

    return Response(content)


@api_view(['POST'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def getConfirmadoUsuario(request):
    content={
        'success':request.user.in_confirmado
    }

    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@permission_classes((AllowAny,))
def getUsuario(request):
    content={}
    user = authenticate(
        username = request.POST['username'],
        password = request.POST['password'] 
        )
    content['success']=True if user is not None else False


    return Response(content)



@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def getcuentaRegistro(request):
     content={
        'success':True,
        'data':''
    }
     if(request.method=='POST'):
        if request.POST['co_moneda']!='':
            #queryset=MaintTb016BancoMoneda.objects.filter(co_moneda=3, in_activo=True)
            queryset=MaintTb016BancoMoneda.objects.filter(co_moneda=request.POST['co_moneda'])
            serializer = bancoRetiroSerializer(queryset , many=True)
            content['data']=serializer.data

        else:
            content['success']=False
     else:
      
        queryset=MaintTb016BancoMoneda.objects.all()
        serializer = bancoRetiroSerializer(queryset , many=True)
        content['data']=serializer.data   

     return Response(content)



