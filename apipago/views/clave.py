from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from papaya_pago.CustomAuth import InConfirmado
from rest_framework.decorators import action
from django.db import transaction
from main.form.general import CheckIdentidadForm,ChangeClaveForm
from main.views.usuario import emailMessage
from main.models import Tb009EmailConfirmation
from django.contrib.auth.hashers import check_password
from pprint import pprint
class claveViewSet(viewsets.ViewSet):
    permission_classes = [AllowAny]
    
    
    @action(detail=False, methods=['post'],url_path='request-reset', url_name='request-reset',permission_classes=[AllowAny])
    def requestReset(self, request):
        content={
            "success":True
        }
        
        form=CheckIdentidadForm(request.data)
        if not form.is_valid():
            content["success"]=False
            content["errorForm"]=form.errors
            return Response(content)
        
        keystore=form.cleaned_data["identidad"]
        content["msg"]="Se le ha enviado a su correo un codigo de validación, para completar el proceso"
        emailMessage(keystore.co_usuario,'email_notif_reset_clave.html',6)
        return Response(content)
    
    @action(detail=False, methods=['post'],url_path='change-password', url_name='change-password',permission_classes=[AllowAny])
    def changePassword(self, request):
        content={
            "success":True
        }
        datos=request.data
        form=ChangeClaveForm(request.data)
        if not form.is_valid():
            content["success"]=False
            content["errorForm"]=form.errors
            return Response(content)
        
        keystore=form.cleaned_data["identidad"]
        emailconfirmation=Tb009EmailConfirmation.objects.filter(co_usuario=keystore.co_usuario.co_usuario,in_activo=True)[0]
        if(keystore.address!=datos['keystore']['address']):
            content["success"]=False
            content["error"]="La llave ingresada no coincide con su llave actual"
            return Response(content)
        
        if check_password(form.cleaned_data["tx_codigo"], emailconfirmation.nu_token):
            try:
                with transaction.atomic():
                    
                    keystore.address=datos['keystore']['address'],
                    keystore.ciphertext=datos['keystore']['crypto']['ciphertext']
                    keystore.nonce=datos['keystore']['crypto']['nonce']
                    keystore.salt=datos['keystore']['crypto']['salt']
                    keystore.n=datos['keystore']['crypto']['scryptOptions']['N']
                    keystore.r=datos['keystore']['crypto']['scryptOptions']['r']
                    keystore.p=datos['keystore']['crypto']['scryptOptions']['p']
                    keystore.dklen=datos['keystore']['crypto']['scryptOptions']['dkLen']
                    keystore.encoding=datos['keystore']['crypto']['scryptOptions']['encoding']
                    keystore.save()
            except Exception as e:
                print(e)
                content["success"]=False
                content['error']='Hubo un error en la operación'
                
        else:
            content["success"]=False
            content["error"]="Codigo invalido"
            return Response(content)
        return Response(content)
    
    @action(detail=False, methods=['post'],url_path='change-password2', url_name='change-password2',permission_classes=[InConfirmado])
    def changePassword2(self, request):
        content={
            "success":True
        }
        datos=request.data
        form=ChangeClaveForm(request.data)
        if not form.is_valid():
            content["success"]=False
            content["errorForm"]=form.errors
            return Response(content)
        keystore=form.cleaned_data["identidad"]
        emailconfirmation=Tb009EmailConfirmation.objects.filter(co_usuario=keystore.co_usuario.co_usuario,in_activo=True)[0]
        if(keystore.address!=datos['keystore']['address']):
            content["success"]=False
            content["error"]="La llave ingresada no coincide con su llave actual"
            return Response(content)
        
        if check_password(form.cleaned_data["tx_codigo"], emailconfirmation.nu_token):
            try:
                with transaction.atomic():
                    
                    keystore.ciphertext=datos['keystore']['crypto']['ciphertext']
                    keystore.nonce=datos['keystore']['crypto']['nonce']
                    keystore.salt=datos['keystore']['crypto']['salt']
                    keystore.n=datos['keystore']['crypto']['scryptOptions']['N']
                    keystore.r=datos['keystore']['crypto']['scryptOptions']['r']
                    keystore.p=datos['keystore']['crypto']['scryptOptions']['p']
                    keystore.dklen=datos['keystore']['crypto']['scryptOptions']['dkLen']
                    keystore.encoding=datos['keystore']['crypto']['scryptOptions']['encoding']
                    keystore.save()
            except Exception as e:
                print(e)
                content["success"]=False
                content['error']='Hubo un error en la operación'
                
        else:
            content["success"]=False
            content["error"]="Codigo invalido"
            return Response(content)
        return Response(content)