from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required,user_passes_test
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import *
from main.models import *
from main.form import *
from django.utils.crypto import get_random_string
from django.contrib.auth.hashers import make_password,check_password
from django.core.mail import EmailMessage
from django.conf import settings
from stellar_sdk import TransactionBuilder, Network, Keypair, Account,Asset,Operation,AllowTrust
from stellar_sdk.server import Server
import requests
from django.db import IntegrityError, transaction
from apipago.serializers import *
from django.core import serializers
import qrcode
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action,api_view,renderer_classes,api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from django.http import Http404
from django_otp.forms import OTPAuthenticationForm
from django.urls import reverse

from django.views.generic.base import View

from django.utils.module_loading import import_string

from django.contrib.sites.shortcuts import get_current_site


from django.views.decorators.cache import never_cache

import qrcode
import qrcode.image.svg
from base64 import b32encode
from binascii import unhexlify
from django_otp import login as otp_login,match_token


from stellar_sdk.server import Server

from django.template.loader import render_to_string

from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required




@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@user2fa_required
@user_staff_required
@permission_classes((IsAuthenticated,))
def getRol(request):
    content={
        'success':True,
        'data':''
    }
    if(request.method=='POST'):
        if request.POST['co_rol']!='':
            
            queryset =MainTb023Rol.objects.filter(co_rol=request.POST['co_rol'])
            serializer = rolSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False  

    return Response(content)

@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@user2fa_required
@user_staff_required

@permission_classes((IsAuthenticated,))
def getInstituto(request):
    content={
        'success':True,
        'data':'',
        'padre':'',
    }
    if(request.method=='POST'):
        if 'co_instituto' in request.POST and request.POST['co_instituto']!='':
            
            queryset =MainTb024Instituto.objects.filter(co_instituto=request.POST['co_instituto'])
            serializer = institutoSerializer(queryset , many=True)
            content['data']=serializer.data
        elif 'co_padre' in request.POST and  request.POST['co_padre']!='':
            queryset =MainTb024Instituto.objects.filter(nu_padre=request.POST['co_padre'])
            serializer = institutoSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False  

    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@user2fa_required
@user_staff_required
@permission_classes((IsAuthenticated,))
def getProceso(request):
    content={
        'success':True,
        'data':'',
    }
    
    
    if 'co_proceso' in request.POST and request.POST['co_proceso']!='':
        queryset =MainTb025Proceso.objects.filter(co_proceso=request.POST['co_proceso'])
        serializer = procesoSerializer(queryset , many=True)
        content['data']=serializer.data

    else:
        content['success']=False  

    return Response(content)


@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@user2fa_required
@user_staff_required

@permission_classes((IsAuthenticated,))
def getTipoSolicitud(request):
    content={
        'success':True,
        'data':'',
        'padre':'',
    }
    if(request.method=='POST'):
        if 'co_tipo_solicitud' in request.POST and request.POST['co_tipo_solicitud']!='':
            
            queryset =MainTb027TipoSolicitud.objects.filter(co_tipo_solicitud=request.POST['co_tipo_solicitud'])
            serializer = tipoSolicitudSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False  

    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@user2fa_required
@user_staff_required
@permission_classes((IsAuthenticated,))
def getTipoSolicitudRol(request):
    content={
        'success':True,
        'data':'',
        'datarol':'',
    }
    
    if 'co_tipo_solicitud' in request.POST and request.POST['co_tipo_solicitud']!='':
        
        queryset =MainTb027TipoSolicitud.objects.all()
        serializer = tipoSolicitudSerializer(queryset , many=True)
        content['data']=serializer.data
    else:
        content['success']=False  

    return Response(content)

@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@user2fa_required
@user_staff_required
@permission_classes((IsAuthenticated,))
def getUsuario(request):
    content={
        'success':True,
        'data':'',
    }
    if(request.method=='POST'):
        if 'co_usuario' in request.POST and request.POST['co_usuario']!='':
       
            queryset =Tb001Usuario.objects.filter(co_usuario=request.POST['co_usuario'])
            serializer = UsuarioConfigSerializer(queryset , many=True)
            content['data']=serializer.data
            content['sdsds']=Tb001Usuario.objects.filter(co_usuario=request.POST['co_usuario']).exists()
        else:
            content['success']=False  

    return Response(content)

@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@user2fa_required
@user_staff_required

@permission_classes((IsAuthenticated,))
def deletRuta(request):
    content={
        'success':True,
        'data':'',
        'padre':'',
    }
    if(request.method=='POST'):
        if 'co_detalle_ruta' in request.POST and request.POST['co_detalle_ruta']!='':
            ruta=MainTb026DetalleRuta.objects.get(co_detalle_ruta=request.POST['co_detalle_ruta'])
            tiposolicitud=ruta.co_tipo_solicitud.co_tipo_solicitud
            ruta.delete()
            queryset =MainTb027TipoSolicitud.objects.filter(co_tipo_solicitud=tiposolicitud)
            serializer = tipoSolicitudSerializer(queryset , many=True)
            content['data']=serializer.data
            
        else:
            content['success']=False  

    return Response(content)


@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@user2fa_required
@user_staff_required
@permission_classes((IsAuthenticated,))
def getMenu(request):
    content={
        'success':True,
        'data':'',
        'padre':'',
    }
    if(request.method=='POST'):
        if 'co_menu' in request.POST and request.POST['co_menu']!='':
            
            queryset =MainTb030Menu.objects.filter(co_menu=request.POST['co_menu'])
            serializer = menuSerializer(queryset , many=True)
            content['data']=serializer.data
        elif 'co_padre' in request.POST and  request.POST['co_padre']!='':
            queryset =MainTb030Menu.objects.filter(nu_padre=request.POST['co_padre'])
            serializer = menuSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False  

    return Response(content)


@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@user2fa_required
@user_staff_required
@permission_classes((IsAuthenticated,))
def getPais(request):
    content={
        'success':True,
        'data':'',
    }
    if(request.method=='POST'):
        if 'co_pais' in request.POST and request.POST['co_pais']!='':
            
            queryset =MainTb002Pais.objects.filter(co_pais=request.POST['co_pais'])
            serializer = PaisSerializer(queryset , many=True)
            content['data']=serializer.data
        else:
            content['success']=False  

    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def getDetalleSolicitud(request):
    content={
        'success':True,
        'data':'',
    }
    if(request.method=='POST'):
        rendered = render_to_string('../templates/solicitudes/prueba.html')
        content['data']=rendered

    return Response(content)