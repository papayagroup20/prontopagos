from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from main.form.general import RegistroParte1Form,RegistroParte2Form,RegistroBasicoForm,VerificarForm
from rest_framework.decorators import action
from django.db import IntegrityError, transaction
from main.models import (
    Tb001Usuario,WalletTb037Keystore,Tb009EmailConfirmation,
    MainTb023Rol,MainTb005Persona,MainTb006TipoDocumento)
from  pprint import pprint
from main.views.usuario import emailMessage
from django.contrib.auth.hashers import check_password
class RegistroViewSet(viewsets.ViewSet):
    permission_classes = [AllowAny]
    def create(self, request):
        #print(request.data)
        content={
            "success":True
        }
        
        form=RegistroParte1Form(request.data)
        if form.is_valid():
            pass
        else:
            content["success"]=False
            content["error"]=form.errors
            

        return Response(content)

    @action(detail=False, methods=['post'],url_path='parte1', url_name='parte1')
    def parte1(self, request, pk=None):
        content={
            "success":True
        }
        
        form=RegistroParte1Form(request.data)
        if form.is_valid():
            pass
        else:
            content["success"]=False
            content["error"]=form.errors
            content["errorForm"]=form.errors
            

        return Response(content)

    @action(detail=False, methods=['post'],url_path='parte2', url_name='parte2')
    def parte2(self, request, pk=None):
        content={
            "success":True
        }
        datos=request.data
        
        form1=RegistroParte1Form(datos['paso2'])
        form2=RegistroParte2Form(datos['paso3'])

        #-------VALIDAR FORMULARIOS OTRA VEZ....----------
        if not form1.is_valid():
            content["success"]=False
            content["error"]=form1.errors
            content["errorForm"]=form1.errors
            return Response(content)

        if not form2.is_valid():
            content["success"]=False
            content["error"]=form2.errors
            content["errorForm"]=form2.errors
            return Response(content)
            

        try:
            with transaction.atomic():
                #documento=MainTb006TipoDocumento.objects.filter(co_tipo_indentidad=form1.cleaned_data['co_tipo_indentidad']).firsts()
                persona=MainTb005Persona(
                    nu_documento=form1.cleaned_data['nu_documento'],
                    co_tipo_documento=MainTb006TipoDocumento.objects.filter(co_tipo_indentidad=form1.cleaned_data['co_tipo_indentidad']).first(),
                    nu_telefono=form2.cleaned_data['nu_telefono']
                )
                persona.save()
                usuario=Tb001Usuario(
                    username=form2.cleaned_data['username'].upper(),
                    email=form2.cleaned_data['email'],
                    co_rol=MainTb023Rol.objects.get(co_rol=7),
                    co_persona=persona,
                )
                usuario.save()
                pprint(datos['keystore'])
                keystore=WalletTb037Keystore(
                    address=datos['keystore']['address'],
                    version='gobelx-10-09-2020',
                    ciphertext=datos['keystore']['crypto']['ciphertext'],
                    nonce=datos['keystore']['crypto']['nonce'],
                    salt=datos['keystore']['crypto']['salt'],
                    n=datos['keystore']['crypto']['scryptOptions']['N'],
                    r=datos['keystore']['crypto']['scryptOptions']['r'],
                    p=datos['keystore']['crypto']['scryptOptions']['p'],
                    dklen=datos['keystore']['crypto']['scryptOptions']['dkLen'],
                    encoding=datos['keystore']['crypto']['scryptOptions']['encoding'],
                    co_usuario=usuario
                )
                keystore.save()

            
        except Exception as e:
            print(e)
            content["success"]=False
            content['error']='Hubo un error en la operación'
            
        
        
            

        return Response(content)
    
    @action(detail=False, methods=['post'],url_path='basico', url_name='basico')
    def parte2(self, request, pk=None):
        content={
            "success":True
        }
        datos=request.data
        
        form=RegistroBasicoForm(datos['form'])

        #-------VALIDAR FORMULARIOS----------

        if not form.is_valid():
            content["success"]=False
            content["error"]=form.errors
            content["errorForm"]=form.errors
            return Response(content)
            

        try:
            with transaction.atomic():
                
                usuario=Tb001Usuario(
                    username=form.cleaned_data['username'].upper(),
                    email=form.cleaned_data['email'],
                    co_rol=MainTb023Rol.objects.get(co_rol=7),
                )
                usuario.save()
                pprint(datos['keystore'])
                keystore=WalletTb037Keystore(
                    address=datos['keystore']['address'],
                    version='gobelx-10-09-2020',
                    ciphertext=datos['keystore']['crypto']['ciphertext'],
                    nonce=datos['keystore']['crypto']['nonce'],
                    salt=datos['keystore']['crypto']['salt'],
                    n=datos['keystore']['crypto']['scryptOptions']['N'],
                    r=datos['keystore']['crypto']['scryptOptions']['r'],
                    p=datos['keystore']['crypto']['scryptOptions']['p'],
                    dklen=datos['keystore']['crypto']['scryptOptions']['dkLen'],
                    encoding=datos['keystore']['crypto']['scryptOptions']['encoding'],
                    co_usuario=usuario
                )
                keystore.save()

            
        except Exception as e:
            print(e)
            content["success"]=False
            content['error']='Hubo un error en la operación'
            
        
        
            

        return Response(content)
    
    @action(detail=False, methods=['post'],url_path='reenviar', url_name='reenviar')
    def reenviar(self, request):
        content={
            "success":True,
            "msg":"Se ha enviado un nuevo código por favor revise su correo"
        }
        emailMessage(request.user,'email_notif_codigo.html')
        
        return Response(content)
    
    @action(detail=False, methods=['post'],url_path='validar', url_name='validar')
    def validar(self, request):
        content={
            "success":True
        }
        datos=request.data
        form=VerificarForm(datos)
        if form.is_valid():
            c1=datos['c1']
            c2=datos['c2']
            c3=datos['c3']
            c4=datos['c4']
            token=str(c1)+str(c2)+str(c3)+str(c4)
            print(token)
            emailconfirmation=Tb009EmailConfirmation.objects.filter(co_usuario=request.user.co_usuario,in_activo=True)[0]
            if check_password(token, emailconfirmation.nu_token):
                
                try:
                    with transaction.atomic():
                        usuario=request.user
                        usuario.in_confirmado=True
                        usuario.in_ip=False
                        usuario.in_aceptar=True
                        usuario.save()
                        content['msg']='Se ha registrado sactifactoriamente'
                        
                except IntegrityError:
                    content['success']=False
                    content['msg']='Hubo un error en la operación'
            else:
                content['success']=False
                content['msg']='Se ha ingresado un codigo incorrecto'
                                   
    
        return Response(content)
    
    
    