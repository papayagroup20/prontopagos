from rest_framework.response import Response
from rest_framework import authentication, permissions,viewsets
from apipago.serializers.operacion import TransaccionSerializer,MetodoPagoSerializers
from apipago.serializers.general import bancoSerializer
from apipago.views.stellar_api import PreciosViewSet
from rest_framework.decorators import action
from main.models import (
    WalletTb015Transaccion,WalletTb040Limite,WalletTb041LimiteUsuario,
    Tb001Usuario,MainTb028Solicitud,WalletTb009Estatus,WalletTb003Moneda,
    WalletTb013TipoTransaccion,MainTb029Ruta,WalletTb010Recarga,WalletTb007Banco,
    WalletTb025MetodoPago,WalletTb056InfoMetodoPago,WalletTb007Banco,WalletTb025MetodoPago)
from pprint import pprint
from papaya_pago.CustomAuth import InConfirmado
from django.db import transaction
from django.utils import timezone
from typing import Union,Coroutine,Dict,Any,List,Tuple
from decimal import Decimal
from datetime import date
from django.db.models import Prefetch
from wallet.form import CompraXLMParte1Form,CompraXLMParte2Form
import json

class CompraXLMViewSet(viewsets.ViewSet):
    authentication_classes = [
        authentication.TokenAuthentication,authentication.BasicAuthentication,
        authentication.SessionAuthentication]
    permission_classes = [InConfirmado]
    
    co_tipo_solicitud=24
    
    userlimite=None
    
    def list(self, request):
        content={
            'success':True
        }
        try:
            listaCompras=WalletTb015Transaccion.objects\
                .filter(co_emisor=request.user, co_tipo_transaccion=6)\
                .order_by('-created_at')
            
            serialize=TransaccionSerializer(listaCompras, many=True)
            content["data"]=serialize.data
            
        except:
            content["success"]=False
            content["error"]="Ha ocurrido un errofr"
        return Response(content)
    
    def create(self,request):
        contentLimite=self.getCheckLimite(request.user)
        data=request.data
        
        content={
            "success":True
            }

        form1=CompraXLMParte1Form(data["form1"])
        form2=CompraXLMParte2Form(data["form2"])
        
        if form1.is_valid() is False:
            errores=json.loads(form1.errors.as_json(escape_html=False))
            data_error={}
            for err in errores.items():
                data_error[err[0]]=[err[1][0]["message"]]
            content["success"]=False
            content["msg"]="1"
            content["errorForm"]=data_error
            return Response(content)
        
        if form2.is_valid() is False:
            errores=json.loads(form2.errors.as_json(escape_html=False))
            data_error={}
            
            for err in errores.items():
                data_error[err[0]]=[err[1][0]["message"]]
            content["success"]=False
            content["msg"]="2"
            content["errorForm"]=data_error
            return Response(content)
                    
        if(contentLimite["success"] is False):
            return Response(contentLimite)
        
        
        elif(self.userlimite.checkLimite(Decimal(form1.cleaned_data['nu_monto'])) is False):
            content={
                "success":False,
                "error":"Ha excedido el monto maximo diario permitido de compra de lumens por nuestro sistema"
            
            }
            
            
            
            return Response(content)
        
        else:
            try:
                with transaction.atomic():
                    
                    precio=PreciosViewSet.getBssPriceXLM()
                    
                    
                    self.userlimite.nu_cantidad=Decimal(self.userlimite.nu_cantidad)+Decimal(form1.cleaned_data['nu_monto'])
                    self.userlimite.save()
                    estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                    
                    solicitud=MainTb028Solicitud.objects.newSolicitud(self.co_tipo_solicitud,request.user)
                    solicitud.co_estatus=estatus
                    solicitud.in_finanza=True
                    solicitud.save()
                    
                    
                    transaccion=WalletTb015Transaccion(
                        nu_monto=form1.cleaned_data['nu_monto'],
                        co_emisor=request.user,
                        co_estatus=estatus,
                        #co_moneda=WalletTb003Moneda.objects.get(co_moneda=request.POST['moneda_depositar']),
                        co_moneda=WalletTb003Moneda.objects.get(co_moneda=3),
                        co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=6),
                        nu_monto2=precio,
                        fe_transaccion=form2.cleaned_data['fe_transaccion'],
                        co_solicitud=solicitud
                    )
                    transaccion.save()
                    
                    
                    
                    referencia=WalletTb010Recarga(
                    tx_codigo=form2.cleaned_data['nu_referencia'],
                    co_banco=form2.cleaned_data['co_banco'],
                    co_transaccion=transaccion,
                    co_solicitud=solicitud,
                    co_metodo_pago=form1.cleaned_data['co_metodo_pago']
                    )
                    referencia.save()
                
                    ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True).first()
                    ruta.in_activo=False
                    ruta.save()
                    
                    
                    
                    
                    content['msg']="Su solicitud de compra ha sido enviada con exito. Tendra respuesta en aproximadamente 30 min y se depositaran los lumens en su billetera"

            
            except Exception as e:
                print(e)
                content["success"]=False
                content["error"]="Ha ocurrido un error"
                
            return Response(content)
    
    @action(detail=False, methods=['GET'],url_path='limite', url_name='limite',permission_classes=(InConfirmado, ))
    def limite(self, request)->Response:

        content=self.getCheckLimite(request.user)
        
        return Response(content)
    
    
    def getCheckLimite(self,_user:Tb001Usuario)->dict:
        content={
            "success":True,
        }
        try:
            with transaction.atomic():
                litppg=WalletTb040Limite.objects.latest('created_at')
                limiteTemp,created=WalletTb041LimiteUsuario.objects.get_or_create(
                    co_limite=litppg,
                    co_usuario=_user,
                    in_activo=True)


                fecha_hoy = timezone.now()  
                dif_hora=(fecha_hoy - limiteTemp.created_at)

                if dif_hora.days>=1:
                        limiteTemp.in_activo=False
                        limiteTemp.save()
                        limite=WalletTb041LimiteUsuario(
                            co_limite=litppg,
                            co_usuario=_user)
                        limite.save()
                else:
                    limite=limiteTemp
                self.userlimite=limite
                if limite.checkLimite(0):
                    limites={
                        "nu_limite":litppg.nu_limite,
                        "nu_cantidad":limite.nu_cantidad
                    }
                    content["data"]=limites
                else:
                    content["success"]=False
                    content["error"]="Ha excedido el monto maximo diario permitido de compra de lumens por nuestro sistema"        
          

        except Exception as e:
            print(e)
            content["success"]=False
            content["error"]="Ha ocurrido un error"
        
        return content
    
    @action(detail=False, methods=['GET'],url_path='metodos-pagos', url_name='metodos-pagos',permission_classes=(InConfirmado, ))
    def getMetodosPagos(self,request):
        content={
            'success':True
        }
        
        
        

        try:
            metodos=WalletTb025MetodoPago.objects\
                .prefetch_related(
                Prefetch('inmepa_mepa',queryset=WalletTb056InfoMetodoPago.objects.order_by('co_info_metodo_pago')),
                )\
                .filter(co_metodo_pago__in=[4])
                

            serialize=MetodoPagoSerializers(metodos,many=True)
            content["data"]=serialize.data
            
        except Exception as e:
            print(e)
            
            content["success"]=False
            content["error"]="Ha ocurrido un errofr"
            
        return Response(content)
    
    @action(detail=False, methods=['GET'],url_path='banco', url_name='banco',permission_classes=(InConfirmado, ))
    def getBancos(self,request):
        content={
            'success':True
        }
        
        try:
            bancos=WalletTb007Banco.objects.all()
                
            serialize=bancoSerializer(bancos,many=True)
            content["data"]=serialize.data
            
        except Exception as e:
            print(e)
            
            content["success"]=False
            content["error"]="Ha ocurrido un errofr"
            
        return Response(content)
        
    
    