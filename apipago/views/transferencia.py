from main.models import *
from django.http import JsonResponse
from django.core import serializers
from rest_framework.response import Response
from django.http import HttpResponse
from apipago.serializers import *
from rest_framework.decorators import action,api_view,renderer_classes,permission_classes
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework import viewsets, status
import requests
from stellar_sdk.server import Server
from stellar_sdk.asset import Asset
from stellar_sdk.sep.stellar_toml import fetch_stellar_toml
from main.views import notificacion_email

from django.contrib.auth.decorators import login_required,user_passes_test
from django.db import IntegrityError, transaction
from django.views.decorators.http import require_http_methods
from stellar_sdk.keypair import Keypair
from main.views.utils import *
import requests
import numpy as np
from requests.exceptions import ConnectionError
from urllib.parse import urlparse