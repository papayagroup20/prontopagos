from logging import getLogger
from django.utils.translation import gettext as _
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.decorators import api_view, renderer_classes,action
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.views import APIView
from rest_framework import viewsets, status,generics,mixins
from rest_framework.parsers import JSONParser
from django.http import Http404
from django.db.models import Prefetch
from django.db import IntegrityError, transaction
from apipago.serializers.operacion import PaypalOperacionSerializer
from main.models import WalletTb047PaypalOperacion
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
import requests

class PaypalOperacionViewSet(viewsets.ModelViewSet):
    queryset = WalletTb047PaypalOperacion.objects.\
        select_related(
          'co_solicitud','co_transaccion').all()
    serializer_class = PaypalOperacionSerializer
    permission_classes = [AllowAny]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['co_paypal_operacion','co_solicitud']

    def create(self, request):
        raise Http404

    def retrieve(self, request, pk=None):
        raise Http404

    def destroy(self, request, pk=None):
        raise Http404