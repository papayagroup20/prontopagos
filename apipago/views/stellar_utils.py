from main.models import WalletTb036Activos,WalletTb037Keystore,Tb001Usuario
from apipago.serializers.stellar import WalletTb042StellarToml
import requests
from stellar_sdk.server import Server
from stellar_sdk.asset import Asset
from stellar_sdk.sep.stellar_toml import fetch_stellar_toml
from main.views.utils import getFecha,getPublicRecortado
from urllib.parse import urlparse
from decimal import Decimal,ROUND_HALF_UP
from typing import Union,Coroutine,Dict,Any,Tuple,List
from django.db.models import Q
from pprint import pprint
from time import time
#------------CLASE BASE DE STELLAR--
# Esta clase tendra los atributos mas basicos para las operaciones con stellar-
class BaseStellar():
    server:Server=Server("https://horizon.stellar.org")
    activos=None
    
    #---OBTENER ACTIVO
    # Consulta y debuelve la informacion de un activo de la BD de stellar
    @staticmethod
    def getAsset(code:str,issue:str=None):
        server=Server("https://horizon.stellar.org")
        if issue is not None:
            asset=server.assets()\
                .for_code(code)\
                .for_issuer(issue)\
                .call()
        else:
            asset=server.assets()\
            .for_code(code)\
            .call()

        return asset
    #--OBTENER FORMATO DE ACTIVO
    # Inicializa un objeto Asset basico en caso de que el tipo de dato sea directorio 
    # y lo debuelve 
    @staticmethod
    def getAssetFormat(asset:Union[Asset,dict])->Asset:

        if not isinstance(asset,Asset):
            return Asset(asset["code"],asset["issuer"]) if asset["code"] != "XLM" else Asset.native()
        
        return asset
        
    #--OBTENER ACTIVO STELLAR
    @staticmethod
    def getAssetRaw(code:str,issuer:str=None)->Asset:
        return Asset(code,issuer) if code != "XLM" or issuer is None else Asset.native()

    #---OBTENER LIBRO DE OFERTAS
    # Consulta y debuelve la informacion del libro de oferta de un par de activos       
    @staticmethod
    def getOrdenBook(asset_base:Asset,asset_request:Asset)->dict:
        server=Server("https://horizon.stellar.org")
        content={
                'success':True,
            }
        try:
            orderbook=server.orderbook(asset_base,asset_request).call()
            content["data"]=orderbook
        except:
            content["success"]=False

        return content
    
    #---OBTENER TOML
    # Este metodo consulta el toml de un activo,
    # de alli saca y formatea la informacion del activo y del toml y la guara en BD, 
    # Por ultimo debuelve esta informacion como un directorio
    @staticmethod
    def getTomlAsset(code:str,issue:str,activo:WalletTb036Activos=None)->dict:
        assetjs=BaseStellar.getAsset(code,issue)
        tx=assetjs["_embedded"]["records"][0] if len(assetjs["_embedded"]["records"])>0 else ""
        content=None
        try:
            if tx != "":
                #pprint(tx)
                if "toml" in tx["_links"]:
                    if tx["_links"]["toml"]["href"] != "": 
                        url=urlparse(tx["_links"]["toml"]["href"])
                        toml=None
                        try:
                            toml=fetch_stellar_toml(url.netloc)
                        except: return content
                        
                        org_sep06=toml["TRANSFER_SERVER"] if "TRANSFER_SERVER" in toml else ""
                        org_sep24=toml["TRANSFER_SERVER_SEP0024"] if "TRANSFER_SERVER_SEP0024" in toml else ""
                        org_sep31=""
                     
                        org_url=""
                        org_desc=""
                        
                        if "DOCUMENTATION" in toml:
                            org_url=toml["DOCUMENTATION"]["ORG_URL"] if "ORG_URL" in toml["DOCUMENTATION"] else ""
                            org_desc=toml["DOCUMENTATION"]["ORG_DESCRIPTION"] if "ORG_DESCRIPTION" in toml["DOCUMENTATION"] else ""


                        uri_key=toml["URI_REQUEST_SIGNING_KEY"] if "URI_REQUEST_SIGNING_KEY" in toml else ""
                        in_concu=False
                        for value in toml["CURRENCIES"]:
                            if value["code"]==code and value["issuer"]==issue:
                                in_concu=True
                                tx_code=value["code"] if "code" in value else ""
                                tx_issuer=value["issuer"] if "issuer" in value else ""
                                cont={
                                    "org_url":org_url if org_url == "" else urlparse(org_url).netloc,
                                    "org_desc":org_desc,
                                    "asset_desc": value["desc"] if "desc" in value else "",
                                    "asset_conditions": value["conditions"] if "conditions" in value else "",
                                    "tx_sep06_url":org_sep06,
                                    "tx_sep24_url":org_sep24,
                                    "tx_sep31_url":org_sep31,
                                    "name":value["name"] if "name" in value else "",
                                    "img":value["image"] if "image" in value else "",
                                    "asset_type":value["anchor_asset_type"] if "anchor_asset_type" in value else "",
                                    "uri_key":uri_key
                                }
                                content=cont
                                stellartoml,_=WalletTb042StellarToml.objects.get_or_create(
                                    tx_org_url=content["org_url"]
                                )
                                if (
                                    stellartoml.tx_org_desc!=content["org_desc"] or stellartoml.tx_uri_key!=content["uri_key"] or
                                    stellartoml.tx_sep06_url!=content["tx_sep06_url"] or stellartoml.tx_sep24_url!=content["tx_sep24_url"] or
                                    stellartoml.tx_sep31_url!=content["tx_sep31_url"]):
                                    
                                    if stellartoml.tx_org_desc!=content["org_desc"]:
                                        stellartoml.tx_org_desc=content["org_desc"]
                                        
                                    if stellartoml.tx_uri_key!=content["uri_key"]:
                                        stellartoml.tx_uri_key=content["uri_key"]
                                        
                                    if stellartoml.tx_sep06_url!=content["tx_sep06_url"]:
                                        stellartoml.tx_sep06_url=content["tx_sep06_url"]
                                                               
                                    if stellartoml.tx_sep24_url!=content["tx_sep24_url"]:
                                        stellartoml.tx_sep24_url=content["tx_sep24_url"]
                                        
                                    if stellartoml.tx_sep31_url!=content["tx_sep31_url"]:
                                        stellartoml.tx_sep31_url=content["tx_sep31_url"]
                                    
                                    
                                    stellartoml.save()
                                    
                                
                                if content["name"] is None or content["name"]=="":
                                    nombre=tx_code
                                else:
                                    nombre=content["name"] 

                                if activo is not None:
                                    activo.toml_asset_desc=content["asset_desc"]
                                    activo.toml_asset_conditions=content["asset_conditions"]
                                    activo.toml_name=nombre
                                    activo.toml_img=content["img"]
                                    activo.co_stellar_toml=stellartoml
                                    activo.toml_asset_type=content["asset_type"]
                                    activo.save()
                                else:
                                    WalletTb036Activos.objects.create(
                                        tx_asset_code=value["code"],
                                        tx_asset_issuer=value["issuer"],
                                        toml_asset_desc=content["asset_desc"],
                                        toml_asset_conditions=content["asset_conditions"],
                                        toml_name=nombre,
                                        toml_img=content["img"],
                                        toml_asset_type=content["asset_type"],
                                        in_activo=True,
                                        in_principal=False,
                                        in_asset_ppg=False
                                    )
                        if not in_concu:
                            if activo is not None:
                                    activo.toml_name=None
                                    activo.save()
                            else:
                                WalletTb036Activos.objects.create(
                                    tx_asset_code=code,
                                    tx_asset_issuer=issue
                                )
                    else:
                        WalletTb036Activos.objects.get_or_create(
                            tx_asset_code=code,
                            tx_asset_issuer=issue
                        )



        except Exception as e:
            print(e,'<---lala')

        return content

    #---OBTENER DETALLE ACTIVOS PPG
    # Este metodo tecnicamente consulta la tabla activo y toml y debuelve los datos 
    # con un formato y la consulta, el detalle es que este metodo excluye consultas si se le pasa un
    # balance(lista) con estos datos ya incluidos, otro caso es que puedes especificar 
    # solo consultar un activo
    @staticmethod
    def getDetalleActivoPPG(
        balance:list=[],asset:Asset=None,tipo_return:int=1
        )->Union[WalletTb036Activos,list,Tuple[list,WalletTb036Activos]]:

        activos=WalletTb036Activos.objects\
            .select_related('co_stellar_toml')\
            .filter(in_live=True)\
            .only('co_activo',
                'tx_asset_code','tx_asset_issuer','co_stellar_toml',
                'toml_name','toml_img','toml_asset_desc','toml_asset_conditions','tx_alias','toml_asset_type',
                'in_activo','in_principal','tx_pool')\
            .order_by('nu_orden')

        list_code=[]
        list_issuer=[]
        list_code2=[]
        list_issuer2=[]

        lista_not_in_db=[]
        is_db=False

        if asset is not None:
            activos=activos.filter(tx_asset_code=asset.code,tx_asset_issuer=asset.issuer)
        elif len(balance)>0:

            for val in balance:
                if val["is_asset"]:
                    
                    #pprint(val)
                    
                    if val["in_detalle"]:
                        list_code.append(val["asset"]["code"])
                        list_issuer.append(val["asset"]["issuer"])

                    list_code2.append(val["asset"]["code"])
                    list_issuer2.append(val["asset"]["issuer"])
            
            activos=activos.filter(
                Q(tx_asset_code__in=list_code2,tx_asset_issuer__in=list_issuer2,in_principal=False) | 
                Q(in_principal=True))
            activos=activos.exclude(tx_asset_code__in=list_code,tx_asset_issuer__in=list_issuer)
            
            #aqui se intanta incluir los datos de la base de datos al listado final
            for item in balance:
                is_db=False
                for item2 in activos:
                    if item["tx_asset"]==item2.tx_asset:
                        is_db=True

                if not is_db and item["asset_type"]!='native':
                    lista_not_in_db.append(item)


            #En caso de que no halla datos de algun activo en la bd, aqui se incluiran
            if len(lista_not_in_db)>0:
                """for activondb in lista_not_in_db:
                    toml=BaseStellar.getTomlAsset(activondb["asset"]["code"],activondb["asset"]["issuer"])
                """

                #Aqui se hara el segundo intento, donde por logica ya debe haber estos datos en db
                activos=WalletTb036Activos.objects\
                .select_related('co_stellar_toml')\
                .filter(in_live=True)\
                .only('co_activo',
                    'tx_asset_code','tx_asset_issuer','co_stellar_toml',
                    'toml_name','toml_img','toml_asset_desc','toml_asset_conditions','tx_alias','toml_asset_type',
                    'in_activo','in_principal','tx_pool')\
                .order_by('nu_orden')
                
                activos=activos.filter(
                Q(tx_asset_code__in=list_code2,tx_asset_issuer__in=list_issuer2,in_principal=False) | 
                Q(in_principal=True))

                activos=activos.exclude(tx_asset_code__in=list_code,tx_asset_issuer__in=list_issuer,in_principal=False)
        else:
            activos=activos.filter(in_principal=True)
            
        if tipo_return==1:
            return activos
        else: 
            lista=[]
            if activos.exists():
                for value in activos:
                    if value.tx_alias is not None:
                        tx_moneda=value.tx_alias
                    elif value.toml_name is not None:
                        tx_moneda=value.toml_name
                    else:
                        tx_moneda=""
                    if value.tx_pool is not None:
                        tx_asset=value.tx_pool
                    else:
                        tx_asset=value.tx_asset_code+":"+value.tx_asset_issuer if value.tx_asset_code!="XLM" else "XLM"   
                    data={
                        "asset":{"code":value.tx_asset_code,"issuer":value.tx_asset_issuer} if value.tx_pool is None else "",
                        "tx_asset":tx_asset,
                        "asset_desc":value.toml_asset_desc if value.toml_asset_desc is not None else "",
                        "asset_conditions":value.toml_asset_conditions if value.toml_asset_conditions is not None else "",
                        "tx_moneda":tx_moneda,
                        "img":value.toml_img if value.toml_img is not None else "",
                        "org_url":value.co_stellar_toml.tx_org_url if value.co_stellar_toml is not None else "",
                        "org_desc":value.co_stellar_toml.tx_org_desc if value.co_stellar_toml is not None else "",
                        "tx_sep06_url":value.co_stellar_toml.tx_sep06_url if value.co_stellar_toml is not None else "",
                        "tx_sep24_url":value.co_stellar_toml.tx_sep24_url if value.co_stellar_toml is not None else "",
                        "tx_sep31_url":value.co_stellar_toml.tx_sep31_url if value.co_stellar_toml is not None else "",
                        "toml_asset_type":value.toml_asset_type if value.toml_asset_type is not None else "",
                        "in_activo":value.in_activo,
                        "in_detalle":True,
                        "in_propio":False,
                        "in_ppg":True,
                        "is_asset":True if value.tx_pool is None else False,
                        "in_dep_ret":value.in_dep_ret
                        }
                    
                    
                    
                    if value.tx_pool is not  None:
                        pass
                        #pprint(value) 
                    
                    
                    
                    

                    lista.append(data)

            if tipo_return==2:
                return lista
            else:
                return lista,activos

    @staticmethod
    def getPagePayment(url:str)->dict:
        res=requests.get(url,None)
        return res.json()
    
    @staticmethod
    def getUSDExternalPrice()->Union[dict,bool]:
        try:
            url='https://api.blockchain.info/price/index?api_code=1770d5d9-bcea-4d28-ad21-6cbd5be018a8&'
            payload={
                'base': 'XLM',
                'quote':'USD',
                'time':(int(time())-500)
            }
            headers = {'Cookie': '__cfduid=d1195d93d5b2f7f965562958b4ea9e9e41607464339'}
            res=requests.get(url,params=payload,headers=headers)
            
            return res.json()
        except Exception as e:
            print(e)
            return False
        
    #----OBTENER LE PRECIO Y REFERENCIA DE UN ACTIVO MEDIANTE PATH
    def getReferenciaPath(
        self,_source_asset:Union[Asset,dict]=Asset.native(),_amount:float=1,_asset:Union[Asset,dict]=None,
        _list_asset:Union[List[Asset],List[dict]]=None)->Union[List[dict],dict,None]:
        
        _source_asset=self.getAssetFormat(_source_asset)
        
        if _asset is not None:
            asset_ppg=None
            asset=None
            if 'tx_asset' in _asset:
                asset=self.getAssetFormat(_asset.asset)
                asset_ppg=_asset
            else:
                asset=self.getAssetFormat(_asset)
            try:
                path=self.server.strict_send_paths(_source_asset,_amount,[asset]).call()
                if len(path["_embedded"]["records"])<=0: 
                    return {
                        "asset":asset,
                        "precio":0,
                        "referencia":0
                        }
                    

                record=path["_embedded"]["records"][0]
                precio=_amount/Decimal(record["destination_amount"])
                cantidad=Decimal(asset_ppg["nu_balance"]) if asset_ppg is not None and asset_ppg["nu_balance"]!="" else 0
                total=Decimal(precio)*cantidad
                balance_activo=Decimal(total.quantize(Decimal('.0000001'), rounding=ROUND_HALF_UP))


                data={
                    "asset":asset,
                    "precio":precio,
                    "referencia":f"{balance_activo.normalize():.7f}"
                }


                return data
            except Exception as e:
                print(e,'<---mamaa')
                return {
                    "asset":asset,
                    "precio":0,
                    "referencia":0
                }
        else:
            
            list_asset_ppg=None
            list_asset:list=[]
            list_tx=[]
            if 'tx_asset' in _list_asset[0]:
                
                for assetppg in _list_asset:
                    list_asset.append(self.getAssetFormat(assetppg["asset"]))
                    list_tx.append(assetppg["tx_asset"])
                    
                list_asset_ppg=_list_asset
            else:
                list_asset=_list_asset
                
            
            
            # Esta varga de abajo es para consultar los precios de una lista de activo 
            # y es asi de raro ya que la libreria de 'strict_send_paths' o el api de cual se basa
            # tiene un limite de 15 activos
            if len(list_asset)>14:
                limite=len(list_asset)-1
                nume=0
                lalistalocal:list=[]
                while nume < limite:
                    local_limit=nume+14 if nume+14<=limite else limite
                    partial_list=list_asset[nume:local_limit]
                    path=self.server.strict_send_paths(_source_asset,_amount,partial_list).call()
                    lalistalocal.extend(path["_embedded"]["records"])
                    
                    
                    
                    
                    nume=nume+14
                    
                    
                    
                
            else:
                path=self.server.strict_send_paths(_source_asset,_amount,list_asset).call()
                if len(path["_embedded"]["records"])<=0: return None
                lalistalocal=path["_embedded"]["records"]
                
                
                
            lista_path:list=lalistalocal
            lista_data:list=[]
            
            #----LLENADO INICIAL CON FILTROS LOCOS, ESPERO QUE FUNCIONE...
            for asset in list_asset:
                data={
                        "asset":asset,
                        "precio":0,
                        "referencia":0
                    }
                filterpath=list(filter(
                    lambda record:
                        ("destination_asset_code" in record and 
                         record["destination_asset_code"]==asset.code and 
                         record["destination_asset_issuer"]==asset.issuer) or
                        
                        (record["destination_asset_type"]=="native" and asset.code=="XLM")
                        ,lista_path))
                
                if len(filterpath)>0: 
                    precio=_amount/Decimal(filterpath[0]["destination_amount"])
                    data["precio"]=precio
                    if list_asset_ppg is not None:
                                                
                        filtroppg=list(filter(
                            lambda record:
                                (filterpath[0]["destination_asset_type"]!="native" and 
                                 record["asset"]["code"]==filterpath[0]["destination_asset_code"] and
                                 record["asset"]["issuer"]==filterpath[0]["destination_asset_issuer"]
                                 ) or
                                (filterpath[0]["destination_asset_type"]=="native" and record["asset"]["code"]=="XLM")
                                ,list_asset_ppg))
                        if len(filtroppg)>0 and "nu_balance" in filtroppg[0]:
                            
                            
                            asset_ppg=filtroppg[0]
                            cantidad=Decimal(asset_ppg["nu_balance"]) if asset_ppg["nu_balance"]!=""  else 0
                            total=cantidad*Decimal(precio)
                            balance_activo=Decimal(total.quantize(Decimal('.0000001'), rounding=ROUND_HALF_UP))
                            data["referencia"]=f"{balance_activo.normalize():.7f}"
                            
                    
                    lista_data.append(data)
                    
            return lista_data
    
    #----OBTENER LE PRECIO DE UN ACTIVO
    # Este metodo consulta la oferta actual de un activo con respecto a otro
    # para luego retornarlos
    def getPrecioOferta(
        self,asset_base:Union[Asset,dict],asset_request:Union[Asset,dict]
        )->float:

        asset_base=self.getAssetFormat(asset_base)
        asset_request=self.getAssetFormat(asset_request)

        orderbook=self.getOrdenBook(asset_base,asset_request)
        price=0
        try:
            price=float(orderbook['data']["asks"][0]["price"])
        except Exception as e:
            pass

        
        precio=price
        return precio

class StellarAccount(BaseStellar):
    public:str=None # Note: Llave publica

    operaciones:Union[Dict[str, Any], Coroutine[Any, Any, Dict[str, Any]]]=None

    historico:dict=None # Historico de transaccion

    balance:list=[] # lista balance formateado
    account_raw:dict={} # balance raw de stellar

    oferta:list=[] # lista de ofertas formateado
    oferta_raw:dict={} # ofertas raw de stellar

    trade:list=[] # Lista de trades formateado
    trade_raw:dict={} # Trades raw de stellar

    activos_mio:WalletTb036Activos=None # Objeto de la consulta de la tabla activo
    
    def __init__(self,public:str):
        self.public=public

    #---OBTENER OPERACION-
    # Este metodo consulta y debuelbe las operaciones de stellar de la direccion inicializada
    # con el formato orifinal.
    def getOperation(self,limite=20)->Union[Dict[str, Any], Coroutine[Any, Any, Dict[str, Any]]]:
        operation=self.server.payments()
        ope=operation.for_account(self.public)
        self.operaciones=ope.limit(limite).order().call()
        return self.operaciones
    
    
    

    #----OBTENER HISTORICO 
    # Este metodo a partir de las operaciones de tipo transpaso de la cuenta stellar, 
    # debueleve los datos formateados.
    def getHistorico(self,page:str=None,limite:int=20)->dict:
        content={
            'data':[],
            "next":"",
            "prev":""
        }
        try:
            tx=self.getOperation(limite=limite) if page is None else self.getPagePayment(page)

            lista=tx["_embedded"]["records"]
            content["next"]=tx["_links"]["next"]["href"]
            content["prev"]=tx["_links"]["prev"]["href"]
            for value in lista:
                if  "from" in value:
                    if value["from"] == self.public:
                        tipo = "Envio" if value["to"]!="GCEOQCK55IEOM6FAWVMW7L2NZQVKZHEECCKSJSAQKRHI2UHRD2UFEMUE" else "Comisión"
                    else:
                        tipo ="Recibido"

                elif value["type"] == "account_merge":
                    tipo = "Eliminar cuenta"
                else:
                    tipo = "Crear cuenta"
                    
                if "asset_type" in value:
                    content['data'].append(self.formatDataPayment(value,tipo,value["type"]))

            self.historico=content
        except Exception as e:
            print(e)
        
        return content

    #----FORMATEAR DATOS PAYMENT
    # Este metodo debuelve un directorio formateado de datos de tipo operacion de transferencia
    def formatDataPayment(self,value:dict,tipo:str,type:str)->dict:
        codigo=value["id"]
        img=""
        moneda="XLM"
        moneda2="Lumens"
        tx_asset="XLM"
        if  "from" in value:
            inde = value["from"] if value["to"] == self.public else value["to"]
            if value["asset_type"] != "native":
                moneda=value["asset_code"]
                moneda2=moneda
                tx_asset=value["asset_code"]+":"+value["asset_issuer"]


            monto=value["amount"]

        elif value["type"] == "account_merge":
            inde =  self.public
            moneda="todas"
            monto="todo"
        else:
            inde = value["funder"]
            monto=value["starting_balance"]


        fecha=getFecha(value["created_at"])
        
        dire=WalletTb037Keystore.objects.filter(address=inde).first()

        if dire is None:
            dire={
                "username":"Anónimo",
                "name":""}
        elif dire.co_usuario is None:
            dire={
                "username":"Anónimo",
                "name":""}
        else:
            dire=dire.co_usuario.username

        datos={
            "id":codigo,
            "monto":monto,
            "tipo":tipo,
            "type":value["type"],
            "hash":value["transaction_hash"],
            "direccion":getPublicRecortado(inde),
            "moneda":moneda,
            "moneda2":moneda2,
            "tx_asset":tx_asset,
            "fecha":fecha,
            "public":inde,
            "usuario":dire,
        }

        return datos

    #----OBTENER USUARIO POR PUBLICA
    # Este metodo debuelve uno o varios usuario segun su publica.
    def getUsuarioPublic(self,publica:Union[str,list])->Union[Tb001Usuario,str,list]:
        if isinstance(publica,list):
            lista=[]
            keystore=WalletTb037Keystore.objects\
                .select_related('co_usuario','co_usuario__co_persona')\
                .filter(address__in=publica)


            if keystore.exists():
                for value in keystore:
                    for val in publica:
                        if value.address==val:
                            data={
                                'public':val,
                                'usuario':'Anónimo'
                            }
                            if value.co_usuario is not None:
                                data['usuario']=value.co_usuario
                            lista.append(data)


            return lista
                
        else:
            keystore=WalletTb037Keystore.objects\
                .select_related('co_usuario','co_usuario__co_persona')\
                .filter(address=publica)\
                .first()
            if keystore is not None:
                return keystore
            else:
                return 'Anónimo'

    def getHistoricoDetalle(self,page:str=None,limite=20)->list:
        if self.historico is None:
            self.getHistorico(page,limite)
        if self.historico is None and len(self.historico['data']<=0):
            return self.historico
        
        
        lista=[]
        for value in self.historico['data']:
            if len(lista)<=0:
                lista.append(value['public'])
            else:
                pase=True
                for item in lista:
                    if item==value['public']:
                        pase=False
                if pase:
                    lista.append(value['public'])
        
        usuarios=self.getUsuarioPublic(lista)
        if isinstance(usuarios,list) and len(usuarios)>0:
            for item_historico in self.historico['data']:
                for item_usuario in usuarios:
                    if item_usuario['public']==item_historico['public']:

                        if item_usuario['usuario']!='Anónimo':
                            user=item_usuario['usuario']
                            data={
                            'username':user.username,
                            'name':user.co_persona.get_nombre_corto() if user.co_persona is not None else None
            
                            }
                        else:
                            data={'username':'Anónimo'}

                        item_historico['usuario']=data
        return self.historico

    #----OBTENER BALANCE
    # Consulta el balance de stellar y lo debuelbe con formato
    def getBalance(self)->list:
        lista=[]
        try: 
            self.account_raw = self.server.accounts().account_id(self.public).call()
            recerva=(2+self.account_raw["subentry_count"])*0.5
            for values in self.account_raw['balances']:
                if values["asset_type"]== "native":
                    tx_asset_issuer=""
                    tx_asset_code="XLM"
                    tx_asset="XLM"
                    asset=Asset.native()
                    asset={"code":asset.code,"issuer":asset.issuer}
                elif values["asset_type"]=="liquidity_pool_shares":
                    tx_asset_issuer=""
                    tx_asset_code=""
                    tx_asset=values["liquidity_pool_id"]
                    asset=""
                else:
                    tx_asset_issuer=values["asset_issuer"]
                    tx_asset_code=values["asset_code"]
                    tx_asset=tx_asset_code+":"+tx_asset_issuer
                    asset=Asset(tx_asset_code,tx_asset_issuer)
                    asset={"code":asset.code,"issuer":asset.issuer}

                    
                
                #"asset":{"code":asset.code,"issuer":asset.issuer},
                datos={
                    "asset":asset,
                    "tx_asset":tx_asset,
                    "org_url":"",
                    "org_desc":"",
                    "tx_sep06_url":"",
                    "tx_sep24_url":"",
                    "tx_sep31_url":"",
                    "asset_desc":"",
                    "asset_conditions":"",
                    "nu_balance":values["balance"],
                    "nu_balance2":str(round(Decimal(values["balance"]),2)),
                    "nu_limit":values["limit"] if "limit" in values else "-",
                    "tx_moneda":"",
                    "img":"",
                    "asset_type":values["asset_type"],
                    "toml_asset_type":"",                   
                    "in_activo":False,
                    "propio":True,
                    "is_asset":True if values["asset_type"]!="liquidity_pool_shares" else False,
                    "in_detalle":False,
                    "in_dep_ret":False
                }
                lista.append(datos)
            #pprint(lista)
            for va in lista:
                if va["asset_type"] == "native":
                    va["nu_balance"]=float(va["nu_balance"])-float(recerva)
                    valor=round(Decimal(va["nu_balance"]),7)
                    siete = f"{valor:.7f}"
                    va["nu_balance2"]=str(siete)


        except Exception as e:
            print(e,'wawa')


        self.balance=lista

        return lista

    #----INYECTAR DETALLE A BALANCE
    # Este es un metodo interno que inyecta a la lista del balance los detalles del activo
    def setDetalleToBalance(self,activo:WalletTb036Activos)->None:
        #--------------Primer llenado del listado
        #---Aqui se actualzara la lista con todos los datos de la base de datos
        try:
            for val in activo:
                in_presente=False
                if val.tx_alias is not None:
                    tx_moneda=val.tx_alias
                elif val.toml_name is not None:
                    tx_moneda=val.toml_name
                else:
                    tx_moneda=""
                    
                for balance_lista in self.balance:
                    
                    if balance_lista["tx_asset"]==val.tx_asset:
                        balance_lista["co_activo"]=val.co_activo
                        balance_lista["asset_desc"]=val.toml_asset_desc if val.toml_asset_desc is not None else ""
                        balance_lista["asset_conditions"]=val.toml_asset_conditions if val.toml_asset_conditions is not None else ""
                        balance_lista["tx_moneda"]=tx_moneda
                        balance_lista["img"]=val.toml_img if val.toml_img is not None else ""
                        balance_lista["org_url"]=val.co_stellar_toml.tx_org_url if val.co_stellar_toml is not None else ""
                        balance_lista["org_desc"]=val.co_stellar_toml.tx_org_desc if val.co_stellar_toml is not None else ""
                        balance_lista["tx_sep06_url"]=val.co_stellar_toml.tx_sep06_url if val.co_stellar_toml is not None and val.co_stellar_toml.tx_sep06_url is not None else ""
                        balance_lista["tx_sep24_url"]=val.co_stellar_toml.tx_sep24_url if val.co_stellar_toml is not None and val.co_stellar_toml.tx_sep24_url is not None else ""
                        balance_lista["tx_sep31_url"]=val.co_stellar_toml.tx_sep31_url if val.co_stellar_toml is not None and val.co_stellar_toml.tx_sep31_url is not None else ""  
                        balance_lista["in_activo"]=val.in_activo
                        balance_lista["in_detalle"]=True
                        balance_lista["in_dep_ret"]=val.in_dep_ret
                        in_presente=True

                if not in_presente:
                    if val.tx_pool is None:
                        tx_asset=val.tx_asset_code+":"+val.tx_asset_issuer if val.tx_asset_code!="XLM" else "XLM"   
                        asset={"code":val.tx_asset_code,"issuer":val.tx_asset_issuer}
                    else:
                        tx_asset=val.tx_pool
                        asset=""
                    
                    data={
                        "asset":asset,
                        "tx_asset":tx_asset,
                        "asset_desc":val.toml_asset_desc if val.toml_asset_desc is not None else "",
                        "asset_conditions":val.toml_asset_conditions if val.toml_asset_conditions is not None else "",
                        "tx_moneda":tx_moneda,
                        "img":val.toml_img if val.toml_img is not None else "",
                        "org_url":val.co_stellar_toml.tx_org_url if val.co_stellar_toml is not None else "",
                        "org_desc":val.co_stellar_toml.tx_org_desc if val.co_stellar_toml is not None else "",
                        "tx_sep06_url":val.co_stellar_toml.tx_sep06_url if val.co_stellar_toml is not None and val.co_stellar_toml.tx_sep06_url is not None else "",
                        "tx_sep24_url":val.co_stellar_toml.tx_sep24_url if val.co_stellar_toml is not None and val.co_stellar_toml.tx_sep24_url is not None else "",
                        "tx_sep31_url":val.co_stellar_toml.tx_sep31_url if val.co_stellar_toml is not None and val.co_stellar_toml.tx_sep31_url is not None else ""  ,
                        "in_activo":val.in_activo,
                        "asset_type":"native" if val.tx_asset_code=="XLM" else "token",
                        "propio":False,
                        "in_detalle":True,
                        "in_dep_ret":val.in_dep_ret
                    }
                    self.balance.append(data)

        except Exception as e:
            print(e,'<---papa')
        


        #--------------segundo llenado del listado
        #Aqui se agregaran los datos del toml a la base de datos en caso de no poseerlos
        for values in self.balance:
            if not values["in_activo"] and values["asset_type"]!="native":
                self.balance.remove(values)
        for values in self.balance:
            if values["tx_moneda"]=="" and values["asset_type"]!="native" and values["asset_type"]!="liquidity_pool_shares":
                act=None
                for valact in activo:
                    if valact.tx_asset_code==values["asset"]["code"] and valact.tx_asset_issuer==values["asset"]["issuer"]:
                        act=valact
                
                data=self.getTomlAsset(values["asset"]["code"],values["asset"]["issuer"],act)
                if data is not None:
                    values["org_url"]=data["org_url"]
                    values["org_desc"]=data["org_url"]
                    values["asset_desc"]=data["asset_desc"]
                    values["asset_conditions"]=data["asset_conditions"]
                    values["tx_moneda"]=data["name"]
                    values["img"]=data["img"]

    #----OBTENER O AGREGAR MAS DETALLE AL BALANCE
    # Este metodo consulta el balance y amplia los datos de los activos, 
    # para luego retornarlos
    def getDetalleActivo(self)->list:
        
        if len(self.balance)<=0:
            self.getBalance()
        if len(self.balance)<=0:
            return self.balance
        list_code=[]
        list_issuer=[]
        for val in self.balance:
            if val['is_asset']:
                list_code.append(val["asset"]["code"])
                list_issuer.append(val["asset"]["issuer"])
        self.activos_mio=WalletTb036Activos.objects\
            .select_related('co_stellar_toml')\
            .filter(tx_asset_code__in=list_code,tx_asset_issuer__in=list_issuer)\
            .only(
                'tx_asset_code','tx_asset_issuer','co_stellar_toml',
                'toml_name','toml_img','toml_asset_desc','toml_asset_conditions','tx_alias',
                'in_activo','tx_pool')\
            .order_by('-nu_orden')

        lista_activo=[]
        lista_not_in_db=[]
        is_db=False
        #aqui se intanta incluir los datos de la base de datos al listado final
        for value in self.balance:
            is_db=False
            for value2 in self.activos_mio:
                if value["tx_asset"]==value2.tx_asset:
                    is_db=True
                    lista_activo.append(value2)
            
            if not is_db and value["asset_type"]!='native':
                lista_not_in_db.append(value)
        

        #En caso de que no halla datos de algun activo en la bd, aqui se incluiran
        if len(lista_not_in_db)>0:
            """for activondb in lista_not_in_db:
                toml=self.getTomlAsset(activondb["asset"]["code"],activondb["asset"]["issuer"])"""

            
            #Aqui se hara el segundo intento, donde por logica ya debe haber estos datos en db
            self.activos_mio=WalletTb036Activos.objects\
            .select_related('co_stellar_toml')\
            .filter(tx_asset_code__in=list_code,tx_asset_issuer__in=list_issuer)\
            .only(
                'tx_asset_code','tx_asset_issuer','co_stellar_toml',
                'toml_name','toml_img','toml_asset_desc','toml_asset_conditions','tx_alias',
                'in_activo','tx_pool')\
            .order_by('-nu_orden')

            lista_activo=[]
            for value in self.balance:
                for value2 in self.activos_mio:
                    if value["tx_asset"]==value2.tx_asset:
                        lista_activo.append(value2)
                
            #pprint(lista_not_in_db)

        self.activos_mio=lista_activo
        self.setDetalleToBalance(lista_activo)

        return self.balance

    def getActivosMioRecomendados(self)->list:
        if len(self.balance)<=0:
            self.getBalance()
            
        lista_activos=self.balance
            
        
        activos=self.getDetalleActivoPPG(balance=lista_activos)
        self.setDetalleToBalance(activos)
        return lista_activos
    #----OBTENER LE PRECIO Y REFERENCIA DE UN ACTIVO
    # Este metodo consulta la oferta actual de un activo con respecto a otro
    # para luego retornarlos
    def getReferenciaOferta(
        self,cantidad:str,asset_base:Union[Asset,dict],asset_request:Union[Asset,dict]
        )->dict:

        asset_base=self.getAssetFormat(asset_base)
        asset_request=self.getAssetFormat(asset_request)

        orderbook=self.getOrdenBook(asset_base,asset_request)
        price=0
        balance_activo=Decimal(0)
        try:
            price=orderbook['data']["asks"][0]["price"]
            total=Decimal(price)*Decimal(cantidad)
            balance_activo=Decimal(total.quantize(Decimal('.0000001'), rounding=ROUND_HALF_UP))
        
        except Exception as e:
            pass

        siete=f"{balance_activo.normalize():.7f}"
        
        data={
            "precio":price,
            "referencia":str(siete)
        }
        return data
    
    
        
            
    #----OBTENER LE PRECIO Y REFERENCIA DEL BALANCE ACTIVO POR PATH
    # Este metodo consulta consulta tu balance y le agrega una referencia
    # de algun activo en concreto
    def getReferenciaActivosPath(self,asset_request:Asset)->list:
        if len(self.balance)<=0:
            self.balance=self.getBalance()

        if len(self.balance)<=0:
            return self.balance
        

        lista_precios=self.getReferenciaPath(_source_asset=asset_request,_list_asset=self.balance)
        for asset_ppg in self.balance:
            filtro=list(filter(lambda item: item["asset"].code==asset_ppg["asset"]["code"] and  item["asset"].issuer==asset_ppg["asset"]["issuer"],lista_precios))
            asset_ppg["precio"]=filtro[0]["precio"] if len(filtro)>0 else 0
            asset_ppg["referencia"]=filtro[0]["referencia"] if len(filtro)>0 else 0
                
        return self.balance
        
        
    #----OBTENER LE PRECIO Y REFERENCIA DEL BALANCE ACTIVO
    # Este metodo consulta consulta tu balance y le agrega una referencia
    # de algun activo en concreto
    def getReferenciaActivos(self,asset_request:Asset)->list:
        if len(self.balance)<=0:
            self.balance=self.getBalance()

        if len(self.balance)<=0:
            return self.balance

        xlm_balance=0
        for value in self.balance:
            if value["is_asset"]:
                asset_base=self.getAssetFormat(value["asset"])
                if "nu_balance" in value and float(value["nu_balance"])>0 and asset_base!=asset_request:
                    oferta=self.getReferenciaOferta(value["nu_balance"],asset_base,asset_request)
                    value["precio"]=oferta["precio"]
                    value["referencia"]=oferta["referencia"]

                else:
                    value["precio"]=0
                    value["referencia"]=0

                if value['asset']['code']=="XLM":
                    xlm_balance=value

        for val in self.balance:
            if "precio" not in val:
                val["precio"]=0
            if "nu_balance" in value and float(val["nu_balance"])>0 and float(val["precio"])<=0 and asset_base!=Asset.native():
                oferta_xlm=self.getReferenciaOferta(val["nu_balance"],asset_base,Asset.native())
                val["precio"]=oferta["precio"]
                val["referencia"]=oferta["referencia"]

                if float(val["precio"])>0:
                    

                    totalxlm=Decimal(val["precio"])*Decimal(val["nu_balance"])
                    totalusd=Decimal(xlm_balance["precio"])*Decimal(totalxlm)
                    balance_activo_xlm=Decimal(totalusd.quantize(Decimal('.0000001'), rounding=ROUND_HALF_UP))
                    val["referencia"]=str(balance_activo_xlm.normalize())
                    val["precio"]=str(Decimal(val["precio"])*Decimal(xlm_balance["precio"]))

        return self.balance
    
    #----INYECTAR EL PRECIO EXTERNO DE USD AL BALANCE
    #----NOTA EL BALANCE YA DEBE TENER UNA REFERENCIA EN XLM, SI NO LOS PRECIOS SERAN 0
    #----O COSAS LOCAS
    def setExternalReferenciaUSD(self,asset_request:Asset=Asset.native())->None:
        precio_externo=self.getUSDExternalPrice()
        if precio_externo==False: return None
        
        if len(self.balance)<=0:
            self.balance=self.getBalance()
            self.getReferenciaActivosPath(asset_request)

        if len(self.balance)<=0:
            return self.balance
        
        
        for asset_ppg in self.balance:
            
                
            if asset_ppg["asset_type"]!="native" and Decimal(asset_ppg["referencia"])>0:
                new_precio=Decimal(precio_externo["price"])*Decimal(asset_ppg["precio"])
                new_referencia=Decimal((Decimal(asset_ppg["referencia"])*Decimal(precio_externo["price"])).quantize(Decimal('.0000001'), rounding=ROUND_HALF_UP))
                
            elif asset_ppg["asset_type"]=="native":
                new_precio=Decimal(precio_externo["price"])
                new_referencia=Decimal((Decimal(asset_ppg["nu_balance"])*new_precio).quantize(Decimal('.0000001'), rounding=ROUND_HALF_UP))
            
            else:
                new_precio=Decimal(0.00)
                new_referencia=Decimal(0.00)
            
            """if 'USD' in asset_ppg["asset"]["code"]:
                print(asset_ppg["asset"]["code"])
                print(asset_ppg["referencia"],"<-->",new_referencia)
                print(asset_ppg["precio"],"<-->",new_precio)"""
                
            asset_ppg["precio"]=str(f"{new_precio.normalize():.7f}")
            asset_ppg["referencia"]=str(f"{new_referencia.normalize():.7f}")
        
        
        
        
    #----OBTENER OFERTAS
    # Este metodo consulta las ofertas, las registra en formato raw y custom
    # y devuelbe solo el custom
    def getOfertas(self)->list:
        self.oferta_raw=self.server.offers().for_seller(self.public).call()
        registros:list=self.oferta_raw["_embedded"]["records"] if len(self.oferta_raw["_embedded"]["records"])>0 else []
        lista:list=[]
        if len(registros)>0:
            for value in registros:
                ofertdata={
                    "id":value["id"],
                    "seller":value["seller"],
                    "sell_asset":{
                        "code":value["selling"]["asset_code"] if "asset_code" in value["selling"] else "XLM",
                        "issuer":value["selling"]["asset_issuer"] if "asset_issuer" in value["selling"] else "",
                        "type":value["selling"]["asset_type"]
                    },
                    "buy_asset":{
                        "code":value["buying"]["asset_code"] if "asset_code" in value["buying"] else "XLM",
                        "issuer":value["buying"]["asset_issuer"] if "asset_issuer" in value["buying"] else "",
                        "type":value["buying"]["asset_type"]
                    },
                    "nu_monto":value["amount"],
                    "nu_precio":value["price"],
                    "nu_total": format(float(value["amount"])*float(value["price"]),'.9f'),  
                }
                lista.append(ofertdata)



        self.oferta=lista

        return self.oferta 


    def getTrades(self)->list:
        self.trade_raw=self.server.trades().for_account(self.public).call()
        registros:list=self.trade_raw["_embedded"]["records"] if len(self.trade_raw["_embedded"]["records"])>0 else ""
        lista:list=[]
        if registros != "":
            for value in registros:
                operacion=self.getPagePayment(value["_links"]["operation"]["href"])
                if operacion["type"] == "manage_buy_offer":
                    ct="Compra"
                elif operacion["type"] == "manage_sell_offer":
                    ct="Venta"
                else:
                    ct=operacion["type"]
                tradedata={
                    "id":value["id"],
                    "counter_offer_id":value["counter_offer_id"],
                    "base_account":value["base_account"],
                    "base_asset_code":value["base_asset_code"] if "base_asset_code" in value else "XLM" ,
                    "base_asset_type":value["base_asset_type"],
                    "base_asset_issuer":value["base_asset_issuer"] if "base_asset_issuer" in value else "" ,
                    "base_amount":value["base_amount"],
                    "counter_account":value["counter_account"],
                    "counter_asset_code":value["counter_asset_code"] if "counter_asset_code" in value else "XLM" ,
                    "counter_asset_type":value["counter_asset_type"],
                    "counter_asset_issuer":value["counter_asset_issuer"] if "counter_asset_issuer" in value else "" ,
                    "counter_amount":value["counter_amount"],
                    "type":operacion["type"],
                    "type_corto":ct,
                    "nu_precio":operacion["price"] if "price" in operacion else "0",
                    "fecha":getFecha(value["ledger_close_time"]) if "ledger_close_time" in value else "",
                    "transaction_hash":operacion["transaction_hash"]
                }
                lista.append(tradedata)

        self.trade=lista

        return self.trade
        
    @staticmethod
    def getTokenDisponibles(MainTb044Keystore):
        server=Server("https://horizon.stellar.org")
        account = server.accounts().account_id(MainTb044Keystore.tx_public_key).call()
        lista=[]
        for value in account['balances']:
            if 'asset_code' in value:
                lista.append(value['asset_code'])
            else:
                lista.append('XLM')
        return lista
