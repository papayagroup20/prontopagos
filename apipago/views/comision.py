from rest_framework import viewsets
from main.models import WalletTb014Comision
from django.db.models import Prefetch,Q
from apipago.serializers.general import ComisionSerializer 
from papaya_pago.CustomAuth import InConfirmado
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from django.http import Http404

class ComisionViewSet(viewsets.ModelViewSet):
    queryset = WalletTb014Comision.objects.all()
    serializer_class = ComisionSerializer
    permission_classes = [InConfirmado]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['created_at']

    def list(self, request, *args, **kwargs):
        raise Http404

    def detail(self, request, pk=None):
        raise Http404

    def create(self, request):
        raise Http404

    def update(self, request, pk=None):
        raise Http404

    def partial_update(self, request, pk=None):
        raise Http404

    def destroy(self, request, pk=None):
        raise Http404