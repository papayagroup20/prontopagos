from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action
from main.form.general import RegistroBasicoForm
from django.db import IntegrityError, transaction

from main.models import (
    Tb001Usuario,MainTb023Rol)


class UsuarioViewSet(viewsets.ViewSet):
    permission_classes = [AllowAny]

    @action(detail=False, methods=['post'],url_path='crm-check-user', url_name='crm-check-user')
    def crm_check_user(self, request):
        content={
            "success":True,
            "error":''
        }
        print("papaya")
        try:
            data=request.data
            if 'co_usuario' in data:
                if not Tb001Usuario.objects.filter(co_usuario_crm=data['co_usuario']).exists():
                    content['success']=False

            else:
                content['success']=False
        except Exception as e:
            print(e)
            content['success']=False
            content['error']='Ha ocurrido un error'
        return Response(content)

    @action(detail=False, methods=['post'],url_path='crm-create-user', url_name='crm-create-user')
    def crm_create_user(self, request):
        content={
            "success":True,
            "error":''
        }
        try:
            with transaction.atomic():
                datos=request.data
                form=RegistroBasicoForm(datos['form'])
                if not form.is_valid():
                    content["success"]=False
                    content["error"]=form.errors
                    content["errorForm"]=form.errors
                    return Response(content)

                usuario=Tb001Usuario(
                    username=form.cleaned_data['username'].upper(),
                    email=form.cleaned_data['email'],
                    co_rol=MainTb023Rol.objects.get(co_rol=7)
                )
                usuario.save()

                if 'co_usuario' in datos:
                    if not Tb001Usuario.objects.filter(co_usuario_crm=datos['co_usuario']).exists():
                        content['success']=False
                else:
                    content['success']=False
        except Exception as e:
            print(e)
            content['success']=False
            content['error']='Ha ocurrido un error'
        return Response(content)

    @action(detail=False, methods=['get'],url_path='test', url_name='test')
    def test(self, request):
        content={
            "success":True
        }
        print("papaya")

        return Response(content)
