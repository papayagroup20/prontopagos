from rest_framework.authentication import (
    TokenAuthentication,SessionAuthentication,BasicAuthentication)
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import AuthenticationFailed
import requests as peticion
from datetime import timedelta
from django.utils import timezone
from django.conf import settings
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action,api_view,renderer_classes,api_view,permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAuthenticated
from papaya_pago.CustomAuth import InConfirmado
from main.models import Tb001Usuario
from main.form.general import LoginForm
from apipago.serializers.auth import UsuarioBasicSerializer
from hashlib import sha256
from typing import Union,Dict
from main.views.utils import getTokenUsuario
import json
def expires_in(token):
    time_elapsed = timezone.now() - token.created
    left_time = timedelta(seconds = settings.TOKEN_EXPIRED_AFTER_SECONDS) - time_elapsed
    return left_time

# token checker if token expired or not
def is_token_expired(token):
    return expires_in(token) < timedelta(seconds = 0)

# if token is expired new token will be established
# If token is expired then it will be removed
# and new one with different key will be created
def token_expire_handler(token):
    is_expired = is_token_expired(token)
    if is_expired:
        token.delete()
        token = Token.objects.create(user = token.user)
    return is_expired, token

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@permission_classes((AllowAny,))
def comprobar2FA(request):
    content={
        'success':False,
        'in_2fa':False,
        'data':''
    }
    if Tb001Usuario.objects.filter(username=request.POST['username'],in_2fa=True).count()>0:
        content['success']=True
        content['in_2fa']=True

    return Response(content)

#________________________________________________
#DEFAULT_AUTHENTICATION_CLASSES
class ExpiringTokenAuthentication(TokenAuthentication):
    """
    If token is expired then it will be removed
    and new one with different key will be created
    """
    def authenticate_credentials(self, key):
        try:
            token = Token.objects.get(key = key)
        except Token.DoesNotExist:
            raise AuthenticationFailed("Invalid Token")
        
        if not token.user.is_active:
            raise AuthenticationFailed("User is not active")

        is_expired, token = token_expire_handler(token)
        if is_expired:
            raise AuthenticationFailed("The Token is expired")
        
        return (token.user, token)

class ValidarToque(APIView):
    permission_classes = [IsAuthenticated]
    def post(self, request, format=None):
        content={
            "success":True
        }
        return Response(content)


#--------CLASE BASE DE AUTENTICACION API
# Esta clase hasta ahora la que de las operaciones basicas de autenticar 
# con keystore a traves de api
class AuthBasico(viewsets.ViewSet):
    authentication_classes = [
        ExpiringTokenAuthentication,BasicAuthentication,
        SessionAuthentication]
    permission_classes = [AllowAny]
    
    #---------Autenticacion del keystore
    #Este metodo es practicamente la autenticacion del usuario 
    # y debuelve un diccionario con el par de llave
    @staticmethod
    def autenticacionKeystore(data:Dict[Tb001Usuario,str])->Union[dict,None]:
        user=data["username"]
        password=data["password"]
        keystore=user.keystore_usuario.first()
        try:
            token=user.auth_token
            token.delete()
        except:
            pass
        tx=sha256(bytes(password,"ASCII"))

        key2=None

        header={
        "Connection":"Close",
        "Content-Type":"application/x-www-form-urlencoded",
        }
        pas2=password+"_prontopagos"
        txh2=sha256(bytes(pas2,"ASCII"))
        url3="http://127.0.0.1:3512/api/keypair/"
        dataget2={
            "address":keystore.address,
            "pass":tx.hexdigest(),
            "pass2":txh2.hexdigest()
        }
        gett3=peticion.post(url3,params=dataget2,headers=header)
        key2=gett3
        if key2.text:
            if "error" not in key2.text:
                return {
                    "public":keystore.address,
                    "secret":key2.text
                }
            else:
               return None
        else:
            return None
            
    #--------Login
    # Este metodo es practivamente el proceso del login 
    # y devuelve e inyecto un diccionario con los datos del usuario, su keystore 
    # y su token de autenticacion.
    def login(self,request)->dict:
        form=LoginForm(request.data)
        context={"success":True}
        if form.is_valid():
            user=form.cleaned_data["username"]
            try:
                token=user.auth_token
                token.delete()
            except:
                pass
            
            serializer=UsuarioBasicSerializer(user)
            context["datos"]=serializer.data
            context["token"]=getTokenUsuario(user).key

        else:
            errores=json.loads(form.errors.as_json(escape_html=False))
            list_error=[]
            data_error={}
            for err in errores.items():
                data_error[err[0]]=[err[1][0]["message"]]

            context["success"]=False
            context["input_error"]=data_error
            #context["error"]="Autenticacion fallida"
        
        return context
        



class AuthPPGPay(AuthBasico):

    def get_permissions(self):
        if self.action == "list":
            
            
            return [InConfirmado()]
        else:
            return [AllowAny()]
        #return [permission() for permission in self.permission_classes]

    def create(self,request):
        context=self.login(request)
        return Response(context)

    def list(self,request):
        
        return Response({'success':True})
    
    @action(detail=False, methods=['get'],url_path='unconfirmed', url_name='unconfirmed')
    def checkUnConfirmado(self, request):
        return Response({'success':True})

"""
Formato de errores de validacion:
    {
        "numbre_campo_error":[
            "error1",
            "error2"
        ]
    }
"""