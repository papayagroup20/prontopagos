from main.models import *
from rest_framework.response import Response
from apipago.serializers import *
from rest_framework.decorators import action,api_view,renderer_classes,permission_classes
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from rest_framework.renderers import JSONRenderer
import requests
from stellar_sdk.server import Server
from stellar_sdk.asset import Asset
from stellar_sdk.sep.stellar_toml import fetch_stellar_toml
from main.views.usuario import notificacion_email
from django.utils.crypto import get_random_string
from django.contrib.auth.decorators import login_required,user_passes_test
from django.db import IntegrityError, transaction
from django.views.decorators.http import require_http_methods
from stellar_sdk.keypair import Keypair
from main.views.utils import *
import requests
import numpy as np
from requests.exceptions import ConnectionError
from urllib.parse import urlparse
from django.views.decorators.cache import cache_page
from decimal import Decimal,getcontext



def getServer():
    #server = Server("https://horizon-testnet.stellar.org")
    server = Server("https://horizon.stellar.org")
    return server


@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def validarActivo(request):
    content={
            'success':True,
            'data':'',
        }

    if WalletTb028Activos.objects.filter(tx_asset_code=request.POST['tx_asset_code'],tx_asset_issuer=request.POST['tx_asset_issuer']).count()<=0:
       ActivoStellar=Asset(request.POST['tx_asset_code'],request.POST['tx_asset_issuer'])

       #NOTIFICACION
       usuario=request.user
       detalles={"username":usuario.username, "codigo":request.POST['tx_asset_code'], "distribuidor":request.POST['tx_asset_issuer']}
       notificacion_email(usuario, detalles, 'notificaciones/email_notif_add_activos.html')

       """server = getServer
       account = server.accounts().account_id(request.POST['tx_asset_issuer']).call()
       
       content['data']=account['balances']"""

    else:
        activo=WalletTb028Activos.objects.filter(tx_asset_code=request.POST['tx_asset_code'],tx_asset_issuer=request.POST['tx_asset_issuer'])[0]

        if WalletTb029UsuarioActivos.objects.filter(
                co_activo=activo.co_activo,
                co_usuario=request.user.co_usuario).count()>=0:

                content['success']=False
                content['msg']="Ya posees este activo registrado"

        
    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def ShowPrivateKey(request):
    content={
            'success':True,
            'data':'',
        }
    
    secreto=Keypair.from_mnemonic_phrase(request.POST['tx_frase'])
    billetera=MainTb044Keystore.objects.filter(co_usuario=request.user.co_usuario)[0]
    if secreto.public_key==billetera.address:
        content['data']=secreto.secret
    else:
        content['success']=False
    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@permission_classes((AllowAny,))
def ValidarFrase(request):
    content={
            'success':True,
            'data':'',
        }
    
    secreto=Keypair.from_mnemonic_phrase(request.POST['tx_frase'])
    if WalletTb037Keystore.objects.filter(address=secreto.public_key).count()>0:
        keystore=WalletTb037Keystore.objects.filter(address=secreto.public_key)[0]
        if keystore.co_usuario is not None:

            if keystore.co_usuario.in_activo:


                if keystore.co_usuario.in_confirmado:

                    token, _ = Token.objects.get_or_create(user = keystore.co_usuario)
                    content['token']=token.key                          
                else:
                    content['success']=False
                    content['msg']="Error el usuario aun no esta validado"
            else:
                content['success']=False
                content['msg']="Error el usuario no esta activo"


            
        else:
            content['success']=False
            content['msg']="El usuario no existe"

    else:
        content['success']=False
        content['msg']="Frase incorrecta"
        
    return Response(content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def GenerateFrase(request):
    content={
            'success':True,
            'data':'',
        }
    
    
    frase=Keypair.generate_mnemonic_phrase('spanish',128)
    secreto=Keypair.from_mnemonic_phrase(frase)

    content['data']=frase
    content['public_key']=secreto.public_key

    return Response(content)

def getStellarAccount(public):
    try:
        server = getServer()
        cuenta = server.accounts().account_id(public)
        c=cuenta.call()
        return c
    except:
        return None 

def getTokenDisponibles(MainTb044Keystore):
    
    server = getServer()
    account = server.accounts().account_id(MainTb044Keystore.tx_public_key).call()
    lista=[]
    for value in account['balances']:
        if 'asset_code' in value:
            lista.append(value['asset_code'])
        else:
            lista.append('XLM')
    return lista

#@cache_page(60 * 15)
def getAsset(code:str,issue:str=None):
    server =getServer()
    if issue is not None:
        asset=server.assets()\
            .for_code(code)\
            .for_issuer(issue)\
            .call()
    else:
        asset=server.assets()\
        .for_code(code)\
        .call()

    return asset
    
#@cache_page(60 * 15)
def getTomlAsset(code:str,issue:str=None)->list:
    assetjs=getAsset(code,issue)
    #print(assetjs)
    tx=assetjs["_embedded"]["records"][0] if len(assetjs["_embedded"]["records"])>0 else ""
    content={
        "tx_asset":"",
        "org_url":"",
        "org_desc":"",
        "asset_desc":"",
        "asset_conditions":"",
        "name":"",
        "issue":"",
        "code":"",
        "img":""
    }
    if tx != "":
        if "toml" in tx["_links"]:
            #print(tx["_links"]["toml"]["href"])
            if tx["_links"]["toml"]["href"] != "":
                #print(tx["_links"]["toml"]["href"])
                try: 
                    url=urlparse(tx["_links"]["toml"]["href"])
                    toml=fetch_stellar_toml(url.netloc)
                    org_url=toml["DOCUMENTATION"]["ORG_URL"] if "ORG_URL" in toml["DOCUMENTATION"] else ""
                    org_desc=toml["DOCUMENTATION"]["ORG_DESCRIPTION"] if "ORG_DESCRIPTION" in toml["DOCUMENTATION"] else ""
                    
                    for value in toml["CURRENCIES"]:
                    #print(value)
                        if value["code"]==code and value["issuer"]==issue:
                            #print(value["name"])
                            tx_code=value["code"] if "code" in value else ""
                            tx_issuer=value["issuer"] if "issuer" in value else ""

                            cont={
                                "tx_asset":tx_code+":"+tx_issuer,
                                "org_url":org_url if org_url == "" else urlparse(org_url).netloc,
                                "org_desc":org_desc,
                                "asset_desc": value["desc"] if "desc" in value else "",
                                "asset_conditions": value["conditions"] if "conditions" in value else "",
                                "name":value["name"] if "name" in value else "",
                                "issue":value["issuer"] if "issuer" in value else "",
                                "code":value["code"] if "code" in value else "",
                                "img":value["image"] if "image" in value else ""
                            }
                            #print(cont)
                            content=cont

                except:
                    pass



                #print(url.netloc)
                
                #print(content)

    
    return content

def getAssetAccountToml(public:str,in_oferta:bool=None)->list:
    content={
        "data":[],
        "oferta":""
    }
    server =getServer()

    toml={
        "tx_asset":"",
        "org_url":"",
        "org_desc":"",
        "asset_desc":"",
        "asset_conditions":"", 
        "tx_asset_issuer":"",
        "nu_balance":"",
        "nu_limit":"",
        "tx_asset_code":"",  
        "tx_moneda":"",
        "img":"",
        "asset_type":""
    }
    montoresta=0
    reserva_por_oferta=0
    if in_oferta is not None:
        try:
            oferta=getOfertasAccount(public)
            content["oferta"]=oferta
        except:
            pass
    try: 
        account = server.accounts().account_id(public).call()
        recerva=(2+account["subentry_count"])*0.5

        for values in account['balances']:
            if values["asset_type"] != "native":
                tx_asset_issuer=values["asset_issuer"]
                tx_asset_code=values["asset_code"]

                toml=getTomlAsset(tx_asset_code,tx_asset_issuer)
                #print(toml)

                name=toml["name"]
                org_url=toml["org_url"]
                org_desc=toml["org_desc"]
                asset_desc=toml["asset_desc"]
                asset_conditions=toml["asset_conditions"]
                #montoresta+=0.5

            else:
                org_url=""
                org_desc=""
                asset_desc=""
                asset_conditions=""
                tx_asset_issuer=""
                tx_asset_code="XLM"
                name="Lumens"
                #montoresta+=1


            datos={
                "tx_asset":tx_asset_code+":"+tx_asset_issuer,
                "org_url":org_url,
                "org_desc":org_desc,
                "asset_desc":asset_desc,
                "asset_conditions":asset_conditions, 
                "tx_asset_issuer":tx_asset_issuer,
                "nu_balance":values["balance"],
                "nu_limit":values["limit"] if "limit" in values else "-",
                "tx_asset_code":tx_asset_code,  
                "tx_moneda":name,
                "img":toml["img"],
                "asset_type":values["asset_type"],
                "propio":True
            }

            content['data'].append(datos)
       # content['data2']=content['data']
        j=0
        for va in content['data']:
            if va["asset_type"] == "native":
                content["data"][j]["nu_balance"]=float(content["data"][j]["nu_balance"])-float(recerva)
            j+=1
        #print(content['data'])
        

    except Exception as e:
        print(e)


    
    
    return content

def getFederacion(federacion):
    try:
        lista=federacion.split("*")
        toml=fetch_stellar_toml(lista[1])
        if "FEDERATION_SERVER" in toml:
            payload = {'q': federacion, 'type': 'name'}
            res=requests.get(toml["FEDERATION_SERVER"],params=payload)
            resjson=res.json()
            if res!="":
                return res.json() 
        return None
    except:
        return None


def forDataPayment(value,tipo,public,toml):
    codigo=value["id"]
    img=""
    moneda="XLM"
    moneda2="Lumens"
    if  "from" in value:
        inde = value["from"] if value["to"] == public else value["to"]
        if value["asset_type"] != "native":
            moneda=value["asset_code"]
            moneda2=moneda
            if "name" in toml:
                moneda2=toml["name"]
                img=toml["img"]
                
        
        monto=value["amount"]

    elif value["type"] == "account_merge":
        inde =  public
        moneda="todas"
        monto="todo"
    else:
        inde = value["funder"]
        monto=value["starting_balance"]

    server =getServer()
    transa=server.transactions().transaction(value["transaction_hash"]).call()
    
    



    memo = transa["memo"] if "memo" in transa else ""
    
    fecha=getFecha(value["created_at"])
    
    datos={
        "id":codigo,
        "monto":monto,
        "tipo":tipo,
        "direccion":getPublicRecortado(inde),
        "moneda":moneda,
        "moneda2":moneda2,
        "img":img,
        "fecha":fecha,
        "public":inde,
        "transaccion":value["transaction_hash"],
        "memo":memo

    }
    return datos

def getPagePayment(url):
    res=requests.get(url,None)
    return res.json()

def getBalanceInit(public):
    server =getServer()
    operation=server.payments()
    operaciones=operation.for_account(public)
    #operaciones._check_pageable(operaciones.order().call())
    #print(operaciones.next())
    return operaciones.order().call()

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def GetBalance(request):
    content={
            'success':True,
            'data':[],
            "next":"",
            "prev":"",
            "ultimo":False

        }
    listAsset={
        "asset":[]
    }
    #public=billetera.tx_public_key
    #public="GAP5LETOV6YIE62YAM56STDANPRDO7ZFDBGSNHJQIYGGKSMOZAHOOS2S"
    public=request.user.getPublic()

    if request.POST["tipo_page"] == "0":
        tx=getBalanceInit(public)
    elif request.POST["tipo_page"] == "1":
        tx=getPagePayment(request.POST['page'])


    lista=tx["_embedded"]["records"]
    #print(lista)
    
    page=tx["_links"]["next"]["href"]
    page2=tx["_links"]["prev"]["href"]

    i=0
    fin=False
    inibo=False
    in_ultimo=False
    paso=request.POST["paso"]
    continuar=False if paso == "1" else True
    j=1
    in_repetido=False
    while len(content['data'])<10:

        if len(lista)<10:
            fin=True
        j=1
        for value in lista:

            if len(content['data'])<10:
                if  "from" in value:
                    if value["from"] == public:
                        #print(public)
                        tipo = "Envio" if value["to"]!="GDMWUBW476J2NO53ID6OPUA4CQF5ID7RIIQUZH4KDSMVAJKZLS2YOJ4I" else "Comisión"
                    else:
                        tipo ="Recibido"


                elif value["type"] == "account_merge":
                    tipo = "Eliminar cuenta"
                else:
                    tipo = "Crear cuenta"
                
                
                toml={}
                if "asset_type" in value:
                #COMPROBAR ASSET REPETIDOS PARA NO VOLVER A CONSULTARLOS
                    if len(listAsset["asset"])>0 and value["asset_type"] != "native":
                        for va in listAsset["asset"]:
                            if value["asset_code"] == va["code"] and value["asset_issuer"] == va["issue"]:
                                in_repetido=True
                                toml={
                                    "name":va["name"],
                                    "issue":va["issue"],
                                    "code":va["code"],
                                    "img":va["img"]
                                }



                        if not in_repetido:
                        
                            toml=getTomlAsset(value["asset_code"],value["asset_issuer"])
                            listAsset["asset"].append({"code":value["asset_code"],"issue":value["asset_issuer"],"name":toml["name"],"img":toml["img"]})

                        else:
                            in_repetido=False 


                                    #print(toml)
                    elif value["asset_type"] != "native":

                            toml=getTomlAsset(value["asset_code"],value["asset_issuer"])
                            listAsset["asset"].append({"code":value["asset_code"],"issue":value["asset_issuer"],"name":toml["name"],"img":toml["img"]})
                        #print(toml)
                
                #print(listAsset["asset"])

                #FORMAR EL LISTADO DE VALORES SEGUN EL TIPO DE PAYMENTS
                if request.POST['tipo'] == "1":
                    content['data'].append(forDataPayment(value,tipo,public,toml))
                         
                elif request.POST['tipo'] == "2" and (tipo == "Envio" or  tipo == "Comisión" or tipo == "Eliminar cuenta") and continuar:
                    
                    content['data'].append(forDataPayment(value,tipo,public,toml))
                
                elif request.POST['tipo'] == "3" and (tipo == "Recibido" or tipo == "Crear cuenta") and continuar:
                    content['data'].append(forDataPayment(value,tipo,public,toml))



                if str(request.POST["codigo"]) == value["id"] and paso == "1":
                    continuar = True
                #print(""+str(len(content['data']))+"-"+str(j))
                if len(content['data'])>=10 and j<10:
                    in_ultimo=False
                elif len(content['data'])>=10 and j>=10:
                    in_ultimo=True

                j+=1

        
        #print(i)
        i+=1
        if fin:
            break
        if len(content['data'])<10:
            siguiente=getPagePayment(page)
            content['next']=page
            content['prev']=page2
            page=siguiente["_links"]["next"]["href"]
            page2=siguiente["_links"]["prev"]["href"]
            lista=siguiente["_embedded"]["records"]


        
        
    if in_ultimo:
        content['next']=page
        content['prev']=page2
    else:
        content["ultimo"]=True
        


    return Response(content)

def getTomlAsset2(code:str,issue:str,activo:WalletTb036Activos)->dict:
    assetjs=getAsset(code,issue)
    #print(assetjs)
    tx=assetjs["_embedded"]["records"][0] if len(assetjs["_embedded"]["records"])>0 else ""
    #print(tx)
    content=None
    try:
        if tx != "":
            if "toml" in tx["_links"]:
                if tx["_links"]["toml"]["href"] != "": 
                        url=urlparse(tx["_links"]["toml"]["href"])
                        toml=fetch_stellar_toml(url.netloc)
                        org_url=toml["DOCUMENTATION"]["ORG_URL"] if "ORG_URL" in toml["DOCUMENTATION"] else ""
                        org_desc=toml["DOCUMENTATION"]["ORG_DESCRIPTION"] if "ORG_DESCRIPTION" in toml["DOCUMENTATION"] else ""
                        uri_key=toml["URI_REQUEST_SIGNING_KEY"] if "URI_REQUEST_SIGNING_KEY" in toml else ""
                        in_concu=False
                        for value in toml["CURRENCIES"]:
                            if value["code"]==code and value["issuer"]==issue:
                                in_concu=True
                                #print(value)
                                #print(value["name"])
                                tx_code=value["code"] if "code" in value else ""
                                tx_issuer=value["issuer"] if "issuer" in value else ""

                                cont={
                                    "tx_asset":tx_code+":"+tx_issuer,
                                    "org_url":org_url if org_url == "" else urlparse(org_url).netloc,
                                    "org_desc":org_desc,
                                    "asset_desc": value["desc"] if "desc" in value else "",
                                    "asset_conditions": value["conditions"] if "conditions" in value else "",
                                    "name":value["name"] if "name" in value else "",
                                    "issue":value["issuer"] if "issuer" in value else "",
                                    "code":value["code"] if "code" in value else "",
                                    "img":value["image"] if "image" in value else "",
                                    "asset_type":value["anchor_asset_type"] if "anchor_asset_type" in value else "",
                                    "uri_key":uri_key
                                }

                                content=cont

                                stellartoml,_=WalletTb042StellarToml.objects.get_or_create(
                                    tx_org_url=content["org_url"]
                                )

                                if stellartoml.tx_org_desc!=content["org_desc"] or stellartoml.tx_uri_key!=content["uri_key"]:
                                    if stellartoml.tx_org_desc!=content["org_desc"]:
                                        stellartoml.tx_org_desc=content["org_desc"]

                                    if stellartoml.tx_uri_key!=content["uri_key"]:
                                        stellartoml.tx_uri_key=content["org_desc"]
                                    stellartoml.save()

                                if content["name"] is None or content["name"]=="":
                                    nombre=tx_code
                                else:
                                    nombre=content["name"] 
                                #Lo de aqui abajo esta mal, quizas haga falta una consulta arrecha
                                #print(activo)
                                if activo is not None:
                                    activo.toml_asset_desc=content["asset_desc"]
                                    activo.toml_asset_conditions=content["asset_conditions"]
                                    activo.toml_name=nombre
                                    activo.toml_img=content["img"]
                                    activo.co_stellar_toml=stellartoml
                                    activo.toml_asset_type=content["asset_type"]
                                    activo.save()
                                else:
                                    WalletTb036Activos.objects.create(
                                        tx_asset_code=content["code"],
                                        tx_asset_issuer=content["issuer"],
                                        toml_asset_desc=content["asset_desc"],
                                        toml_asset_conditions=content["asset_conditions"],
                                        toml_name=nombre,
                                        toml_img=content["img"],
                                        toml_asset_type=content["asset_type"],
                                        in_activo=False,
                                        in_principal=False,
                                        in_asset_ppg=False
                                    )
                        if not in_concu:
                            if activo is not None:
                                    activo.toml_name="sin_toml"
                                    activo.save()
                            else:
                                WalletTb036Activos.objects.create(
                                    tx_asset_code=code,
                                    tx_asset_issuer=issue,
                                    toml_name="sin_toml",
                                    in_activo=False,
                                    in_principal=False,
                                    in_asset_ppg=False
                                )


    except Exception as e:
        print(e)
    
    return content

def getAccountBalance(public:str,in_oferta:bool=None)->list:
    content={
        "data":[],
        "oferta":""
    }
    server =getServer()
    montoresta=0
    if in_oferta is not None:
        try:
            oferta=getOfertasAccount(public)
            content["oferta"]=oferta
        except:
            pass
    try: 
        account = server.accounts().account_id(public).call()
        recerva=(2+account["subentry_count"])*0.5
        for values in account['balances']:
            if values["asset_type"] != "native":
                tx_asset_issuer=values["asset_issuer"]
                tx_asset_code=values["asset_code"]
            else:
                tx_asset_issuer=""
                tx_asset_code="XLM"

            tx_asset=tx_asset_code+":"+tx_asset_issuer if tx_asset_issuer!="" else "XLM"

            datos={
                "tx_asset":tx_asset,
                "tx_asset_issuer":tx_asset_issuer,
                "org_url":"",
                "org_desc":"",
                "asset_desc":"",
                "asset_conditions":"",
                "nu_balance":values["balance"],
                "nu_balance2":str(round(Decimal(values["balance"]),2)),
                "nu_limit":values["limit"] if "limit" in values else "-",
                "tx_asset_code":tx_asset_code,  
                "tx_moneda":"",
                "img":"",
                "asset_type":values["asset_type"],
                "toml_asset_type":"",                   
                "in_activo":False,
                "propio":True
            }

            content['data'].append(datos)
       # content['data2']=content['data']
        j=0
        for va in content['data']:
            if va["asset_type"] == "native":
                content["data"][j]["nu_balance"]=float(content["data"][j]["nu_balance"])-float(recerva)
                va=round(Decimal(values["nu_balance"]),7)
                siete = f"{va:.7f}"
                content["data"][j]["nu_balance2"]=str(siete)
            j+=1
        #print(content['data'])
        

    except Exception as e:
        print(e)


    
    
    return content

def getActivoMioReco2(public:str,in_oferta:bool=None)->dict:
    activospropios=getAccountBalance(public,in_oferta)

    content={
        "success":True,
        "datos":[],
        "ofertas":activospropios["oferta"],
    }

    activos=WalletTb036Activos.objects\
        .select_related('co_stellar_toml')\
        .filter(in_live=True)\
        .only(
            'tx_asset_code','tx_asset_issuer','co_stellar_toml',
            'toml_name','toml_img','toml_asset_desc','toml_asset_conditions','tx_alias','toml_asset_type',
            'in_activo')\
        .order_by('nu_orden')
    lista_activos=[]
    #--------------Primer llenado del listado
    #---Aqui se llenara la lista con todos los datos de la base de datos
    
    #from django.db import connection
    if len(activos)>0:
            for values in activos:
                org_url = values.co_stellar_toml.tx_org_url if values.co_stellar_toml is not None else ""
                org_desc = values.co_stellar_toml.tx_org_desc if values.co_stellar_toml is not None else ""
                #print(values)
                """print('# of Queries: {}'.format(len(connection.queries)))
                for query in connection.queries:
                    print(query['sql'])"""
                if values.tx_alias is not None:
                    tx_moneda=values.tx_alias
                elif values.toml_name is not None:
                    tx_moneda=values.toml_name
                else:
                    tx_moneda=""
                data={
                    "tx_asset":values.tx_asset,
                    "org_url":org_url,
                    "org_desc":org_desc,
                    "asset_desc":values.toml_asset_desc if values.toml_asset_desc is not None else "",
                    "asset_conditions":values.toml_asset_conditions if values.toml_asset_conditions is not None else "",           
                    "tx_asset_issuer":values.tx_asset_issuer,
                    "nu_balance":"",
                    "nu_balance2":"",
                    "nu_limit":"",
                    "tx_asset_code":values.tx_asset_code,  
                    "tx_moneda":tx_moneda,
                    "img":values.toml_img if values.toml_img is not None else "",
                    "asset_type":"token" if values.tx_asset_issuer is not None else "native",
                    "toml_asset_type": values.toml_asset_type if values.toml_asset_type is not None else "",
                    "in_activo":values.in_activo,
                    "propio":False
                    }
                #print(data)
                lista_activos.append(data)

    in_repetido=False
    #--------------Segundo llenado del listado
    #Aqui se anexaran los datos del balance de la cuenta stellar del usuario,
    #si es que hay una coincidencia

    if len(activospropios["data"])>0:
        for act_propios in activospropios["data"]:
            in_repetido=False

            for val in lista_activos:
                if act_propios["tx_asset"]==val["tx_asset"]:
                    #print(val["toml_asset_type"])
                    val["nu_balance"]=act_propios["nu_balance"]
                    #print(str(Decimal(act_propios["nu_balance"]))+"-"+act_propios["nu_balance"]+"-"+str(float(act_propios["nu_balance"])))
                    va=round(Decimal(act_propios["nu_balance"]),7)
                    siete = f"{va:.7f}"
                    #print(Decimal("0.0000001"))
                    val["nu_balance2"]=act_propios["nu_balance2"] if val["toml_asset_type"]=="fiat" else str(siete)
                    val["nu_limit"]=act_propios["nu_limit"]
                    val["asset_type"]=act_propios["asset_type"]
                    val["propio"]=act_propios["propio"]
                    in_repetido=True
            if not in_repetido:
                lista_activos.append(act_propios)

    #--------------tercer llenado del listado
    #Aqui se eliminaran de la lista aquellos activos que no promosionamos
    #Y que el usuario no tiene en su poder
    for values in lista_activos:
        if  not values["propio"] and not values["in_activo"]:
            lista_activos.remove(values)
    
    #--------------cuarto llenado del listado
    #Aqui se agregaran los datos del toml a la base de datos en caso de no poseerlos
    #print(lista_activos)
    for values in lista_activos:
        if values["tx_moneda"]=="" and values["asset_type"]!="native":
            #print(values)
            act=activos.filter(
                    tx_asset_code=values["tx_asset_code"],
                    tx_asset_issuer=values["tx_asset_issuer"]
                    ).first()
            data=getTomlAsset2(values["tx_asset_code"],values["tx_asset_issuer"],act)
            if data is not None:
                #print(values)
                values["org_url"]=data["org_url"]
                values["org_desc"]=data["org_url"]
                values["asset_desc"]=data["asset_desc"]
                values["asset_conditions"]=data["asset_conditions"]
                values["tx_moneda"]=data["name"]
                values["img"]=data["img"]
                if values["toml_asset_type"]=="fiat" and values["nu_balance"]!="":
                    values["nu_balance2"]=str(round(Decimal(values["nu_balance"]),2))
                   
                    

    
    content["datos"]=lista_activos
    return content

def getActivoMio(public:str,in_oferta:bool=None)->dict:
    activospropios=getAccountBalance(public,in_oferta)
    list_code_propios=[]
    list_issuer_propios=[]
    for val in activospropios["data"]:
        list_code_propios.append(val["tx_asset_code"])
        list_issuer_propios.append(val["tx_asset_issuer"])
    #from django.db import connection
    activos=WalletTb036Activos.objects\
        .select_related('co_stellar_toml')\
        .filter(tx_asset_code__in=list_code_propios,tx_asset_issuer__in=list_issuer_propios)\
        .only(
            'tx_asset_code','tx_asset_issuer','co_stellar_toml',
            'toml_name','toml_img','toml_asset_desc','toml_asset_conditions','tx_alias',
            'in_activo')\
        .order_by('-nu_orden')
        
    #print('# of Queries: {}'.format(len(connection.queries)))
    
    """for query in connection.queries:
        print(query['sql'])"""
    #lista_activos=[]

    if len(activospropios["data"])>0:
        for act_propios in activospropios["data"]:
            for val in activos:
                if act_propios["tx_asset"]==val.tx_asset:
                    if val.tx_alias is not None:
                        tx_moneda=val.tx_alias
                    elif val.toml_name is not None:
                        tx_moneda=val.toml_name
                    else:
                        tx_moneda=""
                    act_propios["asset_desc"]=val.toml_asset_desc if val.toml_asset_desc is not None else ""
                    act_propios["asset_conditions"]=val.toml_asset_conditions if val.toml_asset_conditions is not None else ""
                    act_propios["tx_moneda"]=tx_moneda
                    act_propios["img"]=val.toml_img if val.toml_img is not None else ""
                    act_propios["org_url"]=val.co_stellar_toml.tx_org_url if val.co_stellar_toml is not None else ""
                    act_propios["org_desc"]=val.co_stellar_toml.tx_org_desc if val.co_stellar_toml is not None else ""
                    act_propios["in_activo"]=val.in_activo
                    
    
    #--------------cuarto llenado del listado
    #Aqui se agregaran los datos del toml a la base de datos en caso de no poseerlos
    #print(lista_activos)
    for values in activospropios["data"]:
        if not values["in_activo"] and values["asset_type"]!="native":
            activospropios["data"].remove(values)

    for values in activospropios["data"]:
        if values["tx_moneda"]=="" and values["asset_type"]!="native":
            #print(values)
            act=activos.filter(
                    tx_asset_code=values["tx_asset_code"],
                    tx_asset_issuer=values["tx_asset_issuer"]
                    ).first()
            data=getTomlAsset2(values["tx_asset_code"],values["tx_asset_issuer"],act)
            if data is not None:
                values["org_url"]=data["org_url"]
                values["org_desc"]=data["org_url"]
                values["asset_desc"]=data["asset_desc"]
                values["asset_conditions"]=data["asset_conditions"]
                values["tx_moneda"]=data["name"]
                values["img"]=data["img"]
    
    return activospropios


def getActivoMioReco(public):
    i=1
    activospropios=getAssetAccountToml(public)

    content={
        "success":True,
        "datos":[],
        "ofertas":activospropios["oferta"],
    }

    activos=WalletTb036Activos.objects\
        .filter(in_activo=True,in_live=True)\
        .only('tx_asset_code','tx_asset_issuer')\
        .order_by('-nu_orden')
    in_nativo=True
    if len(activos)>0:
    
        for values in activos:
            in_repetido=False
            if  len(activospropios["data"])>0:
                for v in activospropios["data"]:
                    if values.tx_asset_code==v["tx_asset_code"] and values.tx_asset_issuer==v["tx_asset_issuer"]:
                        content["datos"].append(v)
                        in_repetido=True
                    if v["tx_asset_code"]=="XLM" and in_nativo:
                        content["datos"].append(v)
                        in_nativo=False

                        

            if not in_repetido:
                toml=getTomlAsset(values.tx_asset_code,values.tx_asset_issuer)
                dada={
                        "tx_asset":values.tx_asset_code+":"+values.tx_asset_issuer,
                        "org_url":toml["org_url"],
                        "org_desc":toml["org_desc"],
                        "asset_desc":toml["asset_desc"],
                        "asset_conditions":toml["asset_conditions"],           
                        "tx_asset_issuer":values.tx_asset_issuer,
                        "nu_balance":"",
                        "nu_balance2":"",
                        "nu_limit":"",
                        "tx_asset_code":values.tx_asset_code,  
                        "tx_moneda":toml["name"],
                        "img":toml["img"],
                        "asset_type":"token"
                    }
                content["datos"].append(dada)
    return content


@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def getActivo(request):
    if "in_oferta" in request.POST:
        in_oferta=request.POST["in_oferta"]
    else:
        in_oferta=None
    activospropios=getAccountBalance(request.session["public_key"],in_oferta)

    content={
        "success":True,
        "datos":[],
        "ofertas":activospropios["oferta"],
    }

    activos=WalletTb036Activos.objects\
        .select_related('co_stellar_toml')\
        .filter(in_live=True)\
        .only(
            'tx_asset_code','tx_asset_issuer','co_stellar_toml',
            'toml_name','toml_img','toml_asset_desc','toml_asset_conditions','tx_alias',
            'in_activo')\
        .order_by('-nu_orden')
    lista_activos=[]
    #--------------Primer llenado del listado
    #---Aqui se llenara la lista con todos los datos de la base de datos
    
    #from django.db import connection
    if len(activos)>0:
            for values in activos:
                org_url = values.co_stellar_toml.tx_org_url if values.co_stellar_toml is not None else ""
                org_desc = values.co_stellar_toml.tx_org_desc if values.co_stellar_toml is not None else ""
                #print(values)
                """print('# of Queries: {}'.format(len(connection.queries)))
                for query in connection.queries:
                    print(query['sql'])"""
                if values.tx_alias is not None:
                    tx_moneda=values.tx_alias
                elif values.toml_name is not None:
                    tx_moneda=values.toml_name
                else:
                    tx_moneda=""
                data={
                    "tx_asset":values.tx_asset,
                    "org_url":org_url,
                    "org_desc":org_desc,
                    "asset_desc":values.toml_asset_desc if values.toml_asset_desc is not None else "",
                    "asset_conditions":values.toml_asset_conditions if values.toml_asset_conditions is not None else "",           
                    "tx_asset_issuer":values.tx_asset_issuer,
                    "nu_balance":"",
                    "nu_balance2":"",
                    "nu_limit":"",
                    "tx_asset_code":values.tx_asset_code,  
                    "tx_moneda":tx_moneda,
                    "img":values.toml_img if values.toml_img is not None else "",
                    "asset_type":"token" if values.tx_asset_issuer is not None else "native",
                    "toml_asset_type": values.toml_asset_type if values.toml_asset_type is not None else "",
                    "in_activo":values.in_activo,
                    "propio":False
                    }
                lista_activos.append(data)

    in_repetido=False
    #--------------Segundo llenado del listado
    #Aqui se anexaran los datos del balance de la cuenta stellar del usuario,
    #si es que hay una coincidencia

    if len(activospropios["data"])>0:
        for act_propios in activospropios["data"]:
            in_repetido=False
            for val in lista_activos:
                if act_propios["tx_asset"]==val["tx_asset"]:
                    val["nu_balance"]=act_propios["nu_balance"]
                    va=round(Decimal(act_propios["nu_balance"]),7)
                    siete = f"{va:.7f}"
                    val["nu_balance2"]=act_propios["nu_balance2"] if val["toml_asset_type"]=="fiat" else str(siete)
                    val["nu_limit"]=act_propios["nu_limit"]
                    val["asset_type"]=act_propios["asset_type"]
                    val["propio"]=act_propios["propio"]
                    in_repetido=True
            if not in_repetido:
                lista_activos.append(act_propios)

    #--------------tercer llenado del listado
    #Aqui se eliminaran de la lista aquellos activos que no promosionamos
    #Y que el usuario no tiene en su poder
    for values in lista_activos:
        if  not values["propio"] and not values["in_activo"]:
            lista_activos.remove(values)
    
    #--------------cuarto llenado del listado
    #Aqui se agregaran los datos del toml a la base de datos en caso de no poseerlos
    #print(lista_activos)
    for values in lista_activos:
        if values["tx_moneda"]=="" and values["asset_type"]!="native":
            #print(values)
            act=activos.filter(
                    tx_asset_code=values["tx_asset_code"],
                    tx_asset_issuer=values["tx_asset_issuer"]
                    ).first()
            data=getTomlAsset2(values["tx_asset_code"],values["tx_asset_issuer"],act)
            if data is not None:
                values["org_url"]=data["org_url"]
                values["org_desc"]=data["org_url"]
                values["asset_desc"]=data["asset_desc"]
                values["asset_conditions"]=data["asset_conditions"]
                values["tx_moneda"]=data["name"]
                values["img"]=data["img"]
    
    content["datos"]=lista_activos


    return Response(content)

def getActivoRecomendados(publico,request):
 
    p=publico
    activospropios=getAssetAccountToml(p)

    content={
        "success":True,
        "datos":[],
        "oferta":activospropios["oferta"]
    }

    i=1
    activos=WalletTb036Activos.objects.filter(in_principal=True)
    if activos.count()>0:
    
        for values in activos:
            in_repetido=False
            if  len(activospropios["data"])>0:
                for v in activospropios["data"]:
                    if values.tx_asset_code==v["tx_asset_code"] and values.tx_asset_issuer==v["tx_asset_issuer"]:
                        in_repetido=True
            if not in_repetido and i<=3:
                toml=getTomlAsset(values.tx_asset_code,values.tx_asset_issuer)
                dada={
                        "org_url":toml["org_url"],
                        "org_desc":toml["org_desc"],
                        "asset_desc":toml["asset_desc"],
                        "asset_conditions":toml["asset_conditions"],           
                        "tx_asset_issuer":values.tx_asset_issuer,
                        "nu_balance":"",
                        "nu_limit":"",
                        "tx_asset_code":values.tx_asset_code,  
                        "tx_moneda":toml["name"],
                        "img":toml["img"],
                        "asset_type":"token"
                    }
                content["datos"].append(dada)
                i+=1

            
        

    

    return content





@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def getActivoAccount(request):
    
    activos=getAssetAccountToml(request.POST["publico"])
    content={
        "success":True,
        "datos":activos["data"],
        "oferta":activos["oferta"]
    }
    
    return Response(content)

def getTradesAccount(public):
    content={
        "success":True,
        "next":"",
        "datos":[]
    }
    server =getServer()
    trade=server.trades().for_account(public).call()
    tradedata={
        "id":"",
        "offer_id":"",
        "base_account":"",
        "base_asset_type":"",
        "base_asset_code":"",
        "base_asset_issuer":"",
        "base_amount":"",
        "counter_account":"",
        "counter_asset_type":"",
        "counter_asset_code":"",
        "counter_asset_issuer":"",
        "counter_amount":"",
        "type":"",
        "type_corto":"",
        "nu_precio":"",
        "fecha":"",
        "transaction_hash":"",
    }
    if "_links" in trade:
        content["next"]=trade["_links"]["next"]["href"] 
    registros=trade["_embedded"]["records"] if len(trade["_embedded"]["records"])>0 else ""
    
    if registros != "":
        for value in registros:
            operacion=getPagePayment(value["_links"]["operation"]["href"])
            if operacion["type"] == "manage_buy_offer":
                ct="Compra"
            elif operacion["type"] == "manage_sell_offer":
                ct="Venta"
            else:
                ct=operacion["type"]
            tradedata={
                "id":value["id"],
                "offer_id":value["offer_id"],
                "base_account":value["base_account"],
                "base_asset_code":value["base_asset_code"] if "base_asset_code" in value else "XLM" ,
                "base_asset_type":value["base_asset_type"],
                "base_asset_issuer":value["base_asset_issuer"] if "base_asset_issuer" in value else "" ,
                "base_amount":value["base_amount"],
                "counter_account":value["counter_account"],
                "counter_asset_code":value["counter_asset_code"] if "counter_asset_code" in value else "XLM" ,
                "counter_asset_type":value["counter_asset_type"],
                "counter_asset_issuer":value["counter_asset_issuer"] if "counter_asset_issuer" in value else "" ,
                "counter_amount":value["counter_amount"],
                "type":operacion["type"],
                "type_corto":ct,
                "nu_precio":operacion["price"] if "price" in operacion else "0",
                "fecha":getFecha(value["ledger_close_time"]) if "ledger_close_time" in value else "",
                "transaction_hash":operacion["transaction_hash"]
            }
            content["datos"].append(tradedata)



    return content 
    
def getOfertasAccount(public):
    content={
        "success":True,
        "next":"",
        "datos":[]
    }
    
    server =getServer()
    
    oferta=server.offers().account(public).call()
    ofertdata={
        "id":"",
        "seller":"",
        "sell_asset_type":"",
        "sell_asset_code":"",
        "sell_asset_issuer":"",
        "buy_asset_type":"",
        "buy_asset_code":"",
        "buy_asset_issuer":"",
        "nu_monto":"",
        "nu_precio":"",
        "nu_total":"",  
    }
    if "_links" in oferta:
        content["next"]=oferta["_links"]["next"]["href"] 
    registros=oferta["_embedded"]["records"] if len(oferta["_embedded"]["records"])>0 else ""
    
    if registros != "":
        for value in registros:
            ofertdata={
                "id":value["id"],
                "seller":value["seller"],
                "sell_asset_type":value["selling"]["asset_type"],
                "sell_asset_code":value["selling"]["asset_code"] if "asset_code" in value["selling"] else "XLM",
                "sell_asset_issuer":value["selling"]["asset_issuer"] if "asset_issuer" in value["selling"] else "",
                "buy_asset_type":value["buying"]["asset_type"],
                "buy_asset_code":value["buying"]["asset_code"] if "asset_code" in value["buying"] else "XLM",
                "buy_asset_issuer":value["buying"]["asset_issuer"] if "asset_issuer" in value["buying"] else "",
                "nu_monto":value["amount"],
                "nu_precio":value["price"],
                "nu_total": format(float(value["amount"])*float(value["price"]),'.9f'),  
            }

            content["datos"].append(ofertdata)



    #print(content)

    return content 

@api_view(['POST'])
@renderer_classes([JSONRenderer])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def getTradesOfertasAccount(request):
    content={
        "success":True,
        "oferta":"",
        "trades":"",
    }
    public=request.user.getPublic()
    content["oferta"]=getOfertasAccount(public)
    content["trades"]=getTradesAccount(public)

    return Response(content)



def GetHistoricoBasic(public):
    content={
            'success':True,
            'data':[],
            "next":"",
            "prev":"",
            "ultimo":False

        }
    listAsset={
        "asset":[]
    }
    try:
        tx=getBalanceInit(public)

        lista=tx["_embedded"]["records"]

        page=tx["_links"]["next"]["href"]
        in_repetido=False

        for value in lista:
            if  "from" in value:
                tipo = "Envio" if value["from"] == public else "Recibido"
            elif value["type"] == "account_merge":
                tipo = "Eliminar cuenta"
            else:
                tipo = "Crear cuenta"
            toml={}
            if "asset_type" in value:
            #COMPROBAR ASSET REPETIDOS PARA NO VOLVER A CONSULTARLOS
                if len(listAsset["asset"])>0 and value["asset_type"] != "native":
                    for va in listAsset["asset"]:
                        if value["asset_code"] == va["code"] and value["asset_issuer"] == va["issue"]:
                            in_repetido=True
                            toml={
                                "name":va["name"],
                                "issue":va["issue"],
                                "code":va["code"],
                                "img":va["img"]
                            }
                    if not in_repetido:
                    
                        toml=getTomlAsset(value["asset_code"],value["asset_issuer"])
                        listAsset["asset"].append({"code":value["asset_code"],"issue":value["asset_issuer"],"name":toml["name"],"img":toml["img"]})
                    else:
                        in_repetido=False 
                                #print(toml)
                elif value["asset_type"] != "native":
                        toml=getTomlAsset(value["asset_code"],value["asset_issuer"])
                        listAsset["asset"].append({"code":value["asset_code"],"issue":value["asset_issuer"],"name":toml["name"],"img":toml["img"]})
                    #print(toml)
                content['data'].append(forDataPayment(value,tipo,public,toml))
        

    except:
        pass
    
    return content

def getOrdenBook(code1:str,code2:str,issuer1:str=None,issuer2:str=None)->dict:
    content={
            'success':True,
        }
    asset_sell=Asset(code1,issuer1) if code1 != "XLM" else Asset.native()
    asset_buy=Asset(code2,issuer2) if code2 != "XLM" else Asset.native()
    #print(asset_sell)
    #print(asset_buy)
    server = getServer()
    try:
        orderbook=server.orderbook(asset_sell,asset_buy).call()
        #print(orderbook)
        content["data"]=orderbook
    except:
        pass
    return content


    