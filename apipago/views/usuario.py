
from django.contrib.auth import authenticate
from django.http import JsonResponse
from django.core.exceptions import *
from main.models import *
from main.form import *
from main.views import notificacion_email
from django.contrib.auth.hashers import check_password
import requests
from django.db import IntegrityError, transaction
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view,api_view, permission_classes
from rest_framework.permissions import AllowAny,IsAuthenticated
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Prefetch


from django.db.models import Q

from main.views import emailMessage
from apipago.views.stellar import getFederacion,getStellarAccount
from main.views.utils import confirmado_user_required
from hashlib import sha256
import json
import datetime
from main.views.utils import fechaGet,getPublicRecortado

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def verificarCodigo2(request):
     content={
          'success':True,
          'pass':False,
          'data':'',
          'msg':'',
     }
     if request.method=='POST':
          if request.POST['reenviar']=='0':

               emailMessage(request.user,'email_notif_codigo.html')
               content['msg']='Se ha enviado un nuevo código por favor revise su correo'
              
          else:
               form=VerificarForm(request.POST)
               if form.is_valid():

                    c1=request.POST['c1']
                    c2=request.POST['c2']
                    c3=request.POST['c3']
                    c4=request.POST['c4']
                    token=str(c1)+str(c2)+str(c3)+str(c4)
                    emailconfirmation=Tb009EmailConfirmation.objects.filter(co_usuario=request.user.co_usuario,in_activo=True)[0]
                    if check_password(token, emailconfirmation.nu_token):
                         try:
                              with transaction.atomic():

                                   usuario=request.user
                                   usuario.in_confirmado=True
                                   usuario.in_ip=False
                                   usuario.in_aceptar=True
                                   usuario.save()

                                   #generarWalletStellar(usuario)
                                   #plan=WalletTb023PlanesUsuario(
                                   #     co_usuario=request.user,
                                   #     co_plan=WalletTb021Planes.objects.get(co_plan=1)
                                   #)
                                   #plan.save()
                                   
                                   """if usuario.co_federation and usuario.co_federation2 is None:
                                        
                                        
                                        get=buscarFederacion(request,usuario.co_federation)
                                        public=get.json()
                                        create=crearFede(request,usuario.email,public['account_id'],"prontopagos.com")

                                        
                                        usuario.co_federation2=create
                                        usuario.save()"""



                                   content['pass']=True
                                   content['success']=True
                                   content['msg']='Se ha registrado sactifactoriamente'
                         except IntegrityError:
                              content['success']=False
                              content['msg']='Hubo un error en la operación'
                         


                         
                         
                    
                    else:
                         content['success']=False
                         content['msg']='Se ha ingresado un codigo incorrecto'
                    
                    #solo para prueba local
                    #tipodoc=MainTb006TipoDocumento.objects.filter(co_tipo_indentidad=request.POST['co_tipo_indentidad'])
                    """if co_tipo_indentidad in request.POST:
                         queryset =MainTb006TipoDocumento.objects.filter(co_tipo_indentidad=request.POST['co_tipo_indentidad'])
                         serializer = tipoDocumentoSerializer(queryset , many=True)
                         content['data']=serializer.data
                         content['pass']=True"""
     
     return JsonResponse(content)



def verificarCodigo3(request):
     content={
          'success':True,
          'data':'',
          'msg':'',
     }
     if request.method=='POST':
          usu=Tb001Usuario.objects.filter(username=request.POST['usermail'])[0] if Tb001Usuario.objects.filter(username=request.POST['usermail']).count()>0 else None
                    
          if request.POST['reenviar']=='0':

               emailMessage(usu,'emailmensaje.html')
               content['msg']='Se ha enviado un nuevo código por favor revise su correo'
              
          else:
               form=VerificarForm(request.POST)
               if form.is_valid():

                    c1=request.POST['c1']
                    c2=request.POST['c2']
                    c3=request.POST['c3']
                    c4=request.POST['c4']
                    token=str(c1)+str(c2)+str(c3)+str(c4)

                    if(usu is not None):
                         if usu.in_confirmado:
                              token, _ = Token.objects.get_or_create(user = usu)
                              content['token']=token.key






                              
                         else:
                              content['success']=False
                              content['msg']="Error el usuario aun no esta validado"
                    else:
                         content['success']=False
                         content['msg']="Error el usuario no existe"

                    
                    """emailconfirmation=Tb009EmailConfirmation.objects.filter(co_usuario=request.user.co_usuario,in_activo=True)[0]
                    if check_password(token, emailconfirmation.nu_token):
                         usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
                         usuario.in_confirmado=True
                         usuario.save()

                         generarWalletStellar(usuario)
                         plan=WalletTb023PlanesUsuario(
                              co_usuario=request.user,
                              id_plan=WalletTb021Planes.objects.get(id_plan=1)
                         )
                         plan.save()

                         content['pass']=True
                         
                    
                    else:
                         content['success']=False
                         content['msg']='Se ha ingresado un codigo incorrecto'"""
                   
     
     return JsonResponse(content)

@api_view(['POST'])
def validarUsuario(request):
     username=request.POST['field']
     verdad=False
     msg=""
     if(not Tb001Usuario.objects.filter(username=username)):
          verdad=True
          msg="El Usuario ya Existe"
     content={'success':verdad,'msg':msg}
     return JsonResponse(content)

@api_view(['POST'])
@confirmado_user_required
@permission_classes((IsAuthenticated,))
def validarUsuarioPublic(request):
     tipo=request.POST['tipo']
     username=request.POST['field']
     verdad=False
     nuevo=False
     activo=False
     public=""
     federacion=""
     usuario=username
     recortado=""

     if tipo == "0":
          if request.POST['field'] != "":
               verdad=True
               dic=MainTb022Directorio.objects.select_related('co_keystore')\
                    .get(co_directorio=request.POST['field'])
               public=dic.co_keystore.address
               recortado=getPublicRecortado(public)
               activo=False if getStellarAccount(public) is None else True
               usuario=dic.tx_apodo
               
     else:
          keystore=WalletTb037Keystore.objects\
               .select_related('co_usuario')\
               .prefetch_related(Prefetch(
                         'billetera',
                         queryset=MainTb022Directorio.objects.filter(co_usuario=request.user.co_usuario))
               )\
               .filter(Q(co_usuario__username=username.upper())|Q(address=username))
          
          if keystore.exists():
               
               keystore=keystore.first()
               if not keystore.billetera.exists():
                    nuevo=True
               public=keystore.address
               recortado=getPublicRecortado(public)
               activo=False if getStellarAccount(public) is None else True
               verdad=True
               usuario=keystore.co_usuario.username if keystore.co_usuario is not None else keystore.address
          else:
               nuevo=True
               federacion=getFederacion(username)
               if federacion is not None:
                    verdad=True
                    public=federacion["account_id"]
                    recortado=getPublicRecortado(public)
                    usuario=federacion["username"]
                    activo=False if getStellarAccount(public) is None else True
               else:
                    verdad=True
                    public=username
                    activo=False if getStellarAccount(public) is None else True
                    

     content={
          'success':verdad,
          'public':public,
          'recortado':recortado,
          'usuario':usuario,
          'federacion':federacion,
          'nuevo':nuevo,
          'activo':activo
     }
     return JsonResponse(content)

@api_view(['POST'])
@permission_classes((AllowAny,))
def getUsuarioPublic(request):
     username=request.POST['field']
     public=""
     verdad=False
     if Tb001Usuario.objects.filter(username=username.upper()).count() > 0:
          user=Tb001Usuario.objects.filter(username=username.upper())[0]
          if user.getPublic() is not None:
               verdad=True
               public=user.getPublic()
               username=user.username

     elif WalletTb037Keystore.objects.filter(address=username).count()>0:
               key=WalletTb037Keystore.objects.select_related("co_usuario").filter(address=username)[0]
               if key.co_usuario is not None:
                    public=key.address
                    username=key.co_usuario.username
                    verdad=True


     content={'success':verdad,'public':public,'usuario':username}
     return JsonResponse(content)

@api_view(['POST'])
@permission_classes((AllowAny,))
def RecuperarClave(request):
     content={
        'success':False,
        'msg':'',
     }
     payload={
          'mnemonic':request.POST["mnemonic"],
          'pass2':request.POST["pass"],

     }
     print(payload)
     try:
          header={
          "Connection":"Close",
          "Content-Type":"application/x-www-form-urlencoded",
          }
          res=requests.post("http://127.0.0.1:3512/api/mnemonic-recovery-pass",data=payload,headers=header)
          resjson=res.json()
          if "success" in resjson:
               content=resjson
          else:
               content["msg"]="ha ocurrido un error"

     except Exception as e:
          content["msg"]="Ha ocurrido un error"

     return JsonResponse(content)

def validarUsuarioMailReClave(request):
     content={
          'success':False,
          'msg':''}
     usermail=request.POST['usermail']
     usu=Tb001Usuario.objects.filter(username=usermail)[0] if Tb001Usuario.objects.filter(username=usermail).count()>0 else None
     if(usu is not None):
          if usu.in_confirmado:
               content['success']=True

               #aqui va ir el envio del codigo al correo
               
               emailMessage(usu,'emailmensajeclave.html')
          else:
               content['msg']="Error el usuario aun no esta validado"
          
     else:
          content['msg']="Error el usuario no existe"
     return JsonResponse(content)

@api_view(['POST'])
@permission_classes((AllowAny,))
def validarEmail(request):
     email=request.POST['field']
     verdad=False
     if not Tb001Usuario.objects.filter(email=email):
          verdad=True
     content={'success':verdad,'msg':'El Correo ya Existe'}
     return JsonResponse(content)

@api_view(['POST'])
@permission_classes((AllowAny,))
def preRegistro(request):
     content={
          'success':True,
          'msg':'',
          'token':''
     }
     form=PreRegistroForm(request.POST)
     if form.is_valid():
          
          try:
               user=authenticate(request,username=request.POST['username'],password=request.POST['password'])
               
               #user=Tb001Usuario(username=request.POST['username'],nu_telegram=request.POST['nu_telegram'])
               #user.set_password(request.POST['password'])
               #user.save()
               
               token, _ = Token.objects.get_or_create(user = user)
               content['token']=token.key
          except ObjectDoesNotExist:
               
               content['msg']='Ha ocurrido un error'
               verdad=False
          
     else:
          content['success']=False
          content['msg']='Error en los datos'
     
     return JsonResponse(content)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def getRegistrar(request):
     content={
          'success':True,
          'data':'',
          'tipo':True,
          'msg':'',
     }
     if request.method=='POST':
          form=RegistroForm(request.POST)
          if form.is_valid():
               
               try:
                    user=request.user
                    user.email=request.POST['email']
                    user.co_rol=MainTb023Rol.objects.get(co_rol=7)
                    user.in_notificacion=True
                    #user.set_password(request.POST['password'])
                    user.save()
                    #content['tipo'] = False if request.POST['co_tipo_indentidad']=='2' else True

                    emailMessage(user,'email_notif_codigo.html')

               except ObjectDoesNotExist:
                    
                    content['msg']='Ha ocurrido un error'
                    verdad=False
               
               content['msg']='Para continuar ingrese el codigo validación que se ha enviado a su correo.'
               
          else:
               content['success']=False
               content['msg']='Error en los datos'
     
     return JsonResponse(content)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@transaction.atomic
def CambiarClave(request):
          
     content={
        'success':False,
        'msg':'',
     }
     payload={
          'address':request.user.getPublic(),
          'pass1':request.POST["pass1"],
          'pass2':request.POST["pass2"],

     }
     print(payload)
     try:
          header={
          "Connection":"Close",
          "Content-Type":"application/x-www-form-urlencoded",
          }
          res=requests.post("http://127.0.0.1:3512/api/changepassword",data=payload,headers=header)
          resjson=res.json()
          print(resjson)
          if "success" in resjson:
               content=resjson
          else:
               content["msg"]="ha ocurrido un error"

     except Exception as e:
          print(e)
          content["msg"]="Ha ocurrido un error"



               
     return JsonResponse(content)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@transaction.atomic
def CambiarClaveFederacion(request):
          
     content={
        'success':True,
        'msg':'',
     }

     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
     form=claveForm(request.POST)
     if form.is_valid() and usuario.in_confirmado:
          try:
               with transaction.atomic():
                    usuario.set_password(request.POST['password'])
                    usuario.save()


                    username={"username":usuario.username}
                    notificacion_email(usuario, username,'notificaciones/email_notif_cambio_clave.html')
                              
                    content['msg']='La clave ha sido cambiada sactifactoriamente'
          except IntegrityError:
               content['success']=False
               content['msg']='Hubo un error en la operación'
     else:
          content['success']=False
          content['msg']='Hubo un error en los datos'

               
     return JsonResponse(content)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@transaction.atomic
def GuardarUsuarioNatural(request):
          
     content={
        'success':True,
        'msg':'',
     }

     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
     form=RegistroPersonaForm(request.POST,usu=usuario,tipo=True)
     if form.is_valid() and usuario.co_persona is None:
          try:
               with transaction.atomic():
                    persona=MainTb005Persona(
                         nu_documento=request.POST['nu_documento'],
                         co_tipo_documento=MainTb006TipoDocumento.objects.get(co_tipo_documento=request.POST['co_tipo_documento']),
                         tx_primer_nombre=request.POST['tx_primer_nombre'],
                         tx_segundo_nombre=request.POST['tx_segundo_nombre'],
                         tx_primer_apellido=request.POST['tx_primer_apellido'],
                         tx_segundo_apellido=request.POST['tx_segundo_apellido'],
                         nu_telefono=request.POST['nu_telefono'],
                         co_sexo=MainTb007Sexo.objects.get(co_sexo=request.POST['co_sexo']),
                         fe_nacimiento=request.POST['fe_nacimiento'],
                         co_codigo_area=request.POST['co_codigo_area']
                         )
                              

                    direccion=MainTb008Direccion(
                         tx_direccion=request.POST['tx_direccion'],
                         co_ciudad=MainTb004Ciudad.objects.get(co_ciudad=request.POST['co_ciudad']),
                         tx_codigo_postal=request.POST['tx_codigo_postal']
                         )
                    
                    direccion.save()
                    persona.co_direccion=direccion
                    persona.save()
                    usuario.co_persona=persona
                    usuario.in_confirmado=True
                    usuario.save()
                    content['msg']='La operacion ha sido realizada sactifactoriamente'
          except IntegrityError:
               content['success']=False
               content['msg']='Hubo un error en la operación'
     else:
          content['success']=False
          content['msg']='Hubo un error en los datos'

               
     return JsonResponse(content)




@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@transaction.atomic
def GuardarUsuarioJuridico(request):
          
     content={
        'success':True,
        'msg':'',
     }

     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
     form=RegistroPersonaForm(request.POST,usu=usuario,tipo=False)
     if form.is_valid() and usuario.co_persona is None:
          try:
               with transaction.atomic():
                    persona=MainTb005Persona(
                         nu_documento=request.POST['nu_documento'],
                         co_tipo_documento=MainTb006TipoDocumento.objects.get(co_tipo_documento=request.POST['co_tipo_documento']),
                         tx_primer_nombre=request.POST['tx_primer_nombre'],
                         tx_segundo_nombre=request.POST['tx_segundo_nombre'],
                         tx_primer_apellido=request.POST['tx_primer_apellido'],
                         tx_segundo_apellido=request.POST['tx_segundo_apellido'],
                         nu_telefono=request.POST['nu_telefono'],
                         nu_reg_comercio=request.POST['nu_reg_comercio'],
                         tx_denom_comercial=request.POST['tx_denom_comercial'],
                         fe_nacimiento=request.POST['fe_nacimiento'],
                         co_codigo_area=MainTb034CodigoArea.objects.get(co_codigo_area=request.POST['co_codigo_area']) 
                         )
                              

                    direccion=MainTb008Direccion(
                         tx_direccion=request.POST['tx_direccion'],
                         co_ciudad=MainTb004Ciudad.objects.get(co_ciudad=request.POST['co_ciudad']),
                         tx_codigo_postal=request.POST['tx_codigo_postal']
                         )
                    
                    direccion.save()
                    persona.co_direccion=direccion
                    persona.save()
                    usuario.co_persona=persona
                    usuario.in_confirmado=True
                    usuario.save()
                    content['msg']='La operacion ha sido realizada sactifactoriamente'
          except IntegrityError:
               content['success']=False
               content['msg']='Hubo un error en la operación'
     else:
          content['success']=False
          content['msg']='Hubo un error en los datos'

               
     return JsonResponse(content)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def newdirectorio(request):
     content={
          'success':True,
          'msg':'Todo bien',
     }
     try:
          with transaction.atomic():
               if WalletTb037Keystore.objects.filter(address=request.POST['newusuariopublic']).count() > 0:
                    billetera=WalletTb037Keystore.objects.filter(address=request.POST['newusuariopublic'])[0]                
                    if billetera.co_usuario is not None:
                         if billetera.co_usuario.co_persona is not None:
                              apodo = billetera.co_usuario.co_persona.get_nombre_corto
                         else:
                              apodo = billetera.co_usuario.username
               else:
                    billetera=WalletTb037Keystore(
                         address=request.POST['newusuariopublic']
                    )
                    billetera.save()
               
               if request.POST["tx_apodo"] != "":
                    apodo=request.POST["tx_apodo"]

               directorio=MainTb022Directorio(
                   tx_apodo=apodo,
                   co_usuario=request.user,
                   co_keystore=billetera

               )
               directorio.save()
     except IntegrityError:
          content['success']=False
          content['msg']='Hubo un error en la operación'

     return JsonResponse(content)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def ApiAgregarContacto(request):
     content={
          'success':True,
          'msg':[],
     }
     form=agregarContactosForm(request.POST,usu=request.user)
     if form.is_valid():

          try:
               with transaction.atomic():

                    billetera=None

                    if Tb001Usuario.objects.filter(username=request.POST['tx_contacto']).count() > 0:
                         usuario=Tb001Usuario.objects.filter(username=request.POST['tx_contacto'])[0]  
                         billetera=WalletTb037Keystore.objects.filter(co_usuario=usuario.co_usuario)[0]


                    elif WalletTb037Keystore.objects.filter(address=request.POST['tx_contacto']).count() > 0:
                         billetera=WalletTb037Keystore.objects.filter(address=request.POST['tx_contacto'])[0]
                    
                    else:
                         billetera=WalletTb037Keystore(
                              address=request.POST['tx_contacto']
                         )
                         billetera.save()


                    if billetera is not None:
                         
                         directorio=MainTb022Directorio(
                              tx_apodo=request.POST['tx_apodo'],
                              in_activo=True,
                              co_usuario=request.user,
                              tx_memo=request.POST['tx_memo'],
                              co_keystore=billetera
                         )
                         directorio.save()

                         content['success']=True
                         content['msg']='Se agrego un nuevo contacto satisfactoriamente'

                    else:
                         content['success']=False
                         content['msg']='Hubo un error en la operación'

        
          except IntegrityError:
               content['success']=False
               content['msg']='Hubo un error en la operación'
     else:
          content['success']=False
          errores=form.errors.get_json_data()
        
          if 'tx_contacto' in errores:
               for error_contacto in errores['tx_contacto']:
                    content['msg'].append(error_contacto['message'])

          if 'tx_apodo' in errores:
               for error_apodo in errores['tx_apodo']:
                    content['msg'].append(error_apodo['message'])

     return JsonResponse(content)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def ApiEditarContacto(request):
     content={
          'success':True,
          'msg':[],
     }
     form2=editarContactosForm(request.POST)
     if form2.is_valid():

          try:
               with transaction.atomic():

                    directorio=MainTb022Directorio.objects.get(co_directorio=request.POST["co_directorio"])
                    directorio.tx_apodo=request.POST['tx_apodo']
                    directorio.tx_memo=request.POST['tx_memo']

                    directorio.save()
                    
                    content['success']=True
                    content['msg']='Se edito su contacto satisfactoriamente'


          except IntegrityError:
               content['success']=False
               content['msg']='Hubo un error en la operación'
     else:
          content['success']=False
          errores=form2.errors.get_json_data()
        
          if 'tx_contacto_ed' in errores:
               for error_contacto in errores['tx_contacto_ed']:
                    content['msg'].append(error_contacto['message'])

          if 'tx_apodo_ed' in errores:
               for error_apodo in errores['tx_apodo_ed']:
                    content['msg'].append(error_apodo['message'])

     return JsonResponse(content)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def ApiEliminarContacto(request):
     content={
          'success':True,
          'msg':[],
     }
     
     if request.method=='POST':

          directorio=MainTb022Directorio.objects.get(co_directorio=request.POST["co_directorio"])
          directorio.delete()
          content['success']=True
          content['msg']='Se elimino su contacto satisfactoriamente'

     return JsonResponse(content)



@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@confirmado_user_required
def validarClaveFederacion(request):
     content={
          'success':True,
     }

     credital={
        "Authorization": 'Token bf00448f9159c3c3f1122212692b0b7103fb6d1f'
    
     }

     url="https://stellarid.io/api/addresses/"+str(request.user.co_federation)
     gett=requests.get(url,data=None,headers=credital)
     fede=gett.json()

     if 'account_id' not in fede:
          content['success']=False
          return JsonResponse(content)

     tx=sha256(bytes(request.POST['password'],"ASCII"))

     url2="https://api.prontopagos.io/api/authkey"
     dataget={
         "address":fede['account_id'],
         "hash":tx.hexdigest()
     }

     gett2=requests.get(url2,params=dataget,headers=None)
     key=gett2.json()
     if 'address' not in key:
          content['success']=False
          return JsonResponse(content)




     return JsonResponse(content)

@csrf_exempt
def notificaciones(request):
     content={
          'success':True,
          'msg':[],
          'data':'',
     }
 
     data={
     "fecha":"",
     "username_recep":"",
     "username_emisor":"",
     "monto":"",
     "code":""
     }
     emisor=None
     destino=None
     
     keystores=WalletTb037Keystore.objects.select_related("co_usuario")\
     .filter(address__in=[request.POST["b_emisor"],request.POST["b_receptor"]])

     if len(keystores)>1:
          for values in keystores:
               if values.address == request.POST["b_emisor"]:
                    emisor=values
               if values.address == request.POST["b_receptor"]:
                    destino=values

     if emisor is not None and destino is not None:
          fe = datetime.datetime.now()
          fecha = fechaGet(fe)
          data["fecha"]= fecha
          data["username_recep"] = destino.co_usuario.username if destino.co_usuario is not None else destino.address
          data["username_emisor"] = emisor.co_usuario.username  if emisor.co_usuario is not None else emisor.address
          data["monto"]=request.POST["monto"]
          data["code"]=request.POST["code"]
          if emisor.co_usuario is not None:
               if emisor.co_usuario.in_notificacion == True:
                     #----NOTIFICACION EMISOR---
                    notificacion_email(emisor.co_usuario,data,'notificaciones/email_notif_transf_emisor.html')
               
     
               # ----NOTIFICACION RECEPTOR---
          if destino.co_usuario is not None:
               if destino.co_usuario.in_notificacion == True:
                    notificacion_email(destino.co_usuario,data,'notificaciones/email_notif_transf_receptor.html')
          content['success']=True

     else:
          content['success']=False
          content['error']="El emisor o destino estan vacios"

     content['data'] = request.POST
     return JsonResponse(content)
     # return HttpResponse(b)
    
@csrf_exempt
def notif_intercambio(request):
     content={
          'success':True,
          'msg':[],
          'data':'',
     }
 
     usuario= Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
     if usuario.in_notificacion == True:


          datos = request.POST['datos']
          detalles = json.loads(datos)
          # detalles = request.POST['datos']

          
          user=request.user
          notificacion_email(user,detalles,'notificaciones/email_notif_intercambio.html')
          


          content['data'] = detalles
     return JsonResponse(content)
     # return HttpResponse(detalles)