from rest_framework import viewsets
from main.models import (
    MainTb012Servicios,WalletTb014Comision,WalletTb015Transaccion,
    WalletTb013TipoTransaccion,WalletTb009Estatus,MainTb028Solicitud,
    WalletTb005PagoServicios,Tb001Usuario,WalletTb020HistoricoPagoServicios,
    WalletTb006DetallePagoServicios,MainTb013RequisitosServicios,MainTb029Ruta)
from django.db.models import Prefetch,Q
from apipago.serializers.servicios import (
    AllServiciosSerializer,RequestServicioSerializer,pagoServiciosProceso)
from apipago.serializers.general import ComisionSerializer 
from papaya_pago.CustomAuth import InConfirmado
from django_filters.rest_framework import DjangoFilterBackend
from django.db import transaction
from rest_framework.response import Response
from rest_framework.decorators import action
from django.http import Http404
from pprint import pprint
from stellar_sdk.server import Server
from stellar_sdk import TransactionBuilder, Network, Keypair
from pprint import pprint
class ServiciosViewSet(viewsets.ModelViewSet):
    queryset = MainTb012Servicios.objects\
        .select_related('co_tipo_servicios','co_proveedor_servicios')\
        .prefetch_related(
            Prefetch('co_servicios__reqser_servicios')
            )\
        .order_by('-created_at')\
        .all()
    serializer_class = AllServiciosSerializer
    permission_classes = [InConfirmado]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['created_at']

    server = Server(horizon_url="https://horizon.stellar.org")


    def list(self, request, *args, **kwargs)->Response:
        queryset = self.filter_queryset(self.get_queryset())
        queryset=queryset.filter(co_tipo_servicios__co_tipo_servicios=2)
        #print(request.user)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
    
    def detail(self, request, pk=None):
        raise Http404

    def create(self, request):
        content={
            "success":True
        }
        data=request.data
        print(data)
        try:
            serializer = RequestServicioSerializer(data=data)
            if serializer.is_valid():
                print(serializer.errors)
                print("Pues paso")
                try:
                    with transaction.atomic():
                        datos=serializer.data
                        solicitud=MainTb028Solicitud.objects.newSolicitud(15,request.user)
                        solicitud.save()
                        print(datos)
                        
                        transferencia=WalletTb015Transaccion(
                            in_activo=True,
                            co_emisor=request.user,
                            co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                            co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=5),
                            co_solicitud=solicitud,
                            tx_hash_stellar=data['tx_hash'],
                            tx_hash=data['lista_operaciones'][0]
                        )
                        transferencia.save()
                        print(transferencia, solicitud)
                        if transferencia:
                            trans_comision=WalletTb015Transaccion(
                                in_activo=True,
                                co_transaccion_emisor=transferencia,
                                co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                                co_tipo_transaccion=WalletTb013TipoTransaccion.objects.get(co_tipo_transaccion=5),
                                co_solicitud=solicitud,
                                tx_hash=data['lista_operaciones'][1]
                            )
                            trans_comision.save()

                            pagoservicios=WalletTb005PagoServicios(
                                co_servicios=MainTb012Servicios.objects.get(co_servicios=data['co_servicios']),
                                co_usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario),   
                                co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                                co_solicitud=solicitud
                            )
                            pagoservicios.save()

                            print("pagoservicios aca",pagoservicios)

                            pagohistorico=WalletTb020HistoricoPagoServicios(
                                co_transaccion=transferencia,
                                co_pago_servicios=pagoservicios,
                                co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                        
                            )
                            pagohistorico.save()
                            for requisito in data['requisitos']:
                                requisitos=WalletTb006DetallePagoServicios(
                                    tx_respuesta_requisito=requisito["tx_requisito"],
                                    co_pago_servicios=pagoservicios,
                                    co_requisitos_servicios=MainTb013RequisitosServicios.objects.get(co_requisitos_servicios=requisito["co_requisitos_servicios"])
                                )
                                requisitos.save()

                            solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                            solicitud.in_finanza=True
                            solicitud.save()

                            ruta=MainTb029Ruta.objects.filter(co_solicitud=solicitud.co_solicitud,in_activo=True)[0]
                            ruta.in_activo=False
                            ruta.save()

                        else:
                            content['success']=False
                            content['error']='Ha ocurrido un error'


                except Exception as e:
                    print(e)
                    content['success']=False
                    content['error']='Ha ocurrido un error'
            else:
                print(serializer.errors)
                content['success']=False
                content['error']='Ha ocurrido un error'
        except Exception as e:
            print(e)
            content['success']=False
            content['error']='Ha ocurrido un error'

        return Response(content)

    def update(self, request, pk=None):
        raise Http404

    def partial_update(self, request, pk=None):
        raise Http404

    def destroy(self, request, pk=None):
        content={
            "success":True
        }
        try:
            with transaction.atomic():
                root_keypair = Keypair.from_secret('privada aqui')
    
                source_account = self.server.load_account(root_keypair.public_key)
                base_fee = self.server.fetch_base_fee()

                pagoservicios=WalletTb005PagoServicios.objects\
                    .select_related('co_servicios','co_servicios__co_tipo_servicios',
                        'co_servicios__co_proveedor_servicios',
                        'co_estatus','co_solicitud')\
                    .prefetch_related(
                        Prefetch('requisitos',queryset=WalletTb006DetallePagoServicios.objects\
                            .select_related('co_requisitos_servicios')\
                            .all(),to_attr='pase_requisitos'),
                        Prefetch(
                            'hispase_pase',
                            queryset=WalletTb020HistoricoPagoServicios.objects\
                                .select_related('co_transaccion')\
                                .prefetch_related(Prefetch('co_transaccion__subtran_tran',to_attr='comisiones'))
                                .all(),to_attr='pase_historico')
                    )\
                    .filter(co_pago_servicios=pk,co_usuario=request.user.co_usuario)\
                    .first()\
                
                if pagoservicios is None:
                    content['success']=False
                    content['error']='No hubo coincidencias en la busque de la petición'  
                    return Response(content)

                #print(pagoservicios.pase_requisitos)
                    
                main_operacion=self.server.operations().operation(pagoservicios.pase_historico[0].co_transaccion.tx_hash).call()
                #pprint(main_operacion)
                if main_operacion['to']!='GBOT4VSNIXCDV47PO6KGUYYH6E725XGSZUOSQ2MYTSL6IPVZ66YFN6EH':
                    content['success']=False
                    content['error']='Operacion invalida, las cuentas no coinciden'  
                    return Response(content)

                teco=pagoservicios.pase_historico[0].co_transaccion.comisiones[0]
                comi_operacion=self.server.operations().operation(teco.tx_hash).call()
                if comi_operacion['to']!='GBOT4VSNIXCDV47PO6KGUYYH6E725XGSZUOSQ2MYTSL6IPVZ66YFN6EH':
                    content['success']=False
                    content['error']='Operacion invalida, las cuentas no coinciden'  
                    return Response(content)
                
                #pprint(comi_operacion)
                """ 'amount': '0.0000500',
                    'asset_type': 'native',
                    'created_at': '2022-06-10T23:01:55Z',
                    'from': 'GDFJKP3OUC27XPYQCF44CW44E5D43RY3ICUUPAQ7IXWNF24V43RHVZ5M',
                    'id': '177240651628044290',
                    'paging_token': '177240651628044290',
                    'source_account': 'GDFJKP3OUC27XPYQCF44CW44E5D43RY3ICUUPAQ7IXWNF24V43RHVZ5M',
                    'to': 'GBOT4VSNIXCDV47PO6KGUYYH6E725XGSZUOSQ2MYTSL6IPVZ66YFN6EH',
                    'transaction_hash': '7edac07f512352e82f33616777573ffc1e86f842dc43147efd0617e7945424c7',
                    'transaction_successful': True,
                    'type': 'payment',
                    'type_i': 1}"""

                transactions = (
                    TransactionBuilder(
                    source_account=source_account,
                     # If you want to submit to pubnet, you need to change `network_passphrase` to `Network.PUBLIC_NETWORK_PASSPHRASE`
                    network_passphrase=Network.TESTNET_NETWORK_PASSPHRASE,
                    base_fee=base_fee,
                    )
                    .add_text_memo('devolución por motivo de cancelación por parte del cliente.')
                    .append_payment_op(
                        main_operacion['from'],
                        str(main_operacion.amount),
                        "XLM"
                    )
                    .append_payment_op(
                        comi_operacion['from'],
                        str(comi_operacion.amount),
                        "XLM"
                    )
                    .set_timeout(300) 
                    .build()
                )

                transactions.sign(root_keypair)
                print(transactions.to_xdr())
                response = self.server.submit_transaction(transactions)
                print(response)
                """pagoservicios.co_estatus=WalletTb009Estatus.objects.get(co_estatus=3)
                pagoservicios.save()

                pagoservicios.co_solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=3)
                pagoservicios.co_solicitud.save()"""
                
                

        except Exception as e:
            print(e)
            content['success']=False
            content['error']='Ha ocurrido un error'                      

        return Response(content)
    
    #------GETTERS

    @action(detail=False, methods=['get'],url_path='comision', url_name='comision')
    def get_comision(self, request)->Response:
        comision=WalletTb014Comision.objects.filter(co_tipo_transaccion=5, in_activo=True).first()
        serializer = ComisionSerializer(comision)
        return Response(serializer.data)
        
    @action(detail=False, methods=['get'],url_path='solicitud', url_name='solicitud')
    def get_usuario_peticiones(self,request)->Response:
        solicitudes=WalletTb005PagoServicios.objects\
            .select_related('co_servicios','co_servicios__co_tipo_servicios',
                'co_servicios__co_proveedor_servicios',
                'co_estatus','co_solicitud','co_usuario')\
            .prefetch_related(
                Prefetch('requisitos'),
                Prefetch('hispase_pase')
            )\
            .filter(co_usuario=request.user.co_usuario)
        #print(solicitudes)
        serializer = pagoServiciosProceso(solicitudes,many=True)
        #pprint(serializer.data)
        return Response(serializer.data)

    @action(detail=False, methods=['get'],url_path='pendientes', url_name='pendientes')
    def get_peticiones(self,request)->Response:
        solicitudes=WalletTb005PagoServicios.objects\
            .select_related('co_servicios','co_servicios__co_tipo_servicios',
                'co_servicios__co_proveedor_servicios',
                'co_estatus','co_solicitud','co_usuario')\
            .prefetch_related(
                Prefetch('requisitos'),
                Prefetch('hispase_pase')
            )\
            .filter(co_estatus__co_estatus=1)
        #print(solicitudes)
        serializer = pagoServiciosProceso(solicitudes,many=True)
        #pprint(serializer.data)
        return Response(serializer.data)

    #-----SETTERS
    @action(detail=True, methods=['get'],url_path='p2p', url_name='p2p')
    def tomarPeticion(self,request,pk=None)->Response:
        content={
            "success":True
        }
        try:
            with transaction.atomic():
                solicitud=WalletTb005PagoServicios.objects\
                    .select_related('co_usuario')\
                    .get(co_pago_servicios=pk)

                if solicitud.co_usuario.co_usuario ==request.user.co_usuario:
                    content['success']=False
                    content['error']='No puedes tomar tu propia petición'  
                    return Response(content)



                solicitud.co_emisor=request.user
                solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=5)
                solicitud.save()

        
        except Exception as e:
            print(e)
            content['success']=False
            content['error']='Ha ocurrido un error'  

        return Response(content)

    
        