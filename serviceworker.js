//Papaya
var staticCacheName = "django-pwa-v" + new Date().getTime();
var filesToCache = [
    '/offline/',
    //'/css/django-pwa-app.css',

    '/static/img/android-chrome-160x160.png',
    '/static/img/android-chrome-512x512.png',
    '/static/assets/css/nucleo-icons.css',
    '/static/assets/css/black-dashboard.css',
    '/static/assets/demo/demo.css',
    '/static/assets/js/core/jquery.min.js',
    '/static/assets/js/core/popper.min.js',
    '/static/assets/js/core/bootstrap.min.js',
    '/static/assets/js/plugins/perfect-scrollbar.jquery.min.js',
    '/static/assets/js/plugins/moment.min.js',
    '/static/assets/js/plugins/bootstrap-switch.js',
    '/static/assets/js/plugins/sweetalert2.min.js',
    '/static/assets/js/plugins/jquery.tablesorter.js',
    '/static/assets/js/plugins/jquery.validate.min.js',
    '/static/assets/js/plugins/jquery.bootstrap-wizard.js',
    '/static/assets/js/plugins/bootstrap-selectpicker.js',
    '/static/assets/js/plugins/bootstrap-datetimepicker.js',
    '/static/assets/js/plugins/jquery.dataTables.min.js',
    '/static/assets/js/plugins/bootstrap-tagsinput.js',
    '/static/assets/js/plugins/jasny-bootstrap.min.js',
    '/static/assets/js/plugins/fullcalendar/fullcalendar.min.js',
    '/static/assets/js/plugins/fullcalendar/daygrid.min.js',
    '/static/assets/js/plugins/fullcalendar/timegrid.min.js',
    '/static/assets/js/plugins/fullcalendar/interaction.min.js',
    '/static/assets/js/plugins/jquery-jvectormap.js',
    '/static/assets/js/plugins/nouislider.min.js',
    '/static/assets/js/plugins/chartjs.min.js',
    '/static/assets/js/plugins/bootstrap-notify.js',
    '/static/assets/js/black-dashboard.min.js',
    '/static/assets/demo/demo.js',
    '/static/debug_toolbar/css/toolbar.css',
    '/static/debug_toolbar/js/toolbar.js',
    '/static/debug_toolbar/css/print.css',
    '/static/favicon.ico',
    '/static/js/utiles.js',
];


//css?family=Poppins:200,300,400,600,700,800	
//all.css	


// Cache on install
self.addEventListener("install", event => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
        .then(cache => {
            return cache.addAll(filesToCache);
        })
    )
});

// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                .filter(cacheName => (cacheName.startsWith("django-pwa-")))
                .filter(cacheName => (cacheName !== staticCacheName))
                .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});


// Serve from Cache
self.addEventListener("fetch", event => {
    event.respondWith(
        caches.match(event.request)
        .then(response => {
            return response || fetch(event.request);
        })
        .catch(() => {
            return caches.match('/offline/');
        })
    )
});

/*self.addEventListener("fetch", event => {
    if (event.request.method != 'GET') return;
    //esponse = await gola(event);
    event.respondWith(

        caches.match(event.request)
        .then(response => {
            return response || fetch(event.request);
        })
        .catch(() => {
            return caches.match('offline');
        })

    )
});*/

/*self.addEventListener("fetch", function(event) {
    event.respondWith(
        fetch(event.request)
        .then(function(result) {
            return caches.open(staticCacheName)
                .then(function(c) {
                    c.put(event.request.url, result.clone())
                    return result;
                })
        })
        .catch(function(e) {
            return caches.match(event.request)
        })
    )
});*/



/*async function gola(event) {
    try {
        let marcar = await caches.match(event.request);
        return marcar || fetch(event.request);

    } catch (error) {
        console.log('dos');
        console.log(error);
        return await checkOffline();
    }

}

async function checkOffline() {

    console.log("aqui");
    try {
        console.log('-------');
        let resp = await caches.match('offline')

        console.log(resp);
        console.log('-------');
        return resp;

    } catch (error) {
        console.log('tres');
        console.log(error);
    }
}*/