# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AccountEmailaddress(models.Model):
    email = models.CharField(unique=True, max_length=254)
    verified = models.BooleanField()
    primary = models.BooleanField()
    user = models.ForeignKey('MainTb001Usuario', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'account_emailaddress'


class AccountEmailconfirmation(models.Model):
    created = models.DateTimeField()
    sent = models.DateTimeField(blank=True, null=True)
    key = models.CharField(unique=True, max_length=64)
    email_address = models.ForeignKey(AccountEmailaddress, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'account_emailconfirmation'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey('MainTb001Usuario', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoSite(models.Model):
    domain = models.CharField(unique=True, max_length=100)
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'django_site'


class MainTb001Usuario(models.Model):
    co_usuario = models.AutoField(primary_key=True)
    tx_primer_nombre = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'main_tb001_usuario'


class MainTb001Usuario(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    co_usuario = models.BigAutoField(primary_key=True)
    username = models.CharField(unique=True, max_length=255, blank=True, null=True)
    email = models.CharField(unique=True, max_length=255, blank=True, null=True)
    first_name = models.CharField(unique=True, max_length=255, blank=True, null=True)
    last_name = models.CharField(unique=True, max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    is_active = models.BooleanField(blank=True, null=True)
    is_staff = models.BooleanField(blank=True, null=True)
    co_persona = models.ForeignKey('MainTb005Persona', models.DO_NOTHING, db_column='co_persona', blank=True, null=True)
    in_confirmado = models.BooleanField(blank=True, null=True)
    nu_telegram = models.IntegerField(unique=True, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb001usuario'


class MainTb001UsuarioGroups(models.Model):
    tb001usuario = models.ForeignKey(MainTb001Usuario, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'main_tb001usuario_groups'
        unique_together = (('tb001usuario', 'group'),)


class MainTb001UsuarioUserPermissions(models.Model):
    tb001usuario = models.ForeignKey(MainTb001Usuario, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'main_tb001usuario_user_permissions'
        unique_together = (('tb001usuario', 'permission'),)


class MainTb002Pais(models.Model):
    co_pais = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    tx_nacionalidad = models.CharField(max_length=50, blank=True, null=True)
    tx_pais = models.CharField(max_length=50, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb002_pais'


class MainTb003Estado(models.Model):
    co_estado = models.BigAutoField(primary_key=True)
    co_pais = models.ForeignKey(MainTb002Pais, models.DO_NOTHING, db_column='co_pais', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    tx_estado = models.CharField(max_length=50, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb003_estado'


class MainTb004Ciudad(models.Model):
    co_ciudad = models.BigAutoField(primary_key=True)
    co_estado = models.ForeignKey(MainTb003Estado, models.DO_NOTHING, db_column='co_estado', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    tx_ciudad = models.CharField(max_length=50, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb004_ciudad'


class MainTb005Persona(models.Model):
    co_persona = models.BigAutoField(primary_key=True)
    tx_primer_nombre = models.CharField(max_length=50, blank=True, null=True)
    tx_segundo_nombre = models.CharField(max_length=50, blank=True, null=True)
    tx_primer_apellido = models.CharField(max_length=50, blank=True, null=True)
    tx_segundo_apellido = models.CharField(max_length=50, blank=True, null=True)
    nu_documento = models.IntegerField(blank=True, null=True)
    fe_nacimiento = models.DateField(blank=True, null=True)
    nu_telefono = models.CharField(max_length=10, blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    co_sexo = models.ForeignKey('MainTb007Sexo', models.DO_NOTHING, db_column='co_sexo', blank=True, null=True)
    co_tipo_documento = models.ForeignKey('MainTb006TipoDocumento', models.DO_NOTHING, db_column='co_tipo_documento', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb005_persona'


class MainTb006TipoDocumento(models.Model):
    co_tipo_documento = models.BigAutoField(primary_key=True)
    co_pais = models.ForeignKey(MainTb002Pais, models.DO_NOTHING, db_column='co_pais', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    tx_abreviacion = models.CharField(max_length=20, blank=True, null=True)
    tx_tipo_documento = models.CharField(max_length=50, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb006_tipo_documento'


class MainTb007Sexo(models.Model):
    co_sexo = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    tx_sexo = models.CharField(max_length=30, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb007_sexo'


class MainTb008Direccion(models.Model):
    co_direccion = models.BigAutoField(primary_key=True)
    co_ciudad = models.ForeignKey(MainTb004Ciudad, models.DO_NOTHING, db_column='co_ciudad', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    tx_direccion = models.CharField(max_length=255, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    co_persona = models.ForeignKey(MainTb005Persona, models.DO_NOTHING, db_column='co_persona', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb008_direccion'


class MainTb009EmailConfirmation(models.Model):
    co_email_confirmation = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    fe_expiracion = models.DateTimeField(blank=True, null=True)
    nu_token = models.CharField(max_length=255, blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    co_usuario = models.ForeignKey(MainTb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb009_email_confirmation'


class MainTb010TipoServicios(models.Model):
    co_tipo_servicios = models.BigAutoField(primary_key=True)
    tx_servicios = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb010_tipo_servicios'


class MainTb011ProveedorServicios(models.Model):
    co_proveedor_servicios = models.BigAutoField(primary_key=True)
    tx_proveedor_servicios = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    update_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb011_proveedor_servicios'


class MainTb012Servicios(models.Model):
    co_servicios = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    update_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    co_proveedor_servicios = models.ForeignKey(MainTb011ProveedorServicios, models.DO_NOTHING, db_column='co_proveedor_servicios', blank=True, null=True)
    co_tipo_servicios = models.ForeignKey(MainTb010TipoServicios, models.DO_NOTHING, db_column='co_tipo_servicios', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb012_servicios'


class MainTb013RequisitosServicios(models.Model):
    co_requisitos_servicios = models.BigAutoField(primary_key=True)
    tx_requisitos_servicios = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    co_servicios = models.ForeignKey(MainTb012Servicios, models.DO_NOTHING, db_column='co_servicios', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb013_requisitos_servicios'


class MainTb014AgenteServicios(models.Model):
    co_agente_servicios = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    co_usuario = models.ForeignKey(MainTb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'main_tb014_agente_servicios'


class SocialaccountSocialaccount(models.Model):
    provider = models.CharField(max_length=30)
    uid = models.CharField(max_length=191)
    last_login = models.DateTimeField()
    date_joined = models.DateTimeField()
    extra_data = models.TextField()
    user = models.ForeignKey(MainTb001Usuario, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialaccount'
        unique_together = (('provider', 'uid'),)


class SocialaccountSocialapp(models.Model):
    provider = models.CharField(max_length=30)
    name = models.CharField(max_length=40)
    client_id = models.CharField(max_length=191)
    secret = models.CharField(max_length=191)
    key = models.CharField(max_length=191)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialapp'


class SocialaccountSocialappSites(models.Model):
    socialapp = models.ForeignKey(SocialaccountSocialapp, models.DO_NOTHING)
    site = models.ForeignKey(DjangoSite, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialapp_sites'
        unique_together = (('socialapp', 'site'),)


class SocialaccountSocialtoken(models.Model):
    token = models.TextField()
    token_secret = models.TextField()
    expires_at = models.DateTimeField(blank=True, null=True)
    account = models.ForeignKey(SocialaccountSocialaccount, models.DO_NOTHING)
    app = models.ForeignKey(SocialaccountSocialapp, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialtoken'
        unique_together = (('app', 'account'),)


class WalletTb001Plataforma(models.Model):
    co_plataforma = models.BigAutoField(primary_key=True)
    tx_plataforma = models.CharField(max_length=30, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wallet_tb001_plataforma'


class WalletTb002TipoMoneda(models.Model):
    co_tipo_moneda = models.BigAutoField(primary_key=True)
    tx_tipo_moneda = models.CharField(max_length=20, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wallet_tb002_tipo_moneda'


class WalletTb003Moneda(models.Model):
    co_moneda = models.BigAutoField(primary_key=True)
    tx_moneda = models.CharField(max_length=30, blank=True, null=True)
    tx_simbolo = models.CharField(max_length=10, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    co_pais = models.ForeignKey(MainTb002Pais, models.DO_NOTHING, db_column='co_pais', blank=True, null=True)
    co_plataforma = models.ForeignKey(WalletTb001Plataforma, models.DO_NOTHING, db_column='co_plataforma', blank=True, null=True)
    co_tipo_moneda = models.ForeignKey(WalletTb002TipoMoneda, models.DO_NOTHING, db_column='co_tipo_moneda', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wallet_tb003_moneda'


class WalletTb004Billetera(models.Model):
    co_billetera = models.BigAutoField(primary_key=True)
    tx_public_key = models.CharField(max_length=255, blank=True, null=True)
    tx_private_key = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    co_plataforma = models.ForeignKey(WalletTb001Plataforma, models.DO_NOTHING, db_column='co_plataforma', blank=True, null=True)
    co_usuario = models.ForeignKey(MainTb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wallet_tb004_billetera'


class WalletTb005PagoServicios(models.Model):
    co_pago_servicios = models.BigAutoField(primary_key=True)
    nu_monto = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    tx_transaccion = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    co_agente_servicios = models.ForeignKey(MainTb014AgenteServicios, models.DO_NOTHING, db_column='co_agente_servicios', blank=True, null=True)
    co_usuario = models.ForeignKey(MainTb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wallet_tb005_pago_servicios'


class WalletTb006DetallePagoServicios(models.Model):
    co_detalle_pago_servicios = models.BigAutoField(primary_key=True)
    tx_respuesta_requisito = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    update_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    co_pago_servicios = models.ForeignKey(WalletTb005PagoServicios, models.DO_NOTHING, db_column='co_pago_servicios', blank=True, null=True)
    co_requisitos_servicios = models.ForeignKey(MainTb013RequisitosServicios, models.DO_NOTHING, db_column='co_requisitos_servicios', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wallet_tb006_detalle_pago_servicios'


class WalletTb007Banco(models.Model):
    co_banco = models.BigAutoField(primary_key=True)
    tx_banco = models.CharField(max_length=100, blank=True, null=True)
    tx_siglas = models.CharField(max_length=10, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wallet_tb007_banco'


class WalletTb008Cuenta(models.Model):
    co_cuenta = models.BigAutoField(primary_key=True)
    tx_cuenta = models.CharField(max_length=255, blank=True, null=True)
    co_usuario = models.ForeignKey(MainTb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    in_staf = models.BooleanField(blank=True, null=True)
    nombre = models.CharField(max_length=50, blank=True, null=True)
    cedul = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wallet_tb008_cuenta'


class WalletTb009Recargabs(models.Model):
    co_recargabs = models.BigAutoField(primary_key=True)
    tx_codigo = models.CharField(max_length=255, blank=True, null=True)
    co_usuario = models.ForeignKey(MainTb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    nu_monto = models.DecimalField(max_digits=32, decimal_places=2, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)
    co_estatus = models.ForeignKey('WalletTb010Estatus', models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wallet_tb009_recargabs'


class WalletTb010Estatus(models.Model):
    co_estatus = models.BigAutoField(primary_key=True)
    tx_estatus = models.CharField(max_length=30, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wallet_tb010_estatus'
