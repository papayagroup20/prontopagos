
from django.shortcuts import render, redirect
from main.form import RegistroForm,TipoSolicitudForm,ReClaveForm
from django.db import IntegrityError, transaction
from main.models import (
    MainTb037RolSolicitud,MainTb028Solicitud,MainTb040Mensajes,MainTb027TipoSolicitud,
    WalletTb009Estatus,MainTb029Ruta,MainTb038SolicitudFormulario,MainTb026DetalleRuta
    )
from main.views.utils import confirmado_user_required,user2fa_required,getContentBasicoL,getContentBasicoL2
from django.db.models import Q
from stellar_sdk.asset import Asset
from apipago.views.stellar_utils import StellarAccount
from wallet.views.servicios import pagoServicios
import json
# Create your views here.
@confirmado_user_required
@user2fa_required
def inicio(request):
    content={
        'success':True,
        'balance':[],
        'historico':'',
        'msg':''
    }
    content['publico']=request.session["public_key"]
    cuenta=StellarAccount(content['publico'])
    cuenta.getDetalleActivo()
    asset_referencia=Asset("USDC","GA5ZSEJYB37JRC5AVCIA5MOP4RHTM335X2KGX3IHOJAPP5RE34K4KZVN")
    cuenta.getReferenciaActivos(asset_referencia)
    balance=cuenta.balance
    content["data"]=balance
    listaPrecios=[]

    if len(cuenta.balance)<=0:
        balance=[{
            'nu_balance': '0.0000000', 
            'tx_asset_code': 'XLM', 
            'tx_moneda': 'Lumens',  
        }]
    historico=cuenta.getHistorico()
    content['historico']=historico["data"]
    content['balance']=json.dumps(balance)

    
    return render(request, '../templates/inicio/inicio.html',content)


def reclave(request):
    print("waasaby")
    content={
        'success':True,
        'data':'',
        'form':ReClaveForm(),
        'msg':''
    }

    return render(request, '../templates/inicio/recordarclave.html',content)

def recordarClave(request):
    print("waasaby")
    content={
        'success':True,
        'data':'',
        'form':ReClaveForm(),
        'msg':''
    }

    return render(request, '../templates/inicio/recordarclave.html',content)

def cambioClave(request):
    
    content={
        'success':True,
        'data':'',
        'form':RegistroForm(),
        'msg':''
    }

    return render(request, '../templates/inicio/cambiarclave.html',content)

def cambioClaveUsuario(request):
    content=getContentBasicoL2(request.user,None,None)

    return render(request, '../templates/usuario/cambiarclave.html',content)

def bande(request):
    content=getContentBasicoL(request.user,None,TipoSolicitudForm(usu=request.user))
    rolsolicitud=MainTb037RolSolicitud.objects.filter(co_rol=request.user.co_rol.co_rol,in_activo=True).values_list('co_tipo_solicitud')
    if request.method=='POST':
        if 'boton' in request.POST:
            form=TipoSolicitudForm(request.POST)
            content['data']=MainTb028Solicitud.objects.filter(Q(co_tipo_solicitud=request.POST['co_tipo_solicitud']),Q(co_estatus=request.POST['co_estatus']),Q(co_usuario=request.user) | Q(co_usuario2=request.user) )
         
    elif request.method=='GET':
        if 'tx_busqueda' in request.GET:
            
            content['data']=MainTb028Solicitud.objects.filter(Q(co_tipo_solicitud__tx_tipo_solicitud__contains=request.GET['tx_busqueda']),Q(co_usuario=request.user) | Q(co_usuario=request.user) )
            
        else:
            content['data']=MainTb028Solicitud.objects.filter(Q(co_tipo_solicitud__in=list(rolsolicitud)),Q(co_usuario=request.user) | Q(co_usuario=request.user))
            #content['data']=MainTb028Solicitud.objects.filter(co_tipo_solicitud__co_rol_solicitud__co_rol__co_usuario=request.user.co_usuario)
    content['usuario']=request.user
    return content

@confirmado_user_required
def bandejaSolicitud(request):
    
    content=bande(request)

    
    return render(request, '../templates/inicio/bandeja.html',content)
@confirmado_user_required
def bandejaSolicitudSuccess(request,success,codigo):
    
    mensaje=MainTb040Mensajes.objects.get(co_mensajes=codigo)
    content=bande(request)
    content['success']=success
    content['msg']=mensaje.tx_mensajes
     
    return render(request, '../templates/inicio/bandeja.html',content)

@confirmado_user_required
def pushSolicitud(request):
    content={
        'success':False,
        'msg':''
    }
    if request.method=='POST':
        if MainTb027TipoSolicitud.objects.get(co_tipo_solicitud=request.POST['co_tipo_solicitud']) is not None:
                if MainTb028Solicitud.objects.filter(co_tipo_solicitud=request.POST['co_tipo_solicitud'],in_activo=True,co_usuario=request.user.co_usuario).count()>0:
                    return redirect('bandejaInicioSuccess',success=0,codigo=1)
                try:
                    with transaction.atomic():
                        solicitud=MainTb028Solicitud(
                            co_tipo_solicitud=MainTb027TipoSolicitud.objects.get(co_tipo_solicitud=request.POST['co_tipo_solicitud']),
                            co_usuario=request.user,
                            co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                        )
                        solicitud.save()

                        return redirect('controller',co_solicitud=solicitud.co_solicitud)
                except IntegrityError:
                        content['msg']='Hubo un error en la operación'
        else:
            content['msg']='El tipo de solicitud no existe'


    
    return redirect('bandejaInicio')

@confirmado_user_required
def solicitudController(request,co_solicitud):
    content={
        'success':True,
        'data':'',
    }
    

    ruta=MainTb029Ruta.objects.filter(co_solicitud=co_solicitud,in_activo=True)[0]
    if request.user == ruta.co_solicitud.co_usuario:
        formulario=MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None)[0] if MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None).exists() else None
    else:
        formulario=MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=request.user.co_rol.co_rol)[0] if MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=request.user.co_rol.co_rol).exists() else None
    
    if formulario is not None:
    
        return globals()[formulario.tx_funcion](request,co_solicitud)
    else:
        return redirect('inicio')
    #return render(request, '../templates/inicio/emptyDefault.html',content)

@confirmado_user_required
def solicitudControllerEspecifico(request,co_solicitud,co_solicitud_formulario):
    content={
        'success':True,
        'data':'',
    }


    ruta=MainTb029Ruta.objects.filter(co_solicitud=co_solicitud,in_activo=True)[0]
    if request.user == ruta.co_solicitud.co_usuario:
            formulario=MainTb038SolicitudFormulario.objects.filter(co_solicitud_formulario=co_solicitud_formulario,co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None)[0] if MainTb038SolicitudFormulario.objects.filter(co_solicitud_formulario=co_solicitud_formulario,co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None).exists() else None
    else:
            formulario=MainTb038SolicitudFormulario.objects.filter(co_solicitud_formulario=co_solicitud_formulario,co_detalle_ruta=ruta.co_detalle_ruta,co_rol=request.user.co_rol.co_rol)[0] if MainTb038SolicitudFormulario.objects.filter(co_solicitud_formulario=co_solicitud_formulario,co_detalle_ruta=ruta.co_detalle_ruta,co_rol=request.user.co_rol.co_rol).exists() else None    
      
    if formulario is not None:
    
        return globals()[formulario.tx_funcion](request,co_solicitud)
    else:
        return redirect('inicio')


@confirmado_user_required
def preSolicitudController(request,co_tipo_solicitud):
    content={
        'success':True,
        'data':'',
    }
    tipo_solicitud=MainTb027TipoSolicitud.objects.get(co_tipo_solicitud=co_tipo_solicitud)

    if tipo_solicitud.in_concurrencia is False:
        if MainTb028Solicitud.objects.filter(co_tipo_solicitud=co_tipo_solicitud,in_activo=True,co_usuario=request.user.co_usuario).count()>0:
           return redirect('bandejaInicioSuccess',success=0,codigo=3)             
    ruta=MainTb026DetalleRuta.objects.filter(co_tipo_solicitud=co_tipo_solicitud,in_activo=True, nu_orden=1)[0]

    formulario=MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None)[0] if MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None).exists() else None
    
    if formulario is not None:
    
        return globals()[formulario.tx_funcion](request,co_tipo_solicitud)
    else:
        return redirect('bandejaInicio')


@confirmado_user_required
def preSolicitudControllerEspecifico(request,co_tipo_solicitud,co_solicitud_formulario):
    content={
        'success':True,
        'data':'',
    }
    tipo_solicitud=MainTb027TipoSolicitud.objects.get(co_tipo_solicitud=co_tipo_solicitud)

    if tipo_solicitud.in_concurrencia is False:
        if MainTb028Solicitud.objects.filter(co_tipo_solicitud=co_tipo_solicitud,in_activo=True,co_usuario=request.user.co_usuario).count()>0:
           return redirect('bandejaInicioSuccess',success=0,codigo=3)             
    ruta=MainTb026DetalleRuta.objects.filter(co_tipo_solicitud=co_tipo_solicitud,in_activo=True, nu_orden=1)[0]

    formulario=MainTb038SolicitudFormulario.objects.filter(co_solicitud_formulario=co_solicitud_formulario,co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None)[0] if MainTb038SolicitudFormulario.objects.filter(co_solicitud_formulario=co_solicitud_formulario,co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None).exists() else None
    
    if formulario is not None:
    
        return globals()[formulario.tx_funcion](request,co_tipo_solicitud)
    else:
        return redirect('bandejaInicio')



@confirmado_user_required
def NewEditSolicitud(request,co_tipo_solicitud):
    content={
        'success':False,
        'msg':''
    }
    
    if MainTb027TipoSolicitud.objects.get(co_tipo_solicitud=co_tipo_solicitud) is not None:
            try:
                with transaction.atomic():
                    if MainTb028Solicitud.objects.filter(co_tipo_solicitud=co_tipo_solicitud,in_activo=True).count()>0:
                        solicitud=MainTb028Solicitud.objects.filter(co_tipo_solicitud=co_tipo_solicitud,in_activo=True)[0]
                    else:    
                        solicitud=MainTb028Solicitud(
                            co_tipo_solicitud=MainTb027TipoSolicitud.objects.get(co_tipo_solicitud=co_tipo_solicitud),
                            co_usuario=request.user,
                            co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                        )
                        solicitud.save()

                    return redirect('controller',co_solicitud=solicitud.co_solicitud)
            except IntegrityError:
                    content['msg']='Hubo un error en la operación'
    else:
        content['msg']='El tipo de solicitud no existe'


    
    return render(request, '../templates/inicio/bandeja.html',content)


@confirmado_user_required
def emptyDefault(request):
    content={
        'success':True,
        'data':'',
        'msg':''
    }
    
    return render(request, '../templates/inicio/emptyDefault.html',content)
