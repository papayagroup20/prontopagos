from main.views.reportes import Qr_pdf
from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action,api_view,renderer_classes,api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from base64 import b32encode,b32decode
from binascii import unhexlify
from django_otp.plugins.otp_totp.models import TOTPDevice
from django_otp.plugins.otp_static.models import StaticDevice,StaticToken
from django_otp.oath import TOTP
from django_otp import devices_for_user
import pyotp
import secrets
from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required
from django.conf import settings
from main.models import *
from django.db import IntegrityError, transaction
from django_otp import user_has_device
from django.views.decorators.http import require_http_methods,require_POST
from django_otp.decorators import otp_required
import pyqrcode
import io
import base64
@confirmado_user_required
def SeguridadUsuario2(request):
     # content=getContentBasicoL(request.user,None,None)
     content={
          'success':True,
          'msg':'',
          'data':{}
     }
     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)

     if request.method=='POST':
          if usuario.email is not None:
               if 'activar' in request.POST:
                    usuario.in_2fa_email=True
                    usuario.save()
                    content['success']=True
                    content['msg']='ha activado exitosamente su validacion por correo'

               if 'desactivar' in request.POST:
                    usuario.in_2fa_email=False
                    usuario.save()
                    content['success']=True
                    content['msg']='ha desactivado su validacion por correo'
          
          else:
               content['success']=False
               content['msg']='El usuario no tiene una cuenta de email valida'

     content['data']=usuario

     # return redirect('seguridad')
     return render(request, '../templates/usuario/seguridad/seguridad.html',content)
    





@confirmado_user_required
def SeguridadUsuarioBase(request):
     content=getContentBasicoL(request.user,None,None)
     content['data']=request.user

     return content

@confirmado_user_required
def SeguridadUsuarioSuccess(request,success,codigo):
    content=SeguridadUsuarioBase(request)
    mensaje=MainTb040Mensajes.objects.get(co_mensajes=codigo)
    content['success']=success
    content['msg']=mensaje.tx_mensajes

    return render(request, '../templates/usuario/seguridad/seguridad.html',content)


@confirmado_user_required
def SeguridadUsuario(request):
     # content=SeguridadUsuarioBase(request)
     content=getContentBasicoL(request.user,None,None) 

     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)

     if request.method=='POST':
          if usuario.in_2fa == True:
               content['success']=False
               content['msg']='Usted ya Posee un Metodo 2FA Activo'
          
          else:
          
               if usuario.email is not None:
               
                    if 'activar' in request.POST:
                         usuario.in_2fa_email=True
                         usuario.save()
                         content['success']=True
                         content['msg']='ha activado exitosamente su validacion por correo'

                    if 'desactivar' in request.POST:
                         usuario.in_2fa_email=False
                         usuario.save()
                         content['success']=True
                         content['msg']='ha desactivado su validacion por correo'
                    
               else:
                    content['success']=False
                    content['msg']='El usuario no tiene una cuenta de email valida'

     content['data']=usuario
     return render(request, '../templates/usuario/seguridad/seguridad.html',content)


@confirmado_user_required
@require_POST
def Activar2FA(request,co_tipo_solicitud):
     content=getContentBasicoL2(request.user,co_tipo_solicitud,None)
     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
     if usuario.in_2fa_email == True:
          content['success']=False
          content['msg']='Usted ya Posee un Metodo 2FA Activo'
          
     else:
          if "pass" in request.POST:
               if request.POST["pass"]=="merma":
                    try:
                         with transaction.atomic():
                              usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
                              usuario.in_2fa=True
                              usuario.save()
                              
                              device=TOTPDevice.objects.filter(user_id=request.user.co_usuario)[0]
                              device.confirmed=True
                              device.save()

                              estaticoDevice=StaticDevice.objects.filter(user_id=request.user.co_usuario)[0]
                              estaticoDevice.confirmed=True
                              estaticoDevice.save()

                              solicitud=MainTb028Solicitud.objects.unPaso(co_tipo_solicitud,request.user)
                              
                              return redirect('seguridadsuccess',success=1,codigo=14)
                              
                              
                    except IntegrityError:
                         return redirect('seguridadsuccess',success=0,codigo=1)
     return redirect('seguridadsuccess',success=0,codigo=1)


@confirmado_user_required
@require_POST
def Desactivar2FA(request,co_tipo_solicitud):
     content=getContentBasicoL2(request.user,co_tipo_solicitud,None)
     if "pass2" in request.POST:
          if request.POST["pass2"]=="fofo":
               try:
                    with transaction.atomic():
                         usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
                         usuario.in_2fa=False
                         usuario.save()
                         
                         device=TOTPDevice.objects.filter(user_id=request.user.co_usuario)
                         device.delete()

                         

                         estaticoDevice=StaticDevice.objects.filter(user_id=request.user.co_usuario)[0]

                         tokenDevice=StaticToken.objects.filter(device_id=estaticoDevice.id)
                         tokenDevice.delete()

                         estaticoDevice.delete()

                         solicitud=MainTb028Solicitud.objects.unPaso(co_tipo_solicitud,request.user)
                         
                         return redirect('seguridadsuccess',success=1,codigo=15)
                         
                         
               except IntegrityError:
                    return redirect('seguridadsuccess',success=0,codigo=1)

     return redirect('seguridadsuccess',success=0,codigo=1)

def Qr_billetera(request):

     usuario=WalletTb037Keystore.objects.filter(co_usuario=request.user)[0]
     link='tg://resolve?domain=ProntopagosBot&start='+usuario.address
     
     #codigo que crea un qr en imagen
     c = pyqrcode.create(link)
     s = io.BytesIO()
     c.png(s,scale=2)
     encoded = base64.b64encode(s.getvalue()).decode("ascii")
     
     #link='http://prontopagos.io/reporte/qr-pdf/'+usuario.address

     content={
        'img':link,
        'publica':usuario.address,
        'img_qr': encoded
     }

     return render(request, '../templates/usuario/qr_billetera.html',content)