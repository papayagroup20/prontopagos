from django.shortcuts import render,redirect,HttpResponse
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required,user_passes_test, permission_required
from django.contrib.auth.forms import UserCreationForm
from wallet.form import agentesMetodosPagosForm
from django.utils.crypto import get_random_string
from django.contrib.auth.hashers import make_password,check_password
from django.core.mail import EmailMessage
from django.conf import settings
from stellar_sdk import TransactionBuilder, Network, Keypair, Account,Asset,Operation,AllowTrust
from stellar_sdk.server import Server
import requests
from django.db import IntegrityError, transaction
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action,api_view,renderer_classes,api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from django.http import Http404, response
from django_otp.forms import OTPAuthenticationForm
from django.urls import reverse

from django.views.generic.base import View

from django.utils.module_loading import import_string

from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required

from main.models import MainTb005Persona, Tb001Usuario, WalletTb015Transaccion,WalletTb037Keystore
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter,A4
from io import BytesIO
from datetime import datetime
from reportlab.graphics.barcode import qr
from reportlab.graphics.barcode.qr import QrCodeWidget
from reportlab.graphics.shapes import Drawing
from reportlab.graphics import renderPDF

def Qr_pdf(request):
    usuario=WalletTb037Keystore.objects.filter(co_usuario=request.user)[0]
    persona=Tb001Usuario.objects.filter(co_usuario=request.user.co_usuario)[0]
    if persona.co_persona.tx_denom_comercial != None:
        persona2=persona.co_persona.tx_denom_comercial
    else:
        persona2=''
        
    link='tg://resolve?domain=ProntopagosBot&start='+usuario.address
    responce= HttpResponse(content_type='application/pdf')
    responce['Content-Disposition'] = 'attachment; filename= Qr_billetera.pdf'
    buffer= BytesIO()
    c=canvas.Canvas(buffer, pagesize=letter)#(612.0, 792.0)

    #header
    c.setLineWidth(.3)
    c.setFont('Helvetica', 22)

    c.line(30,760,582,760)

    c.setFont('Helvetica', 45)
    c.drawCentredString(306,700,'Aquí aceptamos')

    logo="static/img/logo-prontopago2.jpg"
    c.drawImage(logo,150,600,300,80)#primeros dos son X y Y de ubicacion y luego son X y Y de tamaño de la imagen


    #codigo QR

    qrw = QrCodeWidget(usuario.address)
    b = qrw.getBounds()

    w=b[2]-b[0] 
    h=b[3]-b[1]

    d = Drawing(45,45,transform=[300./w,0,0,300./h,0,0]) 
    d.add(qrw)

    renderPDF.draw(d, c, 156, 300)

    c.setFont('Helvetica', 20)
    c.drawCentredString(306,290,persona2)

    c.setFont('Helvetica', 18)
    c.drawCentredString(306,220,'Utilice el escáner de su teléfono y')
    c.drawCentredString(306,200,'pague rápido y seguro')



    #footer
    c.line(30,80,582,80)

    c.setFont('Helvetica', 12)
    c.drawCentredString(306,30,'© 2021 Gobelx All rights reserved.')


    c.showPage()
    c.save()


    pdf=buffer.getvalue()
    buffer.close()
    responce.write(pdf)

    return responce

def reporteTransferencia(request,co_solicitud):
    now = datetime.now()
    solicitud=WalletTb015Transaccion.objects.filter(co_solicitud=co_solicitud).order_by('co_transaccion')[0]
    
    responce= HttpResponse(content_type='application/pdf')
    responce['Content-Disposition'] = 'attachment; filename= Transferencia.pdf'
    buffer= BytesIO()
    c=canvas.Canvas(buffer, pagesize=letter)#(612.0, 792.0)

    #header
    c.setLineWidth(.3)
    c.setFont('Helvetica', 22)

    logo="static/img/logoppg4.png"
    c.drawImage(logo,10,720,100,45)#primeros dos son X y Y de ubicacion y luego son X y Y de tamaño de la imagen
        
    c.setFont('Helvetica', 20)
    c.drawString(60,750,'Pronto PAGOS')

    c.setFont('Helvetica', 18)
    c.drawString(60,730,'rapido y seguro')

    c.setFont('Helvetica', 8)
    c.drawString(60,710,'RIF: 123456789')

    #fecha
    format = now.strftime('%d/%m/%Y %H:%M')
    c.setFont('Helvetica', 12)
    c.drawString(470,760,format)
    c.line(465,757,565,757)
    #linea final
    c.line(30,700,582,700)

    #cuerpo
    c.setFont('Helvetica', 18)
    c.drawCentredString(306,680,'Comprobante de Transferencia')
    c.line(30,675,582,675)

    #titulo datos emisor
    c.setFont('Helvetica', 14)
    c.drawCentredString(306,660,'Operacion')

    #Codigo de transancion
    c.setFont('Helvetica', 12)
    c.drawString(30,640,'Numero de Transacción:')
    c.setFont('Helvetica', 12)
    c.drawString(400,640,str(solicitud.co_transaccion))

    #nombre del emisor
    c.setFont('Helvetica', 12)
    c.drawString(30,620,'Nombre del emisor:')
    c.setFont('Helvetica', 12)
    c.drawString(400,620,str(solicitud.co_emisor))

    #moneda
    c.setFont('Helvetica', 12)
    c.drawString(30,600,'Moneda:')
    c.setFont('Helvetica', 12)
    c.drawString(400,600,str(solicitud.co_moneda))

    #monto
    c.setFont('Helvetica', 12)
    c.drawString(30,580,'Monto:')
    c.setFont('Helvetica', 12)
    c.drawString(400,580,str(solicitud.nu_monto))

    #Comision
    c.setFont('Helvetica', 12)
    c.drawString(30,560,'Comisión:')
    c.setFont('Helvetica', 12)
    c.drawString(400,560,str(solicitud.get_comision().nu_monto))

    #Usuario receptor
    c.setFont('Helvetica', 12)
    c.drawString(30,540,'Usuario beneficiario:')
    c.setFont('Helvetica', 12)
    c.drawString(400,540,str(solicitud.co_receptor))

    #footer
    c.line(30,100,582,100)

    c.setFont('Helvetica', 12)
    c.drawCentredString(306,30,'© 2020 Gobelx Coded in Django by gobelgroup.')


    c.showPage()
    c.save()

    pdf=buffer.getvalue()
    buffer.close()
    responce.write(pdf)

    return responce


def reporteRetiro(request):
    now = datetime.now()
    #solicitud=WalletTb015Transaccion.objects.filter(co_solicitud=co_solicitud).order_by('co_transaccion')[0]
    
    responce= HttpResponse(content_type='application/pdf')
    responce['Content-Disposition'] = 'attachment; filename= Retiro.pdf'
    buffer= BytesIO()
    c=canvas.Canvas(buffer, pagesize=letter)#(612.0, 792.0)

    #header
    c.setLineWidth(.3)
    c.setFont('Helvetica', 22)

    logo="static/img/logoppg4.png"
    c.drawImage(logo,10,720,100,45)#primeros dos son X y Y de ubicacion y luego son X y Y de tamaño de la imagen
        
    c.setFont('Helvetica', 20)
    c.drawString(60,750,'Pronto PAGOS')

    c.setFont('Helvetica', 18)
    c.drawString(60,730,'rapido y seguro')

    c.setFont('Helvetica', 8)
    c.drawString(60,710,'RIF: 123456789')

    #fecha
    format = now.strftime('%d/%m/%Y %H:%M')
    c.setFont('Helvetica', 12)
    c.drawString(470,760,format)
    c.line(465,757,565,757)
    #linea final
    c.line(30,700,582,700)

    #cuerpo
    c.setFont('Helvetica', 18)
    c.drawCentredString(306,680,'Comprobante de Retiro')
    c.line(30,675,582,675)

    #titulo datos emisor
    c.setFont('Helvetica', 14)
    c.drawCentredString(306,660,'Operacion')

    #Codigo de transancion
    c.setFont('Helvetica', 12)
    c.drawString(30,640,'Numero de Transacción:')
    c.setFont('Helvetica', 12)
    c.drawString(400,640,str(solicitud.co_transaccion))

    #nombre del emisor
    c.setFont('Helvetica', 12)
    c.drawString(30,620,'Nombre del emisor:')
    c.setFont('Helvetica', 12)
    c.drawString(400,620,str(solicitud.co_emisor))

    #moneda
    c.setFont('Helvetica', 12)
    c.drawString(30,600,'Moneda:')
    c.setFont('Helvetica', 12)
    c.drawString(400,600,str(solicitud.co_moneda))

    #monto
    c.setFont('Helvetica', 12)
    c.drawString(30,580,'Monto:')
    c.setFont('Helvetica', 12)
    c.drawString(400,580,str(solicitud.nu_monto))

    #Comision
    c.setFont('Helvetica', 12)
    c.drawString(30,560,'Comisión:')
    c.setFont('Helvetica', 12)
    c.drawString(400,560,str(solicitud.get_comision().nu_monto))

    #Usuario receptor
    c.setFont('Helvetica', 12)
    c.drawString(30,540,'Usuario beneficiario:')
    c.setFont('Helvetica', 12)
    c.drawString(400,540,str(solicitud.co_receptor))

    #footer
    c.line(30,100,582,100)

    c.setFont('Helvetica', 12)
    c.drawCentredString(306,30,'© 2020 Gobelx Coded in Django by gobelgroup.')


    c.showPage()
    c.save()

    pdf=buffer.getvalue()
    buffer.close()
    responce.write(pdf)

    return responce


def reportePagoServicio(request):
    now = datetime.now()
    #solicitud=WalletTb015Transaccion.objects.filter(co_solicitud=co_solicitud).order_by('co_transaccion')[0]
    
    responce= HttpResponse(content_type='application/pdf')
    responce['Content-Disposition'] = 'attachment; filename= Pago-de-servicio.pdf'
    buffer= BytesIO()
    c=canvas.Canvas(buffer, pagesize=letter)#(612.0, 792.0)

    #header
    c.setLineWidth(.3)
    c.setFont('Helvetica', 22)

    logo="static/img/logoppg4.png"
    c.drawImage(logo,10,720,100,45)#primeros dos son X y Y de ubicacion y luego son X y Y de tamaño de la imagen
        
    c.setFont('Helvetica', 20)
    c.drawString(60,750,'Pronto PAGOS')

    c.setFont('Helvetica', 18)
    c.drawString(60,730,'rapido y seguro')

    c.setFont('Helvetica', 8)
    c.drawString(60,710,'RIF: 123456789')

    #fecha
    format = now.strftime('%d/%m/%Y %H:%M')
    c.setFont('Helvetica', 12)
    c.drawString(470,760,format)
    c.line(465,757,565,757)
    #linea final
    c.line(30,700,582,700)

    #cuerpo
    c.setFont('Helvetica', 18)
    c.drawCentredString(306,680,'Comprobante de Pago de Servicio')
    c.line(30,675,582,675)

    #titulo datos emisor
    c.setFont('Helvetica', 14)
    c.drawCentredString(306,660,'Operacion')

    #Codigo de transancion
    c.setFont('Helvetica', 12)
    c.drawString(30,640,'Numero de Transacción:')
    c.setFont('Helvetica', 12)
    c.drawString(400,640,str(solicitud.co_transaccion))

    #nombre del emisor
    c.setFont('Helvetica', 12)
    c.drawString(30,620,'Nombre del emisor:')
    c.setFont('Helvetica', 12)
    c.drawString(400,620,str(solicitud.co_emisor))

    #moneda
    c.setFont('Helvetica', 12)
    c.drawString(30,600,'Moneda:')
    c.setFont('Helvetica', 12)
    c.drawString(400,600,str(solicitud.co_moneda))

    #monto
    c.setFont('Helvetica', 12)
    c.drawString(30,580,'Monto:')
    c.setFont('Helvetica', 12)
    c.drawString(400,580,str(solicitud.nu_monto))

    #Comision
    c.setFont('Helvetica', 12)
    c.drawString(30,560,'Comisión:')
    c.setFont('Helvetica', 12)
    c.drawString(400,560,str(solicitud.get_comision().nu_monto))

    #Usuario receptor
    c.setFont('Helvetica', 12)
    c.drawString(30,540,'Usuario beneficiario:')
    c.setFont('Helvetica', 12)
    c.drawString(400,540,str(solicitud.co_receptor))

    #footer
    c.line(30,100,582,100)

    c.setFont('Helvetica', 12)
    c.drawCentredString(306,30,'© 2020 Gobelx Coded in Django by gobelgroup.')


    c.showPage()
    c.save()

    pdf=buffer.getvalue()
    buffer.close()
    responce.write(pdf)

    return responce

def reporte_prontoamigo(request):
    base=settings.STATIC_ROOT
    fondo=base+"/img/ProntoamiGO_Poster.png"

    link= 'https://t.me/prontopagosbot?start=ref_'+ str(request.user.nu_telegram)

    responce= HttpResponse(content_type='application/pdf')
    responce['Content-Disposition'] = 'attachment; filename=QR_ProntoamiGO_Poster.pdf'
    buffer= BytesIO()
    c=canvas.Canvas(buffer, pagesize=letter)#(612.0, 792.0)

    c.drawImage(fondo,0,0,612,792)#primeros dos son X y Y de ubicacion y luego son X y Y de tamaño de la imagen)

    #codigo QR
    qrw = QrCodeWidget(link)
    b = qrw.getBounds()
    w=b[2]-b[0] 
    h=b[3]-b[1]
    d = Drawing(45,45,transform=[200./w,0,0,200./h,0,0]) 
    d.add(qrw)
    renderPDF.draw(d, c, 400, 20)

    c.showPage()
    c.save()

    pdf=buffer.getvalue()
    buffer.close()
    responce.write(pdf)

    return responce