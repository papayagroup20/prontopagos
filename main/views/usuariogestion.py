from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required,user_passes_test
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import *
from main.models import *
from main.form import *
from wallet.form import agentesMetodosPagosForm
from django.utils.crypto import get_random_string
from django.contrib.auth.hashers import make_password,check_password
from django.core.mail import EmailMessage
from django.conf import settings
from stellar_sdk import TransactionBuilder, Network, Keypair, Account,Asset,Operation,AllowTrust
from stellar_sdk.server import Server
import requests
from django.db import IntegrityError, transaction
from apipago.serializers import *
from django.core import serializers
from rest_framework.authtoken.models import Token
import qrcode
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action,api_view,renderer_classes,api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from django.http import Http404
from django_otp.forms import OTPAuthenticationForm
from django.urls import reverse

from django.views.generic.base import View

from django.utils.module_loading import import_string

from django.contrib.sites.shortcuts import get_current_site


from django.views.decorators.cache import never_cache

import qrcode
import qrcode.image.svg
from base64 import b32encode
from binascii import unhexlify
from django_otp import login as otp_login,match_token

from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required



def logingestion(request):
    content={
        'form':LoginForm(),
        'msg':'',
        'success':True
    }
    if(request.method=='POST'):
        username=request.POST['username']
        password=request.POST['password']
        user=authenticate(request,username=username,password=password)
        

        if user is not None:
            if user.is_staff:
                dj_login(request,user)
                return redirect('inicio') #por los momentos inicio luego engrata a la pagina de gestion
            else:
                content['success']=False
                content['msg']='No eres un usuario administrador'      

    return render(request, '../templates/inicio/logingestion.html',content)