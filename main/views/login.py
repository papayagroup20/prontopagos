from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from main.models import (OtpTotpTotpdevice,Tb001Usuario,Tb009EmailConfirmation,MainTb042RegistroIP,MainTb043HistorialConexion)
from main.form import (dosfaCorreoForm,VerificarForm,Login2FAForm,LoginForm,RegistroForm,backup2FAForm)
from django.contrib.auth.hashers import check_password
import requests
from django.db import IntegrityError, transaction
from django.core.exceptions import ObjectDoesNotExist
from main.views.utils import getContentBasicoL
from django_otp.plugins.otp_totp.models import TOTPDevice
from django_otp.plugins.otp_static.models import StaticDevice
from django_otp import login as otp_login,user_has_device,devices_for_user
from main.views.usuario import notificacion_email,emailMessage
from main.views.utils import getNavegador
from hashlib import sha256
from django.contrib.gis.geoip2 import GeoIP2
from ipware import get_client_ip

def login(request):
    
     if(request.method=='POST'):
          username=request.POST['username']
          password=request.POST['password']
          user=authenticate(request,username=username,password=password)

          if user is not None:
               dj_login(request,user)
               if OtpTotpTotpdevice.objects.filter(user_id=user.co_usuario,confirmed=True).count()>0:

                    return redirect('login2fa')
               else:
                    return redirect('inicio')

               

     return render(request, '../templates/inicio/login.html')


def valCorreo(request,usu,clave):
     content={
          'success':True,
          'form':dosfaCorreoForm(),
          'usu':usu,
          'clave':clave,
          }
     
     return render(request, '../templates/usuario/validacion2fa.html',content)

def verificarPin2fa(request):
     content={
          'success':True,
          'msj':'',
          'form':VerificarForm()
          }
     
     if request.method=='POST':
          
          usuario=Tb001Usuario.objects.get(co_usuario=int(request.POST['usu']))
          if 'reenviar' in request.POST:
               emailMessage(usuario)

               content['msj']='Se ha enviado un nuevo código por favor revise su correo'
               return render(request,'../templates/usuario/validacion2fa.html',content)
              
          else:
               form=VerificarForm(request.POST)
               if form.is_valid():
                    c1=request.POST['c1']
                    c2=request.POST['c2']
                    c3=request.POST['c3']
                    c4=request.POST['c4']
                    token=str(c1)+str(c2)+str(c3)+str(c4)
                    emailconfirmation=Tb009EmailConfirmation.objects.filter(co_usuario=usuario,in_activo=True)[0]
                    if check_password(token, emailconfirmation.nu_token):
                         usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario)
                         user=authenticate(request,username=usuario,password=request.POST['clave'])
                         dj_login(request,user)
                         return redirect('inicio')
                    else:
                         content['msj']='Se ha ingresado un codigo incorrecto'
        
               content['form']=form
     return render(request, '../templates/usuario/validacion2fa.html',content)

# @Verif_reCapchat
def login2fa(request,operation:str=None):
     content={
          'form':Login2FAForm(),
          'form2fa_email':dosfaCorreoForm(),
          'msg':'',
          'success':True
     }
     if operation is not None:
          content["operation"]=operation

     if request.method=='POST':
          if 'username' in request.POST:
               init={
               'usernameh':request.POST["username"],
               'username':request.POST["username"],
               'password':request.POST["password"],
               }
               content["form"]=Login2FAForm(initial=init)
          else:
               if "operation" in request.POST:
                    return redirect('login-sep07',operation=request.POST["operation"])
               return redirect('login')
          
          if 'otp_token' in request.POST:
               form1=LoginForm(request.POST)
               if form1.is_valid():

                    form2=Login2FAForm(request.POST)
                    if form2.is_valid():
                         username=request.POST['username']
                         password=request.POST['password']
                         user=authenticate(request,username=username,password=password)
                         if user is not None:
                              t=devices_for_user(user=user)
                              for val in t:
                                  dev=val if val.name=="client-device" else None
                              dj_login(request,user)
                              otp_login(request,dev)
                              ipHistorico(request,user)

                              if "operation" in request.POST:
                                   return redirect("sep007-2",operation=request.POST["operation"])
                              
                              return redirect('inicio')
                         else:
                              if "operation" in request.POST:
                                   content["operation"]=request.POST["operation"]
                              dj_logout(request)
                              content['success']=False
                              content['msg']='Clave o usuario incorrecto'
                              return render(request, '../templates/inicio/login2.html',content)
                    

                    content['form']=form2

               else:
                    if "operation" in request.POST:
                         content["operation"]=request.POST["operation"]
                    content['form']=form1
                    return render(request, '../templates/inicio/login2.html',content)

          
     
               

     else:
          if "operation" in request.GET:
               return redirect('login-sep07',operation=request.GET["operation"])
          redirect('login')

     return render(request, '../templates/inicio/login2FA.html',content)



def login3(request,success:bool=None,codigo:str=None,operation:str=None):
     content={
          'form':LoginForm(),
          'form2fa_email':dosfaCorreoForm(),
          'msg':'',
          'success':True
     }

     #print(operation)
     if request.method=='POST':
          
          username=request.POST['username']
          #password=request.POST['password']

          form=LoginForm(request.POST)
          if form.is_valid():
               print("uno")
               user=authenticate(request,user=form.cleaned_data["username"])
               if user is not None:
                    if user.in_confirmado:
                         if user.in_2fa == False and user.in_2fa_email == False and user.in_ip==False:
                              dj_login(request,user)
                              user.set_session_key(request.session.session_key)
                              #request.session["merma"]=getMerma(username,password)
                              #request.session.modified = True
                              #ipHistorico(request,user)
                              if "operation" in request.POST:
                                   return redirect("sep007-2",operation=request.POST["operation"])
                              
                              return redirect('inicio')   
                         elif user.in_2fa == True and user.in_ip == False:
                              if "operation" in request.POST:
                                   return login2fa(request,request.POST["operation"])
                              else:
                                   return login2fa(request)
                              """t=devices_for_user(user=user)
                              for val in t:
                                   dev=val if val.name=="client-device" else None
                              dj_login(request,user)
                              otp_login(request,dev)
                              #request.session["merma"]=getMerma(username,password)
                              ipHistorico(request,user)
                              return redirect('inicio')"""
                         
                         elif user.in_2fa == False  and user.in_ip == True:
                              dj_login(request,user)
                              user.set_session_key(request.session.session_key)
                              #request.session["merma"]=getMerma(username,password)
                              return registrarIP(request, user)
                         
                         elif user.in_2fa == True and user.in_ip == True:
                              dj_login(request,user)
                              user.set_session_key(request.session.session_key)
                              #request.session["merma"]=getMerma(username,password)
                              #emailMessage(user,'emailclave2fa.html')
                              #return valCorreo(request,usuario,request.POST['password'])
                              return registrarIP(request,user)
                         elif user.in_2fa_email == False  and user.in_ip == True:
                              dj_login(request,user)
                              user.set_session_key(request.session.session_key)
                              #request.session["merma"]=getMerma(username,password)
                              return registrarIP(request, user)
                         elif user.in_2fa_email == True and user.in_ip == False:
                              #ipHistorico(request,user)
                              emailMessage(user,'emailclave2fa.html')
                              return valCorreo(request,user,'papa')
                         elif user.in_2fa_email == True and user.in_ip == True:
                              return registrarIP(request, user)
                              

                    else:
                        dj_login(request,user)
                        return redirect('registroTelegram')
              
              
               else:
                    dj_logout(request)
                    content['success']=False
                    content['msg']='Clave o usuario incorrecto'
          #print("aqui")
          #print(form.errors)
          #content['form']=form
          if "username" in form.errors:
               eror=form.errors['username'].as_data()
               for e in eror:
                    err=e
               error="".join(err.messages)
               if error == "El usuario debe ingresar el token de autenticación":
                    return render(request, '../templates/inicio/login2FA.html',content)
          
     else:
          if operation is not None:
               content["operation"]=operation

          if 'paso' in request.GET and request.GET['paso']:
              content['msg']='Su registro se ha completado sactifactoriamente, ya puede iniciar sesión con normalidad'           

     return render(request,"../templates/inicio/login2.html",content)

def getMerma(username,password):
     try:
            user = Tb001Usuario.objects.get(username=username)

     except Tb001Usuario.DoesNotExist:
         return None 

     if user.co_federation is None:
         return None
     credital={
        "Authorization": 'Token bf00448f9159c3c3f1122212692b0b7103fb6d1f'
    
     }
     url="https://stellarid.io/api/addresses/"+str(user.co_federation)
     gett=requests.get(url,data=None,headers=credital)
     fede=gett.json()

     if 'account_id' not in fede:
         return None

     tx=sha256(bytes(password,"ASCII"))
     url2="https://api.gobelx.io/api/authkey"
     dataget={
         "address":fede['account_id'],
         "hash":tx.hexdigest()
     }

     gett2=requests.get(url2,params=dataget,headers=None)
     key=gett2.json()
     if 'address' not in key:
         return None

     #return user
     key2=None
     header={
     "Connection":"Close",
     "Content-Type":"application/x-www-form-urlencoded",
     }
     url3="http://127.0.0.1:3512/api/keypair/"
     dataget2={
         "address":fede['account_id'],
         "pass":password
     }
     gett3=requests.post(url3,params=dataget2,headers=header)
     key2=gett3
     if key2.text:
          return key2.text
     else:
         return None

@login_required(login_url='/login')
def logout(request):
     dj_logout(request)
     return HttpResponseRedirect('/login')

def registroPromotor(request):
      return render(request, '../templates/usuario/registro1.html')

def registrar(request):
     content=getContentBasicoL(request.user,None,RegistroForm())
     
     
     return render(request, '../templates/usuario/registroTelegram.html',content)



@login_required(login_url='/login')
def verificarCodigo(request):

     if Tb009EmailConfirmation.objects.filter(co_usuario=request.user.co_usuario).count()<=0:
          emailMessage(request.user)


     content={
          'success':True,
          'msj':'',
          'form':VerificarForm()}

     if request.method=='POST':
          if 'reenviar' in request.POST:

               emailMessage(request.user)


               content['msj']='Se ha enviado un nuevo código por favor revise su correo'
               return render(request,'../templates/usuario/verificar.html',content)
              
          else:
               form=VerificarForm(request.POST)
               if form.is_valid():

                    c1=request.POST['c1']
                    c2=request.POST['c2']
                    c3=request.POST['c3']
                    c4=request.POST['c4']
                    token=str(c1)+str(c2)+str(c3)+str(c4)
                    emailconfirmation=Tb009EmailConfirmation.objects.filter(co_usuario=request.user.co_usuario,in_activo=True)[0]
                    if check_password(token, emailconfirmation.nu_token):
                         usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
                         usuario.in_confirmado=True
                         usuario.save()

                         #generarWalletStellar(usuario)
                         
                         return redirect('inicio')
                    else:
                         content['msj']='Se ha ingresado un codigo incorrecto'



               
               content['form']=form
               



     return render(request,'../templates/usuario/verificar.html',content)

def registroTelegram(request):
     init={
          "username":request.user.username,
          "email":request.user.email
     }
     content=getContentBasicoL(request.user,None,RegistroForm(initial=init))
     
     
     return render(request, '../templates/usuario/registroTelegram.html',content)

     
def backup2FA(request):
    content={
        'success':True,
        'msg':'',
        'form':backup2FAForm()
    }
    if request.method=='POST':
        if 'login' in request.POST:
            form=backup2FAForm(request.POST)
            user=authenticate(request,username=request.POST["username"],password=request.POST["password"])
            if user is not None:
                if user_has_device(user):
                    if form.is_valid():
                        try:
                            with transaction.atomic():
                                estaticoDevice=StaticDevice.objects.filter(user_id=user.co_usuario)[0]
                                if estaticoDevice.verify_token(request.POST["otp_token"]):
                                    
                                    device=TOTPDevice.objects.filter(user_id=user.co_usuario)
                                    device.delete()

                                    estaticoDevice.delete()

                                    user.in_2fa=False
                                    user.save()

                                    #solicitud=MainTb028Solicitud.objects.unPaso(27,user)
                                    

                                    return redirect('logins',success=1,codigo=2)
                                else:
                                    content["success"]=False
                                    content["msg"]="El token de recuperacion es incorrecto"
                        
                        except IntegrityError:
                            content["success"]=False
                            content["msg"]="Ha ocurrido un error"

                    

                    content['form']=form
                else:
                    content["success"]=False
                    content["msg"]="El usuario no tiene activado el metodo 2FA"

            else:
                content["success"]=False
                content["msg"]="La clave o usuario es incorrecto"

     

    return render(request, '../templates/inicio/bakup2fa.html',content)



def registrarIP(request, usuario,operation:str=None):
     content={
          'form':LoginForm(),
          'msg':'',
          'success':True
     }
     
     if usuario.in_2fa==True:
          try:
               with transaction.atomic():
                    consulta=MainTb042RegistroIP.objects.filter(tx_registro_ip=get_client_ip(request)[0], co_usuario=usuario, in_activo=True).first()
               
               if consulta.in_activo  == True and consulta.tx_registro_ip == get_client_ip(request)[0]:
                    
                    # t=devices_for_user(user=usuario)
                    # for val in t:
                    #      dev=val if val.name=="client-device" else None
                    # dj_login(request,usuario)
                    # otp_login(request,dev)
                    # #request.session["merma"]=getMerma(username,password)
                    # # ipHistorico(request,usuario)
                    # return redirect('inicio')
                    if operation is not None:
                         return login2fa(request,operation)
                    return login2fa(request)
                    
          except ObjectDoesNotExist:
               
               t=MainTb042RegistroIP(
               tx_registro_ip=get_client_ip(request)[0],
               co_usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario),
               in_activo = False,
               
               )
               t.save()
               ip=get_client_ip(request)[0]
               usuario_agente = getNavegador(request)
               detalles={"ip":ip, "usuario":usuario.email,"co_usuario":usuario.co_usuario, "co_registro_ip":t.co_registro_ip, "usuario_agente":usuario_agente, "fecha":t.created_at}
               notificacion_email(usuario,detalles,'emailconfirmarip.html')
               content['success']=False
               content['msg']='IP no Verificada, Hemos enviado el link de activacion a su correo'
               
               if operation is not None:
                    content["operation"]=operation 

               return render(request, '../templates/inicio/login2.html',content)
     
     elif usuario.in_2fa_email==True:
           try:
               with transaction.atomic():
                    consulta=MainTb042RegistroIP.objects.filter(tx_registro_ip=get_client_ip(request)[0], co_usuario=usuario, in_activo=True).first()
               
               if consulta.in_activo  == True and consulta.tx_registro_ip == get_client_ip(request)[0]:
                    ipHistorico(request,usuario)
                    emailMessage(usuario,'emailclave2fa.html')
                    return valCorreo(request,usuario,request.POST['password'])
                    
           except ObjectDoesNotExist:
               
               t=MainTb042RegistroIP(
               tx_registro_ip=get_client_ip(request)[0],
               co_usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario),
               in_activo = False,
               
               )
               t.save()
               ip=get_client_ip(request)[0]
               usuario_agente = getNavegador(request)
               detalles={"ip":ip, "usuario":usuario.email,"co_usuario":usuario.co_usuario, "co_registro_ip":t.co_registro_ip, "usuario_agente":usuario_agente, "fecha":t.created_at}
               notificacion_email(usuario,detalles,'emailconfirmarip.html')
               content['success']=False
               content['msg']='IP no Verificada, Hemos enviado el link de activacion a su correo'
               if operation is not None:
                    content["operation"]=operation
               return render(request, '../templates/inicio/login2.html',content)
          
     

     else:
        
          try:
               with transaction.atomic():
                    consulta=MainTb042RegistroIP.objects.filter(tx_registro_ip=get_client_ip(request)[0], co_usuario=usuario, in_activo=True).first()
               
               if consulta.in_activo  == True and consulta.tx_registro_ip == get_client_ip(request)[0]:
                    ipHistorico(request,usuario)
                    if "operation" in request.POST:
                         return redirect("sep007-2",operation=operation)
                    return redirect('inicio')
                    
               
          except ObjectDoesNotExist:
              
               t=MainTb042RegistroIP(
               tx_registro_ip=get_client_ip(request)[0],
               co_usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario),
               in_activo = False,
               )
               t.save()
               ip=get_client_ip(request)[0]
               usuario_agente = getNavegador(request)
               detalles={"ip":ip, "usuario":usuario.email,"co_usuario":usuario.co_usuario, "co_registro_ip":t.co_registro_ip, "usuario_agente":usuario_agente, "fecha":t.created_at}
               notificacion_email(usuario,detalles,'emailconfirmarip.html')
               content['success']=False
               content['msg']='IP no Verificada, Hemos enviado el link de activacion a su correo'
               if operation is not None:
                    content["operation"]=operation
               return render(request, '../templates/inicio/login2.html',content)



def ipHistorico(request,usuario):
     g = GeoIP2('GeoLite2-City_20201103') #Enter folder name
     geo = g.city(get_client_ip(request)[0])
     ciudad=geo.get('city')
     pais=geo.get('country_name')
     cidadpais=str(ciudad)+'/'+str(pais)
     

     ip = MainTb043HistorialConexion(
          tx_registro_ip=get_client_ip(request)[0],
          co_usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario),
          tx_usuario_agente = getNavegador(request),
          tx_ciudad_pais = cidadpais,
          
     )
     ip.save()



