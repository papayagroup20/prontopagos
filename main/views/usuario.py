from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.conf import settings
from main.models import (
     Tb009EmailConfirmation,WalletTb001Plataforma,
     WalletTb037Keystore,WalletTb009Estatus,Tb001Usuario,MainTb028Solicitud,
     MainTb014AgenteServicios,MainTb005Persona,MainTb006TipoDocumento,MainTb007Sexo,
     MainTb008Direccion,MainTb004Ciudad,MainTb020TipoIndentificacion,MainTb021Indentificacion,
     MainTb002Pais,MainTb043HistorialConexion,MainTb042RegistroIP,MainTb022Directorio)
from main.form import ( 
     RegistroPersonaForm,IndenrificacionUsuarioForm,editarContactosForm,
     EditarUsuario2FaForm,registrarCuentasForm,agregarContactosForm,
     LoginForm)
from wallet.form import agentesMetodosPagosForm
from django.utils.crypto import get_random_string
from django.contrib.auth.hashers import make_password
from django.core.mail import EmailMessage
from django.conf import settings
from stellar_sdk import TransactionBuilder, Network, Keypair
from stellar_sdk.server import Server
import requests
from django.db import IntegrityError, transaction
from main.serializers import UsuarioPersonaSerializer,IndentificacionSerializer
from rest_framework.authtoken.models import Token
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view,renderer_classes,api_view
from rest_framework.response import Response
from main.views.utils import getContentBasicoL2,user2fa_required,confirmado_user_required,user_staff_required




#@api_view(['POST','GET'])
#@permission_classes((IsAuthenticated,))
#@login_required(login_url='/login')


          

def emailMessage(_user:Tb001Usuario,_template:str,_longitud:int=4)->None:
     
     
     try:
          if Tb009EmailConfirmation.objects.filter(co_usuario=_user.co_usuario,in_activo=True).count()>0:
               emailc=Tb009EmailConfirmation.objects.filter(co_usuario=_user.co_usuario,in_activo=True)[0]
               emailc.in_activo=False
               emailc.save()


          raw_token=get_random_string(length=_longitud)
          encryp_token=make_password(raw_token)

          newEmail=Tb009EmailConfirmation(co_usuario=_user,nu_token=encryp_token)
          newEmail.save()

          texto="../templates/usuario/"+_template
          body = render_to_string(texto,
               {'codigo':raw_token}
          )



          email=EmailMessage(
               subject='Mensaje de Gobelx',
               body=body,
               from_email=settings.EMAIL_HOST_USER,
               to=[_user.email]

          )
          email.content_subtype='html'
          email.send()
     except Exception as e:
          print(e)
def generarWalletStellar(user):
     keypair = Keypair.random()
     
     url = 'https://friendbot.stellar.org' #generar dinero prueba
     response = requests.get(url, params={'addr': keypair.public_key}) #generar dinero prueba
     plataforma=WalletTb001Plataforma.objects.get(co_plataforma=1)
     wallet=WalletTb037Keystore(tx_public_key=keypair.public_key,co_usuario=user)
     wallet.save()

     root_keypair = Keypair.from_secret(wallet.tx_private_key)
                         
     server = Server(horizon_url="https://horizon-testnet.stellar.org")
     source_account = server.load_account(root_keypair.public_key)
     base_fee = server.fetch_base_fee()

     transactions = (
          TransactionBuilder(
               source_account=source_account,
               # If you want to submit to pubnet, you need to change `network_passphrase` to `Network.PUBLIC_NETWORK_PASSPHRASE`
               network_passphrase=Network.TESTNET_NETWORK_PASSPHRASE,
               base_fee=base_fee,
          ).add_text_memo('transferencia bss')
          .append_change_trust_op(
               asset_code='BSS', 
               asset_issuer='GDNYGQC6464OGM77WSS44H4DK5L4PG4C7E76BYUQHMVVJYQLUXLFCOGD', 
               limit='100000000')
          .append_change_trust_op(
               asset_code='PPG', 
               asset_issuer='GDNYGQC6464OGM77WSS44H4DK5L4PG4C7E76BYUQHMVVJYQLUXLFCOGD', 
               limit='10000')
          .append_change_trust_op(
               asset_code='USD', 
               asset_issuer='GDNYGQC6464OGM77WSS44H4DK5L4PG4C7E76BYUQHMVVJYQLUXLFCOGD', 
               limit='10000')
          .append_change_trust_op(
               asset_code='BTC', 
               asset_issuer='GDNYGQC6464OGM77WSS44H4DK5L4PG4C7E76BYUQHMVVJYQLUXLFCOGD', 
               limit='10000')
          .set_timeout(300) 
          .build()
     )
          
     transactions.sign(root_keypair)
     print(transactions.to_xdr())
     response = server.submit_transaction(transactions)
     print(response)



def basePerfilUsuario(usu,msg,success):
     content={
          'success':success,
          'msg':msg,
          'form': agentesMetodosPagosForm(in_envio=False),
          'data':{
               'persona':'',
               'direccion':'',
               'val_img':'',
               'val_persona':'',
               'val_estado':'',
               'val_agente':'',
               'billetera':'',
          }
     }
     usuario=Tb001Usuario.objects.get(co_usuario=usu.co_usuario)
     if usuario.co_estatus_img is None:
          usuario.co_estatus_img= WalletTb009Estatus.objects.get(co_estatus=4)
          usuario.save()
     if usuario.co_estatus_persona is None:
          usuario.co_estatus_persona= WalletTb009Estatus.objects.get(co_estatus=4)
          usuario.save()
     content['data']['billetera']=WalletTb037Keystore.objects.filter(co_usuario=usu)[0]
     content['data']['val_img']=usuario.co_estatus_img.co_estatus
     content['data']['val_persona']=usuario.co_estatus_persona.co_estatus
     content['data']['val_estado']=MainTb028Solicitud.objects.filter(co_tipo_solicitud=2,co_usuario=usu.co_usuario,in_activo=True).exclude(co_estatus=3)[0] if MainTb028Solicitud.objects.filter(co_tipo_solicitud=2,co_usuario=usu.co_usuario,in_activo=True).exclude(co_estatus=3).count()>0 else ''
     content['data']['val_agente']=True if MainTb014AgenteServicios.objects.filter(co_usuario=usu.co_usuario, in_activo=True).count()>0 else False
     
     if usuario.co_persona is not None:
          content['data']['persona']=usuario.co_persona
          if usuario.co_persona.co_direccion is not None:
               content['data']['direccion']=usuario.co_persona.co_direccion 
     #print(content['data']['billetera'])
     #print(content['data']['persona'])
     return content

@confirmado_user_required
@user2fa_required
def perfilUsuario(request):
     content=basePerfilUsuario(request.user,'',True)

     return render(request,'../templates/usuario/perfil.html',content)
     

@transaction.atomic
def editarUsuario(request):
     init={}
     
     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
     #queryset2 =Tb001Usuario.objects.filter(co_usuario=request.user.co_usuario)
     #serializer2 = UsuarioPersonaSerializer(queryset2 , many=True)
     #f=''
     #print(serializer2.data)
     if usuario.co_persona is not None:
          init['tx_primer_nombre']=usuario.co_persona.tx_primer_nombre
          init['tx_segundo_nombre']=usuario.co_persona.tx_segundo_nombre
          init['tx_primer_apellido']=usuario.co_persona.tx_primer_apellido
          init['tx_segundo_apellido']=usuario.co_persona.tx_segundo_apellido
          init['fe_nacimiento']=usuario.co_persona.fe_nacimiento
          init['nu_documento']=usuario.co_persona.nu_documento
          init['co_tipo_documento']=usuario.co_persona.co_tipo_documento.co_tipo_documento
          init['co_sexo']=usuario.co_persona.co_sexo.co_sexo
          init['nu_telefono']=usuario.co_persona.nu_telefono
     
          if usuario.co_persona.co_direccion is not None:
               init['co_pais']=usuario.co_persona.co_direccion.co_ciudad.co_estado.co_pais.co_pais
               init['co_estado']=usuario.co_persona.co_direccion.co_ciudad.co_estado.co_estado
               init['co_ciudad']=usuario.co_persona.co_direccion.co_ciudad.co_ciudad
               init['tx_direccion']=usuario.co_persona.co_direccion.tx_direccion
          

     #queryset =MainTb005Persona.objects.filter(co_usuario=request.user.co_usuario)
     #serializer = PersonaSerializer(queryset , many=True)
     
     tipo=False if usuario.co_persona.co_tipo_documento.co_tipo_indentidad.co_tipo_indentidad==1 else True

     content={
          'success':True,
          'data':{
               'persona':''
          },
          'msg':'',
          'form':RegistroPersonaForm(usu=usuario,initial=init,tipo=tipo)
     }
     if request.method=='POST':
          if 'guardar' in request.POST:
               form=RegistroPersonaForm(request.POST,usu=usuario)
               if form.is_valid():
                    try:
                         with transaction.atomic():
                              if usuario.co_persona:

                                   persona=MainTb005Persona.objects.get(co_persona=usuario.co_persona)
                                   persona.nu_documento=request.POST['nu_documento']
                                   persona.co_tipo_documento=request.POST['co_tipo_documento']
                                   persona.tx_primer_nombre=request.POST['tx_primer_nombre']
                                   persona.tx_segundo_nombre=request.POST['tx_segundo_nombre']
                                   persona.tx_primer_apellido=request.POST['tx_primer_apellido']
                                   persona.tx_segundo_apellido=request.POST['tx_segundo_apellido']
                                   persona.nu_telefono=request.POST['nu_telefono']
                                   persona.co_sexo=request.POST['co_sexo']
                                   persona.fe_nacimiento=request.POST['fe_nacimiento']
                                   

                              else:
                                   persona=MainTb005Persona(
                                        nu_documento=request.POST['nu_documento'],
                                        co_tipo_documento=MainTb006TipoDocumento.objects.get(co_tipo_documento=request.POST['co_tipo_documento']),
                                        tx_primer_nombre=request.POST['tx_primer_nombre'],
                                        tx_segundo_nombre=request.POST['tx_segundo_nombre'],
                                        tx_primer_apellido=request.POST['tx_primer_apellido'],
                                        tx_segundo_apellido=request.POST['tx_segundo_apellido'],
                                        nu_telefono=request.POST['nu_telefono'],
                                        co_sexo=MainTb007Sexo.objects.get(co_sexo=request.POST['co_sexo']),
                                        fe_nacimiento=request.POST['fe_nacimiento'])
                              
                    
                              

                              if persona.co_direccion is not None:
                                   direccion=MainTb008Direccion.objects.get(co_direccion=persona.co_direccion)
                                   direccion.tx_direccion=request.POST['tx_direccion']
                                   direccion.co_ciudad=request.POST['co_ciudad']
                                   
                                   
                              else:
                                   direccion=MainTb008Direccion(
                                        tx_direccion=request.POST['tx_direccion'],
                                        co_ciudad=MainTb004Ciudad.objects.get(co_ciudad=request.POST['co_ciudad'])
                                        )
                                   persona.co_direccion=direccion
                              
                              persona.save()
                              direccion.save()
                              usuario.co_persona=persona
                              usuario.save()
                              content['msg']='La operacion ha sido realizada sactifactoriamente'
                    except IntegrityError:
                         content['msg']='Hubo un error en la operación'

               
               content['form']=form
     return render(request,'../templates/usuario/registroPersona.html',content)
     
@confirmado_user_required
@user2fa_required
def IndentificacionUsuario(request):
     content={
          'success':True,
          'data':{},
          'msg':'',
          'form':IndenrificacionUsuarioForm(),
          'algo':''
     }
     if request.user.co_indentificacion is None:
     #content['data']=MainTb021Indentificacion.objects.get(co_indentificacion=4)
          if request.method=='POST':
               if 'procesar' in request.POST:
                    form=IndenrificacionUsuarioForm(request.POST, request.FILES)
                    if form.is_valid():
                         try:
                              with transaction.atomic():
                                   tipo_indentificacion=MainTb020TipoIndentificacion.objects.get(co_tipo_indentificacion=request.POST['co_tipo_indentificacion'])
                                   usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
                                  
                                   indentificacion = MainTb021Indentificacion(
                                        co_tipo_indentificacion = tipo_indentificacion,
                                        img_uno = form.cleaned_data['img_uno_file'],
                                        img_dos = form.cleaned_data['img_dos_file'],
                                        img_selfie = form.cleaned_data['img_selfie_file'],
                                        in_activo=False,
                                        co_usuario=usuario,
                                        co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                                        )
                                   indentificacion.save()
                                   """usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
                                   usuario.co_indentificacion=indentificacion
                                   usuario.save()"""
                                   content['msg']='La operacion ha sido realizada sactifactoriamente'
                                   cont=basePerfilUsuario(request.user,content['msg'],True)  
                                   return render(request,'../templates/usuario/perfil.html',cont)

                         except IntegrityError:
                              
                              content['success']=False
                              content['msg']='Hubo un error en la operación'

                    else:
                         content['success']=False
                         content['msg']='Hubo problemas'
                    content['form']=form    
          return render(request,'../templates/usuario/seguridad/documentos.html',content)
     elif request.user.co_indentificacion.co_estatus.co_estatus==3:
          if request.method=='POST':
               if 'procesar' in request.POST:
                    form=IndenrificacionUsuarioForm(request.POST, request.FILES)
                    if form.is_valid():
                         try:
                              with transaction.atomic():
                                   tipo_indentificacion=MainTb020TipoIndentificacion.objects.get(co_tipo_indentificacion=request.POST['co_tipo_indentificacion'])
                                   indentificacion = MainTb021Indentificacion(
                                        co_tipo_indentificacion = tipo_indentificacion,
                                        img_uno = form.cleaned_data['img_uno_file'],
                                        img_dos = form.cleaned_data['img_dos_file'],
                                        img_selfie = form.cleaned_data['img_selfie_file'],
                                        in_activo=False,
                                        co_estatus=WalletTb009Estatus.objects.get(co_estatus=1),
                                        co_usuario=request.user)
                                   indentificacion.save()
                                   usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
                                   usuario.co_indentificacion=indentificacion
                                   usuario.save() 
                                   content['msg']='La operacion ha sido realizada sactifactoriamente'
                                   cont=basePerfilUsuario(usuario,content['msg'],True)  
                                   return render(request,'../templates/usuario/perfil.html',cont)

                         except IntegrityError:
                              content['msg']='Hubo un error en la operación'

                    else:
                         content['msg']='Hubo problemas'
                    content['form']=form    
          return render(request,'../templates/usuario/seguridad/documentos.html',content)          
     else:
          ms='Sus datos se encuentran en verificación'
          cont=basePerfilUsuario(request.user,ms,False)  
          return render(request,'../templates/usuario/perfil.html',cont) 


     

@confirmado_user_required
@user2fa_required
@user_staff_required
def VerificarIndUsuario(request):
     content={
          'success':True,
          'data':'',
          'msg':''
     }
     #bandeja=MainTb021Indentificacion.objects.filter(co_estatus=1)
     bandeja=Tb001Usuario.objects.filter(co_estatus_img=1,co_estatus_persona=1)
     content['data']=bandeja
     return render(request,'../templates/usuario/seguridad/bandeja.html',content)

@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
def getIndentificacion(request):
     content={
        'success':False,
        'data':{
             'persona':'',
             'imagenes':''
        },
        'msg':'',
     }
     if request.method=='POST':
          if 'co_usuario' in request.POST and request.POST['co_usuario']!='':
               if request.user.is_staff and request.user.in_confirmado:
                    queryset=Tb001Usuario.objects.filter(co_usuario=request.POST['co_usuario'])

                    serializer = UsuarioPersonaSerializer(queryset , many=True)
                    content['data']['persona']=serializer.data

                    queryset2=MainTb021Indentificacion.objects.filter(co_indentificacion=queryset[0].co_indentificacion.co_indentificacion)
                    serializer2 = IndentificacionSerializer(queryset2 , many=True)
                    content['data']['imagenes']=serializer2.data

     return Response(content)

@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
def validarIndentificacion(request):
     content={
        'success':False,
        'data':'',
        'msg':'',
     }
     if request.method=='POST':
          if 'co_usuario' in request.POST and request.POST['co_usuario']!='' and 'co_estatus' in request.POST :
               if request.user.is_staff and request.user.in_confirmado:
                    try:
                         with transaction.atomic():
                              indentificacion=Tb001Usuario.objects.get(co_usuario=request.POST['co_usuario'])
                              indentificacion.co_estatus_img=WalletTb009Estatus.objects.get(co_estatus=request.POST['co_estatus'])
                              indentificacion.co_estatus_persona=WalletTb009Estatus.objects.get(co_estatus=request.POST['co_estatus'])
                              indentificacion.save()
                              if request.POST['co_estatus']==3:
                                   content['msg']='Se ha rechazado la indentificacion sactifactoriamente'
                              else:
                                   content['msg']='Se ha aprovado la indentificacion sactifactoriamente'

                              content['success']=True
                    except IntegrityError:
                              content['msg']='Hubo un error en la operación'
        
     return Response(content)


@confirmado_user_required
#@user2fa_required
def Config2FAUsuario(request):
     return None
     """
     #
     
     #default_qr_factory = qrcode.image.svg.SvgPathImage
     #qrcode_url = 'prontopagos:qr'
     
     #skey=qrcode_url.get_key()
     estaticoDevice=StaticDevice(
          user_id=request.user.co_usuario
     )

     estaticoDevice=StaticDevice.objects.get(id=9)
     #estaticoDevice.save()
     tooken=random_hex_str(5)
     estaticoToken=estaticoDevice.token_set.create(
          token=StaticToken.random_token()
     )
     
     device=TOTPDevice(
          user_id=request.user.co_usuario
     )
     key2=pyotp.random_base32()
     #estaticoDevice.token_set.create(token=key2)
     #estaticoToken=StaticToken()
     #tooken=b32encode(estaticoToken.random_token()).decode("ascii")
     estaticoToken=StaticToken(
          device_id=estaticoDevice.id,
          token=StaticToken.random_token()
     )
     #estaticoToken.save()

     print(estaticoDevice.verify_token("XSZW45OL5K276BOF"))


     #device.save()
     print(device)
     key = random_hex_str(20)
     print(key2+"-"+str(len(key2)))
     #rawkey = unhexlify(key.encode('ascii'))
     b32key = b32encode(bytes(device.key,"ascii")).decode("ascii")

     #print(b32key)

     #issuer=get_current_site(request).name
     issuer="gobelx.io"
     if 'key' not in request.session:
          request.session['key']=b32key
          request.session['key2']=key

     #cosas=request.session['key']+"-"+key+"-"
     #link="otpauth://totp/"+issuer+":albertostruve27@gmail.com?secret="+b32key+"&issuer="+issuer+"&algorithm=SHA1&digits=6&period=30"
     
     link=pyotp.totp.TOTP(b32key).provisioning_uri(name='albertostruve33@gmail.com', issuer_name=issuer)
     print(link)
     cosas=device.key+"-"+tooken
     #print(link)
     #qrcode.make('Some data here')
     content={
          'success':True,
          'data':'',
          'msg':cosas,
          'form':Autenticar2FAForm(user=request.user),
          'img':''
     }
     #content['img']=reverse('qr2')
     #print(qrcode.make(link))
     content['img']=link
     
     if request.method=='POST':
          
          #form=registerTOTPDeviceForm(request.POST,user=request.user)
          form=Autenticar2FAForm(request.POST,user=request.user)
          if form.is_valid():
               content['msg']="dwasaby fdfd"
               if request.user.in_confirmado:
                    try:
                         with transaction.atomic():
                              
                              device=OtpTotpTotpdevice(
                                        user=request.user,
                                        confirmed=True,
                                        last_t=-1,
                                        key=unhexlify(request.session['key2'].encode('ascii')),
                                        tolerance=tolerance,
                                        throttling_failure_count=0,
                                        t0=t0,
                                        step=step,
                                        drift=drift,
                                        digits=digits,
                                        name='default')
                              
                              #device.save()
                              testdevice = match_token(request.user,str(request.POST['otp_token']))
                              dev=OtpTotpTotpdevice.objects.filter(user_id=request.user)[0]
                              dev2=pyotp.totp.TOTP(b32encode(bytes(dev.key,"ascii")).decode("ascii"))
                              #DC3Z4O6ASJ62ACCK
                              #dev2=pyotp.totp.TOTP("DC3Z4O6ASJ62ACCK")
                              
                              print('OTP code:', dev2.now())
                              print('Code Valid:',dev2.verify(int(request.POST['otp_token'])))
                              #tOTP=TOTP(b32encode(bytes(dev.key,"ascii")))
                              #tOTP.drift=dev.drift
                              print(request.POST['otp_token'])
                              #print(tOTP.verify(request.POST['otp_token'],tolerance=1))

                              print(testdevice)
                              if testdevice is not None:
                                   print("todo bien")
                                   content['msg']="todo bien"

                              else:
                                   print("algo paso")
                                   content['msg']="algo paso"
                              

                    except IntegrityError:
                              content['msg']='Hubo un error en la operación'
               else:
                    content['msg']='Hubo un error en la operación2'
          else:
               content['msg']='Hubo un problemas'
          content['form']=form
   
     return render(request,'../templates/usuario/seguridad/configuracion2fa.html',content)"""





@transaction.atomic
@confirmado_user_required
def editarUsuario2Fa(request):
     init={}
     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
     
     if usuario.co_persona is not None:
          init['tx_primer_nombre']=usuario.co_persona.tx_primer_nombre
          init['tx_segundo_nombre']=usuario.co_persona.tx_segundo_nombre
          init['tx_primer_apellido']=usuario.co_persona.tx_primer_apellido
          init['tx_segundo_apellido']=usuario.co_persona.tx_segundo_apellido
          init['fe_nacimiento']=usuario.co_persona.fe_nacimiento
          init['nu_documento']=usuario.co_persona.nu_documento
          init['co_tipo_documento']=usuario.co_persona.co_tipo_documento.co_tipo_documento if usuario.co_persona.co_tipo_documento is not None else None
          init['co_sexo']=usuario.co_persona.co_sexo.co_sexo if usuario.co_persona.co_sexo is not None else None
          init['nu_telefono']=usuario.co_persona.nu_telefono
          init['tx_denom_comercial']=usuario.co_persona.tx_denom_comercial
     
          if usuario.co_persona.co_direccion is not None:
               init['co_pais']=usuario.co_persona.co_direccion.co_pais
               init['tx_direccion']=usuario.co_persona.co_direccion.tx_direccion
               if usuario.co_persona.co_direccion.co_ciudad is not None:
                    init['co_ciudad']=usuario.co_persona.co_direccion.co_ciudad.co_ciudad
                    if usuario.co_persona.co_direccion.co_ciudad.co_estado is not None:    
                         init['co_estado']=usuario.co_persona.co_direccion.co_ciudad.co_estado.co_estado
                         if usuario.co_persona.co_direccion.co_ciudad.co_estado.co_pais is not None:
                              init['co_pais']=usuario.co_persona.co_direccion.co_ciudad.co_estado.co_pais.co_pais

               
               
               


          indentificacion=MainTb021Indentificacion.objects.filter(co_usuario=usuario.co_usuario,in_activo=True)[0] if MainTb021Indentificacion.objects.filter(co_usuario=usuario.co_usuario,in_activo=True).count()>0 else None  
          if indentificacion is not None:
               if indentificacion.co_tipo_indentificacion is not None:
                    init['co_tipo_indentificacion']=indentificacion.co_tipo_indentificacion.co_tipo_indentificacion
          

     token, _ = Token.objects.get_or_create(user = request.user)
     content={
          'success':True,
          'data':{
               'persona':''
          },
          'msg':'',
          'token':token,
          'form':EditarUsuario2FaForm(usu=usuario,initial=init),
     }
     if request.method=='POST':
          if 'guardar' in request.POST:
               form=EditarUsuario2FaForm(request.POST, request.FILES)
               if form.is_valid():
                    
                    content['msg']='aqui todo bien'
                    try:
                         with transaction.atomic():
                              if usuario.co_persona is not None:

                                   persona=MainTb005Persona.objects.get(co_persona=usuario.co_persona.co_persona)
                                   persona.nu_documento=request.POST['nu_documento']
                                   
                                   if request.POST['co_tipo_documento'] =='':
                                        persona.co_tipo_documento=None
                                   else:
                                        persona.co_tipo_documento=MainTb006TipoDocumento.objects.get(co_tipo_documento=request.POST['co_tipo_documento'])

                                   persona.tx_primer_nombre=request.POST['tx_primer_nombre']
                                   persona.tx_segundo_nombre=request.POST['tx_segundo_nombre']
                                   persona.tx_primer_apellido=request.POST['tx_primer_apellido']
                                   persona.tx_segundo_apellido=request.POST['tx_segundo_apellido']
                                   persona.nu_telefono=request.POST['nu_telefono']
                                   persona.tx_denom_comercial=request.POST['tx_denom_comercial']

                                   if request.POST['co_sexo'] == '':
                                        persona.co_sexo=None
                                   else:
                                        persona.co_sexo=MainTb007Sexo.objects.get(co_sexo=request.POST['co_sexo'])

                                   if request.POST['fe_nacimiento'] == '':
                                        persona.fe_nacimiento=None
                                   else:
                                        persona.fe_nacimiento=request.POST['fe_nacimiento']
                                   

                              else:
                                   persona=MainTb005Persona(
                                        nu_documento=request.POST['nu_documento'],
                                        co_tipo_documento=MainTb006TipoDocumento.objects.get(co_tipo_documento=request.POST['co_tipo_documento']),
                                        tx_primer_nombre=request.POST['tx_primer_nombre'],
                                        tx_segundo_nombre=request.POST['tx_segundo_nombre'],
                                        tx_primer_apellido=request.POST['tx_primer_apellido'],
                                        tx_segundo_apellido=request.POST['tx_segundo_apellido'],
                                        nu_telefono=request.POST['nu_telefono'],
                                        co_sexo=MainTb007Sexo.objects.get(co_sexo=request.POST['co_sexo']),
                                        fe_nacimiento=request.POST['fe_nacimiento'],
                                        tx_denom_comercial=request.POST['tx_denom_comercial'])
                              

                              if persona.co_direccion is not None:
                                   direccion=persona.co_direccion
                                   direccion.co_pais=MainTb002Pais.objects.get(co_pais=request.POST['co_pais'])
                                   direccion.save()

                              else:
                                   direccion=MainTb008Direccion(
                                        #tx_direccion=request.POST['tx_direccion'],
                                        #co_ciudad=MainTb004Ciudad.objects.get(co_ciudad=request.POST['co_ciudad'])
                                        co_pais=MainTb002Pais.objects.get(co_pais=request.POST['co_pais'])
                                        )
                                   direccion.save()
                                   persona.co_direccion=direccion
                                   persona.save()
                                   #if request.POST['co_ciudad'] == '':
                                        #direccion.co_ciudad=None
                                   #else:
                                        #direccion.co_ciudad=MainTb004Ciudad.objects.get(co_ciudad=request.POST['co_ciudad'])
                                   
                              #elif request.POST['co_ciudad'] == '':
                              #     direccion=MainTb008Direccion(
                              #          tx_direccion=request.POST['tx_direccion'],
                              #          co_ciudad=None
                              #          )
                               #    persona.co_direccion=direccion
                                   
                                   #persona.co_direccion=direccion
                              

                              #if request.POST['co_tipo_indentificacion'] == '':
                              #     tipo_indentificacion=None
                              #else:
                              #     tipo_indentificacion=MainTb020TipoIndentificacion.objects.get(co_tipo_indentificacion=request.POST['co_tipo_indentificacion'])
                              
                              #if indentificacion is None:

                               #    indentificacion = MainTb021Indentificacion(
                                #   co_tipo_indentificacion = tipo_indentificacion,
                                #   img_uno = form.cleaned_data['img_uno_file'],
                                #   img_dos = form.cleaned_data['img_dos_file'],
                                #   img_selfie = form.cleaned_data['img_selfie_file'],
                                #   co_usuario=usuario,
                                #   co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                                #   )

                              #else:
                              #     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)
                              #     if request.POST['co_tipo_indentificacion'] == '':
                              #         indentificacion.co_tipo_indentificacion=None
                              #     else:
                              #          indentificacion.co_tipo_indentificacion=MainTb020TipoIndentificacion.objects.get(co_tipo_indentificacion=request.POST['co_tipo_indentificacion'])
                              #     
                              #     indentificacion.img_uno= form.cleaned_data['img_uno_file']
                              #     indentificacion.img_dos = form.cleaned_data['img_dos_file']
                              #     indentificacion.img_selfie = form.cleaned_data['img_selfie_file']

                              #print(persona)
                              
                              #usuario.co_persona=persona
                              #print(usuario)
                              #print(indentificacion)

                              
                              
                              persona.save()
                              
                              usuario.co_persona=persona
                              usuario.save()
                              #indentificacion.save()

                              content['msg']='Los datos fueron guardados satisfactoriamente'
                    except IntegrityError:
                         content['msg']='Hubo un error al guardar'
               else:
                    content['msg']="Hubo un error en los datos"

               
               content['form']=form
     return render(request,'../templates/usuario/editarPersona.html',content)





def registrar_cuentas_usuarios(request):

     token, _ =Token.objects.get_or_create(user = request.user)

     content={
          'success':True,
          'data':'',
          'msg':'',
          'form':registrarCuentasForm(),
          'token':token,
          
     }

     content['msg']=""
     if  request.method=='POST':
          
          form=registrarCuentasForm(request.POST)
          if form.is_valid():
         

            try:
                with transaction.atomic():
                    
                   
                    """cuenta_usuario=MaintTb017Cuenta(
                        tx_cuenta=request.POST['tx_cuenta'],
                        co_banco_moneda=MaintTb016BancoMoneda.objects.get(co_banco_moneda=request.POST['co_banco']),
                        co_usuario=request.user,
                        
                        )
                    cuenta_usuario.save()"""
                  
                    content['msg']=" Su registro se realizó sactifactoriamente"
                         
            except IntegrityError:
                content['success']=False
                content['msg']=""
                   

          content['form']=form
          


     return render(request, '../templates/usuario/registrarcuentas.html',content)



@confirmado_user_required
def ValidarIdentidad(request,co_tipo_solicitud):
     content=getContentBasicoL2(request.user,co_tipo_solicitud,None)
     content['data']=Tb001Usuario.objects.filter(co_persona=request.user.co_persona)

     identidad=MainTb028Solicitud.objects.newSolicitud(co_tipo_solicitud,request.user)
                    
     
     return redirect('bandejaInicioSuccess',success=1,codigo=2)


def notificacion_email(user, detalles,template):
     
     
     
     texto="../templates/usuario/"+template
     body = render_to_string(texto,
          {'codigo':detalles}
     )


     email=EmailMessage(
          subject='Notificación de ProntoPagos',
          body=body,
          from_email=settings.EMAIL_HOST_USER,
          to=[user.email]

     )
     email.content_subtype='html'
     email.send()


def ConfirmacionIP(request):
     content={
          'success':True,
          'msg':'',
          'data':{}
     }
     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)

     if request.method=='POST':
          if usuario.email is not None:
               if 'activar' in request.POST:
                    usuario.in_ip=True
                    usuario.save()
                    content['success']=True
                    content['msg']='ha activado exitosamente las notificaciones de IP'

               if 'desactivar' in request.POST:
                    usuario.in_ip=False
                    usuario.save()
                    content['success']=True
                    content['msg']='ha desactivado las notificaciones de IP'
          
          else:
               content['success']=False
               content['msg']='El usuario no tiene una cuenta de email valida'

     content['data']=usuario

     return render(request,'../templates/usuario/seguridad/confirmacionIP.html',content)



# def registrarIP(request, usuario):
#      content={
#           'form':LoginForm(),
#           'msg':'',
#           'success':True
#      }
     
#      if usuario.in_2fa==True:
#           consulta=MainTb042RegistroIP.objects.filter(tx_registro_ip=request.META['REMOTE_ADDR'], co_usuario=usuario)
#           try:
#                with transaction.atomic():
#                     consulta=MainTb042RegistroIP.objects.get(tx_registro_ip=request.client_ip, co_usuario=usuario)
               
#                if consulta.in_activo  == True and consulta.tx_registro_ip == request.client_ip:
#                     emailMessage(usuario,'emailclave2fa.html')
#                     return valCorreo(request,usuario,request.POST['password'])
                    
               
#                elif consulta.in_activo == False and consulta.tx_registro_ip==request.client_ip:
#                     ip=request.client_ip
#                     detalles={"ip":ip, "usuario":usuario.username,"co_usuario":usuario.co_usuario, "co_registro_ip":consulta.co_registro_ip}
#                     notificacion_email(usuario,detalles,'emailconfirmarip.html')
#                     content['success']=False
#                     content['msg']='IP no Verificada, Hemos enviado el link de activacion a su correo'
                    
#                     return render(request, '../templates/inicio/login2.html',content)
#           except ObjectDoesNotExist:

#                t=MainTb042RegistroIP(
#                tx_registro_ip=request.client_ip,
#                co_usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario),
#                in_activo = False,
#                tx_usuario_agente = request.META['HTTP_USER_AGENT'],
#                )
#                t.save()
#                ip=request.client_ip
#                detalles={"ip":ip, "usuario":usuario.username,"co_usuario":usuario.co_usuario, "co_registro_ip":t.co_registro_ip}
#                notificacion_email(usuario,detalles,'emailconfirmarip.html')
#                content['success']=False
#                content['msg']='IP no Verificada, Hemos enviado el link de activacion a su correo'
#                return render(request, '../templates/inicio/login2.html',content)
          
     

#      else:
#           consulta=MainTb042RegistroIP.objects.filter(tx_registro_ip=request.client_ip, co_usuario=usuario)
#           try:
#                with transaction.atomic():
#                     consulta=MainTb042RegistroIP.objects.get(tx_registro_ip=request.client_ip, co_usuario=usuario)
               
#                if consulta.in_activo  == True and consulta.tx_registro_ip == request.client_ip:
                    
#                     return redirect('inicio')
                    
               
#                elif consulta.in_activo == False and consulta.tx_registro_ip==request.client_ip:
#                     ip=request.client_ip
#                     detalles={"ip":ip, "usuario":usuario.username,"co_usuario":usuario.co_usuario, "co_registro_ip":consulta.co_registro_ip}
#                     notificacion_email(usuario,detalles,'emailconfirmarip.html')
#                     content['success']=False
#                     content['msg']='IP no Verificada, Hemos enviado el link de activacion a su correo'
                    
#                     return render(request, '../templates/inicio/login2.html',content)
#           except ObjectDoesNotExist:

#                t=MainTb042RegistroIP(
#                tx_registro_ip=request.client_ip,
#                co_usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario),
#                in_activo = False,
#                tx_usuario_agente = request.META['HTTP_USER_AGENT']
#                )
#                t.save()
#                ip=request.client_ip
#                detalles={"ip":ip, "usuario":usuario.username,"co_usuario":usuario.co_usuario, "co_registro_ip":t.co_registro_ip}
#                notificacion_email(usuario,detalles,'emailconfirmarip.html')
#                content['success']=False
#                content['msg']='IP no Verificada, Hemos enviado el link de activacion a su correo'
#                return render(request, '../templates/inicio/login2.html',content)
          
               



# def activacionIP(request, co_registro_ip, co_usuario):
#      content={
#           'form':LoginForm(),
#           'msg':'',
#           'success':True,
#      }

#      registro_ip=co_registro_ip
#      usuario=co_usuario
#      ip = MainTb042RegistroIP.objects.get(co_registro_ip=registro_ip, co_usuario=usuario)
#      ip.in_activo=True
#      ip.save()


#      content['success']=True
#      content['msg']='IP Confirmada Sastifactoriamente.'
#      return redirect('login')



# def registrarIP(request, usuario):
#      content={
#           'form':LoginForm(),
#           'msg':'',
#           'success':True
#      }
     
#      if usuario.in_2fa==True:
#           try:
#                with transaction.atomic():
#                     consulta=MainTb042RegistroIP.objects.filter(tx_registro_ip=get_client_ip(request)[0], co_usuario=usuario, in_activo=True)[0]
               
#                if consulta.in_activo  == True and consulta.tx_registro_ip == get_client_ip(request)[0]:
                    
#                     t=devices_for_user(user=usuario)
#                     for val in t:
#                          dev=val if val.name=="client-device" else None
#                     dj_login(request,usuario)
#                     otp_login(request,dev)
#                     #request.session["merma"]=getMerma(username,password)
#                     ipHistorico(request,usuario)
#                     return redirect('inicio')
                    
#           except ObjectDoesNotExist:
               
#                t=MainTb042RegistroIP(
#                tx_registro_ip=get_client_ip(request)[0],
#                co_usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario),
#                in_activo = False,
               
#                )
#                t.save()
#                ip=get_client_ip(request)[0]
#                detalles={"ip":ip, "usuario":usuario.username,"co_usuario":usuario.co_usuario, "co_registro_ip":t.co_registro_ip}
#                notificacion_email(usuario,detalles,'emailconfirmarip.html')
#                content['success']=False
#                content['msg']='IP no Verificada, Hemos enviado el link de activacion a su correo'
#                return render(request, '../templates/inicio/login2.html',content)
     
#      elif usuario.in_2fa_email==True:
#            try:
#                with transaction.atomic():
#                     consulta=MainTb042RegistroIP.objects.filter(tx_registro_ip=get_client_ip(request)[0], co_usuario=usuario, in_activo=True)[0]
               
#                if consulta.in_activo  == True and consulta.tx_registro_ip == get_client_ip(request)[0]:
#                     # ipHistorico(request,usuario)
#                     emailMessage(usuario,'emailclave2fa.html')
#                     return valCorreo(request,usuario,request.POST['password'])
                    
#            except ObjectDoesNotExist:
               
#                t=MainTb042RegistroIP(
#                tx_registro_ip=get_client_ip(request)[0],
#                co_usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario),
#                in_activo = False,
               
#                )
#                t.save()
#                ip=get_client_ip(request)[0]
#                detalles={"ip":ip, "usuario":usuario.username,"co_usuario":usuario.co_usuario, "co_registro_ip":t.co_registro_ip}
#                notificacion_email(usuario,detalles,'emailconfirmarip.html')
#                content['success']=False
#                content['msg']='IP no Verificada, Hemos enviado el link de activacion a su correo'
#                return render(request, '../templates/inicio/login2.html',content)
          
     

#      else:
        
#           try:
#                with transaction.atomic():
#                     consulta=MainTb042RegistroIP.objects.filter(tx_registro_ip=get_client_ip(request)[0], co_usuario=usuario, in_activo=True).get()
               
#                if consulta.in_activo  == True and consulta.tx_registro_ip == get_client_ip(request)[0]:
#                     ipHistorico(request,usuario)
                    
#                     return redirect('inicio')
                    
               
#           except ObjectDoesNotExist:
              
#                t=MainTb042RegistroIP(
#                tx_registro_ip=get_client_ip(request)[0],
#                co_usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario),
#                in_activo = False,
#                )
#                t.save()
#                ip=get_client_ip(request)[0]
#                detalles={"ip":ip, "usuario":usuario.username,"co_usuario":usuario.co_usuario, "co_registro_ip":t.co_registro_ip}
#                notificacion_email(usuario,detalles,'emailconfirmarip.html')
#                content['success']=False
#                content['msg']='IP no Verificada, Hemos enviado el link de activacion a su correo'
#                return render(request, '../templates/inicio/login2.html',content)
          
               



def activacionIP(request, co_registro_ip, co_usuario):
     content={
          'form':LoginForm(),
          'msg':'',
          'success':True,
     }

     registro_ip=co_registro_ip
     usuario=co_usuario
     ip = MainTb042RegistroIP.objects.get(co_registro_ip=registro_ip, co_usuario=usuario)
     ip.in_activo=True
     ip.save()


     content['success']=True
     content['msg']='IP Confirmada Sastifactoriamente.'
     return redirect('login')


def Usuario_sesiones(request):
     content={
        'success':True,
        'data':'',
        'user':'',
        
     }
     sesiones = MainTb043HistorialConexion.objects.filter(co_usuario=request.user).order_by('-created_at')

     content['data']=sesiones

     
     # return HttpResponse(escape(repr(todo)))
     
     


     return render(request, '../templates/usuario/sesiones.html',content)




# def ipHistorico(request,usuario):
#      g = GeoIP2('GeoLite2-City_20201103') #Enter folder name
#      geo = g.city(get_client_ip(request)[0])
#      ciudad=geo.get('city')
#      pais=geo.get('country_name')
#      cidadpais=str(ciudad)+'/'+str(pais)
     

#      ip = MainTb043HistorialConexion(
#           tx_registro_ip=get_client_ip(request)[0],
#           co_usuario=Tb001Usuario.objects.get(co_usuario=usuario.co_usuario),
#           tx_usuario_agente = getNavegador(request),
#           tx_ciudad_pais = cidadpais,
          
#      )
#      ip.save()


def contactos(request):
     content=getContentBasicoL2(request.user,None,agregarContactosForm())
     content["form2"]=editarContactosForm()

     contacto=MainTb022Directorio.objects.filter(co_usuario=request.user)
     content['data']=contacto
     
     if 'agregar' in request.POST:
          form=agregarContactosForm(request.POST,usu=request.user)

          content["form"]=form
     
     return render(request, '../templates/usuario/contactos.html',content)



def DesactivarNotificacion(request):
     content={
          'success':True,
          'msg':'',
          'data':{}
     }
     usuario=Tb001Usuario.objects.get(co_usuario=request.user.co_usuario)

     if 'activar' in request.POST:
          usuario.in_notificacion=True
          usuario.save()
          content['success']=True
          content['msg']='ha activado exitosamente las Notificaciones'

     if 'desactivar' in request.POST:
          usuario.in_notificacion=False
          usuario.save()
          content['success']=True
          content['msg']='ha desactivado las Notificaciones'
     
     content['data']=usuario

     return render(request,'../templates/usuario/notificaciones/Desactivar_notif.html',content)


     