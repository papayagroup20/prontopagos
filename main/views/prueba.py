from django.shortcuts import render, redirect

from stellar_sdk.server import Server
from stellar_sdk import TransactionBuilder, Network, Keypair, Account,Asset,Operation,AllowTrust
from stellar_sdk.sep import stellar_toml
from stellar_sdk.sep.stellar_web_authentication import read_challenge_transaction
from stellar_sdk.transaction_envelope import TransactionEnvelope
from stellar_sdk.transaction import Transaction
from stellar_sdk.operation import Operation
from main.views.utils import getContentBasicoL2,getContentBasicoL
from main.models import WalletTb038MensajeStellar
from apipago.serializers import MensajeStellarSerializer
from urllib.parse import parse_qs
from django.core import serializers
from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required

from rest_framework.response import Response
from rest_framework.decorators import action,api_view,renderer_classes,permission_classes,parser_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import requests as peticion
from hashlib import sha256
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
import base64
import hashlib
from Crypto.Cipher import AES
from base64 import b64encode
from Crypto.Cipher import AES
from Crypto import Random 


from wallet.views.general import getHeaderFederacion
import json
from apipago.views.stellar import (
    getAssetAccountToml,getActivoRecomendados,getActivoMioReco,
    getOfertasAccount,getTradesAccount,getOrdenBook,
    getActivoMioReco2)

from stellar_sdk.sep.stellar_uri import TransactionStellarUri,PayStellarUri
from stellar_sdk import asset, memo as stellar_memo

from Crypto.Hash import SHA256
from Crypto.Cipher import AES
from base64 import b64encode
from Crypto.Util.Padding import pad
import requests
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated


def pruebastellar1(request):

    content=getContentBasicoL2(request.user,None,None)
    #content={}
    activo=asset.Asset("BSS","GA6E6YDYXXAXG4VZAOGIX3N3HETLMGVBLQKAAIZQ3PIMQGHAQ63RA6EO")
    content['publico']=request.user.getPublic()

    uri=PayStellarUri(
        destination="GAKMNYOYQVBCWONKCMA42GRS2UTFQ24TNEL2MSI67HBSUAGUNCIZVFNP",
        amount="0.0001",
        origin_domain="localhost:8000",
        asset=activo,
        memo=stellar_memo.TextMemo("prueba memo"),

        )


    uri.sign("SCOQ3CGT4UI2ZO4GEXA4XYHV5JBB6VVPIQD65R63KVAFK62CUGA4ERPX")
    print(uri.to_uri())
    content["uri"]=uri.to_uri()
    #content['publico']="GD3JP476GGDFXUGSQUFJHYMPBWRDSUWLFXYC4CNZDT27FE72FCJSNOF3"
    msjstellar=MensajeStellarSerializer(WalletTb038MensajeStellar.objects.all(),many=True)
    content['msjstellarjson']=json.dumps(msjstellar.data)
    return render(request, '../templates/pruebas/prueba.html',content)


def pruebauri(request):

    content={

    }
    #print(request.GET)
    uri=PayStellarUri.from_uri(request.GET["operation"])
    print(uri)
    return render(request, '../templates/pruebas/pruebalink1.html',content)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
def getToml(request):
    toml=stellar_toml.fetch_stellar_toml(request.POST['dominio'])
    return Response(toml)

@api_view(['POST'])
@renderer_classes([JSONRenderer])
def getTransactionEnvelope(request):
    
    root_keypair = Keypair.from_secret('SBSA2AXCVXY7QOXAKHUZOTIMUHCTZNCKF54UMJSUUBWUGSMKXZQBIFCP')

    transactions=TransactionEnvelope.from_xdr(request.POST['transaccion'],request.POST['network'])
    transactions.sign(root_keypair)
    #print(transactions.signatures)
    transa={
        'network_passphrase': request.POST['network'],
        #'signatures': transactions.signatures,
    }


    content={
        'transaccion':transa,
        'XDR':transactions.to_xdr()
    }


    return Response(content)


@api_view(['POST'])
@renderer_classes([JSONRenderer])
def getChequearTransaccion(request):
    poste={
         'asset_code':"SRT",
         'account':"GAFYTFOIIUEDGNFDQG6IIAO3TGBRVL4HCVX5XM7K5INWGZXDNDYOLE52",
          'lang':"en"
    }
    header={
        "Authorization": 'Bearer '+request.POST['token']
    }
    por=peticion.post(request.POST['url'],data=poste,headers=header)
    content=por.json()

    return Response(content)


@api_view(['POST'])
@renderer_classes([JSONRenderer])
def getBalanceSTR(request):
    
    server = Server("https://horizon.stellar.org")
    account = server.accounts().account_id(request.user.getPublic()).call()
    
    balance=None

    for stval in account['balances']:
        if 'asset_issuer' in stval:
            if stval['asset_code']=="BSS":
                balance=stval['balance']
    content={
        'balance':balance,
        'todo':account['balances']
    }
    return Response(content)


def pruebaLo1(request):
    tx=sha256(b"Prueba4.")
    print(tx)
    
    url="https://stellarid.io/api/addresses/"+str(1089)
    gett=peticion.get(url,data=None,headers=getHeaderFederacion(request))
    fede=gett.json()

    url2="https://api.gobelx.io/api/authkey"
    dataget={
        "address":fede['account_id'],
        "hash":tx.hexdigest()
    }
    gett2=peticion.get(url2,params=dataget,headers=None)
    key=gett2.json()
    key2=""
    if 'address' in key:
        header={
        "Connection":"Close",
        "Content-Type":"application/x-www-form-urlencoded",
        }

        url3="http://127.0.0.1:3512/api/keypair/"
        dataget2={
            "address":fede['account_id'],
            "pass":"Prueba4."
        }
        gett3=peticion.post(url3,params=dataget2,headers=header)
        key2=gett3
        print(key2.text)

    else:
        print("adada")
    #tx.hexdigest()
    content={
        "texto":key2.text
    }

    return render(request, '../templates/pruebas/pruebalo1.html',content) 


@confirmado_user_required
@user2fa_required
def librosPrueba(request,code1:str=None,issuer1:str=None,code2:str=None,issuer2:str=None)->render:
    content=getContentBasicoL2(request.user,None,None)
    content['publico']=request.session["public_key"]
    msjstellar=MensajeStellarSerializer(WalletTb038MensajeStellar.objects.all(),many=True)
    content['msjstellarjson']=json.dumps(msjstellar.data)  
    actrec=getActivoMioReco2(content['publico'],True)
    content['activosrecoment']=actrec["datos"]
    #content['publico']="GBY6ZDFNONM26OMQCT4VWWMFSI64WSMGOGJ7OMN7D2CAIGHZMDPT6ARX"
    #misAsset=getAssetAccountToml(content['publico'])
    misAsset=[]
    #print(misAsset)
    content["ofertas"]=actrec["ofertas"]
    #print(content["ofertas"])
    content["trades"]=getTradesAccount(content['publico'])
    #print(content["trades"])
    
    #if "data" in getOrdenBook
    #content[""]
    for val in content['activosrecoment']:
        if "propio" in val or val["asset_type"] == "native":
            misAsset.append(val)

    content['code1']=code1 if code1 is not None else "0"
    content['issuer1']=issuer1 if issuer1 is not None else "0"
    content['code2']=code2 if code2 is not None else "0"
    content['issuer2']=issuer2 if issuer2 is not None else "0"

    i=True
    j=True
    k=True
    content['actinit']=""
    content['actinitbuy']=""
    content['actinitsell']=""
    if len(misAsset)>0:
        

        for va in misAsset:
            #print(va)
            if va["asset_type"] != "native" and i:
                content['actinitbuy']=va
                i=False
            elif va["asset_type"] == "native":
                content['actinitsell']=va

        if code1 is not None and issuer1 is not None:
            #print(code1)
            for val in content['activosrecoment']:
                if j:
                    if code1=="XLM" and val["tx_asset_code"]=="XLM":
                        content['actinitsell']=val
                        j=False

                    if val["tx_asset_code"]==code1 and val["tx_asset_issuer"]==issuer1:
                        content['actinitsell']=val
                        j=False
                    elif issuer1=="0":
                        if code2=="XLM":
                            content['actinitsell']=val
                            j=False
                        elif val["tx_asset_code"]=="XLM":
                            content['actinitsell']=val
                            j=False

        if code2 is not None and issuer2 is not None:
            for val in content['activosrecoment']:
                if k:
                    if code2=="XLM" and val["tx_asset_code"]=="XLM":
                        content['actinitbuy']=val
                        k=False

                    elif val["tx_asset_code"]==code2 and val["tx_asset_issuer"]==issuer2:
                        content['actinitbuy']=val
                        k=False
                    elif issuer2=="0":
                        if code1=="XLM":
                            content['actinitbuy']=val
                            k=False
                        elif val["tx_asset_code"]=="XLM":
                            content['actinitbuy']=val
                            k=False

        if content['actinitbuy']!="":
            content['code2']=content['actinitbuy']['tx_asset_code'] if content['code2'] == "0" else content['code2']
            content['issuer2']=content['actinitbuy']['tx_asset_issuer'] if content['issuer2']== "0" else content['issuer2']
            if content['issuer2']=="":
                content['issuer2']="0"


    content["orderbook"]=getOrdenBook(
        code1=content['code1'] if content['code1']!="0" and content['code1']!="" else "XLM",
        code2=content['code2'] if content['code2']!="0" and content['code2']!="" else "XLM",
        issuer1=content['issuer1'] if content['issuer1']!="0" and content['issuer1']!="" else None,
        issuer2=content['issuer2'] if content['issuer2']!="0" and content['issuer2']!="" else None,
    )
    content["referencia"]=0
    if "data" in content["orderbook"]:
        if len(content["orderbook"]["data"]["asks"])>0:
            content["referencia"]=content["orderbook"]["data"]["asks"][0]["price"]



                

    content['misactivos']=misAsset



    content['actinitbuyjson']=json.dumps(content['actinitbuy'])
    content['actinitselljson']=json.dumps(content['actinitsell'])
    
    content['tradesjson']=json.dumps(content['trades'])

    





    print(content)

    return render(request,'../templates/pruebas/prueba3.html',content)




def prueba2tdc(request):
    content=getContentBasicoL2(request.user,None,None)

    return render(request,'../templates/pruebas/prueba4.html', content)


@api_view(['POST','GET'])
@renderer_classes([JSONRenderer])
@permission_classes((IsAuthenticated,))
def pruebatdc(request):
    content={
        'success':True,
        'data':''
    }
    h = SHA256.new()
    h.update(b'A9279120481620090622AA30')
    print (h.hexdigest()[:16])

    key = h.digest()[:16]
    cipher = AES.new(key, AES.MODE_ECB)

    print(cipher)

    l = request.POST["cvv"]
    print(l)
    arr = bytes(l, 'utf-8')
    data = b'752'
    data2 = b'1234'
    
    ct_bytes = cipher.encrypt(pad(arr, AES.block_size))
    ct_bytes2 = cipher.encrypt(pad(data2, AES.block_size))
    # iv = b64encode(cipher.iv).decode('utf-8')
    ct = b64encode(ct_bytes).decode('utf-8')
    # ct2 = b64encode(ct_bytes2).decode('utf-8')
    # result = json.dumps({'iv':iv, 'ciphertext':ct})
    # result = json.dumps({'cvv':ct2})
    
    # print(result)
    content['data'] = ct
    return JsonResponse(content)
# -----------------------------------------------------------------------------
    # url = "https://apimbu.mercantilbanco.com/mercantil-banco/sandbox/v1/payment/pay"

    # payload = json.dumps({
    # "merchant_identify": {
    #     "integratorId": 31,
    #     "merchantId": 150332,
    #     "terminalId": "abcde"
    # },
    # "client_identify": {
    #     "ipaddress": "10.0.0.1",
    #     "browser_agent": "Chrome 18.1.3",
    #     "mobile": {
    #     "manufacturer": "Samsung",
    #     "model": "S9",
    #     "os_version": "Oreo 9.1",
    #     "location": {
    #         "lat": 37.422476,
    #         "lng": 122.08425
    #     }
    #     }
    # },
    # 'transaction': {
    #     'trx_type': 'compra',
    #     "payment_method": "tdc",
    #     "card_number": "5412474303840324",
    #     "customer_id": "V17166936",
    #     "invoice_number": "78556753",
    #     "account_type": "",
    #     "twofactor_auth": "",
    #     "expiration_date": "2024/06",
    #     "cvv": "kT/0kk3I7c9SAYBCj3wvsw==",
    #     "currency": "VES",
    #     "amount": 2525.33
    # }
    # })
    # headers = {
    # 'Content-Type': 'application/json',
    # 'X-IBM-Client-Id': '81188330-c768-46fe-a378-ff3ac9e88824'
    # }

    # response = requests.request("POST", url, headers=headers, data=payload)
    
    # content['data'] = response
    # if(response):
    #     content['msg']=" operacion realizada con exito"
    
    # else:
    #     content['msg']="error en la operacion"
    #     content['success'] = False


    # print(response.text)
    # # print(response["transaction_response"])
    # return Response(result)

    

   
    # return render(request,'../templates/pruebas/prueba4.html')


