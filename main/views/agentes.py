from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required,user_passes_test
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import *
from main.models import *
from main.form import *
from django.utils.crypto import get_random_string
from django.contrib.auth.hashers import make_password,check_password
from django.core.mail import EmailMessage
from django.conf import settings
from stellar_sdk import TransactionBuilder, Network, Keypair, Account,Asset,Operation,AllowTrust
from stellar_sdk.server import Server
import requests
from django.db import IntegrityError, transaction
from apipago.serializers import *
from django.core import serializers
from rest_framework.authtoken.models import Token
import qrcode
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action,api_view,renderer_classes,api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from django.http import Http404
from django_otp.forms import OTPAuthenticationForm
from django.urls import reverse
from wallet.form import *
from django.views.generic.base import View

from django.utils.module_loading import import_string

from django.contrib.sites.shortcuts import get_current_site


from django.views.decorators.cache import never_cache

import qrcode
import qrcode.image.svg
from base64 import b32encode
from binascii import unhexlify
from django_otp import login as otp_login,match_token
from main.views import basePerfilUsuario
from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required,user_agente_required



@confirmado_user_required
@user2fa_required
def agentesSolicitudEnvio(request):
    content=basePerfilUsuario(request.user,'',True)

    if  request.method=='POST':
        try:
            with transaction.atomic():
                oldsolicitud=MainTb028Solicitud.objects.filter(co_tipo_solicitud=2,co_usuario=request.user.co_usuario)[0] if MainTb028Solicitud.objects.filter(co_tipo_solicitud=2,co_usuario=request.user.co_usuario).count()>0 else None
                if oldsolicitud is not None and oldsolicitud.co_estatus.co_estatus!=2:
                    if oldsolicitud.co_estatus.co_estatus==1:#pendiente
                        content['success']=False
                        content['msg']="ya tiene una solicitud pendiente"
                elif oldsolicitud is None or oldsolicitud.co_estatus.co_estatus==2:
                    solicitud=MainTb028Solicitud(
                            co_usuario=request.user,
                            co_tipo_solicitud=MainTb027TipoSolicitud.objects.get(co_tipo_solicitud=2),
                            co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                        )
                    if MainTb014AgenteServicios.objects.filter(co_usuario=request.user.co_usuario).count()<=0:

                        agente=MainTb014AgenteServicios(
                            co_usuario=request.user,
                            in_activo= False
                        )
                        agente.save()
                        agente_metodo_pago= MainTb036AgenteMetodoPago(
                        co_agente_servicios= agente,
                        co_metodo_pago= WalletTb025MetodoPago.objects.get(co_metodo_pago=1)
                        )
                        agente_metodo_pago.save()
                    
                    solicitud.save()
                    content=basePerfilUsuario(request.user,'',True)
                    content['msg']="La operacion ha sido realizada sactifactoriamente"

        except IntegrityError:
                content['success']=False
                content['msg']="Ha habido un error"


    return render(request,'../templates/usuario/perfil.html',content)

def agentesSolicitudLista(request):
    content={
          'success':True,
          'data':'',
          'msj':'',
        }
    
    usuario=MainTb028Solicitud.objects.filter(co_estatus=1,in_activo=True)
    content['data']=usuario

    if  request.method=='POST':
        try:
            with transaction.atomic():
                solicitud=MainTb028Solicitud.objects.get(co_solicitud=request.POST['codigo'])
                agente=MainTb014AgenteServicios.objects.filter(co_usuario=solicitud.co_usuario.co_usuario)[0]
                autoriza=MainTb014AgenteServicios.objects.get(co_agente_servicios=agente.co_agente_servicios)
                solicitud.in_activo=False
                solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=2)
                autoriza.in_activo=True
                autoriza.save()
                solicitud.save()
                content['msg']="La operacion ha sido realizada sactifactoriamente"

        except IntegrityError:
                content['success']=False
                content['msg']="Ha habido un error"

    return render(request, '../templates/wallet/servicios/solicitudesagentes.html',content)

def agenteDesafiliar(request):
    
    msg=''
    success=True
    if  request.method=='POST':
        try:
            with transaction.atomic():
                agente=MainTb014AgenteServicios.objects.filter(co_usuario=request.user.co_usuario)[0]
                desafiliar= MainTb014AgenteServicios.objects.get(co_agente_servicios=agente.co_agente_servicios)
                desafiliar.in_activo=False
                desafiliar.save()
                msg="La operacion ha sido realizada sactifactoriamente"

        except IntegrityError:
                success=False
                msg="Ha habido un error"

    content=basePerfilUsuario(request.user,msg,success)

    return render(request,'../templates/usuario/perfil.html',content)

@confirmado_user_required
def banAge(request):
    content=getContentBasicoL(request.user,None,bandejaServiciosForm(in_envio=False))

    rolsolicitud=MainTb037RolSolicitud.objects.filter(co_rol=request.user.co_rol.co_rol,in_activo=True).values_list('co_tipo_solicitud')
    bandejaServicios=WalletTb012BandejaServicios.objects.filter(in_activo=True).values_list('co_pago_servicios')
    serviciosDisp=MainTb041AgenteDetalleServicios.objects.filter(co_agente_servicios=request.user.getAgente().co_agente_servicios,in_activo=True).values_list('co_servicios')
    print(bandejaServicios)        
    if request.method=='POST':
        if 'boton' in request.POST:
            form=bandejaServiciosForm(in_envio=True)
            #content['data']=MainTb028Solicitud.objects.filter(Q(co_tipo_solicitud=request.POST['co_tipo_solicitud']),Q(co_estatus=request.POST['co_estatus']),Q(co_usuario=request.user) | Q(co_usuario2=request.user) )
            #if request.POST['co_servicios'] is not None:
            tomado=WalletTb012BandejaServicios.objects.filter(co_usuario=request.user.co_usuario,in_activo=True)
            ban=WalletTb005PagoServicios.objects.filter(
                co_solicitud__co_tipo_solicitud=15,
                co_solicitud__in_activo=True,
                co_servicios__in=serviciosDisp)
            if request.POST['co_estatus']!='':
                ban=ban.filter(co_estatus=request.POST['co_estatus'])
                tomado=tomado.filter(co_pago_servicios__co_estatus=request.POST['co_estatus'])

            if request.POST['co_servicios']!='':
                ban=ban.filter(co_servicios=request.POST['co_servicios'])
                tomado=tomado.filter(co_pago_servicios__co_servicios=request.POST['co_servicios'])


            elif request.POST['co_proveedor_servicios']!='':
                ban=ban.filter(co_servicios__co_proveedor_servicios=request.POST['co_proveedor_servicios'])
                tomado=tomado.filter(co_pago_servicios__co_proveedor_servicios=request.POST['co_proveedor_servicios'])

            ban=ban.exclude(co_pago_servicios__in=bandejaServicios).exclude(co_usuario=request.user.co_usuario)
            
            content['data']=ban
            content['tomado']=tomado
    elif request.method=='GET':
            content['data']=WalletTb005PagoServicios.objects.filter(
                co_solicitud__co_tipo_solicitud=15,
                co_solicitud__in_activo=True,
                co_servicios__in=serviciosDisp,
                ).exclude(co_pago_servicios__in=bandejaServicios
                ).exclude(co_usuario=request.user.co_usuario)
            content['tomado']=WalletTb012BandejaServicios.objects.filter(co_usuario=request.user.co_usuario,in_activo=True)
            #content['data']=MainTb028Solicitud.objects.filter(co_tipo_solicitud__co_rol_solicitud__co_rol__co_usuario=request.user.co_usuario)
    

    
    return content


@confirmado_user_required
#@user_agente_required
@user2fa_required
def bandejaAgente(request):
    content=banAge(request)
    return render(request, '../templates/wallet/servicios/bandejaagente.html',content)

@confirmado_user_required
#@user_agente_required
@user2fa_required
def bandejaAgenteSuccess(request,success,codigo):

    content=banAge(request)
    mensaje=MainTb040Mensajes.objects.get(co_mensajes=codigo)
    content['success']=success
    content['msg']=mensaje.tx_mensajes

    return render(request, '../templates/wallet/servicios/bandejaagente.html',content)