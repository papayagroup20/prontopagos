from io import BytesIO
from django.http import response
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required,user_passes_test
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.shortcuts import render,HttpResponse,redirect
from django.db.models import Q
from main.models import *
from django.db.models import Prefetch
from rest_framework.authtoken.models import Token
from rest_framework.response import Response


from reportlab.graphics.barcode import qr
from reportlab.graphics.barcode.qr import QrCodeWidget

import qrcode

import json

import PIL.Image as Image
from django.core.files.storage import default_storage as storage

from django.conf import settings
from main.views.utils import getContentSuperBasico

import pyqrcode
import io
import base64

user_login_required = user_passes_test(lambda user: user.in_confirmado, login_url='/usuario/verificar')

def pronto_amigo(request):
    content=getContentSuperBasico(request.user)

    referidos=MainTb045Referidos.objects.filter(co_usuario=request.user, in_validado=True)
    lista=[]
    nu_usuarios=0
    for values in referidos:
        lista.append(values.nu_telegram)
        nu_usuarios+=1
    usuario= Tb001Usuario.objects.filter(nu_telegram__in=lista)
    content['data']=usuario
    content['num']=nu_usuarios
    
    
    

    #links para los QRs
    link= 'https://t.me/prontopagosbot?start=ref_'+ str(request.user.nu_telegram)
    content['link']=link
    content['img_qr']=link
    
    #codigo que crea un qr en imagen
    c = pyqrcode.create(link)
    s = io.BytesIO()
    
    c.png(s,scale=2)
    encoded = base64.b64encode(s.getvalue()).decode("ascii")
    #encoded = base64.b64encode(c.text())
    content['img']=encoded
    #print(encoded)    
    content['codigo']=request.user.nu_telegram

    """transa=WalletTb050ComisionAfiliado.objects
        .prefetch_related(
            Prefetch('integrante_comision',queryset=WalletTb053IntegranteComision.objects.filter(co_referido__nu_telegram__in=lista)),       
        )"""

    transa=WalletTb053IntegranteComision.objects.select_related('co_comision_afiliado').filter(co_referido__nu_telegram__in=lista)

    content['data2']=transa
    

    return render(request, '../templates/usuario/prontoamigo.html',content)