from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required,user_passes_test
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import *
from main.models import *
from main.form import *
from django.utils.crypto import get_random_string
from django.contrib.auth.hashers import make_password,check_password
from django.core.mail import EmailMessage
from django.conf import settings
from stellar_sdk import TransactionBuilder, Network, Keypair, Account,Asset,Operation,AllowTrust
from stellar_sdk.server import Server
import requests
from django.db import IntegrityError, transaction
from apipago.serializers import *
from django.core import serializers
import qrcode
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import action,api_view,renderer_classes
from rest_framework.response import Response
from django.http import Http404
from django_otp.forms import OTPAuthenticationForm
from django.urls import reverse

from rest_framework.authtoken.models import Token
from django.views.generic.base import View

from django.utils.module_loading import import_string

from django.contrib.sites.shortcuts import get_current_site


from django.views.decorators.cache import never_cache

from django.views.generic.detail import DetailView
from django.views.generic import ListView

from django.views.generic.edit import FormView
import qrcode
import qrcode.image.svg
from base64 import b32encode
from binascii import unhexlify
from django_otp import login as otp_login,match_token

from main.views.utils import getContentBasicoL2,getContentBasicoL,user2fa_required,confirmado_user_required,user_staff_required


"""
class rolConfigList(ListView):
    
    template_name = '../templates/configuracion/rol.html'
    model = MainTb023Rol
    context_object_name = 'data'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['success'] = True
        context['msg'] = ''
        context['form']=RolForm()
        return context"""


@confirmado_user_required
@user2fa_required
def rolConfig(request):
    
    content={
        'success':True,
        'data':'',
        'form':RolForm(),
        'msg':''
    }

    #content['data']=rol
    
    if request.method=='POST':
        if 'boton' in request.POST:
            form=RolForm(request.POST)
            estatus= True if 'estatus' in request.POST else False
            
            if request.POST['boton']=="3" or request.POST['boton']=="4":
                if request.POST['boton']=="3":
                    content['data']=MainTb023Rol.objects.filter(tx_rol__contains=request.POST['tx_rol'],in_activo=estatus)
                else:
                    content['data']=MainTb023Rol.objects.all()
                
                return render(request, '../templates/configuracion/rol.html',content)
            
            if form.is_valid():
                
                try:
                    with transaction.atomic():
                        
                        if request.POST['boton']=="1":
                            rol=MainTb023Rol(
                                tx_rol=request.POST['tx_rol'],
                                in_activo=estatus,
                                co_usuario=request.user

                            )
                        else:
                            rol=MainTb023Rol.objects.get(co_rol=request.POST['codigo'])
                            rol.tx_rol=request.POST['tx_rol']
                            rol.in_activo=estatus
                            rol.co_usuario=request.user

                        rol.save()
                        content['msg']='La operacion ha sido realizada sactifactoriamente.'
                except IntegrityError:
                    content['success']=False
                    content['msg']='Hubo un error en la operación'
                
            else:
                content['form']=form

    content['data']=MainTb023Rol.objects.all()

    
    return render(request, '../templates/configuracion/rol.html',content)

@confirmado_user_required
@user2fa_required
def rolSolicitud(request):
    
    token, _ = Token.objects.get_or_create(user = request.user)
    content={
        'success':True,
        'data':'',
        'form':RolSolicitud(),
        'token':token,
        'msg':''
    }

    return render(request, '../templates/configuracion/rolsolicitud.html',content)


@confirmado_user_required
@user2fa_required
def institutoConfig(request):
    
    token, _ = Token.objects.get_or_create(user = request.user)
    content={
        'success':True,
        'data':'',
        'form':InstitutoForm(),
        'token':token,
        'msg':''
    }

    #content['data']=rol
    
    if request.method=='POST':
        if 'boton' in request.POST:
            form=InstitutoForm(request.POST)
            estatus= True if 'estatus' in request.POST else False
            padre= request.POST['co_padre'] if request.POST['co_padre'] != '' else 0
            if request.POST['boton']=="3" or request.POST['boton']=="4":
                if request.POST['boton']=="3":
                    content['data']=MainTb024Instituto.objects.filter(tx_instituto__contains=request.POST['tx_instituto'],in_activo=estatus)
                else:
                    content['data']=MainTb024Instituto.objects.all()
                
                return render(request, '../templates/configuracion/instituto.html',content)
            
            if form.is_valid():
                
                try:
                    with transaction.atomic():
                        
                        if request.POST['boton']=="1":
                            instituto=MainTb024Instituto(
                                tx_instituto=request.POST['tx_instituto'],
                                nu_padre=padre,
                                in_activo=estatus,
                                co_usuario=request.user

                            )
                        else:
                            instituto=MainTb024Instituto.objects.get(co_instituto=request.POST['codigo'])
                            instituto.tx_instituto=request.POST['tx_instituto']
                            instituto.nu_padre=padre
                            instituto.in_activo=estatus
                            instituto.co_usuario=request.user

                        instituto.save()
                        content['msg']='La operacion ha sido realizada sactifactoriamente.'
                except IntegrityError:
                    content['success']=False
                    content['msg']='Hubo un error en la operación'
                
            else:
                content['form']=form

    content['data']=MainTb024Instituto.objects.filter(nu_padre=0)

    
    return render(request, '../templates/configuracion/instituto.html',content)


@confirmado_user_required
@user2fa_required
def tipoSolicitudConfig(request):
    
    content={
        'success':True,
        'data':'',
        'form':TipoSolicitudForm(),
        'msg':''
    }

    #content['data']=tipo_solicitud
    
    if request.method=='POST':
        
        if 'boton' in request.POST:
            
            form=TipoSolicitudForm(request.POST)
            estatus= True if 'estatus' in request.POST else False
            
            if request.POST['boton']=="1":
                qr=MainTb027TipoSolicitud.objects.filter(tx_tipo_solicitud__contains=request.POST['tx_tipo_solicitud'],in_activo=estatus)
                if request.POST['co_instituto'] !='':
                    qr.filter(co_instituto=request.POST['co_instituto'])
                content['data']=qr
                return render(request, '../templates/configuracion/tipoSolicitud.html',content)
            
            if form.is_valid():
                try:
                    with transaction.atomic():
                        instituto=MainTb024Instituto.objects.get(co_instituto=request.POST['co_instituto'])
                        if request.POST['boton']=="2":
                            tipo_solicitud=MainTb027TipoSolicitud(
                                tx_tipo_solicitud=request.POST['tx_tipo_solicitud'],
                                in_activo=estatus,
                                co_usuario=request.user,
                                co_instituto=instituto,
                            )
                            
                        else:
                            tipo_solicitud=MainTb027TipoSolicitud.objects.get(co_tipo_solicitud=request.POST['codigo'])
                            tipo_solicitud.tx_tipo_solicitud=request.POST['tx_tipo_solicitud']
                            tipo_solicitud.in_activo=estatus
                            tipo_solicitud.co_usuario=request.user
                            tipo_solicitud.co_instituto=MainTb024Instituto.objects.get(co_instituto=request.POST['co_instituto'])[0]
                        
                        tipo_solicitud.save()
                        if request.POST['boton']=="2":
                            ruta=MainTb026DetalleRuta(
                                co_tipo_solicitud=tipo_solicitud,
                                nu_orden=1,
                                co_instituto=instituto,
                                co_usuario=request.user,
                                co_proceso=MainTb025Proceso.objects.get(co_proceso=request.POST['co_proceso'])
                                

                            )
                            ruta.save()
                        content['msg']='La operacion ha sido realizada sactifactoriamente.'
                except IntegrityError:
                    content['success']=False
                    content['msg']='Hubo un error en la operación'
                
            else:
                content['form']=form
    elif request.method=='GET':
        if 'tx_busqueda' in request.GET:
            
            content['data']=MainTb027TipoSolicitud.objects.filter(tx_tipo_solicitud__contains=request.GET['tx_busqueda'])
            return render(request, '../templates/configuracion/tipoSolicitud.html',content)

    content['data']=MainTb027TipoSolicitud.objects.all()

    
    return render(request, '../templates/configuracion/tipoSolicitud.html',content)

@confirmado_user_required
@user2fa_required
def procesoConfig(request):
    
    token, _ = Token.objects.get_or_create(user = request.user)
    content={
        'success':True,
        'data':'',
        'form':ProcesoForm(),
        'token':token,
        'msg':''
    }

    #content['data']=tipo_solicitud
    
    if request.method=='POST':
        if 'boton' in request.POST:
            form=ProcesoForm(request.POST)
            estatus= True if 'estatus' in request.POST else False
            
            if request.POST['boton']=="1":
                content['data']=MainTb025Proceso.objects.filter(tx_proceso__contains=request.POST['tx_proceso'],in_activo=estatus)
                return render(request, '../templates/configuracion/proceso.html',content)
            
            if form.is_valid():
                try:
                    with transaction.atomic():
                        
                        if request.POST['boton']=="2":
                            proceso=MainTb025Proceso(
                                tx_proceso=request.POST['tx_proceso'],
                                in_activo=estatus,
                                co_usuario=request.user,
                              
                            )
                        else:
                            proceso=MainTb025Proceso.objects.get(co_proceso=request.POST['codigo'])
                            proceso.tx_proceso=request.POST['tx_proceso']
                            proceso.in_activo=estatus
                            proceso.co_usuario=request.user
                           
                        proceso.save()
                        content['msg']='La operacion ha sido realizada sactifactoriamente.'
                except IntegrityError:
                    content['success']=False
                    content['msg']='Hubo un error en la operación'
                
            else:
                content['form']=form
    elif request.method=='GET':
        if 'tx_busqueda' in request.GET:
            
            content['data']=MainTb025Proceso.objects.filter(tx_proceso__contains=request.GET['tx_busqueda'])
            return render(request, '../templates/configuracion/proceso.html',content)

    content['data']=MainTb025Proceso.objects.all()

    
    return render(request, '../templates/configuracion/proceso.html',content)

@confirmado_user_required
@user2fa_required
def rutaConfig(request):
    content={
        'success':True,
        'data':'',
        'form':TipoSolicitudForm(),
        'msg':''
    }
    if request.method=='POST':
        if 'boton' in request.POST:
            form=RutaForm(request.POST)
            if form.is_valid():
                try:
                    with transaction.atomic():
                        oldruta=MainTb026DetalleRuta.objects.filter(co_tipo_solicitud=request.POST['codigo2']).order_by('-nu_orden')[0]
                        orden=oldruta.nu_orden+1
                        ruta=MainTb026DetalleRuta(
                            co_tipo_solicitud=MainTb027TipoSolicitud.objects.get(co_tipo_solicitud=request.POST['codigo2']),
                            co_usuario=request.user,
                            co_proceso=MainTb025Proceso.objects.get(co_proceso=request.POST['co_proceso']),
                            co_instituto=MainTb024Instituto.objects.get(co_instituto=request.POST['co_instituto']),
                            nu_orden=orden,
                            )
                        ruta.save()
                        
                        content['msg']='La ruta ha sido añadida sactifactoriamente'

                except IntegrityError:
                    content['success']=False
                    content['msg']='Hubo un error en la operación'

    content['data']=MainTb027TipoSolicitud.objects.all()
    return render(request, '../templates/configuracion/tipoSolicitud.html',content)

@confirmado_user_required
@user2fa_required
def usuarioConfig(request):
    token, _ = Token.objects.get_or_create(user = request.user)
    content={
        'success':True,
        'data':'',
        'form':UsuarioConfigForm(),
        'token':token,
        'msg':''
    }

    #content['data']=tipo_solicitud
    
    if request.method=='POST':
        if 'boton' in request.POST:
            form=UsuarioConfigForm(request.POST)
            estatus= True if 'estatus' in request.POST else False
            
            if request.POST['boton']=="1":
                qr=Tb001Usuario.objects.filter(username__contains=request.POST['username'],is_active=estatus)
                if request.POST['co_instituto'] !='':
                    qr.filter(co_instituto=request.POST['co_instituto'])
                if request.POST['co_rol'] !='':
                    qr.filter(co_instituto=request.POST['co_rol'])
                content['data']=qr
                return render(request, '../templates/configuracion/usuario.html',content)
            
            if form.is_valid():
                try:
                    with transaction.atomic():
                        
                        proceso=Tb001Usuario.objects.get(co_usuario=request.POST['codigo'])
                        proceso.is_active=estatus
                        proceso.co_rol=MainTb023Rol.objects.get(co_rol=request.POST['co_rol'])
                        proceso.co_instituto=MainTb024Instituto.objects.get(co_instituto=request.POST['co_instituto'])
                           
                        proceso.save()
                        content['msg']='La operacion ha sido realizada sactifactoriamente.'
                except IntegrityError:
                    content['success']=False
                    content['msg']='Hubo un error en la operación'
                
            else:
                content['form']=form
    elif request.method=='GET':
        if 'tx_busqueda' in request.GET:
            
            content['data']=Tb001Usuario.objects.filter(username__contains=request.GET['tx_busqueda'])
            return render(request, '../templates/configuracion/usuario.html',content)

    content['data']=Tb001Usuario.objects.all()

    
    return render(request, '../templates/configuracion/usuario.html',content)


"""        lista=Tb001Usuario.objects.all()
    elif request.method=='GET':
        if 'tx_busqueda' in request.GET:
            
            lista=Tb001Usuario.objects.filter(username__contains=request.GET['tx_busqueda'])
            content['search']=request.GET['tx_busqueda']
        
        else: 
            lista=Tb001Usuario.objects.all()
        
        page_number = request.GET.get('page')
    
    paginator = Paginator(lista, 5)
    page_obj = paginator.get_page(page_number)
    content['data']=page_obj"""


def builtMenu1():
    menu=MainTb030Menu.objects.all().order_by('tx_pocision')
    text=""
    #for values in menu.filter(nu_nivel=0):
    #    text+="<div><h4>"+values.tx_menu+"-"+str(values.nu_nivel)+"</h4></div>"
    #v.tx_pocision[:-1]
    difnivel=0
    nivelanterior=0
    liant=0
    padreant=0
    for v in menu:

        if v.in_link:
            if  padreant!=v.nu_padre:
                dif=nivelanterior-int(v.nu_nivel) if nivelanterior>v.nu_nivel  else 1
                for x in range (dif):
                    text+="</ul>"
                    text+="</div>"
                    text+="</li>"

            text+="<li class='list-group-item'>"
            text+="<i class='tim-icons "+v.co_icono.tx_icono+"'></i>  "+v.tx_menu
            text+="<a href='javascript:void(0)' class='btn btn-link btn-primary btn-icon btn-sm edit validar' data-toggle='modal' data-target='#exampleModal' id='"+str(v.co_menu)+"'><i class='tim-icons icon-pencil'></i></a>"
            text+="</li>"
            padreant=v.nu_padre
            liant=1
        else:
            if nivelanterior>v.nu_nivel or liant==2:
                
                dif=nivelanterior-int(v.nu_nivel)
                if liant==2:
                    dif=1
                #print("anterior:"+str(nivelanterior))
                
                #print("actual:"+str(v.nu_nivel))
                
                #print("diferencia:"+str(dif))
                for x in range (dif):
                    print(x)
                    text+="</ul>"
                    text+="</div>"
                    text+="</li>"
            
            text+="<li class='list-group-item'>"
            text+="<a class='btn btn-simple btn-primary btn-sm' data-toggle='collapse' href='#p"+v.tx_pocision+"'>"
            text+="<i class='tim-icons "+v.co_icono.tx_icono+"'></i> "+v.tx_menu
            text+="</a>"
            text+="<div class='collapse' id='p"+v.tx_pocision+"'>"
            text+="<ul class='list-group'>"

            padreant=v.co_menu
            liant=2
        nivelanterior=v.nu_nivel
        #text+="<div><h4>"+v.tx_menu+":"+v.tx_pocision[:-1]+"</h4></div>"
            
    text+="</ul>"
    text+="</div>"
    text+="</li>"




    return text


@confirmado_user_required
@user2fa_required
def menuConfig(request):
    
    token, _ = Token.objects.get_or_create(user = request.user)
    content={
        'success':True,
        'data':'',
        'form':MenuForm(),
        'token':token.key,
        'msg':'',
        'menu':''
    }
    
    #content['data']=tipo_solicitud
    
    if request.method=='POST':
        if 'boton' in request.POST:
            form=MenuForm(request.POST)
            estatus= True if 'estatus' in request.POST else False
            in_link= True if 'in_link' in request.POST else False
            if request.POST['co_menu']=='': 
                padre=0 
            else:
                padre= request.POST['co_menu']
            
            print(padre)
         
            if form.is_valid():
                try:
                    with transaction.atomic():

                        
                        
                        orden=1
                        nivel=0
                        pocision=1
                        if MainTb030Menu.objects.filter(nu_padre=padre).exists():
                            oldmenu=MainTb030Menu.objects.filter(nu_padre=padre).order_by('-nu_orden')[0]
                            orden=oldmenu.nu_orden+1
                            nivel=oldmenu.nu_nivel
                            pocision=str(oldmenu.tx_pocision[:-1])+str(oldmenu.nu_orden+1)

                        elif MainTb030Menu.objects.get(co_menu=padre):
                            oldmenu=MainTb030Menu.objects.get(co_menu=padre)
                            nivel=oldmenu.nu_nivel+1
                            pocision=str(oldmenu.tx_pocision)+str(1)
                        else:
                            ceromenu=MainTb030Menu.objects.filter(nu_nivel=0).order_by('-nu_orden')[0]
                            orden=ceromenu.nu_orden+1
                            pocision=ceromenu.nu_orden+1

                        if request.POST['boton']=="2":
                            menu=MainTb030Menu(
                                tx_menu=request.POST['tx_menu'],
                                tx_url=request.POST['tx_url'],
                                nu_padre=padre,
                                nu_orden=orden,
                                nu_nivel=nivel,
                                tx_pocision=pocision,
                                in_link=in_link,
                                in_activo=estatus,
                                co_usuario=request.user,
                                co_icono=MainTb033Iconos.objects.get(co_icono=request.POST['co_icono'])
                              
                            )
                        else:
                            menu=MainTb030Menu.objects.get(co_menu=request.POST['codigo'])
                            menu.tx_menu=request.POST['tx_menu']
                            menu.tx_url=request.POST['tx_url']
                            menu.nu_padre=int(padre)
                            menu.nu_orden=orden
                            menu.nu_nivel=nivel
                            menu.in_link=in_link
                            menu.in_activo=estatus
                            menu.tx_pocision=pocision
                            menu.co_usuario=request.user
                           
                        menu.save()
                        content['msg']='La operacion ha sido realizada sactifactoriamente.'
                except IntegrityError:
                    content['success']=False
                    content['msg']='Hubo un error en la operación'
                
            else:
                content['form']=form
    elif request.method=='GET':
        if 'tx_busqueda' in request.GET:
            
            content['data']=MainTb030Menu.objects.filter(tx_menu__contains=request.GET['tx_busqueda'])
            return render(request, '../templates/configuracion/menu.html',content)

    content['data']=MainTb030Menu.objects.all()
    content['menu']=builtMenu1()
    
    return render(request, '../templates/configuracion/menu.html',content)


@confirmado_user_required
@user2fa_required
def paisConfig(request):
    
    token, _ = Token.objects.get_or_create(user = request.user)
    content={
        'success':True,
        'data':'',
        'form':PaisForm(),
        'token':token,
        'msg':''
    }

    #content['data']=tipo_solicitud
    
    if request.method=='POST':
        if 'boton' in request.POST:
            form=PaisForm(request.POST)
            estatus= True if 'estatus' in request.POST else False
            
            if request.POST['boton']=="1":
                content['data']=MainTb002Pais.objects.filter(tx_pais__contains=request.POST['tx_pais'],in_activo=estatus)
                return render(request, '../templates/configuracion/pais.html',content)
            
            if form.is_valid():
                try:
                    with transaction.atomic():
                        
                        if request.POST['boton']=="2":
                            pais=MainTb002Pais(
                                tx_pais=request.POST['tx_pais'],
                                tx_nacionalidad=request.POST['tx_nacionalidad'],
                                in_activo=estatus,
                                #co_usuario=request.user,
                              
                            )
                        else:
                            pais=MainTb002Pais.objects.get(co_pais=request.POST['codigo'])
                            pais.tx_pais=request.POST['tx_pais']
                            pais.tx_nacionalidad=request.POST['tx_nacionalidad']
                            pais.in_activo=estatus
                            #pais.co_usuario=request.user
                           
                        pais.save()
                        content['msg']='La operacion ha sido realizada sactifactoriamente.'
                except IntegrityError:
                    content['success']=False
                    content['msg']='Hubo un error en la operación'
                
            else:
                content['form']=form
    elif request.method=='GET':
        if 'tx_busqueda' in request.GET:
            
            content['data']=MainTb002Pais.objects.filter(tx_pais__contains=request.GET['tx_busqueda'])
            return render(request, '../templates/configuracion/pais.html',content)

    content['data']=MainTb002Pais.objects.all()

    
    return render(request, '../templates/configuracion/pais.html',content)