from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib.auth.decorators import login_required,user_passes_test
from django.http import HttpResponse
from main.models import *
from stellar_sdk.server import Server
from stellar_sdk.asset import Asset
from django.db.models import Q
from wallet.form import *
from django.db import IntegrityError, transaction
from main.views.utils import  getContentBasicoL,getContentBasicoL2
from wallet.views.operaciones import darConfianza
from stellar_sdk.keypair import Keypair
import requests

def prueba2fa1(request):
    redirect("qr")