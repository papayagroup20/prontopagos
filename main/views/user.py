from django.views.generic.base import TemplateView
from main.form.general import RegistroParte1Form,RegistroParte2ioForm,WalletTb037Keystore
from apipago.serializers.general import KeystoreSerializer
import json
from pprint import pprint
from django.utils.decorators import method_decorator
from main.views.utils import user2fa_required,confirmado_user_required
from django.contrib.auth.hashers import make_password,check_password
from django.db import IntegrityError, transaction
from main.models import Tb001Usuario

decorators = [confirmado_user_required, user2fa_required]

class RegistroUserViews(TemplateView):
    template_name = "../templates/usuario/registroNormal.html"
    context={}

    def get_context_data(self, **kwargs):
        self.context = super().get_context_data(**kwargs)
        self.context["form01"]=RegistroParte1Form()
        self.context["form02"]=RegistroParte2ioForm()
        
        return self.context




@method_decorator(decorators, name='dispatch')
class VincularTelegramViews(TemplateView):
    template_name = "../templates/usuario/seguridad/vinculartelegram.html"
    context={}
    #https://t.me/prontopagosbot?start=vincular_[co_usuario]_[token]

    def get_context_data(self, **kwargs):
        self.context = super().get_context_data(**kwargs)
        self.context["success"]=True
        if self.request.user.nu_telegram is not None:
            self.context["in_telegram"]=True
        else:

            raw_texto=self.request.user.username+"lataya"
            usuario=Tb001Usuario.objects.get(co_usuario=self.request.user.co_usuario)

            if usuario.token_telegram is None:
                try:
                    with transaction.atomic():
                        usuario.token_telegram=make_password(raw_texto)
                        usuario.save()
                except Exception as e:
                    print(e)
                    self.context["success"]=False
                    return self.context
                


            self.context["in_telegram"]=False
            self.context["tokenv"]=usuario.token_telegram
        return self.context

