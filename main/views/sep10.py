import os
import binascii
import time
import jwt
from urllib.parse import urlparse

from django.utils.translation import gettext
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated
from stellar_sdk.operation import ManageData
from stellar_sdk.sep.stellar_toml import fetch_stellar_toml
from stellar_sdk.sep.stellar_web_authentication import (
    build_challenge_transaction,
    read_challenge_transaction,
    verify_challenge_transaction_threshold,
    verify_challenge_transaction_signed_by_client_master_key,
)
from stellar_sdk.sep.exceptions import (
    InvalidSep10ChallengeError,
    StellarTomlNotFoundError,
)
from stellar_sdk.exceptions import (
    NotFoundError,
    ConnectionError,
    Ed25519PublicKeyInvalidError,
)
from stellar_sdk import Keypair
from stellar_sdk.client.requests_client import RequestsClient
from stellar_sdk.muxed_account import MuxedAccount
from stellar_sdk.server import Server
from typing import Union,Coroutine,Dict,Any,List,Tuple
from pprint import pprint
#VARIABLES GENERALES
MIME_URLENCODE, MIME_JSON = "application/x-www-form-urlencoded", "application/json"

STELLAR_NETWORK_PASSPHRASE="Public Global Stellar Network ; September 2015"
HOST_URL="http://localhost:80"

SIGNING_SEED="SDGWMGICPGONOJU6LETVWZWFTAYUHZGU6OEAKD5CZQCSVU4YD65ORAHJ"
SIGNING_KEY = Keypair.from_secret(SIGNING_SEED).public_key
HORIZON_SERVER = Server(horizon_url="https://horizon.stellar.org")

SERVER_JWT_KEY = "lapapayaestaentinoenmienti"

SEP10_HOME_DOMAINS = [
    'http://localhost',
    'http://localhost:8080',
    'https://prontopagos.io',
    'https://pay.prontopagos.io',
    ]


def render_error_response(
    description: str,
    status_code: int = status.HTTP_400_BAD_REQUEST,
    content_type: str = "application/json",
) -> Response:
    """
    Renders an error response in Django.

    Currently supports HTML or JSON responses.
    """
    resp_data = {
        "data": {"error": description},
        "status": status_code,
        "content_type": content_type,
    }
    if content_type == "text/html":
        resp_data["data"]["status_code"] = status_code
        resp_data["template_name"] = "polaris/error.html"
    return Response(**resp_data)


class SEP10Auth(APIView):
    """
    `GET /auth` can be used to get a challenge Stellar transaction.
    The client can then sign it using their private key and hit `POST /auth`
    to receive a JSON web token. That token can be used to authenticate calls
    to the other SEP 24 endpoints.
    """

    parser_classes = [JSONParser, MultiPartParser, FormParser]
    renderer_classes = [JSONRenderer, BrowsableAPIRenderer]
    permission_classes = [AllowAny]

    ###############
    # GET functions
    ###############
    def get(self, request, *_args, **_kwargs) -> Response:
        account = request.GET.get("account")
        if not account:
            return Response(
                {"error": "no 'account' provided"}, status=status.HTTP_400_BAD_REQUEST
            )

        home_domain = request.GET.get("home_domain")
       
        if home_domain and home_domain not in SEP10_HOME_DOMAINS:
            return Response(
                {
                    "error": f"invalid 'home_domain' value. Accepted values: {SEP10_HOME_DOMAINS}"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        elif not home_domain:
            home_domain = SEP10_HOME_DOMAINS[0]

        client_domain, client_signing_key = request.GET.get("client_domain"), None
        
        try:
            transaction = self._challenge_transaction(
                account, home_domain, client_domain, client_signing_key
            )
        except ValueError as e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)

        #print(f"Returning SEP-10 challenge for account {account}")
        return Response(
            {
                "transaction": transaction,
                "network_passphrase": STELLAR_NETWORK_PASSPHRASE,
            }
        )

    @staticmethod
    def _challenge_transaction(
        client_account, home_domain, client_domain=None, client_signing_key=None
    ):
        """
        Generate the challenge transaction for a client account.
        This is used in `GET <auth>`, as per SEP 10.
        Returns the XDR encoding of that transaction.
        """
        return build_challenge_transaction(
            server_secret=SIGNING_SEED,
            client_account_id=client_account,
            home_domain=home_domain,
            web_auth_domain=urlparse(HOST_URL).netloc,
            network_passphrase=STELLAR_NETWORK_PASSPHRASE,
            timeout=900,
            client_domain=client_domain,
            client_signing_key=client_signing_key,
        )

    ################
    # POST functions
    ################
    def post(self, request: Request, *_args, **_kwargs) -> Response:
        envelope_xdr = request.data.get("transaction")
        if not envelope_xdr:
            return render_error_response(gettext("'transaction' is required"))
            
        client_domain, error_response = self._validate_challenge_xdr(envelope_xdr)
        print(client_domain)
        if error_response:
            return error_response
        else:
            return Response({"token": self._generate_jwt(envelope_xdr, client_domain)})

    @staticmethod
    def _validate_challenge_xdr(envelope_xdr: str):
        """
        Validate the provided TransactionEnvelope XDR (base64 string).

        If the source account of the challenge transaction exists, verify the weight
        of the signers on the challenge are signers for the account and the medium
        threshold on the account is met by those signers.

        If the source account does not exist, verify that the keypair used as the
        source for the challenge transaction has signed the challenge. This is
        sufficient because newly created accounts have their own keypair as signer
        with a weight greater than the default thresholds.
        """
        print("Validating challenge transaction")
        generic_err_msg = gettext("error while validating challenge: %s")
        try:
            
            ChallengeTransaction= read_challenge_transaction(
                challenge_transaction=envelope_xdr,
                server_account_id=SIGNING_KEY,
                home_domains=SEP10_HOME_DOMAINS,
                web_auth_domain=urlparse(HOST_URL).netloc,
                network_passphrase=STELLAR_NETWORK_PASSPHRASE,
            )
            #print(ChallengeTransaction)
        #except Exception as e:
        except (InvalidSep10ChallengeError, TypeError) as e:
            return None, render_error_response(generic_err_msg % (str(e)))
        client_domain = None
        #print(ChallengeTransaction.transaction.transaction.operations)
        for operation in ChallengeTransaction.transaction.transaction.operations:
            print(operation.data_name,ManageData)
            if (
                isinstance(operation, ManageData)
                and operation.data_name == "client_domain"
            ):
                client_domain = operation.data_value.decode()
                break
        print(client_domain)
        try:
            account = HORIZON_SERVER.load_account(ChallengeTransaction.client_account_id)
        except NotFoundError:
            print("Account does not exist, using client's master key to verify")
            try:
                verify_challenge_transaction_signed_by_client_master_key(
                    challenge_transaction=envelope_xdr,
                    server_account_id=SIGNING_KEY,
                    home_domains=SEP10_HOME_DOMAINS,
                    web_auth_domain=urlparse(HOST_URL).netloc,
                    network_passphrase=STELLAR_NETWORK_PASSPHRASE,
                )
                if (client_domain and len(ChallengeTransaction.transaction.signatures) != 3) or (
                    not client_domain and len(ChallengeTransaction.transaction.signatures) != 2
                ):
                    raise InvalidSep10ChallengeError(
                        gettext(
                            "There is more than one client signer on a challenge "
                            "transaction for an account that doesn't exist"
                        )
                    )
            except InvalidSep10ChallengeError as e:
                print(
                    f"Missing or invalid signature(s) for {ChallengeTransaction.client_account_id}: {str(e)}"
                )
                return None, render_error_response(generic_err_msg % (str(e)))
            else:
                print("Challenge verified using client's master key")
                return client_domain, None
        
        signers = account.load_ed25519_public_key_signers()
        threshold = account.thresholds.med_threshold
        try:
            signers_found = verify_challenge_transaction_threshold(
                challenge_transaction=envelope_xdr,
                server_account_id=SIGNING_KEY,
                home_domains=SEP10_HOME_DOMAINS,
                web_auth_domain=urlparse(HOST_URL).netloc,
                network_passphrase=STELLAR_NETWORK_PASSPHRASE,
                threshold=threshold,
                signers=signers,
            )
        except InvalidSep10ChallengeError as e:
            return None, render_error_response(generic_err_msg % (str(e)))

        print(f"Challenge verified using account signers: {[s.account_id for s in signers_found]}")
        return client_domain, None
    @staticmethod
    def validar_challenge_xdr(envelope_xdr: str)->Tuple[bool,str]:
        """
        Validate the provided TransactionEnvelope XDR (base64 string).

        If the source account of the challenge transaction exists, verify the weight
        of the signers on the challenge are signers for the account and the medium
        threshold on the account is met by those signers.

        If the source account does not exist, verify that the keypair used as the
        source for the challenge transaction has signed the challenge. This is
        sufficient because newly created accounts have their own keypair as signer
        with a weight greater than the default thresholds.
        """
        #print("Validating challenge transaction")
        generic_err_msg = gettext("error while validating challenge: %s")
        try:
            
            ChallengeTransaction= read_challenge_transaction(
                challenge_transaction=envelope_xdr,
                server_account_id=SIGNING_KEY,
                home_domains=SEP10_HOME_DOMAINS,
                web_auth_domain=urlparse(HOST_URL).netloc,
                network_passphrase=STELLAR_NETWORK_PASSPHRASE,
            )
            #print(ChallengeTransaction)
        #except Exception as e:
        except (InvalidSep10ChallengeError, TypeError) as e:
            return False, generic_err_msg % (str(e))
        client_domain = None
        #print(ChallengeTransaction.transaction.transaction.operations)
        for operation in ChallengeTransaction.transaction.transaction.operations:
            if (
                isinstance(operation, ManageData)
                and operation.data_name == "client_domain"
            ):
                client_domain = operation.data_value.decode()
                break
        try:
            account = HORIZON_SERVER.load_account(ChallengeTransaction.client_account_id)
        except NotFoundError:
            print("Account does not exist, using client's master key to verify")
            try:
                verify_challenge_transaction_signed_by_client_master_key(
                    challenge_transaction=envelope_xdr,
                    server_account_id=SIGNING_KEY,
                    home_domains=SEP10_HOME_DOMAINS,
                    web_auth_domain=urlparse(HOST_URL).netloc,
                    network_passphrase=STELLAR_NETWORK_PASSPHRASE,
                )
                if (client_domain and len(ChallengeTransaction.transaction.signatures) != 3) or (
                    not client_domain and len(ChallengeTransaction.transaction.signatures) != 2
                ):
                    return False, gettext(
                            "There is more than one client signer on a challenge "
                            "transaction for an account that doesn't exist"
                        )
                    
            except InvalidSep10ChallengeError as e:
                print(
                    f"Missing or invalid signature(s) for {ChallengeTransaction.client_account_id}: {str(e)}"
                )
                return False, generic_err_msg % (str(e))
            else:
                print("Challenge verified using client's master key")
                return client_domain, None
        
        signers = account.load_ed25519_public_key_signers()
        threshold = account.thresholds.med_threshold
        try:
            signers_found = verify_challenge_transaction_threshold(
                challenge_transaction=envelope_xdr,
                server_account_id=SIGNING_KEY,
                home_domains=SEP10_HOME_DOMAINS,
                web_auth_domain=urlparse(HOST_URL).netloc,
                network_passphrase=STELLAR_NETWORK_PASSPHRASE,
                threshold=threshold,
                signers=signers,
            )
        except InvalidSep10ChallengeError as e:
            return False, generic_err_msg % (str(e))

        #print(f"Challenge verified using account signers: {[s.account_id for s in signers_found]}")
        return True,None

    @staticmethod
    def _generate_jwt(envelope_xdr: str, client_domain: str = None) -> str:
        """
        Generates the JSON web token from the challenge transaction XDR.
        See: https://github.com/stellar/stellar-protocol/blob/master/ecosystem/sep-0010.md#token
        """
        challenge = read_challenge_transaction(
            challenge_transaction=envelope_xdr,
            server_account_id=SIGNING_KEY,
            home_domains=SEP10_HOME_DOMAINS,
            web_auth_domain=urlparse(HOST_URL).netloc,
            network_passphrase=STELLAR_NETWORK_PASSPHRASE,
        )
        print(
            f"Challenge verified, generating SEP-10 token for account {challenge.client_account_id}"
        )
        # set iat value to minimum timebound of the challenge so that the JWT returned
        # for a given challenge is always the same.
        # https://github.com/stellar/stellar-protocol/pull/982
        issued_at = challenge.transaction.transaction.time_bounds.min_time
        jwt_dict = {
            "iss": os.path.join(HOST_URL, "auth"),
            "sub": challenge.client_account_id,
            "iat": issued_at,
            "exp": issued_at + 24 * 60 * 60,
            "jti": challenge.transaction.hash().hex(),
            "client_domain": client_domain,
        }

        return jwt.encode(jwt_dict, SERVER_JWT_KEY, algorithm="HS256")

    @staticmethod
    def _get_client_signing_key(client_domain):
        client_toml_contents = fetch_stellar_toml(
            client_domain,
            client=RequestsClient(
                request_timeout=3
            ),
        )
        client_signing_key = client_toml_contents.get("SIGNING_KEY")
        if not client_signing_key:
            raise ValueError(gettext("SIGNING_KEY not present on 'client_domain' TOML"))
        try:
            Keypair.from_public_key(client_signing_key)
        except Ed25519PublicKeyInvalidError:
            raise ValueError(
                gettext("invalid SIGNING_KEY value on 'client_domain' TOML")
            )
        return client_signing_key