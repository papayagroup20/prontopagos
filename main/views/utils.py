
from rest_framework.authtoken.models import Token
from django.contrib.auth.decorators import login_required,user_passes_test, permission_required
from django_otp import user_has_device
from django_otp.decorators import otp_required
from datetime import date
from datetime import datetime
from time import strptime
from dateutil.parser import parse
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse

from django.conf import settings
from django.views.generic.base import View
from django.http import HttpResponseForbidden

import requests
from ipware import get_client_ip
from django_user_agents.utils import get_user_agent
from stellar_sdk.server import Server


user_login_required = user_passes_test(lambda user: user.in_confirmado, login_url='/registro/telegram')
#user_uncorfirmado_required = user_passes_test(lambda user: True if not user.in_confirmado, login_url='/login')
user_staf = user_passes_test(lambda user: user.is_staff, login_url='/inicio')

user_login2fa_required=user_passes_test(lambda user:  user_has_device(user) if user.in_2fa else True,login_url='/usuario/seguridad/')
user_agente = user_passes_test(lambda user: user.checkAgente(), login_url='/inicio')

def user2fa_required(view_func):
    decorated_view_func = login_required(user_login2fa_required(view_func),login_url='/login')
    return decorated_view_func

def confirmado_user_required(view_func):
    decorated_view_func = login_required(user_login_required(view_func),login_url='/login')
    return decorated_view_func

def user_staff_required(view_func):
    decorated_view_func = login_required(user_staf(view_func),login_url='/login')
    return decorated_view_func

def user_agente_required(view_func):
    decorated_view_func = login_required(user_agente(view_func),login_url='/login')
    return decorated_view_func

def getTokenUsuario(usuario):
    token, _ = Token.objects.get_or_create(user = usuario)
    return token


def getContentSuperBasico(usuario=None,form=None)->dict:
    key=None
    if usuario is not None:
        #u=usuario.objects.prefetch_related("auth_token")
        try:
            token=usuario.auth_token
        except:
            token=getTokenUsuario(usuario)
        #print(usuario.auth_token)
        #token= getTokenUsuario(usuario)
        key=token.key

    content={
        'success':True,
        'msg':'',
        'data':'',
        'form':form,
        'token':key
    }
    return content


def getContentBasicoL(usuario,co_solicitud,form):
    token= getTokenUsuario(usuario)
    content={
        'success':True,
        'token':token.key,
        'msg':'',
        'data':'',
        'form':form,
        'co_solicitud':co_solicitud
    }
    return content

def getContentBasicoL2(usuario,co_tipo_solicitud,form):
    token= getTokenUsuario(usuario)
    content={
        'success':True,
        'token':token.key,
        'msg':'',
        'data':'',
        'form':form,
        'co_tipo_solicitud':co_tipo_solicitud
    }
    return content

def getPublicRecortado(tx_public):
    p1=tx_public[0:5]
    p2=tx_public[-5:]
    p3=p1+"..."+p2
    return p3

def getFecha(fecha):
    objDate = parse(fecha)
    return objDate.strftime("%d/%m/%Y: %I:%M:%S %p")



def validacionRecapchat(request):
   
    return requests.post(
        settings.GOOGLE_VERIFY_RECAPTCHA_URL,
        data={
            'secret': settings.RECAPTCHA_SECRET_KEY,
            'response': request.POST.get('g-recaptcha-response'),
            'remoteip': get_client_ip(request)
        },
        verify=True
    ).json().get("success", False)


def Verif_reCapchat(view_func):
    """
    This decorator is aimed to verify Google recaptcha in the backend side.
    """
    def wrapped(request, *args, **kwargs):
        if validacionRecapchat(request):
            return view_func(request, *args, **kwargs)
        else:
            return HttpResponseForbidden()
    return wrapped


def getNavegador(request):

    os = request.user_agent.os.family
    version = request.user_agent.os.version
    navegador =  request.user_agent.browser.family
    todo = str(navegador)+'('+str(os)+''+str(version[0])+')'

    return todo


def grecaptcha_verify(request):
    if request.method == 'POST':
        response = {}
        data = request.POST
        captcha_rs = data.get('g-recaptcha-response')
        url = "https://www.google.com/recaptcha/api/siteverify"
        params = {
            'secret': settings.RECAPTCHA_SECRET_KEY,
            'response': captcha_rs,
            # 'remoteip': get_client_ip(request)
        }
        verify_rs = requests.get(url, params=params, verify=True)
        verify_rs = verify_rs.json()
        response["status"] = verify_rs.get("success", False)
        response['message'] = verify_rs.get('error-codes', None) or "Unspecified error."
        return HttpResponse(response)


def fechaGet(date):
    months = ("Enero", "Febrero", "Marzo", "Abri", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
    day = date.day
    month = months[date.month - 1]
    year = date.year
    messsage = "{} de {} de {}".format(day, month, year)

    return messsage



def iplocalizacion(request):

    
    data = {
        'ip': get_client_ip(request)[0],
        'navegador': request.user_agent.browser.family
    }
    
    print(data)
    return data












