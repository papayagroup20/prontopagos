# Generated by Django 3.0.4 on 2020-05-22 15:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0051_wallettb016oferta_nu_offerid'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wallettb007banco',
            name='cedul',
        ),
        migrations.RemoveField(
            model_name='wallettb007banco',
            name='nombre',
        ),
        migrations.AddField(
            model_name='wallettb008cuenta',
            name='cedul',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='wallettb008cuenta',
            name='nombre',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
