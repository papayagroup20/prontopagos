# Generated by Django 2.2.16 on 2021-06-21 21:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0173_auto_20210616_1104'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wallettb050comisionafiliado',
            name='co_emisor',
        ),
        migrations.AddField(
            model_name='wallettb050comisionafiliado',
            name='co_usuario',
            field=models.ForeignKey(db_column='co_usuario', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.Tb001Usuario'),
        ),
        migrations.AlterField(
            model_name='wallettb050comisionafiliado',
            name='co_receptor',
            field=models.ForeignKey(db_column='co_receptor', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb037Keystore'),
        ),
        migrations.CreateModel(
            name='WalletTb051FacturacionTdc',
            fields=[
                ('co_facturacion', models.BigAutoField(primary_key=True, serialize=False)),
                ('tx_persona', models.CharField(max_length=255)),
                ('tx_referencia_pago', models.CharField(max_length=255)),
                ('tx_estatus', models.CharField(max_length=255)),
                ('tx_concepto', models.CharField(max_length=255)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('in_activo', models.BooleanField(default=True)),
                ('co_solicitud', models.ForeignKey(db_column='co_solicitud', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='solicitud_tdc', to='main.MainTb028Solicitud')),
                ('co_tansaccion', models.ForeignKey(db_column='co_transaccion', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='transaccion_tdc', to='main.WalletTb015Transaccion')),
            ],
            options={
                'db_table': 'wallet_tb049_facturacionTdc',
                'managed': True,
            },
        ),
    ]
