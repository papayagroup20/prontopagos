# Generated by Django 2.2.16 on 2021-02-08 22:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0140_auto_20210207_1808'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maintb029ruta',
            name='co_solicitud',
            field=models.ForeignKey(blank=True, db_column='co_solicitud', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='rutas', to='main.MainTb028Solicitud'),
        ),
        migrations.AlterField(
            model_name='maintb038solicitudformulario',
            name='co_detalle_ruta',
            field=models.ForeignKey(blank=True, db_column='co_detalle_ruta', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='formularioruta', to='main.MainTb026DetalleRuta'),
        ),
        migrations.AlterField(
            model_name='wallettb036activos',
            name='toml_asset_conditions',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='wallettb036activos',
            name='toml_asset_desc',
            field=models.TextField(blank=True, null=True),
        ),
    ]
