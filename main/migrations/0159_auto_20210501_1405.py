# Generated by Django 2.2.16 on 2021-05-01 18:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0158_wallettb042stellartoml_tx_origin_domain'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wallettb042stellartoml',
            name='tx_origin_domain',
        ),
        migrations.CreateModel(
            name='WalletTb045AplicationSEP07',
            fields=[
                ('co_aplicacion_sep07', models.BigAutoField(primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('co_stellar_toml', models.ForeignKey(blank=True, db_column='co_stellar_toml', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb042StellarToml')),
            ],
            options={
                'verbose_name': 'Aplicacion sep07',
                'verbose_name_plural': 'Aplicaciones sep07',
                'db_table': 'wallet_tb045_aplication_sep07',
                'managed': True,
            },
        ),
    ]
