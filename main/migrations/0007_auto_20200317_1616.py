# Generated by Django 3.0.4 on 2020-03-17 20:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20200317_1615'),
    ]

    operations = [
        migrations.AddField(
            model_name='maintb002pais',
            name='created_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb002pais',
            name='in_activo',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb002pais',
            name='tx_nacionalidad',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='maintb002pais',
            name='tx_pais',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='maintb002pais',
            name='updated_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb003estado',
            name='co_pais',
            field=models.ForeignKey(blank=True, db_column='co_pais', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.MainTb002Pais'),
        ),
        migrations.AddField(
            model_name='maintb003estado',
            name='created_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb003estado',
            name='in_activo',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb003estado',
            name='tx_estato',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='maintb003estado',
            name='updated_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb004ciudad',
            name='co_estado',
            field=models.ForeignKey(blank=True, db_column='co_estado', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.MainTb003Estado'),
        ),
        migrations.AddField(
            model_name='maintb004ciudad',
            name='created_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb004ciudad',
            name='in_activo',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb004ciudad',
            name='tx_ciudad',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='maintb004ciudad',
            name='updated_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb006tipodocumento',
            name='co_pais',
            field=models.ForeignKey(blank=True, db_column='co_pais', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.MainTb002Pais'),
        ),
        migrations.AddField(
            model_name='maintb006tipodocumento',
            name='created_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb006tipodocumento',
            name='in_activo',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb006tipodocumento',
            name='tx_abreviacion',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='maintb006tipodocumento',
            name='tx_tipo_documento',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='maintb006tipodocumento',
            name='updated_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb007sexo',
            name='created_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb007sexo',
            name='tx_sexo',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='maintb007sexo',
            name='updated_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb008direccion',
            name='co_ciudad',
            field=models.ForeignKey(blank=True, db_column='co_ciudad', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.MainTb004Ciudad'),
        ),
        migrations.AddField(
            model_name='maintb008direccion',
            name='created_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb008direccion',
            name='in_activo',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='maintb008direccion',
            name='tx_direccion',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='maintb008direccion',
            name='updated_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
