# Generated by Django 2.2.16 on 2021-06-07 20:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0165_wallettb047paypaloperacion_img_paypal'),
    ]

    operations = [
        migrations.CreateModel(
            name='WalletTb048TasaAfiliado',
            fields=[
                ('co_tasa_afiliados', models.BigAutoField(primary_key=True, serialize=False)),
                ('nu_tasa', models.DecimalField(blank=True, decimal_places=5, max_digits=32, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('in_activo', models.BooleanField(default=True)),
            ],
            options={
                'db_table': 'wallet_tb048_tasa_afiliado',
                'managed': True,
            },
        ),
        migrations.AddField(
            model_name='wallettb015transaccion',
            name='co_tasa_afiliado',
            field=models.ForeignKey(blank=True, db_column='co_tasa_afiliados', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb048TasaAfiliado'),
        ),
    ]
