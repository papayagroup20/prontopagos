# Generated by Django 2.2.16 on 2021-10-29 18:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0188_tb001usuario_in_frase'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wallettb036activos',
            name='toml_name',
            field=models.CharField(blank=True, default='sin_toml', max_length=255, null=True),
        ),
    ]
