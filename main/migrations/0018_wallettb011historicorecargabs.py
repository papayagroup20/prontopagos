# Generated by Django 3.0.4 on 2020-03-31 14:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0017_wallettb010recargabs_co_cuenta'),
    ]

    operations = [
        migrations.CreateModel(
            name='WalletTb011HistoricoRecargabs',
            fields=[
                ('co_historico_recargabs', models.BigAutoField(primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, default=True, null=True)),
                ('co_estatus', models.ForeignKey(blank=True, db_column='co_estatus', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb009Estatus')),
                ('co_recargabs', models.ForeignKey(blank=True, db_column='co_recargabs', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb010Recargabs')),
                ('co_usuario', models.ForeignKey(blank=True, db_column='co_usuario', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.Tb001Usuario')),
            ],
            options={
                'db_table': 'wallet_tb011_historico_recargabs',
                'managed': True,
            },
        ),
    ]
