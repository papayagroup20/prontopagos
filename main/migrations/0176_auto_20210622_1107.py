# Generated by Django 2.2.16 on 2021-06-22 15:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0175_auto_20210621_1717'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wallettb050comisionafiliado',
            name='co_usuario',
        ),
        migrations.AddField(
            model_name='wallettb050comisionafiliado',
            name='co_emisor',
            field=models.ForeignKey(db_column='co_emisor', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='emisor_comision', to='main.Tb001Usuario'),
        ),
        migrations.AlterField(
            model_name='maintb045referidos',
            name='nu_telegram',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='tb001usuario',
            name='nu_telegram',
            field=models.IntegerField(blank=True, db_index=True, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='wallettb050comisionafiliado',
            name='co_receptor',
            field=models.ForeignKey(db_column='co_receptor', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='receptor_comision', to='main.Tb001Usuario'),
        ),
    ]
