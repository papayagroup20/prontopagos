# Generated by Django 3.0.5 on 2020-06-16 14:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0069_auto_20200612_1153'),
    ]

    operations = [
        migrations.AddField(
            model_name='maintb030menu',
            name='in_link',
            field=models.BooleanField(blank=True, default=True, null=True),
        ),
        migrations.AlterField(
            model_name='maintb026detalleruta',
            name='co_tipo_solicitud',
            field=models.ForeignKey(blank=True, db_column='co_tipo_solicitud', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='ruta', to='main.MainTb027TipoSolicitud'),
        ),
    ]
