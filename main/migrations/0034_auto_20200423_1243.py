# Generated by Django 3.0.4 on 2020-04-23 16:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0033_auto_20200423_1001'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='maintb021indentificacion',
            name='co_estatus',
        ),
        migrations.AddField(
            model_name='tb001usuario',
            name='co_estatus_img',
            field=models.ForeignKey(blank=True, db_column='co_estatus_img', default=4, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='estatus_img', to='main.WalletTb009Estatus'),
        ),
        migrations.AddField(
            model_name='tb001usuario',
            name='co_estatus_persona',
            field=models.ForeignKey(blank=True, db_column='co_estatus_persona', default=4, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='estatus_persona', to='main.WalletTb009Estatus'),
        ),
    ]
