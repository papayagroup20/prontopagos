# Generated by Django 3.0.5 on 2020-08-28 16:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0100_telegramtb001rutasservicios_telegramtb002rutastelegram_wallettb028activos_wallettb029usuarioactivos_'),
    ]

    operations = [
        migrations.AddField(
            model_name='maintb027tiposolicitud',
            name='in_concurrencia',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
    ]
