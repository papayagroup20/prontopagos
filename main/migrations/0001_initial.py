# Generated by Django 3.0.4 on 2020-03-17 19:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainTb002Pais',
            fields=[
                ('co_pais', models.BigAutoField(primary_key=True, serialize=False)),
                ('tx_nacionalidad', models.CharField(blank=True, max_length=50, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, null=True)),
                ('tx_pais', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'db_table': 'main_tb002_pais',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='MainTb003Estado',
            fields=[
                ('co_estado', models.BigAutoField(primary_key=True, serialize=False)),
                ('tx_estato', models.CharField(blank=True, max_length=50, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, null=True)),
            ],
            options={
                'db_table': 'main_tb003_estado',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='MainTb004Ciudad',
            fields=[
                ('co_ciudad', models.BigAutoField(primary_key=True, serialize=False)),
                ('tx_ciudad', models.CharField(blank=True, max_length=50, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, null=True)),
            ],
            options={
                'db_table': 'main_tb004_ciudad',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='MainTb006TipoDocumento',
            fields=[
                ('co_tipo_documento', models.BigAutoField(primary_key=True, serialize=False)),
                ('tx_tipo_documento', models.CharField(blank=True, max_length=50, null=True)),
                ('tx_abreviacion', models.CharField(blank=True, max_length=20, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, null=True)),
            ],
            options={
                'db_table': 'main_tb006_tipo_documento',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='MainTb007Sexo',
            fields=[
                ('co_sexo', models.BigAutoField(primary_key=True, serialize=False)),
                ('tx_sexo', models.CharField(blank=True, max_length=30, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'main_tb007_sexo',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='MainTb008Direccion',
            fields=[
                ('co_direccion', models.BigAutoField(primary_key=True, serialize=False)),
                ('tx_direccion', models.CharField(blank=True, max_length=255, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, null=True)),
            ],
            options={
                'db_table': 'main_tb008_direccion',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='MainTb005Persona',
            fields=[
                ('co_persona', models.BigAutoField(primary_key=True, serialize=False)),
                ('tx_primer_nombre', models.CharField(blank=True, max_length=50, null=True)),
                ('tx_segundo_nombre', models.CharField(blank=True, max_length=50, null=True)),
                ('tx_primer_apellido', models.CharField(blank=True, max_length=50, null=True)),
                ('tx_segundo_apellido', models.CharField(blank=True, max_length=50, null=True)),
                ('nu_documento', models.IntegerField(blank=True, null=True)),
                ('fe_nacimiento', models.DateField(blank=True, null=True)),
                ('nu_telefono', models.CharField(blank=True, max_length=10, null=True)),
                ('in_activo', models.BooleanField(blank=True, null=True)),
                ('co_sexo', models.ForeignKey(blank=True, db_column='co_sexo', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.MainTb007Sexo')),
                ('co_tipo_documento', models.ForeignKey(blank=True, db_column='co_tipo_documento', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.MainTb006TipoDocumento')),
            ],
            options={
                'db_table': 'main_tb005_persona',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Tb001Usuario',
            fields=[
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('co_usuario', models.BigAutoField(primary_key=True, serialize=False)),
                ('username', models.CharField(blank=True, max_length=255, null=True, unique=True)),
                ('email', models.CharField(blank=True, max_length=255, null=True, unique=True)),
                ('first_name', models.CharField(blank=True, max_length=255, null=True, unique=True)),
                ('last_name', models.CharField(blank=True, max_length=255, null=True, unique=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('is_active', models.BooleanField(blank=True, default=True, null=True)),
                ('is_staff', models.BooleanField(blank=True, default=False, null=True)),
                ('co_persona', models.ForeignKey(blank=True, db_column='co_persona', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.MainTb005Persona')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
