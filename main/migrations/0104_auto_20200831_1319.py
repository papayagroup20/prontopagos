# Generated by Django 3.0.5 on 2020-08-31 17:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0103_auto_20200831_1306'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wallettb030solicitudconfianza',
            name='in_proceso',
        ),
        migrations.AddField(
            model_name='wallettb030solicitudconfianza',
            name='co_solicitud',
            field=models.ForeignKey(blank=True, db_column='co_solicitud', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.MainTb028Solicitud'),
        ),
    ]
