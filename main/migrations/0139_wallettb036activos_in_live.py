# Generated by Django 2.2.16 on 2021-01-12 14:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0138_wallettb039precioppg_wallettb040limite_wallettb041limiteusuario'),
    ]

    operations = [
        migrations.AddField(
            model_name='wallettb036activos',
            name='in_live',
            field=models.BooleanField(blank=True, default=True, null=True),
        ),
    ]
