# Generated by Django 2.2.16 on 2022-06-02 13:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0201_tb001usuario_co_usuario_crm'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainTb046Memo',
            fields=[
                ('co_memo', models.BigAutoField(primary_key=True, serialize=False)),
                ('tx_memo', models.CharField(blank=True, max_length=255, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, default=True, null=True)),
            ],
            options={
                'db_table': 'main_tb046_memo',
                'managed': True,
            },
        ),
        migrations.AddField(
            model_name='maintb022directorio',
            name='co_memo',
            field=models.ForeignKey(blank=True, db_column='co_memo', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='directorio_memo', to='main.MainTb046Memo'),
        ),
    ]
