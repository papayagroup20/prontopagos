# Generated by Django 2.2.16 on 2022-06-17 17:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0206_auto_20220615_1153'),
    ]

    operations = [
        migrations.AddField(
            model_name='wallettb005pagoservicios',
            name='co_emisor',
            field=models.ForeignKey(blank=True, db_column='co_emisor', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='pase_emisor', to='main.Tb001Usuario'),
        ),
        migrations.AlterField(
            model_name='wallettb005pagoservicios',
            name='co_usuario',
            field=models.ForeignKey(blank=True, db_column='co_usuario', null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='pase_usuario', to='main.Tb001Usuario'),
        ),
    ]
