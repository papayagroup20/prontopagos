# Generated by Django 2.2.16 on 2021-01-08 15:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0137_auto_20201228_1201'),
    ]

    operations = [
        migrations.CreateModel(
            name='WalletTb039PrecioPPG',
            fields=[
                ('co_precio_ppg', models.AutoField(primary_key=True, serialize=False)),
                ('nu_precio', models.DecimalField(blank=True, decimal_places=5, max_digits=32, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, default=True, null=True)),
            ],
            options={
                'db_table': 'wallet_tb039_precio_ppg',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='WalletTb040Limite',
            fields=[
                ('co_limite', models.AutoField(primary_key=True, serialize=False)),
                ('nu_limite', models.DecimalField(blank=True, decimal_places=5, max_digits=32, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, default=True, null=True)),
            ],
            options={
                'db_table': 'wallet_tb040_limite',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='WalletTb041LimiteUsuario',
            fields=[
                ('co_limite_usuario', models.AutoField(primary_key=True, serialize=False)),
                ('nu_cantidad', models.DecimalField(blank=True, decimal_places=5, default=0, max_digits=32, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, default=True, null=True)),
                ('co_limite', models.ForeignKey(blank=True, db_column='co_limite', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb040Limite')),
                ('co_usuario', models.ForeignKey(blank=True, db_column='co_usuario', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.Tb001Usuario')),
            ],
            options={
                'db_table': 'wallet_tb041_limite_usuario',
                'managed': True,
            },
        ),
    ]
