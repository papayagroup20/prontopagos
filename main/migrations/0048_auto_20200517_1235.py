# Generated by Django 3.0.4 on 2020-05-17 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0047_wallettb010recarga_co_cuenta_emisor'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wallettb010recarga',
            name='co_cuenta_emisor',
        ),
        migrations.AddField(
            model_name='wallettb010recarga',
            name='tx_comentario',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
