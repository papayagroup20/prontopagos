# Generated by Django 2.2.16 on 2021-06-15 22:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0167_auto_20210615_1737'),
    ]

    operations = [
        migrations.AddField(
            model_name='wallettb050comisionafiliado',
            name='co_tipo_transaccion',
            field=models.ForeignKey(blank=True, db_column='co_tipo_transaccion', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb013TipoTransaccion'),
        ),
    ]
