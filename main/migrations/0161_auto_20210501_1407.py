# Generated by Django 2.2.16 on 2021-05-01 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0160_wallettb045aplicationsep07_tx_domain_origin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wallettb045aplicationsep07',
            name='tx_domain_origin',
            field=models.CharField(default='papaya', max_length=255),
            preserve_default=False,
        ),
    ]
