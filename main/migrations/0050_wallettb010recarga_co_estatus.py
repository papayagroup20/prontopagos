# Generated by Django 3.0.4 on 2020-05-20 23:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0049_auto_20200520_1844'),
    ]

    operations = [
        migrations.AddField(
            model_name='wallettb010recarga',
            name='co_estatus',
            field=models.ForeignKey(blank=True, db_column='co_estatus', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb009Estatus'),
        ),
    ]
