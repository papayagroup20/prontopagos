# Generated by Django 3.0.5 on 2020-06-07 19:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0058_wallettb018retiro_co_transaccion'),
    ]

    operations = [
        migrations.AddField(
            model_name='wallettb010recarga',
            name='co_banco',
            field=models.ForeignKey(blank=True, db_column='co_banco', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb007Banco'),
        ),
    ]
