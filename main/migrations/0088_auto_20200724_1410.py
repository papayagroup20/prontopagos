# Generated by Django 3.0.5 on 2020-07-24 18:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0087_maintb028solicitud_co_usuario2'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wallettb018retiro',
            name='co_comision',
        ),
        migrations.RemoveField(
            model_name='wallettb018retiro',
            name='nu_monto',
        ),
        migrations.RemoveField(
            model_name='wallettb027retirohistorico',
            name='co_transaccion',
        ),
        migrations.AddField(
            model_name='wallettb018retiro',
            name='co_estatus',
            field=models.ForeignKey(blank=True, db_column='co_estatus', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb009Estatus'),
        ),
    ]
