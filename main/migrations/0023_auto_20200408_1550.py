# Generated by Django 3.0.4 on 2020-04-08 19:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0022_wallettb015transaccion_nu_telegram'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wallettb015transaccion',
            name='nu_telegram',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
