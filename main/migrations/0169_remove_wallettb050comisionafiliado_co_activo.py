# Generated by Django 2.2.16 on 2021-06-15 22:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0168_wallettb050comisionafiliado_co_tipo_transaccion'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wallettb050comisionafiliado',
            name='co_activo',
        ),
    ]
