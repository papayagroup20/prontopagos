# Generated by Django 2.2.16 on 2020-10-06 19:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0108_auto_20200929_1118'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='MaintTb015Banco',
            new_name='MainTb015Banco',
        ),
        migrations.RenameModel(
            old_name='MaintTb016BancoMoneda',
            new_name='MainTb016BancoMoneda',
        ),
        migrations.RenameModel(
            old_name='MaintTb017Cuenta',
            new_name='MainTb017Cuenta',
        ),
        migrations.AddField(
            model_name='tb001usuario',
            name='in_ip',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
        migrations.CreateModel(
            name='MainTb042RegistroIP',
            fields=[
                ('co_registro_ip', models.BigAutoField(primary_key=True, serialize=False)),
                ('tx_registro_ip', models.CharField(blank=True, max_length=50, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('in_activo', models.BooleanField(blank=True, default=True, null=True)),
                ('co_usuario', models.ForeignKey(blank=True, db_column='co_usuario', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.Tb001Usuario')),
            ],
            options={
                'db_table': 'main_tb042_registro_ip',
                'managed': True,
            },
        ),
    ]
