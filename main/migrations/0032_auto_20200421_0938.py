# Generated by Django 3.0.4 on 2020-04-21 13:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0031_auto_20200420_1225'),
    ]

    operations = [
        migrations.AddField(
            model_name='maintb021indentificacion',
            name='co_estatus',
            field=models.ForeignKey(blank=True, db_column='co_estatus', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.WalletTb009Estatus'),
        ),
        migrations.AddField(
            model_name='maintb021indentificacion',
            name='co_usuario',
            field=models.ForeignKey(blank=True, db_column='co_usuario', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.Tb001Usuario'),
        ),
    ]
