from django import template
from main.models import *
register = template.Library()


@register.simple_tag
def get_incidencia_staff(*args, **kwargs):
    
    co_solicitud = kwargs['co_solicitud']
    usuario = kwargs['usuario']
    ruta=MainTb029Ruta.objects.filter(co_solicitud=co_solicitud,in_activo=True)[0]
    
    formulario=MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=usuario.co_rol.co_rol,co_aplicaciones=settings.CODIGO_APLICACION)[0] if MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=usuario.co_rol.co_rol,co_aplicaciones=settings.CODIGO_APLICACION).count()>0 else None
    
    return formulario

@register.simple_tag()
def multiplicar(monto, cantidad, *args, **kwargs):
    # you would need to do any localization of the result here
    return monto * cantidad
    