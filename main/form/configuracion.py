from main.models import *
from django import forms
import re
from django.core.files.storage import FileSystemStorage

from django.utils.translation import gettext_lazy as _
from binascii import unhexlify
from time import time
from django_otp.forms import OTPAuthenticationFormMixin
from django_otp.oath import totp
from django_otp.plugins.otp_totp.models import TOTPDevice
from django_otp import match_token



class PaisForm(forms.Form):
    tx_pais=forms.CharField(max_length=50,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_nacionalidad=forms.CharField(max_length=50,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))


class RolForm(forms.Form):
    tx_rol=forms.CharField(max_length=50,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))

class InstitutoForm(forms.Form):
    tx_instituto=forms.CharField(max_length=50,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_padre=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','disabled':'disabled'}))
    co_inst = forms.ModelChoiceField(queryset=MainTb024Instituto.objects.all(), empty_label="Seleccione...", to_field_name="co_instituto",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'}))

    def __init__(self, *args, **kwargs):
        
        super(InstitutoForm, self).__init__(*args, **kwargs)
        self.fields['co_inst'].queryset = MainTb024Instituto.objects.filter(nu_padre = 0)

class TipoSolicitudForm(forms.Form):
    tx_tipo_solicitud=forms.CharField(max_length=50,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_busqueda=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white mr-sm-2','type':'search','placeholder':'Buscar'}))
    co_instituto = forms.ModelChoiceField(queryset=MainTb024Instituto.objects.all(), empty_label="Seleccione...", to_field_name="co_instituto",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_tipo_solicitud = forms.ModelChoiceField(queryset=MainTb027TipoSolicitud.objects.all(), empty_label="Seleccione...", to_field_name="co_tipo_solicitud",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_proceso = forms.ModelChoiceField(queryset=MainTb025Proceso.objects.all(), empty_label="Seleccione...", to_field_name="co_proceso",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_estatus = forms.ModelChoiceField(queryset=WalletTb009Estatus.objects.all(), empty_label="Seleccione...", to_field_name="co_estatus",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    
    #Cuando halla internet, investigar y programar que al usuario le aparesca a escoges en su busqueda 
    #solo los tipo de solicitud que el ha hecho.
    def __init__(self, *args, **kwargs): 

        
        self.user=kwargs.pop('usu') if 'usu' in kwargs else None

        super(TipoSolicitudForm, self).__init__(*args, **kwargs)

        if self.user is not None:
            self.fields['co_tipo_solicitud'].queryset = MainTb037RolSolicitud.objects.filter(co_rol=self.user.co_rol.co_rol,in_activo=True)
               

class RutaForm(forms.Form):
    co_instituto = forms.ModelChoiceField(queryset=MainTb024Instituto.objects.all(), empty_label="Seleccione...", to_field_name="co_instituto",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_proceso = forms.ModelChoiceField(queryset=MainTb025Proceso.objects.all(), empty_label="Seleccione...", to_field_name="co_proceso",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
       
class ProcesoForm(forms.Form):
    tx_proceso=forms.CharField(max_length=50,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_busqueda=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white mr-sm-2','type':'search','placeholder':'Buscar'}))
class UsuarioConfigForm(forms.Form):
    username=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    co_rol = forms.ModelChoiceField(queryset=MainTb023Rol.objects.all(), empty_label="Seleccione...", to_field_name="co_rol",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_instituto = forms.ModelChoiceField(queryset=MainTb024Instituto.objects.all(), empty_label="Seleccione...", to_field_name="co_instituto",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    tx_busqueda=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white mr-sm-2','type':'search','placeholder':'Buscar'}))

class MenuForm(forms.Form):
    tx_menu=forms.CharField(max_length=50,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_url=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    co_menu = forms.ModelChoiceField(queryset=MainTb030Menu.objects.filter(in_link=False), empty_label="Seleccione...", to_field_name="co_menu",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_icono=forms.ModelChoiceField(queryset=MainTb033Iconos.objects.all(), empty_label="Seleccione...", to_field_name="co_icono",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    tx_busqueda=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white mr-sm-2','type':'search','placeholder':'Buscar'}))

class RolSolicitud(forms.Form):
    co_rol=forms.ModelChoiceField(queryset=MainTb023Rol.objects.all(), empty_label="Seleccione...", to_field_name="co_rol",required=True,widget=forms.Select(attrs={'class': 'selectpicker','title':'Single Select', 'title':'Seleccione...','data-size': '7',}))