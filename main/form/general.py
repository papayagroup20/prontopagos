from main.models import *
from django import forms
import re
from django.core.files.storage import FileSystemStorage
from stellar_sdk.server import Server
from django.utils.translation import gettext_lazy as _
from binascii import unhexlify
from time import time
from django_otp.forms import OTPAuthenticationFormMixin
from django_otp.oath import totp
from django_otp.plugins.otp_totp.models import TOTPDevice
from django_otp import match_token
import pyotp
from base64 import b32encode,b32decode
from django.db.models import Prefetch
import requests
from django.conf import settings
from pprint import pprint
from main.views.sep10 import SEP10Auth
from django.db.models import Q

class claveForm(forms.Form):
    password=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.PasswordInput(attrs={'class': 'form-control text-white',"placeholder":"Contraseña"}))
    password2=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.PasswordInput(attrs={'class': 'form-control text-white',"placeholder":"Repetir Contraseña"}))
   
    def clean_password(self):
        password=self.cleaned_data['password']
        if password.isalpha():
            raise forms.ValidationError("La contraseña debe contener numeros")
        if password.isdigit():
            raise forms.ValidationError("La contraseña debe contener letras")
        if password.islower():
            raise forms.ValidationError("La contraseña debe contener mayusculas")
        if password.isupper():
            raise forms.ValidationError("La contraseña debe contener minusculas")
    
        if not re.findall('[()[\]{}|\\`~!@#$%^&*_\-+=;:\'",<>./?]', password):
            raise forms.ValidationError("La contraseña debe contener caracteres especiales")
                
    def clean_password2(self):
        password=self.data.get('password')
        password2=self.cleaned_data['password2']

        if password!=password2:
            raise forms.ValidationError("Las contraseñas no coinciden")
class PreRegistroForm(forms.Form):
    username=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white ',"placeholder":"Usuario","disabled":"disabled"}))
    password=forms.CharField(min_length=4,max_length=255,required=True,widget=forms.PasswordInput(attrs={'class': 'form-control text-white',"placeholder":"Contraseña"}))
    nu_telegram=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'number'}))
    
    #co_tipo_indentidad=forms.ModelChoiceField(queryset=MainTb035TipoIndentidad.objects.filter(co_tipo_indentidad__in=[1,2]), empty_label="Seleccione...", to_field_name="co_tipo_indentidad",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))

    def clean_password(self):
        password=self.cleaned_data['password']
        if password.isalpha():
            raise forms.ValidationError("La contraseña debe contener numeros")
        if password.isdigit():
            raise forms.ValidationError("La contraseña debe contener letras")

class ReClaveForm(forms.Form):
    password=forms.CharField(min_length=4,max_length=255,required=True,widget=forms.PasswordInput(attrs={'class': 'form-control text-white clave',"placeholder":"Contraseña"}))
    password2=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.PasswordInput(attrs={'class': 'form-control text-white clave',"placeholder":"Repetir Contraseña"}))
    tx_frase=forms.CharField(required=False,widget=forms.Textarea(attrs={'class': 'form-control text-white frase'}))
    tx_usuario=forms.CharField(min_length=4,max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white ',"placeholder":"Usuario/llave publica"}))
    
    def clean_password(self):
        password=self.cleaned_data['password']
        if password.isalpha():
            raise forms.ValidationError("La contraseña debe contener numeros")
        if password.isdigit():
            raise forms.ValidationError("La contraseña debe contener letras")
        if password.islower():
            raise forms.ValidationError("La contraseña debe contener mayusculas")
        if password.isupper():
            raise forms.ValidationError("La contraseña debe contener minusculas")
    
        if not re.findall('[()[\]{}|\\`~!@#$%^&*_\-+=;:\'",<>./?]', password):
            raise forms.ValidationError("La contraseña debe contener caracteres especiales")
                
    def clean_password2(self):
        password=self.data.get('password')
        password2=self.cleaned_data['password2']

        if password!=password2:
            raise forms.ValidationError("Las contraseñas no coinciden")


class RegistroForm(forms.Form):
    username=forms.CharField(min_length=4,max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white ',"placeholder":"Usuario","disabled":"disabled"}))
    email=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.EmailInput(attrs={'class': 'form-control text-white',"placeholder":"Correo"}))
    #password=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.PasswordInput(attrs={'class': 'form-control text-white',"placeholder":"Contraseña"}))
    #password2=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.PasswordInput(attrs={'class': 'form-control text-white',"placeholder":"Repetir Contraseña"}))
    #co_usuario=forms.CharField(required=True,widget=forms.HiddenInput())
    #co_tipo_indentidad=forms.ModelChoiceField(queryset=MainTb035TipoIndentidad.objects.filter(co_tipo_indentidad__in=[1,2]), empty_label="Tipo de indentidad", to_field_name="co_tipo_indentidad",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    
    usermail=forms.CharField(min_length=4,max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white ',"placeholder":"Usuario"}))
    
    #input de valdacion de codigo
    c1=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white ico','id':'id_c1','onkeyup':'if(this.value.length==1) id_c2.focus()'}))
    c2=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white ico','id':'id_c2','onkeyup':'if(this.value.length==1) id_c3.focus()'}))
    c3=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white ico','id':'id_c3','onkeyup':'if(this.value.length==1) id_c4.focus()'}))
    c4=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white ico','id':'id_c4','onkeyup':'if(this.value.length==1) boton2.focus()'}))
    
    def __init__(self, *args, **kwargs):
        
        super(RegistroForm, self).__init__(*args, **kwargs)
        if 'email' in self.initial and self.initial['email'] is not None:
            self.fields['email'].widget.attrs.update({'disabled':'disabled'})

        
    #paso de natural
    """tx_primer_nombre=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white texto jtexto c50'}))
    tx_segundo_nombre=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white texto jtexto '}))
    tx_primer_apellido=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white texto jtexto c50'}))
    tx_segundo_apellido=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white texto jtexto '}))
    co_sexo = forms.ModelChoiceField(queryset=MainTb007Sexo.objects.all(), empty_label="Seleccione...", to_field_name="co_sexo",required=False,widget=forms.Select(attrs={'class': 'form-control text-white texto'}))
    co_pais = forms.ModelChoiceField(queryset=MainTb002Pais.objects.all(), empty_label="Seleccione...", to_field_name="co_pais",required=False,widget=forms.Select(attrs={'class': 'form-control text-white texto jtexto pais'}))
    co_estado = forms.ModelChoiceField(queryset=MainTb003Estado.objects.none(), empty_label="Seleccione...", to_field_name="co_estado",required=False,widget=forms.Select(attrs={'class': 'form-control text-white texto jtexto estado'}))
    co_ciudad = forms.ModelChoiceField(queryset=MainTb004Ciudad.objects.none(), empty_label="Seleccione...", to_field_name="co_ciudad",required=False,widget=forms.Select(attrs={'class': 'form-control text-white texto jtexto ciudad'}))

    fe_nacimiento = forms.CharField(required=False,widget=forms.TextInput(attrs={'type':'date','class': 'form-control text-white texto jtexto'}))#'%d/%m/%Y'
    nu_documento=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white texto jtexto','type':'number'}))
    nu_telefono=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white texto jtexto','type':'number'}))
    co_codigo_area=forms.ModelChoiceField(queryset=MainTb034CodigoArea.objects.all(), empty_label="-", to_field_name="co_codigo_area",required=False,widget=forms.Select(attrs={'class': 'form-control text-white texto jtexto'}))
    tx_codigo_postal=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white texto jtexto'}))
    
    tx_direccion=forms.CharField(max_length=255,required=False,widget=forms.Textarea(attrs={'class': 'form-control text-white texto jtexto','rows':5, 'cols':20}))
    
    co_tipo_documento = forms.ModelChoiceField(queryset=MainTb006TipoDocumento.objects.all(), empty_label="-", to_field_name="co_tipo_documento",required=False, widget=forms.Select(attrs={'class': 'form-control text-white texto jtexto'}))
    #paso juridico
    nu_reg_comercio=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white jtexto c50'}))
    tx_denom_comercial=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white  jtexto c50'}))
"""    
    
    """def clean_username(self):
        username=self.cleaned_data['username']
        if Tb001Usuario.objects.filter(username=username):
            raise forms.ValidationError("el usuario ya existe")

        return username"""

    def clean_email(self):
        email=self.cleaned_data['email']
        username=self.data.get('username')
        usuario=Tb001Usuario.objects.filter(username=username)[0]
        if usuario.email is not None:
            if usuario.email!=email:
                raise forms.ValidationError("El correo no coincide con su correo registrado")
        elif Tb001Usuario.objects.filter(email=email):
            raise forms.ValidationError("Ya existe este correo")
        return email
        
    
    """def clean_password(self):
        password=self.cleaned_data['password']
        if password.isalpha():
            raise forms.ValidationError("La contraseña debe contener numeros")
        if password.isdigit():
            raise forms.ValidationError("La contraseña debe contener letras")
        if password.islower():
            raise forms.ValidationError("La contraseña debe contener mayusculas")
        if password.isupper():
            raise forms.ValidationError("La contraseña debe contener minusculas")
    
        if not re.findall('[()[\]{}|\\`~!@#$%^&*_\-+=;:\'",<>./?]', password):
            raise forms.ValidationError("La contraseña debe contener caracteres especiales")
                
    def clean_password2(self):
        password=self.data.get('password')
        password2=self.cleaned_data['password2']

        if password!=password2:
            raise forms.ValidationError("Las contraseñas no coinciden")"""

class VerificarForm(forms.Form):
    c1=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white'}))
    c2=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white'}))
    c3=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white'}))
    c4=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white'}))
    
class RegistroPersonaForm(forms.Form):
    tx_primer_nombre=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_segundo_nombre=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_primer_apellido=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_segundo_apellido=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    co_sexo = forms.ModelChoiceField(queryset=MainTb007Sexo.objects.all(), empty_label="Seleccione...", to_field_name="co_sexo",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_pais = forms.ModelChoiceField(queryset=MainTb002Pais.objects.all(), empty_label="Seleccione...", to_field_name="co_pais",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_estado = forms.ModelChoiceField(queryset=MainTb003Estado.objects.all(), empty_label="Seleccione...", to_field_name="co_estado",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    co_ciudad = forms.ModelChoiceField(queryset=MainTb004Ciudad.objects.all(), empty_label="Seleccione...", to_field_name="co_ciudad",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))

    fe_nacimiento = forms.CharField(required=False,widget=forms.TextInput(attrs={'type':'date','class': 'form-control text-white'}))#'%d/%m/%Y'
    nu_documento=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'number'}))
    nu_telefono=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'number'}))
    tx_direccion=forms.CharField(max_length=255,required=False,widget=forms.Textarea(attrs={'class': 'form-control text-white materialize-textarea','rows':5, 'cols':20}))
    co_codigo_area=forms.ModelChoiceField(queryset=MainTb034CodigoArea.objects.all(), empty_label="-", to_field_name="co_codigo_area",required=True,widget=forms.Select(attrs={'class': 'form-control text-white texto jtexto'}))
    tx_codigo_postal=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    nu_reg_comercio=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white jtexto c50'}))
    tx_denom_comercial=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white  jtexto c50'}))
    
    co_tipo_documento = forms.ModelChoiceField(queryset=MainTb006TipoDocumento.objects.all(), empty_label="-", to_field_name="co_tipo_documento",required=True, widget=forms.Select(attrs={'class': 'form-control text-white'}))
    
    def __init__(self, *args, **kwargs):
        
        self.user=kwargs.pop('usu')
        self.tipo=kwargs.pop('tipo') if 'tipo' in kwargs else True
        super(RegistroPersonaForm, self).__init__(*args, **kwargs)
        if self.tipo is True:
            self.fields['tx_denom_comercial'].required=False
            self.fields['nu_reg_comercio'].required=False
        else:
            self.fields['co_sexo'].required=False

        if self.user.co_persona is not None:
            if self.user.co_persona.co_direccion is not None:
                self.fields['co_estado'].queryset = MainTb003Estado.objects.filter(co_pais = self.user.co_persona.co_direccion.co_ciudad.co_estado.co_pais.co_pais)
                self.fields['co_ciudad'].queryset = MainTb004Ciudad.objects.filter(co_estado = self.user.co_persona.co_direccion.co_ciudad.co_estado.co_estado)
class IndenrificacionUsuarioForm(forms.Form):
    co_tipo_indentificacion = forms.ModelChoiceField(queryset=MainTb020TipoIndentificacion.objects.all(), empty_label="Seleccione...", to_field_name="co_tipo_indentificacion",required=False,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    #img_uno= forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'file-path validate white-text','type':'text','placeholder':'Solo acepta PNG y JPG','lang':'es'}))
    #img_dos= forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'custom-file-input text-white','type':'file','placeholder':'Solo acepta PNG y JPG','lang':'es'}))
    #img_selfie= forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'custom-file-input text-white','type':'file','placeholder':'Solo acepta PNG y JPG','lang':'es'}))
    

    #img_uno_file= forms.ImageField(required=False,widget=forms.TextInput(attrs={'class': 'custom-file-input text-white','type':'file','lang':'es'}))
    #img_dos_file= forms.ImageField(required=False,widget=forms.TextInput(attrs={'class': 'custom-file-input text-white','type':'file','lang':'es','accept':'image/*'}))
    #img_selfie_file= forms.ImageField(required=False,widget=forms.TextInput(attrs={'class': 'custom-file-input text-white','type':'file','lang':'es'}))
    img_uno_file= forms.ImageField(required=True)
    img_dos_file= forms.ImageField(required=True)
    img_selfie_file= forms.ImageField(required=True)
    
    


class Autenticar2FAForm(OTPAuthenticationFormMixin, forms.Form):
    otp_token = forms.CharField(min_length=6,max_length=6,
                                required=True,
                                widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'number'}))

    #otp_token.widget.attrs.update({'autofocus': 'autofocus'})

    # Our authentication form has an additional submit button to go to the
    # backup token form. When the `required` attribute is set on an input
    # field, that button cannot be used on browsers that implement html5
    # validation. For now we'll use this workaround, but an even nicer
    # solution would be to move the button outside the `<form>` and into
    # its own `<form>`.

   


    #use_required_attribute = False

    def __init__(self, *args, **kwargs):
        

        
        self.user=kwargs.pop('user')
        super(Autenticar2FAForm, self).__init__(*args, **kwargs)

        # YubiKey generates a OTP of 44 characters (not digits). So if the
        # user's primary device is a YubiKey, replace the otp_token
        # IntegerField with a CharField.
class registerTOTPDeviceForm(forms.Form):
    """token = forms.CharField(min_length=6,max_length=6,
                                required=True,
                                widget=forms.TextInput(attrs={'class': 'form-control text-white','type':'number'}))

    error_messages = {
        'invalid_token': _('Entered token is not valid.'),
    }

    def __init__(self,*args, **kwargs):
        
        self.user=kwargs.pop('user')
        self.key=kwargs.pop('key')
        
        #self.key = key
        self.tolerance = 1
        self.t0 = 0
        self.step = 30
        self.drift = 0
        self.digits = totp_digits()
        #self.user = user
        self.metadata = {}
        super(registerTOTPDeviceForm, self).__init__(*args, **kwargs)

    @property
    def bin_key(self):
        
        return unhexlify(self.key.encode())

    def clean_token(self):
        token = self.cleaned_data['token']
        validated = False
        t0s = [self.t0]
        key = self.bin_key
        if 'valid_t0' in self.metadata:
            t0s.append(int(time()) - self.metadata['valid_t0'])
        for t0 in t0s:
            for offset in range(-self.tolerance, self.tolerance):
                if totp(key, self.step, t0, self.digits, self.drift + offset) == token:
                    self.drift = offset
                    self.metadata['valid_t0'] = int(time()) - t0
                    validated = True
        if not validated:
            raise forms.ValidationError('token invalido')
        return tokendef save(self):
        return TOTPDevice.objects.create(user=self.user, key=self.key,
                                         tolerance=self.tolerance, t0=self.t0,
                                         step=self.step, drift=self.drift,
                                         digits=self.digits,
                                         name='default')"""
class RegistroBasicoForm(forms.Form):
    username = forms.CharField(min_length=4,max_length=50,required=True,widget=forms.TextInput(attrs={"class": "form-control text-white form2","placeholder":"Usuario"}))
    email=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.EmailInput(attrs={'class': 'form-control text-white form2',"placeholder":"Correo"}))
    
    def clean_username(self):
        username=self.cleaned_data['username']
        if Tb001Usuario.objects.filter(username=username):
            raise forms.ValidationError("Ya existe este usuario")
        return username
    
    def clean_email(self):
        email=self.cleaned_data['email']
        if Tb001Usuario.objects.filter(email=email):
            raise forms.ValidationError("Ya existe este correo")

        return email
    
class RegistroParte1Form(forms.Form):
    co_tipo_indentidad = forms.ModelChoiceField(queryset=MainTb035TipoIndentidad.objects.exclude(co_tipo_indentidad=3), empty_label="Seleccione...", to_field_name="co_tipo_indentidad",required=True,widget=forms.Select(attrs={'class': 'selectpicker','title':'Single Select', 'title':'Seleccione...','data-size': '7','data-style':'btn btn-primary',}))
    nu_documento = forms.CharField(max_length=50,required=True,widget=forms.TextInput(attrs={"class": "form-control text-white","placeholder":"Documento"}))
    
    def clean_nu_documento(self):
        nu_documento=self.cleaned_data['nu_documento']
        if MainTb005Persona.objects.filter(nu_documento=nu_documento).exists():
            raise forms.ValidationError("Este documento ya existe")
        return nu_documento
    
class RegistroParte2Form(RegistroBasicoForm):
    nu_telefono = forms.CharField(min_length=4,max_length=50,required=True,widget=forms.TextInput(attrs={"class": "form-control text-white form2","placeholder":"Telefono"}))
     

    def clean_nu_telefono(self):
        nu_telefono=self.cleaned_data['nu_telefono']
        if MainTb005Persona.objects.filter(nu_telefono=nu_telefono).exists():
            raise forms.ValidationError("Este telefono ya existe")
        try:
            if not isinstance(int(nu_telefono),int):
                 raise forms.ValidationError("El telefono debe ser un numero")
        except:
            raise forms.ValidationError("El telefono debe ser un numero2")
        return nu_telefono


    
    """def clean_password(self):
        password=self.cleaned_data['password']
        if password.isalpha():
            raise forms.ValidationError("Debe ser alfanumerico")
        if password.isdigit():
            raise forms.ValidationError("Debe ser alfanumerico")
        if password.islower():
            raise forms.ValidationError("Debe tener mayusculas")
        if password.isupper():
            raise forms.ValidationError("Debe tener minusculas")
 
        if not re.findall('[()[\]{}|\\`~!@$%^&*_\-+=;:\'",<>./?]', password):
            raise forms.ValidationError("Debe tener caracteres especiales")
        
        if re.findall('[#]', password):
            raise forms.ValidationError("La clave contiene caractires invalidos")
        
        return password

    def clean_password2(self):
        password=self.data.get('password')
        password2=self.cleaned_data['password2']

        if password!=password2:
            raise forms.ValidationError("Las contraseñas no coinciden")"""

class RegistroParte2ioForm(RegistroParte2Form):
    password=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.PasswordInput(attrs={"class": "form-control text-white form2","placeholder":"Contraseña"}))
    password2=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.PasswordInput(attrs={'class': 'form-control text-white form2',"placeholder":"Repetir Contraseña"}))
    
    def clean_password(self):
        password=self.cleaned_data['password']
        if password.isalpha():
            raise forms.ValidationError("Debe ser alfanumerico")
        if password.isdigit():
            raise forms.ValidationError("Debe ser alfanumerico")
        if password.islower():
            raise forms.ValidationError("Debe tener mayusculas")
        if password.isupper():
            raise forms.ValidationError("Debe tener minusculas")
 
        if not re.findall('[()[\]{}|\\`~!@$%^&*_\-+=;:\'",<>./?]', password):
            raise forms.ValidationError("Debe tener caracteres especiales")
        
        """if re.findall('[#]', password):
            raise forms.ValidationError("La clave contiene caractires invalidos")"""
        
        return password

    def clean_password2(self):
        password=self.data.get('password')
        password2=self.cleaned_data['password2']

        if password!=password2:
            raise forms.ValidationError("Las contraseñas no coinciden")
        
    
class CheckIdentidadForm(forms.Form):
    identidad = forms.CharField(min_length=4,required=True,widget=forms.TextInput(attrs={"class": "form-control text-white form2","placeholder":"Identidad"}))
    def clean_identidad(self):
        identidad=self.cleaned_data['identidad']
        keystore=WalletTb037Keystore.objects\
            .select_related("co_usuario")\
            .filter(
               (Q(co_usuario__username=identidad)|
                Q(co_usuario__email=identidad)|
                Q(address=identidad)),co_usuario__isnull=False)\
            .first()
        if keystore is None:
            raise forms.ValidationError("El usuario no existe")
        return keystore

class ChangeClaveForm(CheckIdentidadForm):
    tx_codigo = forms.CharField(min_length=6,max_length=6,required=True,widget=forms.TextInput(attrs={"class": "form-control text-white form2","placeholder":"Codigo"}))
   
    
class BaseLoginForm(forms.Form):
    username = forms.CharField(min_length=4,max_length=50,required=True,widget=forms.TextInput(attrs={"class": "form-control text-white logininput","placeholder":"Usuario"}))
    
    def clean_username(self):
        username=self.cleaned_data['username']
        otp_token=self.data.get('otp_token')
        usuario=Tb001Usuario.objects\
        .select_related("last_session_key","auth_token")\
        .prefetch_related("keystore_usuario")\
        .filter(username=username)
        if not usuario.exists():
            raise forms.ValidationError("El usuario no existe")
        return usuario.first()
        
class LoginForm(BaseLoginForm):
    #password=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.PasswordInput(attrs={"class": "form-control text-white logininput","placeholder":"Contraseña"}))
    token=forms.CharField(required=True,widget=forms.TextInput(attrs={"type":"hidden"}))
    transaction=forms.CharField(required=True,widget=forms.TextInput(attrs={"type":"hidden"}))
    
    def clean_token(self):
        token=self.cleaned_data['token']
        key='6LfB0UgcAAAAABF0WtEyemp-JvBo-9YQkXhaN5L3'
        if 'ppg' in self.data:
            key=settings.RECAPTCHA_SECRET_KEY
        
        header={
          "Connection":"Close",
          "Content-Type":"application/x-www-form-urlencoded",
        }
        payload={
            'secret':key,
            'response':token
        }
        try:
            res=requests.post("https://www.google.com/recaptcha/api/siteverify",data=payload,headers=header)
            resjson=res.json()
            print(resjson)
            if resjson["success"]==False:
                raise  forms.ValidationError("Kaptcha invalido")
        except Exception as e:
            print(e)
            raise  forms.ValidationError("Kaptcha invalido")

        return token
    def clean_transaction(self):
        envelope_xdr = self.cleaned_data['transaction']
        valido,error=SEP10Auth.validar_challenge_xdr(envelope_xdr)
        if valido==False:
            raise forms.ValidationError(error)
        return envelope_xdr
        


class Login2FAForm(forms.Form):
    otp_token = forms.CharField(min_length=6,max_length=6,required=True, widget=forms.TextInput(attrs={'autocomplete': 'off',"class": "form-control text-white","placeholder":"Token 2FA","type":"number"}))
    usernameh = forms.CharField(required=False,widget=forms.TextInput(attrs={"class": "form-control text-white","placeholder":"Usuario","disabled":"disabled"}))
    password=forms.CharField(required=False,widget=forms.TextInput(attrs={"type":"hidden"}))
    username = forms.CharField(required=False,widget=forms.TextInput(attrs={"type":"hidden"}))
    

    def clean_otp_token(self):
        token=username=self.cleaned_data['otp_token']
        username=self.data.get('username')
        usuario=Tb001Usuario.objects.filter(username=username)[0]

        if token is not None and token!="":
            if OtpTotpTotpdevice.objects.filter(user_id=usuario.co_usuario,confirmed=True).count()<=0:
                raise forms.ValidationError("El usuario no tiene un dispositivo activo")

            dev=OtpTotpTotpdevice.objects.filter(user_id=usuario.co_usuario,confirmed=True)[0]
            dev2=pyotp.totp.TOTP(b32encode(bytes(dev.key,"ascii")).decode("ascii"))

            if not dev2.verify(int(token)):
                raise forms.ValidationError("El token ingresado es incorrecto")

    

class backup2FAForm(forms.Form):
    username = forms.CharField(min_length=4,max_length=50,required=True,widget=forms.TextInput(attrs={"class": "form-control text-white","placeholder":"Usuario"}))
    password=forms.CharField(min_length=4,max_length=50,required=True,widget=forms.PasswordInput(attrs={"class": "form-control text-white","placeholder":"Contraseña"}))
    otp_token = forms.CharField(min_length=1,max_length=16,required=True, widget=forms.TextInput(attrs={'autocomplete': 'off',"class": "form-control text-white","placeholder":"Token de recuperación 2FA"}))

    def clean_username(self):
        username=self.cleaned_data['username']
        otp_token=self.data.get('otp_token')
        if Tb001Usuario.objects.filter(username=username).count()<=0:
            raise forms.ValidationError("El usuario no existe")

                            
    def clean_password(self):
        password=self.cleaned_data['password']
        if password.isalpha():
            raise forms.ValidationError("La contraseña debe contener numeros")
        if password.isdigit():
            raise forms.ValidationError("La contraseña debe contener letras")
        if password.islower():
            raise forms.ValidationError("La contraseña debe contener mayusculas")
        if password.isupper():
            raise forms.ValidationError("La contraseña debe contener minusculas")
#formulario para editar datos nuevo
class EditarUsuario2FaForm(forms.Form):
    tx_primer_nombre=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_segundo_nombre=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_primer_apellido=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    tx_segundo_apellido=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    co_sexo = forms.ModelChoiceField(queryset=MainTb007Sexo.objects.all(), empty_label="Seleccione...", to_field_name="co_sexo",required=False,widget=forms.Select(attrs={'class': 'selectpicker','title':'Single Select', 'title':'Seleccione...','data-size': '7','data-style':'btn btn-primary',}))
    co_pais = forms.ModelChoiceField(queryset=MainTb002Pais.objects.all(), empty_label="Seleccione...", to_field_name="co_pais",required=False,widget=forms.Select(attrs={'class': 'selectpicker','title':'Single Select', 'title':'Seleccione...','data-size': '7','data-style':'btn btn-primary',}))
    co_estado = forms.ModelChoiceField(queryset=MainTb003Estado.objects.all(), empty_label="Seleccione...", to_field_name="co_estado",required=False,widget=forms.Select(attrs={'class': 'selectpicker','title':'Single Select', 'title':'Seleccione...','data-size': '7','data-style':'btn btn-primary',}))
    co_ciudad = forms.ModelChoiceField(queryset=MainTb004Ciudad.objects.all(), empty_label="Seleccione...", to_field_name="co_ciudad",required=False,widget=forms.Select(attrs={'class': 'selectpicker','title':'Single Select', 'title':'Seleccione...','data-size': '7','data-style':'btn btn-primary',}))

    fe_nacimiento = forms.CharField(required=False,widget=forms.TextInput(attrs={'type':'date','class': 'form-control text-white'}))#'%d/%m/%Y'
    nu_documento=forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control','type':'number'}))
    nu_telefono=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','type':'text'}))
    tx_direccion=forms.CharField(max_length=255,required=False,widget=forms.Textarea(attrs={'class': 'form-control text-white materialize-textarea','rows':5, 'cols':20}))
    #co_codigo_area=forms.ModelChoiceField(queryset=MainTb034CodigoArea.objects.all(), empty_label="-", to_field_name="co_codigo_area",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    #tx_codigo_postal=forms.CharField(max_length=50,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))
    #nu_reg_comercio=forms.CharField(max_length=50,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white jtexto c50'}))
    tx_denom_comercial=forms.CharField(max_length=50,required=False,widget=forms.TextInput(attrs={'class': 'form-control text-white  jtexto c50'}))
    
    co_tipo_documento = forms.ModelChoiceField(queryset=MainTb006TipoDocumento.objects.all(), empty_label="-", to_field_name="co_tipo_documento",required=False, widget=forms.Select(attrs={'class': 'selectpicker','title':'Single Select', 'title':'Seleccione...','data-size': '7','data-style':'btn btn-primary',}))
    

    co_tipo_indentificacion = forms.ModelChoiceField(queryset=MainTb020TipoIndentificacion.objects.all(), empty_label="Seleccione...", to_field_name="co_tipo_indentificacion",required=False,widget=forms.Select(attrs={'class': 'selectpicker','title':'Single Select', 'title':'Seleccione...','data-size': '7','data-style':'btn btn-primary',}))
    #img_uno= forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'file-path validate text-white','type':'text','placeholder':'Solo acepta PNG y JPG','lang':'es'}))
    #img_dos= forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'custom-file-input text-white','type':'file','placeholder':'Solo acepta PNG y JPG','lang':'es'}))
    #img_selfie= forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'custom-file-input text-white','type':'file','placeholder':'Solo acepta PNG y JPG','lang':'es'}))
    

    #img_uno_file= forms.ImageField(required=False,widget=forms.TextInput(attrs={'type':'file'}))
    #img_dos_file= forms.ImageField(required=False,widget=forms.TextInput(attrs={'class': 'custom-file-input text-white','type':'file','lang':'es','accept':'image/*'}))
    #img_selfie_file= forms.ImageField(required=False,widget=forms.TextInput(attrs={'class': 'custom-file-input text-white','type':'file','lang':'es'}))
    

    img_uno_file= forms.ImageField(required=False)
    img_dos_file= forms.ImageField(required=False)
    img_selfie_file= forms.ImageField(required=False)

    def __init__(self, *args, **kwargs):
        
        self.user=kwargs.pop('usu') if 'usu' in kwargs else None
        super(EditarUsuario2FaForm, self).__init__(*args, **kwargs)
        """if self.tipo is True:
            self.fields['tx_denom_comercial'].required=False
            self.fields['nu_reg_comercio'].required=False
        else:
            self.fields['co_sexo'].required=False"""
        if self.user is not None:
            if self.user.co_persona is not None:
                if self.user.co_persona.co_direccion is not None:
                    if self.user.co_persona.co_direccion.co_ciudad is not None:
                        if self.user.co_persona.co_direccion.co_ciudad.co_estado is not None:
                            if self.user.co_persona.co_direccion.co_ciudad.co_estado.co_pais is not None:
                                self.fields['co_estado'].queryset = MainTb003Estado.objects.filter(co_pais = self.user.co_persona.co_direccion.co_ciudad.co_estado.co_pais.co_pais)
                                self.fields['co_ciudad'].queryset = MainTb004Ciudad.objects.filter(co_estado = self.user.co_persona.co_direccion.co_ciudad.co_estado.co_estado)



class registrarCuentasForm(forms.Form):
    co_moneda=forms.ModelChoiceField(queryset=WalletTb003Moneda.objects.all(), empty_label="Seleccione...", to_field_name="co_moneda",required=True,widget=forms.Select(attrs={'class':'form-control text-white'}))
    co_banco=forms.ModelChoiceField(queryset=MainTb015Banco.objects.all(), empty_label="Seleccione...", to_field_name="co_banco",required=True,widget=forms.Select(attrs={'class': 'form-control text-white'}))
    tx_cuenta=forms.CharField(max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control text-white'}))

    #co_proveedor_servicios=forms.ModelChoiceField(queryset=MainTb011ProveedorServicios.objects.all(), empty_label="Seleccione...", to_field_name="co_proveedor_servicios",required=True,widget=forms.Select(attrs={'class': 'selectpicker','data-size': '7','data-style':'btn btn-primary','title':'Seleccione...'}))


class dosfaCorreoForm(forms.Form):
    username = forms.CharField(min_length=4,max_length=50,required=True,widget=forms.TextInput(attrs={"class": "form-control text-white","placeholder":"Usuario"}))
    c1=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white'}))
    c2=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white'}))
    c3=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white'}))
    c4=forms.CharField(min_length=1,max_length=1,required=False,widget=forms.PasswordInput(attrs={'class': 'form-control text-white'}))


class editarContactosForm(forms.Form):
    tx_apodo_ed=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Apodo'}))
    tx_contacto_ed=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Usuario o Billetera','onfocus':'this.blur()'}))
    tx_memo_ed=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Memo(opcional)'}))

class agregarContactosForm(forms.Form):
    tx_apodo=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Apodo'}))
    tx_contacto=forms.CharField(required=True,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Usuario o Billetera'}))
    tx_memo=forms.CharField(required=False,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Memo(opcional)'}))

    def __init__(self, *args, **kwargs):
        
        self.user=kwargs.pop('usu') if 'usu' in kwargs else None
        super(agregarContactosForm, self).__init__(*args, **kwargs)

    def clean_tx_contacto(self):

        tx_contacto=self.cleaned_data['tx_contacto']
        directorio=MainTb022Directorio.objects.filter(co_usuario=self.user.co_usuario)

        if Tb001Usuario.objects.filter(username=tx_contacto).count()>0:
            usuario=Tb001Usuario.objects.filter(username=tx_contacto)[0]

            if usuario.co_usuario==self.user.co_usuario:
                raise forms.ValidationError("No puedes agregarte a ti mismo")

            if WalletTb037Keystore.objects.filter(co_usuario=usuario.co_usuario).count()>0:
                keystore=WalletTb037Keystore.objects.filter(co_usuario=usuario.co_usuario)[0]

                for value in directorio:
                    if keystore.co_keystore == value.co_keystore:
                        raise forms.ValidationError("Ya tienes esta billetera agregada")
            else:
                raise forms.ValidationError("El usuario no tiene una billetera activa")
        
        elif WalletTb037Keystore.objects.filter(address=tx_contacto).count()>0:
            keystore=WalletTb037Keystore.objects.filter(address=tx_contacto)[0]
            
            if keystore.getCuentaStellar() is None:
                raise forms.ValidationError("La cuenta no existe en stellar o el codigo no es una cuenta stellar valida")


            if keystore.co_usuario is not None:
                if keystore.co_usuario==self.user.co_usuario:
                    raise forms.ValidationError("No puedes agregarte a ti mismo")

            for value in directorio:
                if keystore.co_keystore == value.co_keystore.co_keystore:
                    raise forms.ValidationError("Ya tienes esta billetera agregada")
        
        else:
            try:
                server = Server(horizon_url="https://horizon.stellar.org")
                cuenta = server.accounts().account_id(tx_contacto)
                c=cuenta.call()

            except:
                raise forms.ValidationError("La cuenta no existe en stellar o el codigo no es una cuenta stellar valida")







      
    

        