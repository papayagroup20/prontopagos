# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.contrib.auth.models import (AbstractBaseUser,BaseUserManager,PermissionsMixin)
from uuid import uuid4
from datetime import date
import os
from private_storage.fields import PrivateFileField
from django.conf import settings
from stellar_sdk.server import Server
from django.core.validators import MinLengthValidator
from django.contrib.sessions.models import Session
from base64 import urlsafe_b64encode as b64e, urlsafe_b64decode as b64d
import secrets
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from django.utils.encoding import force_bytes

class EncryptedTextField(models.TextField):
    """
    A custom field for ensuring its data is always encrypted at the DB
    layer and only decrypted by this object when in memory.

    Uses Fernet (https://cryptography.io/en/latest/fernet/) encryption,
    which relies on Django's SECRET_KEY setting for generating
    cryptographically secure keys.
    """

    @staticmethod
    def get_key(secret, salt):
        return b64e(
            PBKDF2HMAC(
                algorithm=hashes.SHA256(),
                length=32,
                salt=salt,
                iterations=100000,
                backend=default_backend(),
            ).derive(secret)
        )

    @classmethod
    def decrypt(cls, value):
        from django.conf import settings

        decoded = b64d(value.encode())
        salt, encrypted_value = decoded[:16], b64e(decoded[16:])
        key = cls.get_key(force_bytes(settings.SECRET_KEY), salt)
        return Fernet(key).decrypt(encrypted_value).decode()

    @classmethod
    def encrypt(cls, value):
        from django.conf import settings

        salt = secrets.token_bytes(16)
        key = cls.get_key(force_bytes(settings.SECRET_KEY), salt)
        encrypted_value = b64d(Fernet(key).encrypt(value.encode()))
        return b64e(b"%b%b" % (salt, encrypted_value)).decode()

    def from_db_value(self, value, *args):
        if value is None:
            return value
        return self.decrypt(value)

    def get_db_prep_value(self, value, *args, **kwargs):
        if value is None:
            return value
        return self.encrypt(value)



class OtpTotpTotpdevice(models.Model):
    name = models.CharField(max_length=64)
    confirmed = models.BooleanField()
    key = models.CharField(max_length=80)
    step = models.SmallIntegerField()
    t0 = models.BigIntegerField()
    digits = models.SmallIntegerField()
    tolerance = models.SmallIntegerField()
    drift = models.SmallIntegerField()
    last_t = models.BigIntegerField()
    user = models.ForeignKey('Tb001Usuario', models.DO_NOTHING)
    throttling_failure_count = models.IntegerField()
    throttling_failure_timestamp = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'otp_totp_totpdevice'
        
class AccountEmailaddress(models.Model):
    email = models.CharField(unique=True, max_length=254)
    verified = models.BooleanField()
    primary = models.BooleanField()
    user = models.ForeignKey('Tb001Usuario', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'account_emailaddress'

class Tb009EmailConfirmation(models.Model):   
    co_email_confirmation = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    fe_expiracion = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    nu_token = models.CharField(blank=False, null=False,max_length=255)
    co_usuario = models.ForeignKey('Tb001Usuario', models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    class Meta:
        managed = True
        db_table = 'main_tb009_email_confirmation'

class CustomBaseUserManager(BaseUserManager):
    def create_user(self,username,password):
        user=self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self,username,password):
        user=self.create_user(username,password)
        user.is_staff=True
        user.is_superuser=True
        user.save(using=self._db)
        return user



class Tb001Usuario(AbstractBaseUser,PermissionsMixin):
    co_usuario = models.BigAutoField(primary_key=True)
    username = models.CharField(unique=True, max_length=255, blank=True, null=True,db_index=True)
    email = models.CharField(unique=True, max_length=255, blank=True, null=True)
    #tx_email = models.CharField(unique=True, max_length=255, blank=True, null=True)
    first_name=models.CharField(unique=False, max_length=255, blank=True, null=True)
    last_name=models.CharField(unique=False, max_length=255, blank=True, null=True)
    tx_usuario_referido = models.CharField(max_length=100, blank=True, null=True)
    
    #password = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    is_active = models.BooleanField(blank=True, null=True,default=True)
    is_staff= models.BooleanField(blank=True, null=True,default=False)
    in_confirmado= models.BooleanField(blank=True, null=True,default=False)
    co_persona = models.ForeignKey('MainTb005Persona', models.DO_NOTHING, db_column='co_persona', blank=True, null=True)
    co_indentificacion = models.ForeignKey('MainTb021Indentificacion', models.DO_NOTHING, db_column='co_indentificacion', blank=True, null=True,related_name='indentificacion')
    nu_telegram = models.IntegerField(blank=True, null=True,db_index=True,unique=True )
    #co_usuario_registro = models.ForeignKey('self', models.DO_NOTHING, db_column='co_usuario_registro', blank=True, null=True)
    co_estatus_img = models.ForeignKey('WalletTb009Estatus', models.DO_NOTHING, db_column='co_estatus_img', blank=True, null=True,default=4,related_name='estatus_img')
    co_estatus_persona = models.ForeignKey('WalletTb009Estatus', models.DO_NOTHING, db_column='co_estatus_persona', blank=True, null=True,default=4,related_name='estatus_persona')
    co_rol= models.ForeignKey('MainTb023Rol', models.DO_NOTHING, db_column='co_rol', blank=True, null=True)
    co_instituto= models.ForeignKey('MainTb024Instituto', models.DO_NOTHING, db_column='co_instituto', blank=True, null=True)
    in_2fa=models.BooleanField(blank=True, null=True,default=False)
    in_2fa_email=models.BooleanField(blank=True, null=True,default=False)
    in_ip=models.BooleanField(blank=True, null=True,default=False)
    co_federation=models.BigIntegerField(blank=True, null=True,unique=True)
    co_federation2=models.BigIntegerField(blank=True, null=True,unique=True)
    in_aceptar=models.BooleanField(blank=True, null=True,default=True)
    in_notificacion=models.BooleanField(blank=True, null=True,default=True)
    last_session_key= models.ForeignKey(Session, models.SET_NULL,to_field='session_key', db_column='last_session_key', blank=True, null=True)
    co_tasa_afiliados=models.ForeignKey('WalletTb048TasaAfiliado', models.DO_NOTHING, db_column='co_tasa_afiliados', blank=True, null=True)
    in_frase=models.BooleanField(default=False)
    token_telegram=models.CharField(unique=True, max_length=255, blank=True, null=True)
    co_usuario_crm = models.BigIntegerField(blank=True, null=True)

    USERNAME_FIELD='username'

    class Meta:
        managed = True
        db_table = 'main_tb001usuario'

    objects=CustomBaseUserManager()

    def get_full_name(self):
        return self.tx_usuario

    def get_short_name(self):
        return self.tx_usuario

    def getAgente(self):
        usuario=self.co_usuario
        agente=MainTb014AgenteServicios.objects.filter(co_usuario=usuario,in_activo=True)[0] if MainTb014AgenteServicios.objects.filter(co_usuario=usuario,in_activo=True).count()>0 else None
        return agente
    def checkAgente(self):
        usuario=self.co_usuario
        agente=True if MainTb014AgenteServicios.objects.filter(co_usuario=usuario,in_activo=True).count()>0 else False
        return agente
    def getPublic(self):
        usuario=self.co_usuario
        if WalletTb037Keystore.objects.filter(co_usuario=usuario).count()>0:
            billetera=WalletTb037Keystore.objects.get(co_usuario=usuario)
            if billetera.getCuentaStellar() is not None:
                return billetera.address
            else:
                return billetera.address
        else:
            return None

    def set_session_key(self, key):
        if self.last_session_key is not None:
            if self.last_session_key.session_key != key:
                self.last_session_key.delete()

        self.last_session_key = Session.objects.get(session_key=key)
        self.save()


class MainTb002Pais(models.Model):
    co_pais = models.BigAutoField(primary_key=True)
    tx_nacionalidad = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    tx_pais = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'main_tb002_pais'

    def __str__(self):
        return self.tx_pais

class MainTb003Estado(models.Model):
    co_estado = models.BigAutoField(primary_key=True)
    tx_estado = models.CharField(max_length=50, blank=True, null=True)
    co_pais = models.ForeignKey(MainTb002Pais, models.DO_NOTHING, db_column='co_pais', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb003_estado'

    def __str__(self):
        return self.tx_estado


class MainTb004Ciudad(models.Model):
    co_ciudad = models.BigAutoField(primary_key=True)
    tx_ciudad = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_estado = models.ForeignKey(MainTb003Estado, models.DO_NOTHING, db_column='co_estado', blank=True, null=True,related_name='ciudad')

    class Meta:
        managed = True
        db_table = 'main_tb004_ciudad'
    def __str__(self):
        return self.tx_ciudad


class MainTb005Persona(models.Model):
    co_persona = models.BigAutoField(primary_key=True)
    tx_primer_nombre = models.CharField(max_length=50, blank=True, null=True)
    tx_segundo_nombre = models.CharField(max_length=50, blank=True, null=True)
    tx_primer_apellido = models.CharField(max_length=50, blank=True, null=True)
    tx_segundo_apellido = models.CharField(max_length=50, blank=True, null=True)
    nu_documento = models.CharField(max_length=50,blank=True, null=True)
    nu_reg_comercio = models.CharField(max_length=50,blank=True, null=True)
    tx_denom_comercial=models.CharField(max_length=100, blank=True, null=True)
    co_tipo_documento = models.ForeignKey('MainTb006TipoDocumento', models.DO_NOTHING, db_column='co_tipo_documento', blank=True, null=True)
    co_sexo = models.ForeignKey('MainTb007Sexo', models.DO_NOTHING, db_column='co_sexo', blank=True, null=True)
    fe_nacimiento = models.DateField(blank=True, null=True)
    co_codigo_area=models.ForeignKey('MainTb034CodigoArea', models.DO_NOTHING, db_column='co_codigo_area', blank=True, null=True)
    nu_telefono = models.CharField(max_length=20, blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_direccion = models.ForeignKey('MainTb008Direccion', models.DO_NOTHING, db_column='co_direccion', blank=True, null=True)
    

    class Meta:
        managed = True
        db_table = 'main_tb005_persona'

    def get_nombre_completo(self):
        pn = str(self.tx_primer_nombre).lower().capitalize() if self.tx_primer_nombre else ''
        sn = str(self.tx_segundo_nombre).lower().capitalize() if self.tx_segundo_nombre else ''
        pa = str(self.tx_primer_apellido).lower().capitalize() if self.tx_primer_apellido else ''
        sa = str(self.tx_segundo_apellido).lower().capitalize() if self.tx_segundo_apellido else ''
        nombre=pn+" "+sn+" "+pa+" "+sa
        
        
        return nombre

    def get_nombre_corto(self):
        pn = str(self.tx_primer_nombre).lower().capitalize() if self.tx_primer_nombre else ''
        pa = str(self.tx_primer_apellido).lower().capitalize() if self.tx_primer_apellido else ''
        nombre=pn+" "+pa

        return nombre

class MainTb006TipoDocumento(models.Model):
    co_tipo_documento = models.BigAutoField(primary_key=True)
    co_pais = models.ForeignKey(MainTb002Pais, models.DO_NOTHING, db_column='co_pais', blank=True, null=True)
    tx_tipo_documento = models.CharField(max_length=50, blank=True, null=True)
    tx_abreviacion = models.CharField(max_length=20, blank=True, null=True)
    co_tipo_indentidad=models.ForeignKey('MainTb035TipoIndentidad', models.DO_NOTHING, db_column='co_tipo_indentidad', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb006_tipo_documento'

    def __str__(self):
        return self.tx_abreviacion
class MainTb007Sexo(models.Model):
    co_sexo = models.BigAutoField(primary_key=True)
    tx_sexo = models.CharField(max_length=30, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)

    class Meta:
        managed = True
        db_table = 'main_tb007_sexo'

    def __str__(self):
        return self.tx_sexo
        
class MainTb008Direccion(models.Model):
    co_direccion = models.BigAutoField(primary_key=True)
    tx_direccion = models.CharField(max_length=255, blank=True, null=True)
    co_ciudad = models.ForeignKey(MainTb004Ciudad, models.DO_NOTHING, db_column='co_ciudad', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    tx_codigo_postal = models.CharField(max_length=100, blank=True, null=True)
    co_pais = models.ForeignKey(MainTb002Pais, models.DO_NOTHING, db_column='co_pais', blank=True, null=True)
    tx_estado = models.CharField(max_length=100, blank=True, null=True)
    tx_ciudad = models.CharField(max_length=100, blank=True, null=True)
    
    
    
    def get_ciudad(self):
        ciudad=str(self.co_ciudad.co_estado.co_pais.tx_pais)+"/"+str(self.co_ciudad.co_estado.tx_estado)+"/"+str(self.co_ciudad.tx_ciudad)
        return ciudad
    class Meta:
        managed = True
        db_table = 'main_tb008_direccion'
class MainTb010TipoServicios(models.Model):
    co_tipo_servicios = models.BigAutoField(primary_key=True)
    tx_servicios = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb010_tipo_servicios'


class MainTb011ProveedorServicios(models.Model):
    co_proveedor_servicios = models.BigAutoField(primary_key=True)
    tx_proveedor_servicios = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    update_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb011_proveedor_servicios'

    def __str__(self):
        return self.tx_proveedor_servicios


class MainTb012Servicios(models.Model):
    co_servicios = models.BigAutoField(primary_key=True)
    co_tipo_servicios = models.ForeignKey(MainTb010TipoServicios, models.DO_NOTHING, db_column='co_tipo_servicios', blank=True, null=True,related_name='servicios_tpser')
    co_proveedor_servicios = models.ForeignKey(MainTb011ProveedorServicios, models.DO_NOTHING, db_column='co_proveedor_servicios', blank=True, null=True,related_name='serviciosp')
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    update_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb012_servicios'


class MainTb013RequisitosServicios(models.Model):
    co_requisitos_servicios = models.BigAutoField(primary_key=True)
    tx_requisitos_servicios = models.CharField(max_length=50, blank=True, null=True)
    co_servicios = models.ForeignKey(MainTb012Servicios, models.DO_NOTHING, db_column='co_servicios', blank=True, null=True,related_name='reqser_servicios')
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb013_requisitos_servicios'


class MainTb014AgenteServicios(models.Model):
    co_agente_servicios = models.BigAutoField(primary_key=True)
    
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb014_agente_servicios'

class MainTb015Banco(models.Model):
    co_banco = models.BigAutoField(primary_key=True)
    tx_banco = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)  
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb015_banco'

    def __str__(self):
        return self.tx_banco

class MainTb016BancoMoneda(models.Model):
    co_banco_moneda = models.BigAutoField(primary_key=True)
    co_banco = models.ForeignKey(MainTb015Banco, models.DO_NOTHING, db_column='co_banco', blank=True, null=True)
    #co_banco2 = models.ForeignKey('WalletTb007Banco', models.DO_NOTHING, db_column='co_banco2', blank=True, null=True)
    co_moneda = models.ForeignKey('WalletTb003Moneda', models.DO_NOTHING, db_column='co_moneda', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)  
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb016_banco_moneda'

    def __str__(self):
        return self.co_banco.tx_banco


class MainTb017Cuenta(models.Model):
    co_cuenta = models.BigAutoField(primary_key=True)
    tx_cuenta = models.CharField(max_length=255, blank=True, null=True)
    co_banco_moneda = models.ForeignKey(MainTb016BancoMoneda, models.DO_NOTHING, db_column='co_banco_moneda', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)  
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'main_tb017_cuenta'
    def __str__(self):
        cuenta = str(self.tx_cuenta).upper()
        return cuenta



class MainTb019ComisionBanco(models.Model):
    co_comision_banco = models.BigAutoField(primary_key=True)
    co_banco_moneda=models.ForeignKey(MainTb016BancoMoneda, models.DO_NOTHING, db_column='co_banco_moneda', blank=True, null=True)
    #co_tipo_comision=models.ForeignKey(MainTb018TipoComision, models.DO_NOTHING, db_column='co_tipo_comision', blank=True, null=True)
    co_tipo_transaccion = models.ForeignKey('WalletTb013TipoTransaccion', models.DO_NOTHING, db_column='co_tipo_transaccion', blank=True, null=True)
    
    nu_monto = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb019_comision_banco'

class MainTb020TipoIndentificacion(models.Model):
    co_tipo_indentificacion = models.BigAutoField(primary_key=True)
    tx_tipo_indentificacion = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb020_tipo_indentificacion'

    def __str__(self):
        return self.tx_tipo_indentificacion


class MainTb021Indentificacion(models.Model):

    def _generar_ruta_imagen1(self,filename):
        extension = os.path.splitext(filename)[1][1:]
        ruta = os.path.join('img/documentos/frente', date.today().strftime("%Y/%m"))
        nombre_archivo = '{}.{}'.format(uuid4().hex, extension)
        return os.path.join(ruta, nombre_archivo)
    def _generar_ruta_imagen2(self,filename):
        extension = os.path.splitext(filename)[1][1:]
        ruta = os.path.join('img/documentos/atras', date.today().strftime("%Y/%m"))
        nombre_archivo = '{}.{}'.format(uuid4().hex, extension)
        return os.path.join(ruta, nombre_archivo)
    def _generar_ruta_imagen3(self,filename):
        extension = os.path.splitext(filename)[1][1:]
        ruta = os.path.join('img/documentos/selfie', date.today().strftime("%Y/%m"))
        nombre_archivo = '{}.{}'.format(uuid4().hex, extension)
        return os.path.join(ruta, nombre_archivo)
    
    co_indentificacion = models.BigAutoField(primary_key=True)
    co_tipo_indentificacion=models.ForeignKey(MainTb020TipoIndentificacion, models.DO_NOTHING, db_column='co_tipo_indentificacion', blank=True, null=True)
    img_uno=PrivateFileField (upload_to = _generar_ruta_imagen1,content_types=['jpg','png'], blank=True, null=True)
    img_dos=PrivateFileField (upload_to = _generar_ruta_imagen2,content_types=['jpg','png'], blank=True, null=True)
    img_selfie=PrivateFileField (upload_to = _generar_ruta_imagen3,content_types=['jpg','png'], blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_estatus = models.ForeignKey('WalletTb009Estatus', models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)


    class Meta:
        managed = True
        db_table = 'main_tb021_indentificacion'

class MainTb022Directorio(models.Model):
    co_directorio = models.BigAutoField(primary_key=True)
    tx_apodo = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True,related_name='usuario')
    #co_usuario_guardado = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario_guardado', blank=True, null=True,related_name='usuario_guardado')
    #co_billetera=models.ForeignKey('WalletTb004Billetera', models.DO_NOTHING, db_column='co_billetera', blank=True, null=True,related_name='billetera')
    co_keystore=models.ForeignKey('WalletTb037Keystore', models.DO_NOTHING, db_column='co_keystore', blank=True, null=True,related_name='billetera')
    tx_memo = models.CharField(max_length=255, blank=True, null=True)
    co_memo=models.ForeignKey('MainTb046Memo', models.DO_NOTHING, db_column='co_memo', blank=True, null=True,related_name='directorio_memo')
    
    class Meta:
        managed = True
        db_table = 'main_tb022_directorio'

    
    def __str__(self):
        return self.tx_apodo

class MainTb023Rol(models.Model):
    co_rol = models.BigAutoField(primary_key=True)
    tx_rol = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True) 
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'main_tb023_rol'

    
    def __str__(self):
        return self.tx_rol

class MainTb024Instituto(models.Model):
    co_instituto = models.BigAutoField(primary_key=True)
    tx_instituto = models.CharField(max_length=50, blank=True, null=True)
    nu_padre=models.BigIntegerField(blank=True, null=True) 
    #nu_orden=models.IntegerField(blank=True, null=True) 
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True) 
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'main_tb024_instituto'

    
    def __str__(self):
        return self.tx_instituto
    
    def get_padre(self):
        codigo=self.nu_padre
        padre=MainTb024Instituto.objects.get(co_instituto=codigo)
        return padre



class MainTb025Proceso(models.Model):
    co_proceso = models.BigAutoField(primary_key=True)  
    tx_proceso = models.CharField(max_length=50, blank=True, null=True,unique=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    

    class Meta:
        managed = True
        db_table = 'main_tb025_proceso'
    
    
    def __str__(self):
        return self.tx_proceso

class MainTb026DetalleRuta(models.Model):
    co_detalle_ruta = models.BigAutoField(primary_key=True)
    co_tipo_solicitud= models.ForeignKey('MainTb027TipoSolicitud', models.DO_NOTHING, db_column='co_tipo_solicitud', blank=True, null=True,related_name='ruta')
    co_proceso= models.ForeignKey(MainTb025Proceso, models.DO_NOTHING, db_column='co_proceso', blank=True, null=True)
    co_instituto= models.ForeignKey(MainTb024Instituto, models.DO_NOTHING, db_column='co_instituto', blank=True, null=True)
    nu_orden=models.IntegerField(blank=True, null=True) 
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
     
    class Meta:
        managed = True
        db_table = 'main_tb026_detalle_ruta'

class MainTb027TipoSolicitud(models.Model):
    co_tipo_solicitud = models.BigAutoField(primary_key=True)
    #co_detalle_ruta= models.ForeignKey(MainTb026DetalleRuta, models.DO_NOTHING, db_column='co_detalle_ruta', blank=True, null=True)
    co_instituto= models.ForeignKey(MainTb024Instituto, models.DO_NOTHING, db_column='co_instituto', blank=True, null=True)
    tx_tipo_solicitud = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    in_concurrencia=models.BooleanField(blank=True, null=True,default=False)
    class Meta:
        managed = True
        db_table = 'main_tb027_tipo_solicitud'

    
    def __str__(self):
        return self.tx_tipo_solicitud

class SolicitudManager(models.Manager):
    def newSolicitud(self, co_tipo_solicitud,usuario):
        solicitud = self.create(co_tipo_solicitud=MainTb027TipoSolicitud.objects.get(co_tipo_solicitud=co_tipo_solicitud),
                                co_usuario=usuario,
                                co_estatus=WalletTb009Estatus.objects.get(co_estatus=1)
                                )
        return solicitud

    def unPaso(self,co_tipo_solicitud,usuario):
        solicitud=MainTb028Solicitud.objects.newSolicitud(co_tipo_solicitud,usuario)
        solicitud.save()

        solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=2)
        solicitud.in_activo=False
        solicitud.save()
        return solicitud

class MainTb028Solicitud(models.Model):
    co_solicitud = models.BigAutoField(primary_key=True)
    co_tipo_solicitud= models.ForeignKey(MainTb027TipoSolicitud, models.DO_NOTHING, db_column='co_tipo_solicitud', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True) 
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_estatus = models.ForeignKey('WalletTb009Estatus', models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)
    co_usuario2 = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario2', blank=True, null=True,related_name='usuario2')
    in_finanza = models.BooleanField(blank=True, null=True,default=False) 
    tx_motivo_rechazo=models.CharField(max_length=255, blank=True, null=True)
    objects=SolicitudManager()

    class Meta:
        managed = True
        db_table = 'main_tb028_solicitud'

    def get_incidencia(self):
        codigo=self.co_solicitud
        ruta=MainTb029Ruta.objects.filter(co_solicitud=codigo,in_activo=True)[0]
        formulario=MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None,co_aplicaciones=settings.CODIGO_APLICACION)[0] if MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=None,co_aplicaciones=settings.CODIGO_APLICACION).count()>0 else None 
        return formulario
    
    def cancelar(self):
        
        codigo=self.co_solicitud
        solicitud=MainTb028Solicitud.objects.get(co_solicitud=codigo)
        solicitud.in_activo=False
        solicitud.co_estatus=WalletTb009Estatus.objects.get(co_estatus=3)
        solicitud.save()

    def get_incidencia2(self):
        codigo=self.co_solicitud
        usuario=self.co_usuario2
        ruta=MainTb029Ruta.objects.filter(co_solicitud=codigo,in_activo=True)[0]
        formulario=MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=usuario.co_rol.co_rol,co_aplicaciones=settings.CODIGO_APLICACION)[0] if MainTb038SolicitudFormulario.objects.filter(co_detalle_ruta=ruta.co_detalle_ruta,co_rol=usuario.co_rol.co_rol,co_aplicaciones=settings.CODIGO_APLICACION).count()>0 else None 
        return formulario

class MainTb029Ruta(models.Model):
    co_ruta = models.BigAutoField(primary_key=True)
    co_solicitud= models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud', blank=True, null=True,related_name='rutas')
    co_detalle_ruta=models.ForeignKey(MainTb026DetalleRuta, models.DO_NOTHING, db_column='co_detalle_ruta', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True) 
    class Meta:
        managed = True
        db_table = 'main_tb029_ruta'

class MainTb030Menu(models.Model):
    co_menu = models.BigAutoField(primary_key=True)
    tx_menu = models.CharField(max_length=50, blank=True, null=True)
    tx_url = models.CharField(max_length=255, blank=True, null=True)
    nu_padre=models.BigIntegerField(blank=True, null=True) 
    nu_orden=models.IntegerField(blank=True, null=True) 
    nu_nivel=models.IntegerField(blank=True, null=True,default=0) 
    tx_pocision=models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True) 
    in_link = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True) 
    co_icono= models.ForeignKey('MainTb033Iconos', models.DO_NOTHING, db_column='co_icono', blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'main_tb030_menu'
    
    
    def __str__(self):
        return self.tx_menu

    def get_padre(self):
        codigo=self.nu_padre
        padre=MainTb030Menu.objects.get(co_menu=codigo)
        return padre


class MainTb031MenuRol(models.Model):
    co_menu_rol = models.BigAutoField(primary_key=True)
    tx_menu = models.CharField(max_length=50, blank=True, null=True)
    co_menu= models.ForeignKey(MainTb030Menu, models.DO_NOTHING, db_column='co_menu', blank=True, null=True)
    co_rol= models.ForeignKey(MainTb023Rol, models.DO_NOTHING, db_column='co_rol', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True) 
    
    class Meta:
        managed = True
        db_table = 'main_tb031_menu_rol'

class MainTb033Iconos(models.Model):
    co_icono = models.BigAutoField(primary_key=True)
    tx_icono = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb033_iconos'

    def __str__(self):
        return self.tx_icono

class MainTb034CodigoArea(models.Model):
    co_codigo_area = models.BigAutoField(primary_key=True)
    tx_codigo_area = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb034_codigo_area'

    def __str__(self):
        return self.tx_codigo_area

class MainTb035TipoIndentidad(models.Model):
    co_tipo_indentidad = models.BigAutoField(primary_key=True)
    tx_tipo_indentidad = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb035_tipo_indentidad'

    def __str__(self):
        return self.tx_tipo_indentidad

class MainTb036AgenteMetodoPago(models.Model):
    co_agente_metodo_pago = models.BigAutoField(primary_key=True)
    co_metodo_pago = models.ForeignKey('WalletTb025MetodoPago', models.DO_NOTHING, db_column='co_metodo_pago', blank=True, null=True)
    co_agente_servicios = models.ForeignKey(MainTb014AgenteServicios, models.DO_NOTHING, db_column='co_agente_servicios', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb036_agente_metodo_pago'

class MainTb037RolSolicitud(models.Model):
    co_rol_solicitud = models.BigAutoField(primary_key=True)
    co_tipo_solicitud= models.ForeignKey(MainTb027TipoSolicitud, models.DO_NOTHING, db_column='co_tipo_solicitud', blank=True, null=True)
    co_rol= models.ForeignKey(MainTb023Rol, models.DO_NOTHING, db_column='co_rol', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True) 
    
    class Meta:
        managed = True
        db_table = 'main_tb037_rol_solicitud'
    
    def __str__(self):
        return self.co_tipo_solicitud.tx_tipo_solicitud

class MainTb038SolicitudFormulario(models.Model):
    co_solicitud_formulario = models.BigAutoField(primary_key=True)
    co_detalle_ruta = models.ForeignKey(MainTb026DetalleRuta, models.DO_NOTHING, db_column='co_detalle_ruta', blank=True, null=True,related_name="formularioruta")
    co_aplicaciones = models.ForeignKey('MainTb038Aplicaciones', models.DO_NOTHING, db_column='co_aplicaciones', blank=True, null=True)
    tx_url_template = models.CharField(max_length=50, blank=True, null=True)
    tx_funcion = models.CharField(max_length=50, blank=True, null=True)
    tx_api_funcion = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_rol= models.ForeignKey(MainTb023Rol, models.DO_NOTHING, db_column='co_rol', blank=True, null=True)
    in_delete = models.BooleanField(blank=True, null=True,default=False)
    class Meta:
        managed = True
        db_table = 'main_tb038_solicitud_formulario'

class MainTb038Aplicaciones(models.Model):
    co_aplicaciones = models.BigAutoField(primary_key=True)
    tx_aplicaciones = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True) 
    
    class Meta:
        managed = True
        db_table = 'main_tb039_aplicaciones'
class MainTb040Mensajes(models.Model):
    co_mensajes = models.BigAutoField(primary_key=True)
    tx_mensajes = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    
    class Meta:
        managed = True
        db_table = 'main_tb040_mensajes'

class MainTb041AgenteDetalleServicios(models.Model):
    co_agente_detalle_servicios = models.BigAutoField(primary_key=True)
    co_servicios = models.ForeignKey(MainTb012Servicios, models.DO_NOTHING, db_column='co_servicios', blank=True, null=True)
    co_agente_servicios = models.ForeignKey(MainTb014AgenteServicios, models.DO_NOTHING, db_column='co_agente_servicios', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'main_tb041_agente_detalle_servicios'

class MainTb042RegistroIP(models.Model):
    co_registro_ip = models.BigAutoField(primary_key=True)
    tx_registro_ip = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    #tx_usuario_agente = models.CharField(max_length=150, blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    #tx_ciudad_pais = models.CharField(max_length=255, blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'main_tb042_registro_ip'

class MainTb043HistorialConexion(models.Model):
    co_historial_conexion = models.BigAutoField(primary_key=True)
    tx_registro_ip = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    tx_usuario_agente = models.CharField(max_length=150, blank=True, null=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    tx_ciudad_pais = models.CharField(max_length=255, blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'main_tb043_historial_conexion'

class MainTb045Referidos(models.Model):
    co_referidos = models.BigAutoField(primary_key=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario',related_name='usuario_afiliador')
    nu_telegram = models.IntegerField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    in_validado = models.BooleanField(default=False)
    
    
    class Meta:
        managed = True
        db_table = 'main_tb045_referidos'

class MainTb046Memo(models.Model):
    co_memo = models.BigAutoField(primary_key=True)
    tx_memo = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    class Meta:
        managed = True
        db_table = 'main_tb046_memo'

    
    def __str__(self):
        return self.tx_memo


class WalletTb001Plataforma(models.Model):
    co_plataforma = models.BigAutoField(primary_key=True)
    tx_plataforma = models.CharField(max_length=30, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb001_plataforma'


class WalletTb002TipoMoneda(models.Model):
    co_tipo_moneda = models.BigAutoField(primary_key=True)
    tx_tipo_moneda = models.CharField(max_length=20, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb002_tipo_moneda'
    def __str__(self):
        return self.tx_tipo_moneda

class WalletTb003Moneda(models.Model):
    co_moneda = models.BigAutoField(primary_key=True)
    tx_moneda = models.CharField(max_length=30, blank=True, null=True)
    tx_simbolo = models.CharField(max_length=10, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_tipo_moneda = models.ForeignKey(WalletTb002TipoMoneda, models.DO_NOTHING, db_column='co_tipo_moneda', blank=True, null=True)
    co_plataforma = models.ForeignKey(WalletTb001Plataforma, models.DO_NOTHING, db_column='co_plataforma', blank=True, null=True)
    co_pais = models.ForeignKey(MainTb002Pais, models.DO_NOTHING, db_column='co_pais', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb003_moneda'
    def __str__(self):
        return self.tx_simbolo


class WalletTb004Billetera(models.Model):
    co_billetera = models.BigAutoField(primary_key=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_plataforma = models.ForeignKey(WalletTb001Plataforma, models.DO_NOTHING, db_column='co_plataforma', blank=True, null=True)
    tx_public_key = models.CharField(max_length=255, blank=True, null=True)
    tx_private_key = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    in_frase = models.BooleanField(blank=True, null=True,default=True)
    co_solicitud=models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud', blank=True, null=True)


    class Meta:
        managed = True
        db_table = 'wallet_tb004_billetera'
    def __str__(self):
        billetera = str(self.tx_public_key).upper()
        return billetera

class WalletTb005PagoServicios(models.Model):
    co_pago_servicios = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True,related_name="pase_usuario")
    co_servicios = models.ForeignKey(MainTb012Servicios, models.DO_NOTHING, db_column='co_servicios', blank=True, null=True)
    co_estatus = models.ForeignKey('WalletTb009Estatus', models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)
    co_metodo_pago = models.ForeignKey('WalletTb025MetodoPago', models.DO_NOTHING, db_column='co_metodo_pago', blank=True, null=True)
    tx_codigo_validacion = models.CharField(max_length=255, blank=True, null=True)
    co_solicitud=models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud', blank=True, null=True)
    co_emisor = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_emisor', blank=True, null=True,related_name='pase_emisor')
    
    class Meta:
        managed = True
        db_table = 'wallet_tb005_pago_servicios'
    def get_hist_transaccion(self):
        codigo=self.co_pago_servicios
        hist=WalletTb020HistoricoPagoServicios.objects.filter(co_pago_servicios=codigo)[0]
        return hist


class WalletTb006DetallePagoServicios(models.Model):
    co_detalle_pago_servicios = models.BigAutoField(primary_key=True)
    co_pago_servicios = models.ForeignKey(WalletTb005PagoServicios, models.DO_NOTHING, db_column='co_pago_servicios', blank=True, null=True,related_name='requisitos')
    co_requisitos_servicios = models.ForeignKey(MainTb013RequisitosServicios, models.DO_NOTHING, db_column='co_requisitos_servicios', blank=True, null=True)
    tx_respuesta_requisito = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    update_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb006_detalle_pago_servicios'

class WalletTb007Banco(models.Model):
    co_banco = models.BigAutoField(primary_key=True)
    tx_banco = models.CharField(max_length=100, blank=True, null=True)
    tx_siglas = models.CharField(max_length=10, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    #co_moneda = models.ForeignKey(WalletTb003Moneda, models.DO_NOTHING, db_column='co_moneda', blank=True, null=True)
    co_activo=models.ForeignKey('WalletTb036Activos', models.DO_NOTHING, db_column='co_activo', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb007_banco'

    def __str__(self):
        banco = str(self.tx_banco).upper()
        return banco


class WalletTb008Cuenta(models.Model):
    co_cuenta = models.BigAutoField(primary_key=True)
    tx_cuenta = models.CharField(max_length=255, blank=True, null=True)
    #co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_banco = models.ForeignKey(WalletTb007Banco, models.DO_NOTHING, db_column='co_banco', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    in_staf = models.BooleanField(blank=True, null=True)
    nombre = models.CharField(max_length=50, blank=True, null=True)
    cedul = models.IntegerField(blank=True, null=True) 

    class Meta:
        managed = True
        db_table = 'wallet_tb008_cuenta'

    def __str__(self):
        cuenta = str(self.tx_cuenta).upper()
        return cuenta


class WalletTb009Estatus(models.Model):
    co_estatus = models.BigAutoField(primary_key=True)
    tx_estatus = models.CharField(max_length=30, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb009_estatus'
    
    def __str__(self):
        return self.tx_estatus

class WalletTb010Recarga(models.Model):
    co_recarga = models.BigAutoField(primary_key=True)
    tx_codigo = models.CharField(max_length=255, blank=True, null=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    #co_cuenta = models.ForeignKey(WalletTb008Cuenta, models.DO_NOTHING, db_column='co_cuenta', blank=True, null=True)
    #co_cuenta_emisor = models.ForeignKey(MaintTb017Cuenta, models.DO_NOTHING, db_column='co_cuenta_emisor', blank=True, null=True)
    tx_comentario = models.CharField(max_length=100, blank=True, null=True)
    co_banco = models.ForeignKey(WalletTb007Banco, models.DO_NOTHING, db_column='co_banco', blank=True, null=True)
   
    #nu_monto = models.DecimalField(max_digits=32, decimal_places=2, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    #co_moneda = models.ForeignKey(WalletTb003Moneda, models.DO_NOTHING, db_column='co_moneda', blank=True, null=True)
    co_transaccion = models.ForeignKey('WalletTb015Transaccion', models.DO_NOTHING, db_column='co_transaccion', blank=True, null=True)
    co_estatus = models.ForeignKey(WalletTb009Estatus, models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)
    co_solicitud=models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud', blank=True, null=True)
    co_metodo_pago=models.ForeignKey('WalletTb025MetodoPago', models.DO_NOTHING, db_column='co_metodo_pago', blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb010_recarga'

class WalletTb011HistoricoRecarga(models.Model):
    co_historico_recargabs = models.BigAutoField(primary_key=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_recarga = models.ForeignKey(WalletTb010Recarga, models.DO_NOTHING, db_column='co_recarga', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_estatus = models.ForeignKey(WalletTb009Estatus, models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb011_historico_recarga'

class WalletTb012BandejaServicios(models.Model):
    co_bandeja_servicios = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)#fecha de creacion del pedido
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)#fecha de moficicacion del pedido
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_pago_servicios = models.ForeignKey(WalletTb005PagoServicios, models.DO_NOTHING, db_column='co_pago_servicios', blank=True, null=True)
    co_estatus = models.ForeignKey(WalletTb009Estatus, models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb012_bandeja_servicios'

class WalletTb013TipoTransaccion(models.Model):
    co_tipo_transaccion = models.BigAutoField(primary_key=True)
    tx_tipo_transaccion = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb013_tipo_transaccion'
    def __str__(self):
        return self.tx_tipo_transaccion
        

class WalletTb014Comision(models.Model):
    co_comision = models.BigAutoField(primary_key=True)
    #co_tipo_comision=models.ForeignKey(MainTb018TipoComision, models.DO_NOTHING, db_column='co_tipo_comision', blank=True, null=True)
    nu_monto = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_tipo_transaccion = models.ForeignKey('WalletTb013TipoTransaccion', models.DO_NOTHING, db_column='co_tipo_transaccion', blank=True, null=True)
    is_porcentaje=models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'wallet_tb014_Comision'


class WalletTb015Transaccion(models.Model):
    co_transaccion = models.BigAutoField(primary_key=True)
    co_tipo_transaccion = models.ForeignKey(WalletTb013TipoTransaccion, models.DO_NOTHING, db_column='co_tipo_transaccion', blank=True, null=True)
    co_comision = models.ForeignKey(WalletTb014Comision, models.DO_NOTHING, db_column='co_comision', blank=True, null=True)
    #co_tasa_afiliados=models.ForeignKey('WalletTb048TasaAfiliado', models.DO_NOTHING, db_column='co_tasa_afiliados', blank=True, null=True)
    co_moneda=models.ForeignKey(WalletTb003Moneda, models.DO_NOTHING, db_column='co_moneda', blank=True, null=True)
    co_activo=models.ForeignKey('WalletTb036Activos', models.DO_NOTHING, db_column='co_activo', blank=True, null=True)
    nu_telegram = models.IntegerField(blank=True, null=True)
    tx_hash = models.CharField(max_length=255, blank=True, null=True)
    tx_hash_stellar = models.CharField(max_length=255, blank=True, null=True)
    nu_monto = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    nu_monto2 = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_receptor = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_receptor', blank=True, null=True,related_name='receptor')
    co_emisor = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_emisor', blank=True, null=True,related_name='emisor')
    co_transaccion_emisor = models.ForeignKey('WalletTb015Transaccion', models.DO_NOTHING, db_column='co_transaccion_emisor', blank=True, null=True,related_name="subtran_tran")
    co_estatus = models.ForeignKey(WalletTb009Estatus, models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)
    co_solicitud=models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud', blank=True, null=True)
    fe_transaccion=models.DateField(blank=True, null=True)
    class Meta:
        managed = True
        db_table = 'wallet_tb015_Transaccion'

    
    def __str__(self):
        transaccion = str(self.co_transaccion).upper()
        return transaccion

    def get_comision(self):
        codigo=self.co_transaccion
        transaccion=WalletTb015Transaccion.objects.get(co_transaccion_emisor=codigo)
        return transaccion

    def get_comisiones(self):
        codigo=self.co_transaccion
        transaccion=WalletTb015Transaccion.objects.filter(co_transaccion_emisor=codigo).all()
        return transaccion

class WalletTb016Oferta(models.Model):
    co_oferta = models.BigAutoField(primary_key=True)
    co_moneda_venta=models.ForeignKey(WalletTb003Moneda, models.DO_NOTHING, db_column='co_moneda_venta', blank=True, null=True,related_name='moneda_venta')
    co_moneda_compra=models.ForeignKey(WalletTb003Moneda, models.DO_NOTHING, db_column='co_moneda_compra', blank=True, null=True,related_name='moneda_compra')
    nu_monto = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    nu_cantidad = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)  
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)  
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_estatus = models.ForeignKey(WalletTb009Estatus, models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)
    nu_offerid = models.BigIntegerField(blank=True, null=True,default=True)
    co_tipo_oferta = models.ForeignKey('WalletTb024TipoOferta', models.DO_NOTHING, db_column='co_tipo_oferta', blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb016_oferta'
    
class WalletTb017AdmMoneda(models.Model):
    co_adm_moneda = models.BigAutoField(primary_key=True)
    co_moneda=models.ForeignKey(WalletTb003Moneda, models.DO_NOTHING, db_column='co_moneda', blank=True, null=True)
    nu_monto_minimo = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    nu_monto_maximo = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)  
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
 
    class Meta:
        managed = True
        db_table = 'wallet_tb017_adm_moneda'

class WalletTb018Retiro(models.Model):
    co_retiro = models.BigAutoField(primary_key=True)
    co_cuenta=models.ForeignKey(MainTb017Cuenta, models.DO_NOTHING, db_column='co_cuenta', blank=True, null=True,related_name='moneda_venta')
    #co_comision = models.ForeignKey(WalletTb014Comision, models.DO_NOTHING, db_column='co_comision', blank=True, null=True)
    #nu_monto = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)  
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_transaccion = models.ForeignKey(WalletTb015Transaccion, models.DO_NOTHING, db_column='co_transaccion', blank=True, null=True)
    co_estatus = models.ForeignKey(WalletTb009Estatus, models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)
    co_solicitud=models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud', blank=True, null=True)

 
    class Meta:
        managed = True
        db_table = 'wallet_tb018_retiro'
    
    def __str__(self):
        transaccion = str(self.co_transaccion).upper()
        return transaccion

    def get_retiros_transaccion(self):
        codigo=self.co_retiro
        hist=WalletTb018Retiro.objects.filter(co_retiro=codigo)[0]
        return hist


class WalletTb019Deposito(models.Model):
    co_deposito = models.AutoField(primary_key=True)
    nu_referencia = models.CharField(max_length=255, blank=True, null=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_moneda=models.ForeignKey(WalletTb003Moneda, models.DO_NOTHING, db_column='co_moneda', blank=True, null=True)
    in_procesado = models.BooleanField(blank=True, null=True,default=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_solicitud=models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud', blank=True, null=True)


    class Meta:
        managed = True
        db_table = 'wallet_tb019_deposito'

class WalletTb020HistoricoPagoServicios(models.Model):
    co_historico_pago_servicios = models.BigAutoField(primary_key=True)
    co_pago_servicios = models.ForeignKey(WalletTb005PagoServicios, models.DO_NOTHING, db_column='co_pago_servicios', blank=True, null=True,related_name='hispase_pase')
    co_transaccion = models.ForeignKey(WalletTb015Transaccion, models.DO_NOTHING, db_column='co_transaccion', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_estatus = models.ForeignKey(WalletTb009Estatus, models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)
   
    #time_limit = models.DateTimeField(blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb020_historico_pago_servicios'

class WalletTb021Planes(models.Model):
    co_plan = models.AutoField(primary_key=True)
    tx_plan = models.CharField(max_length=50)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    #co_activo = models.ForeignKey(WalletTb028Activos, models.DO_NOTHING, db_column='co_activo', blank=True, null=True)
    class Meta:
        managed = True
        db_table = 'wallet_tb021_planes'

class WalletTb022PlanesMoneda(models.Model):
    co_planes_moneda = models.AutoField(primary_key=True)
    co_moneda=models.ForeignKey(WalletTb003Moneda, models.DO_NOTHING, db_column='co_moneda', blank=True, null=True)
    co_plan = models.ForeignKey(WalletTb021Planes, models.DO_NOTHING, db_column='co_plan', blank=True, null=True)
    nu_monto = models.BigIntegerField(blank=True, null=True)
    nu_monto_retiro = models.BigIntegerField(blank=True, null=True)
    nu_monto_deposito = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb022_planes_moneda'

class WalletTb023PlanesUsuario(models.Model):
    co_planes_usuario = models.AutoField(primary_key=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_plan = models.ForeignKey(WalletTb021Planes, models.DO_NOTHING, db_column='co_plan', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb023_planes_usuario'

class WalletTb024TipoOferta(models.Model):
    co_tipo_oferta = models.AutoField(primary_key=True)
    tx_tipo_oferta = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb024_tipo_oferta'

class WalletTb025MetodoPago(models.Model):
    co_metodo_pago = models.BigAutoField(primary_key=True)
    tx_metodo_pago = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb025_metodo_pago'

    def __str__(self):
        return self.tx_metodo_pago
        
    def detalle(self):
        codigo=self.co_metodo_pago
        detalle=WalletTb026DetalleMetodoPago.objects.filter(co_metodo_pago=codigo)
        return detalle
class WalletTb026DetalleMetodoPago(models.Model):
    co_detalle_metodo_pago = models.BigAutoField(primary_key=True)
    co_tipo_servicios = models.ForeignKey(MainTb010TipoServicios, models.DO_NOTHING, db_column='co_tipo_servicios', blank=True, null=True)
    co_metodo_pago = models.ForeignKey(WalletTb025MetodoPago, models.DO_NOTHING, db_column='co_metodo_pago', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb026_detalle_metodo_pago'

class WalletTb027RetiroHistorico(models.Model):
    co_retiro_historico = models.BigAutoField(primary_key=True)
    co_retiro = models.ForeignKey(WalletTb018Retiro, models.DO_NOTHING, db_column='co_retiro', blank=True, null=True)
    #co_transaccion = models.ForeignKey(WalletTb015Transaccion, models.DO_NOTHING, db_column='co_transaccion', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    co_estatus = models.ForeignKey(WalletTb009Estatus, models.DO_NOTHING, db_column='co_estatus', blank=True, null=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    
    #time_limit = models.DateTimeField(blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb027_retiro_historico'


class WalletTb028Activos(models.Model):
    co_activo = models.BigAutoField(primary_key=True)
    tx_asset_code = models.CharField(max_length=12, blank=True, null=True)
    tx_asset_issuer = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    in_principal = models.BooleanField(blank=True, null=True,default=False)
    co_solicitud=models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud', blank=True, null=True)


    class Meta:
        managed = True
        db_table = 'wallet_tb028_activos'

class WalletTb029UsuarioActivos(models.Model):
    co_usuario_activos = models.BigAutoField(primary_key=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_activo = models.ForeignKey(WalletTb028Activos, models.DO_NOTHING, db_column='co_activo', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    in_confianza = models.BooleanField(blank=True, null=True,default=False)
   
    class Meta:
        managed = True
        db_table = 'wallet_tb029_usuario_activos'

    def getSolicitud(self):
        
        codigo=self.co_usuario_activos
        solicitudConfianza=WalletTb030SolicitudConfianza.objects.filter(
            co_usuario_activos=codigo,
            in_activo=True)[0] if  WalletTb030SolicitudConfianza.objects.filter(
            co_usuario_activos=codigo,
            in_activo=True).count()>0 else None
        
        return solicitudConfianza.co_solicitud if solicitudConfianza is not None else None


class WalletTb030SolicitudConfianza(models.Model):
    co_solicitud_confianza = models.BigAutoField(primary_key=True)
    co_usuario_activos = models.ForeignKey(WalletTb029UsuarioActivos, models.DO_NOTHING, db_column='co_usuario_activos', blank=True, null=True)
    co_solicitud=models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
   
   
    class Meta:
        managed = True
        db_table = 'wallet_tb030_solicitud_confianza'
    
class WalletTb031PreciosServicios(models.Model):
    co_precios_servicios = models.BigAutoField(primary_key=True)
    co_servicios = models.ForeignKey(MainTb012Servicios, models.DO_NOTHING, db_column='co_servicios', blank=True, null=True)
    nu_monto = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb031_precios_servicios'

class WalletTb032Anchors(models.Model):
    co_anchor = models.BigAutoField(primary_key=True)
    tx_anchor = models.CharField(max_length=100, blank=True, null=True)
    tx_url = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb032_anchors'


class WalletTb033ProcesoPlan(models.Model):
    co_proceso_plan = models.BigAutoField(primary_key=True)
    co_plan = models.ForeignKey(WalletTb021Planes, models.DO_NOTHING, db_column='co_plan', blank=True, null=True)
    co_proceso= models.ForeignKey(MainTb025Proceso, models.DO_NOTHING, db_column='co_proceso', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb033_proceso_plan'


class WalletTb034PeriodoPlan(models.Model):
    co_periodo_plan = models.BigAutoField(primary_key=True)
    co_proceso_plan = models.ForeignKey(WalletTb033ProcesoPlan, models.DO_NOTHING, db_column='co_proceso_plan', blank=True, null=True)
    nu_cantidad_dias=models.IntegerField(blank=True, null=True) 
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb034_periodo_plan'

class WalletTb035ConfiguracionPlan(models.Model):
    co_configuracion_plan = models.BigAutoField(primary_key=True)
    co_proceso_plan = models.ForeignKey(WalletTb033ProcesoPlan, models.DO_NOTHING, db_column='co_proceso_plan', blank=True, null=True)
    nu_cantidad_op=models.IntegerField(blank=True, null=True) 
    nu_monto_max = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    nu_monto_min = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb035_configuracion_plan'

class WalletTb036Activos(models.Model):
    co_activo = models.BigAutoField(db_index=True,primary_key=True)
    tx_asset_code = models.CharField(db_index=True,max_length=24, blank=True, null=True)
    tx_asset_issuer = models.CharField(db_index=True,max_length=255, blank=True, null=True)
    tx_alias = models.CharField(max_length=255, blank=True, null=True)
    tx_pool = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    toml_asset_desc=models.TextField(blank=True, null=True)
    toml_asset_conditions=models.TextField(blank=True, null=True)
    toml_name=models.CharField(max_length=255, blank=True, null=True)
    toml_img=models.CharField(max_length=255, blank=True, null=True)
    toml_asset_type=models.CharField(max_length=50, blank=True, null=True)
    co_stellar_toml= models.ForeignKey('WalletTb042StellarToml', models.DO_NOTHING, db_index=True, db_column='co_stellar_toml', blank=True, null=True) 
    co_cuenta_channel=models.ForeignKey('WalletTb043CuentaChannel', models.DO_NOTHING, db_column='co_cuenta_channel', blank=True, null=True)
    in_activo = models.BooleanField(db_index=True,default=True)
    in_live = models.BooleanField(db_index=True,default=True)
    in_principal = models.BooleanField(db_index=True,default=False)
    in_asset_ppg = models.BooleanField(db_index=True,default=False)
    in_dep_ret=models.BooleanField(db_index=True,default=False)
    nu_orden=models.IntegerField(db_index=True,blank=True, null=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb036_activos'
    
    def __str__(self):
        public =self.tx_asset_code+":"+self.tx_asset_issuer if self.tx_asset_issuer is not None else "XLM"
        return public
        
    @property
    def tx_asset(self):
        if self.tx_pool is None:
            tx_asset =self.tx_asset_code+":"+self.tx_asset_issuer if self.tx_asset_code!="XLM" else "XLM"
        else:
            tx_asset=self.tx_pool
        return tx_asset
        
class WalletTb037Keystore(models.Model):
    co_keystore = models.AutoField(primary_key=True)
    version = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    ciphertext = models.CharField(max_length=255, blank=True, null=True)
    nonce = models.CharField(max_length=255, blank=True, null=True)
    salt = models.CharField(max_length=255, blank=True, null=True)
    n = models.BigIntegerField(db_column='N', blank=True, null=True)  # Field name made lowercase.
    r = models.BigIntegerField(blank=True, null=True)
    p = models.BigIntegerField(blank=True, null=True)
    dklen = models.BigIntegerField(db_column='dkLen', blank=True, null=True)  # Field name made lowercase.
    encoding = models.CharField(max_length=255, blank=True, null=True)
    hash = models.CharField(max_length=255, blank=True, null=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True,related_name='keystore_usuario')
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    
    class Meta:
        managed = True
        db_table = 'keystore'

    def __str__(self):
        public = str(self.address).upper()
        return public
    
    def usuario(self):
        return self.co_usuario

        
    def getCuentaStellar(self):
        try:
            server = Server(horizon_url="https://horizon.stellar.org")
            cuenta = server.accounts().account_id(self.address)
            c=cuenta.call()
            return c
        except:
            return None

class WalletTb038MensajeStellar(models.Model):
    co_mensaje_stellar = models.AutoField(primary_key=True)
    co_tipo_error_stellar=models.ForeignKey('WalletTb054TipoErrorStellar', models.DO_NOTHING, db_column='co_tipo_error_stellar', blank=True, null=True)
    tx_codigo = models.CharField(max_length=255, blank=True, null=True)
    tx_mensaje_original = models.TextField(blank=True, null=True)
    tx_mensaje_traduccion = models.TextField( blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb038_mensaje_stellar'

    def __str__(self):
        return self.tx_codigo

class WalletTb039PrecioPPG(models.Model):
    co_precio_ppg = models.AutoField(primary_key=True)
    nu_precio =  models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb039_precio_ppg'

    def __str__(self):
        return str(self.nu_precio)

class WalletTb040Limite(models.Model):
    co_limite = models.AutoField(primary_key=True)
    nu_limite =  models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb040_limite'

    def __str__(self):
        return str(self.nu_limite)


class WalletTb041LimiteUsuario(models.Model):
    co_limite_usuario = models.AutoField(primary_key=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario', blank=True, null=True)
    co_limite= models.ForeignKey(WalletTb040Limite, models.DO_NOTHING, db_column='co_limite', blank=True, null=True)
    nu_cantidad = models.DecimalField(max_digits=32, decimal_places=5, blank=True, null=True,default=0)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb041_limite_usuario'

    def __str__(self):
        return str(self.nu_cantidad)

    
    def checkLimite(self,add:float)->bool:
        cantidadActual=self.nu_cantidad
        limite=self.co_limite.nu_limite
        total=cantidadActual+add
        if total>limite:
            return False
        else:
            return True

class WalletTb042StellarToml(models.Model):
    co_stellar_toml = models.AutoField(primary_key=True)
    tx_org_url=models.CharField(max_length=255, blank=True, null=True)
    tx_org_desc=models.TextField(blank=True, null=True)
    tx_uri_key=models.CharField(max_length=255, blank=True, null=True)
    tx_sep06_url=models.CharField(max_length=255, blank=True, null=True)
    tx_sep24_url=models.CharField(max_length=255, blank=True, null=True)
    tx_sep31_url=models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb042_stellar_toml'

    def __str__(self):
        return str(self.tx_org_url)

class WalletTb043CuentaChannel(models.Model):
    co_cuenta_channel = models.BigAutoField(primary_key=True)
    tx_cuenta_channel = models.CharField(max_length=56, validators=[MinLengthValidator(56)])
    #co_activo = models.ForeignKey(Asset, models.DO_NOTHING, db_column='co_activo',related_name="cuentachannel_activo")
    channel_seed = EncryptedTextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)
    in_activo = models.BooleanField(default=True)

    class Meta:
        managed = True
        ordering = ['created_at']
        db_table = 'wallet_tb043_cuenta_channel'
        verbose_name = "Cuenta Channel"
        verbose_name_plural = "Cuentas Channels"


class WalletTb044ParActivo(models.Model):
    co_par_activo = models.BigAutoField(primary_key=True)
    co_activo_uno=models.ForeignKey(WalletTb036Activos, models.DO_NOTHING, db_column='co_activo_uno',related_name='activo_uno')
    co_activo_dos=models.ForeignKey(WalletTb036Activos, models.DO_NOTHING, db_column='co_activo_dos',related_name='activo_dos')
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)
    in_activo = models.BooleanField(default=True)
    nu_orden=models.IntegerField()

    class Meta:
        managed = True
        ordering = ['nu_orden']
        db_table = 'wallet_tb044_pares_activos'
        verbose_name = "Par Activo"
        verbose_name_plural = "Pares Activos"

    @property
    def tx_par_activo(self):
        return self.co_activo_uno.tx_asset_code+"/"+self.co_activo_dos.tx_asset_code

    @property
    def tx_par_activo_invertido(self):
        return self.co_activo_dos.tx_asset_code+"/"+self.co_activo_uno.tx_asset_code


class WalletTb045AplicationSEP07(models.Model):
    co_aplicacion_sep07=models.BigAutoField(primary_key=True)
    co_stellar_toml= models.ForeignKey(WalletTb042StellarToml, models.DO_NOTHING, db_index=True, db_column='co_stellar_toml', blank=True, null=True) 
    tx_origin_domain=models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)
    in_activo = models.BooleanField(default=True)
    nu_reporte=models.IntegerField(default=0)

    class Meta:
        managed = True
        db_table = 'wallet_tb045_aplication_sep07'
        verbose_name = "Aplicacion sep07"
        verbose_name_plural = "Aplicaciones sep07"

class WalletTb046AplicationKeystore(models.Model):
    co_aplication_keystore=models.BigAutoField(primary_key=True)
    co_aplication_sep07=models.ForeignKey(WalletTb045AplicationSEP07, models.DO_NOTHING, db_index=True, db_column='co_aplication_sep07') 
    co_keystore=models.ForeignKey(WalletTb037Keystore, models.DO_NOTHING,db_index=True,  db_column='co_keystore')
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)
    in_activo = models.BooleanField(default=True)
    class Meta:
        managed = True
        db_table = 'wallet_tb046_aplication_keystore'


def _generar_ruta_imagen_paypal(self,filename:str):
        extension = os.path.splitext(filename)[1][1:]
        ruta = os.path.join('img/documentos/paypal', date.today().strftime("%Y/%m"))
        nombre_archivo = '{}.{}'.format(uuid4().hex, extension)
        return os.path.join(ruta, nombre_archivo)
   
class WalletTb047PaypalOperacion(models.Model):
    co_paypal_operacion=models.BigAutoField(primary_key=True)
    tx_nombres=models.CharField(max_length=255)
    tx_apellidos=models.CharField(max_length=255)
    tx_correo = models.CharField(max_length=255)
    tx_estatus = models.CharField(max_length=50)
    img_paypal = PrivateFileField(upload_to = _generar_ruta_imagen_paypal,content_types=['jpg','png'])
    co_solicitud= models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud',related_name="paypal_solicitud")
    co_transaccion = models.ForeignKey(WalletTb015Transaccion, models.DO_NOTHING, db_column='co_transaccion',related_name="paypal_transaccion")
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)
    in_activo = models.BooleanField(default=True)
    class Meta:
        managed = True
        db_table = 'wallet_tb047_paypal_operacion'


class WalletTb048TasaAfiliado(models.Model):
    co_tasa_afiliados=models.BigAutoField(primary_key=True)
    co_tipo_tasa_afiliados= models.ForeignKey('WalletTb049TipoTasaAfiliado', models.DO_NOTHING, db_column='co_tipo_tasa_afiliados',blank=True, null=True)
    nu_tasa=models.DecimalField(max_digits=32, decimal_places=5)
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)
    in_activo = models.BooleanField(default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb048_tasa_afiliado'


class WalletTb049TipoTasaAfiliado(models.Model):
    co_tipo_tasa_afiliados=models.BigAutoField(primary_key=True)
    tx_tipo_tasa=models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)
    in_activo = models.BooleanField(default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb049_tipo_tasa_afiliado'

class WalletTb050ComisionAfiliado(models.Model):
    co_comision_afiliado = models.BigAutoField(primary_key=True)
    co_comision = models.ForeignKey(WalletTb014Comision, models.DO_NOTHING, db_column='co_comision')
    tx_hash = models.CharField(max_length=255, blank=True, null=True)
    co_tipo_transaccion = models.ForeignKey(WalletTb013TipoTransaccion, models.DO_NOTHING, db_column='co_tipo_transaccion', blank=True, null=True)
    co_tasa_afiliados=models.ForeignKey(WalletTb048TasaAfiliado, models.DO_NOTHING, db_column='co_tasa_afiliados')
    co_factura=models.ForeignKey('WalletTb052FacturaReferido', models.DO_NOTHING, db_column='co_factura', blank=True, null=True,related_name="factura_comision")
    co_factura2=models.ForeignKey('WalletTb052FacturaReferido', models.DO_NOTHING, db_column='co_factura2', blank=True, null=True,related_name="factura_comision2")
    nu_monto = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    in_activo = models.BooleanField(default=True)
    #co_receptor = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_receptor',null=True,related_name="receptor_comision")
    #co_emisor = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_emisor',null=True,related_name="emisor_comision")
    
    class Meta:
        managed = True
        db_table = 'wallet_tb050_comision_afiliado'


class WalletTb051FacturacionTdc(models.Model):

    co_facturacion = models.BigAutoField(primary_key=True)
    tx_persona = models.CharField(max_length=255)
    tx_referencia_pago = models.CharField(max_length=255)
    tx_estatus = models.CharField(max_length=255)
    tx_concepto = models.CharField(max_length=255)
    co_tansaccion = models.ForeignKey(WalletTb015Transaccion, models.DO_NOTHING, db_column='co_transaccion',related_name="transaccion_tdc", null=True)
    co_solicitud= models.ForeignKey(MainTb028Solicitud, models.DO_NOTHING, db_column='co_solicitud',related_name="solicitud_tdc", null=True)
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)
    in_activo = models.BooleanField(default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb051_facturacionTdc'

    def __str__(self):
        return str(self.co_facturacion)

class WalletTb052FacturaReferido(models.Model):

    co_factura_referido = models.BigAutoField(primary_key=True)
    co_estatus = models.ForeignKey('WalletTb009Estatus', models.DO_NOTHING, db_column='co_estatus')
    co_pronto_amigo = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_pronto_amigo',related_name="feactura_amigo", null=True)
    nu_monto=models.DecimalField(max_digits=32, decimal_places=7,blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)
    in_activo = models.BooleanField(default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb052_factura_referido'

class WalletTb053IntegranteComision(models.Model):
    """
    El atributo in_emisor es true si es emisor false si es receptor
    """
    co_integrante_comision=models.BigAutoField(primary_key=True)
    co_comision_afiliado=models.ForeignKey(WalletTb050ComisionAfiliado, models.DO_NOTHING, db_column='co_comision_afiliado',related_name="integrante_comision")
    co_referido = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_referido',related_name="intecomi_usuario")
    in_emisor = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)
    
    class Meta:
        managed = True
        db_table = 'wallet_tb053_integrante_comision'


class WalletTb054TipoErrorStellar(models.Model):
    co_tipo_error_stellar=models.BigAutoField(primary_key=True)
    tx_tipo_error_stellar=models.CharField(max_length=100)
    in_operation = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb054_tipo_error_stellar'


class WalletTb055SolicitudPago(models.Model):
    co_solicitud_pago=models.BigAutoField(primary_key=True)
    co_usuario = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario',related_name='sogo_usuario')
    co_usuario_solicita = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario_solicita',related_name='sogo_usuta')
    nu_cantidad=models.DecimalField(max_digits=32, decimal_places=7)
    co_activo=models.ForeignKey(WalletTb036Activos, models.DO_NOTHING, db_column='co_activo', blank=True, null=True,related_name='sogo_activo')
    tx_hash=models.TextField(null=True)
    descripcion = models.TextField(null=True)
    in_activo_emisor = models.BooleanField(default=True)
    in_activo_receptor = models.BooleanField(default=True)
    in_asset_base = models.BooleanField(default=False)
    co_estatus = models.ForeignKey(WalletTb009Estatus, models.DO_NOTHING, db_column='co_estatus',default=1,related_name='sogo_estatus')
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb055_solicitud_pago'


class WalletTb056InfoMetodoPago(models.Model):
    co_info_metodo_pago = models.BigAutoField(primary_key=True)
    tx_titulo = models.CharField(max_length=50)
    tx_info = models.CharField(max_length=100)
    co_metodo_pago = models.ForeignKey(WalletTb025MetodoPago, models.DO_NOTHING, db_column='co_metodo_pago',related_name='inmepa_mepa')
    created_at = models.DateTimeField(blank=True, null=True,auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True,auto_now=True)
    in_activo = models.BooleanField(blank=True, null=True,default=True)

    class Meta:
        managed = True
        db_table = 'wallet_tb056_info_metodo_pago'

    def __str__(self):
        return self.tx_titulo+": "+self.tx_info
    
    
        

class Evento01Tb001RecompensaAfiliado(models.Model):
    co_recompensa_afiliado=models.BigAutoField(primary_key=True)
    tx_hash=models.CharField(max_length=250)
    #monto en BSS
    nu_monto=models.DecimalField(max_digits=32, decimal_places=5)
    tx_hash_recompensa=models.CharField(max_length=250)
    #monto en XLM
    nu_monto_recompensa=models.DecimalField(max_digits=32, decimal_places=5)
    co_usuario_referido = models.ForeignKey(Tb001Usuario, models.DO_NOTHING, db_column='co_usuario_referido',related_name="rausre_usuario", null=True)
    co_referido = models.OneToOneField(MainTb045Referidos, models.DO_NOTHING, db_column='co_referido',related_name="raref_referido", null=True)
    created_at = models.DateTimeField(auto_now_add=True)  
    updated_at = models.DateTimeField(auto_now=True)


    class Meta:
        managed = True
        db_table = 'evento01_tb001_recompensa_afiliado'