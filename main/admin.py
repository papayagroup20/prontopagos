from django.contrib import admin
from main.models import Tb001Usuario
from django.contrib.auth.admin import UserAdmin
# Register your models here.

class CustomUserAdmin(UserAdmin):
    fieldsets=()
    add_fieldsets=(
        (None,{
            'fields':('username','password1','tx_password2'),
            
        }),
    )
    list_display=('username','is_active','is_staff',)
    search_field=('username',)
    ordering=('username',)

admin.site.register(Tb001Usuario,CustomUserAdmin)