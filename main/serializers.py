from rest_framework import serializers
from main.models import Tb001Usuario,MainTb021Indentificacion

class IndentificacionSerializer(serializers.ModelSerializer):
    
    
    class Meta:
        model=MainTb021Indentificacion
        
        fields=['co_indentificacion','img_uno','img_dos','img_selfie']
        
class UsuarioPersonaSerializer(serializers.ModelSerializer):
    persona = serializers.ReadOnlyField(source='co_persona.co_persona')
    tx_primer_nombre=serializers.ReadOnlyField(source='co_persona.tx_primer_nombre')
    tx_segundo_nombre=serializers.ReadOnlyField(source='co_persona.tx_segundo_nombre')
    tx_primer_apellido=serializers.ReadOnlyField(source='co_persona.tx_primer_apellido')
    tx_segundo_apellido=serializers.ReadOnlyField(source='co_persona.tx_segundo_apellido')
    tx_tipo_documento=serializers.ReadOnlyField(source='co_persona.co_tipo_documento.tx_tipo_documento')
    tx_nombre=serializers.ReadOnlyField(source='co_persona.get_nombre_completo')
    tx_abreviacion=serializers.ReadOnlyField(source='co_persona.co_tipo_documento.tx_abreviacion')
    
    nu_documento=serializers.ReadOnlyField(source='co_persona.nu_documento')
    
    
    tx_sexo=serializers.ReadOnlyField(source='co_persona.co_sexo.tx_sexo')
    nu_telefono=serializers.ReadOnlyField(source='co_persona.nu_telefono')
    fe_nacimiento=serializers.ReadOnlyField(source='co_persona.fe_nacimiento')
    
    
    tx_ciudad=serializers.ReadOnlyField(source='co_persona.co_direccion.co_ciudad.tx_ciudad')
    tx_estado=serializers.ReadOnlyField(source='co_persona.co_direccion.co_ciudad.co_estado.tx_estado')
    tx_pais=serializers.ReadOnlyField(source='co_persona.co_direccion.co_ciudad.co_estado.co_pais.tx_pais')
    tx_codigo_postal=serializers.ReadOnlyField(source='co_persona.co_direccion.tx_codigo_postal')
    tx_direccion=serializers.ReadOnlyField(source='co_persona.co_direccion.tx_direccion')
 
    class Meta:
        model=Tb001Usuario
        fields=[
            'co_usuario',
            'username',
            'persona',
            'tx_nombre',
            'tx_primer_nombre',
            'tx_segundo_nombre',
            'tx_primer_apellido',
            'tx_segundo_apellido',
            'tx_tipo_documento',
            'nu_documento',
            'tx_abreviacion',
            'tx_sexo',
            'nu_telefono',
            'fe_nacimiento',
            'tx_direccion',
            'tx_codigo_postal',
            'tx_ciudad',
            'tx_estado',
            'tx_pais',
            'co_indentificacion'

        ]
    