//---------------------------------VALIDACIONES------------------

const url = window.location.protocol + "//" + window.location.host + "";
const error_default = "Ha ocurrido un error. Recargue la pagina";
/**
 *@param{string} msj 
 *@param{string} tipo 
 *@param{string} tipo 
 *@param{string} container 
 *@returns {null} 
 */
var newMensaje = function(msj, ida, tipo, container) {
    if ($("#" + ida).length == 0) {
        var tx1 = '<div class="alert alert-' + tipo + '" id="' + ida + '" >';
        var tx2 = '<button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">';
        var tx3 = '<i class="tim-icons icon-simple-remove"></i>';
        var tx4 = '</button>';
        var tx5 = '<span id="alertText">' + msj + '</span></div>';

        $("#" + container).append(tx1 + tx2 + tx3 + tx4 + tx5);
    }
}


/**
 *@param{string} clase 
 *@param{number} tipo 
 *@param{object=} error 
 *@returns {null} 
 */
function validarInputClase(clase, tipo, error) {
    $.each($("." + clase), function(index, value) {
        let id = $(value).attr("name");
        if (tipo == 1) {
            console.log("apiui");
            $("#id_" + id).closest('.form-group').removeClass('has-danger').removeClass('has-success');
            $("#id_" + id).closest('.form-check').removeClass('has-danger').removeClass('has-success');
            $("#error_" + id).text("");
        } else if (tipo == 2) {
            $("#id_" + id).closest('.form-group').removeClass('has-danger').addClass('has-success');
            $("#id_" + id).closest('.form-check').removeClass('has-danger').addClass('has-success');
            $("#error_" + id).text("");
        } else if (tipo == 3) {
            console.log("dos")
            if (error[id]) {
                $("#id_" + id).closest('.form-group').removeClass('has-success').addClass('has-danger');
                $("#id_" + id).closest('.form-check').removeClass('has-success').addClass('has-danger');
                $.each(error[id], function(index, val) {
                    $("#error_" + id).text(val);
                });
            } else if (Array.isArray(error)) {
                $("#id_" + id).closest('.form-group').removeClass('has-success').addClass('has-danger');
                $("#id_" + id).closest('.form-check').removeClass('has-success').addClass('has-danger');
                $("#error_" + id).text(error[0]);
            } else {
                $("#id_" + id).closest('.form-group').removeClass('has-danger').removeClass('has-success');
                $("#id_" + id).closest('.form-check').removeClass('has-danger').removeClass('has-success');
                $("#error_" + id).text("");
            }
        }
    });
}


/**
 *@param{string} id 
 *@param{number} tipo 
 *@param{object=} error 
 *@returns {null} 
 */
function validarInput(id, tipo, error) {
    if (tipo == 1) {
        $("#id_" + id).closest('.form-group').removeClass('has-danger').removeClass('has-success');
        $("#id_" + id).closest('.form-check').removeClass('has-danger').removeClass('has-success');
        $("#error_" + id).text("");
    } else if (tipo == 2) {
        $("#id_" + id).closest('.form-group').removeClass('has-danger').addClass('has-success');
        $("#id_" + id).closest('.form-check').removeClass('has-danger').addClass('has-success');
        $("#error_" + id).text("");
    } else if (tipo == 3) {
        if (error[id]) {
            $("#id_" + id).closest('.form-group').removeClass('has-success').addClass('has-danger');
            $("#id_" + id).closest('.form-check').removeClass('has-success').addClass('has-danger');
            $.each(error[id], function(index, val) {
                $("#error_" + id).text(val);
            });
        } else if (Array.isArray(error)) {
            $("#id_" + id).closest('.form-group').removeClass('has-success').addClass('has-danger');
            $("#id_" + id).closest('.form-check').removeClass('has-success').addClass('has-danger');
            $("#error_" + id).text(error[0]);
        } else {
            $("#id_" + id).closest('.form-group').removeClass('has-danger').removeClass('has-success');
            $("#id_" + id).closest('.form-check').removeClass('has-danger').removeClass('has-success');
            $("#error_" + id).text("");
        }
    }
}

/**
 *@param{object} error
 *@param{string} iderror
 *@param{string} iddiv
 *@param{string} inputclass
 *@returns {null}
 */
function erroresInputPorResponse(error, iderror, iddiv, inputclass) {
    if (error.responseJSON) {
        if (error.responseJSON.detail) {
            newMensaje(error.responseJSON.detail, iderror, "danger", iddiv);
            validarInput(inputclass, 1);
        } else {
            validarInput(inputclass, 3, error.responseJSON);
        }
    } else {
        newMensaje("Ha ocurrido un error", iderror, "danger", iddiv);
    }

}

/**
 *@param{object} error
 *@param{string} iddiv
 *@returns {null}
 */
function erroresInputResponse(error, iddiv) {
    let i = 0;
    $.each(error, function(index, value) {

        if (Array.isArray(value)) {
            validarInput(index, 3, value);
        } else {
            //validarInput(index, 1);
            newMensaje(value, "error_" + index + i, "danger", iddiv);
        }
        i++;
    });
}



/**
 * obtener mensaje de stellar
 * @param {string} code codigo del mensaje de error
 * @param {object[]} lista listado de los mensajes de error
 * @returns el mensaje de error correspodiente a la coincidencia del codigo y la lista
 * @param {number|string} tipo El codigo del tipo de error es segun la operacion
 */
var getMsjStellar = function(code, lista, tipo = false) {
        var tx = code
        $.each(lista, function(idx, value) {
            if (code == value.tx_codigo && !tipo) {
                tx = value.tx_mensaje_traduccion;
            } else if (code == value.tx_codigo && tipo == value.co_tipo) {
                tx = value.tx_mensaje_traduccion;
            }
        });

        return tx;

    }
    /**
     * lista mensaje stellar
     * @param {string[]} lista lista de errores de stellar
     * @param {object[]} listm lista de errores de ppg
     * @param {string} clase la clase que llevara el contenedor html del mensaje
     * @param {string} name el id/nombre que llevara el contenedor html del mensaje
     * @param {number|string} tipo El codigo del tipo de error es segun la operacion
     */
var ListaMsjStellar = function(lista, listm, clase, name, tipo = false) {

    var j = 1;
    $.each(lista, function(idx, value) {
        var ide = "r" + j;
        var ms = getMsjStellar(value, listm, tipo = tipo);
        newMensaje(ms, ide, clase, name);
        j++;
    });


}

/**
 * Imprimir el mensaje de error stellar
 * @param {object} res Objecto de respuesta de stellar
 * @param {object[]} listm lista de mensaje de ppg
 * @param {string} clase la clase que llevara el contenedor html del mensaje
 * @param {string} name El id/nombre que llevara el contenedor html del mensaje
 * @param {number|string} tipo El codigo del tipo de error es segun la operacion
 */
var printMsjStellar = function(res, listm, clase, name, tipo = false) {
    if (res.data.extras) {

        /*if (res.data.extras.result_codes.transaction) {
            var detalle = res.data.extras.result_codes.transaction;
            newMensaje(getMsjStellar(detalle, listm), "trans-error", clase, name);

        }*/
        if (res.data.extras.result_codes.operations) {
            var listError = res.data.extras.result_codes.operations;
            ListaMsjStellar(listError, listm, clase, name, tipo = tipo);
        }


    } else {
        var detalle = res.data.detail

        newMensaje(getMsjStellar(detalle, listm), "errorste", clase, name);

    }








}

function setFormValidation(id) {
    $(id).validate({
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
            $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
            $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
        },
        errorPlacement: function(error, element) {
            $(element).closest('.form-group').append(error);
        },
    });
}

/**
 * formateo de keystore
 * @param keystoreDB es el keystore formato db
 * @returns keystore formateado
 */
function formatKeystore(keystoreDB) {
    const keystoreformateado = {
        'address': keystoreDB.address,
        'crypto': {
            'ciphertext': keystoreDB.ciphertext,
            'nonce': keystoreDB.nonce,
            'salt': keystoreDB.salt,
            'scryptOptions': {
                'N': keystoreDB.n,
                'dkLen': keystoreDB.dklen,
                'encoding': keystoreDB.encoding,
                'p': keystoreDB.p,
                'r': keystoreDB.r,
            }
        },
        'version': keystoreDB.version
    }
    return keystoreformateado;
}