def allow_owner_user(private_file):
        request = private_file.request
        storage = private_file.storage
        relative_name=private_file.relative_name
        verdad=False
        if request.user.is_authenticated and request.user.in_confirmado and request.user.co_indentificacion is not None:
            img1=request.user.co_indentificacion.img_uno
            img2=request.user.co_indentificacion.img_dos
            img3=request.user.co_indentificacion.img_selfie
            if img1 is not None and relative_name == img1:
                verdad=True
            elif img2 is not None and relative_name == img2:
                verdad=True
            elif img3 is not None and relative_name == img3:
                verdad=True
            
        elif request.user.is_authenticated and request.user.is_staff or request.user.is_superuser: 
            verdad=True
        
        return verdad