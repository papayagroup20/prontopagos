"""
Django settings for papaya_pago project.

Generated by 'django-admin startproject' using Django 3.0.3.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

import os
import sys
import environ
from firebase_admin import initialize_app
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_IMG = 'C:/prontopagoimg'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'nk0dhf-uc)m19o4wjl0dhrj3e7itgp=((*@8$@$7lf4t4%kxyk'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
SECURE_SSL_REDIRECT=True

SESSION_COOKIE_SECURE = True

CSRF_COOKIE_SECURE = True

SECURE_BROWSER_XSS_FILTER = True

#host para local
#ALLOWED_HOSTS = ["prontopago.gobelx.io","prontopagos.gobelx.io","localhost","localhost:8000","192.168.0.192","127.0.0.1:3512", "192.168.1.100"]


#host para produccion
ALLOWED_HOSTS = ["app.prontopagos.io","prontopagos.io","www.prontopagos.io","prontopagos.gobelx.io","pay.prontopagos.io","dev.prontopagos.io","pay.gobelx.io","anchor.gobelx.io"]


"""SSL_CONTEXT=False
SECURE_HSTS_SECONDS = 0
CSRF_COOKIE_NAME = 'csrftoken'
CSRF_COOKIE_SECURE = False
SESSION_COOKIE_SECURE = False"""

env=environ.Env()
if os.path.exists(os.path.join(BASE_DIR,".env")):
    env.read_env(os.path.join(BASE_DIR,".env"))

"""try:
    if "runsslserver" in sys.argv:
        SSL_CONTEXT = True
        SECURE_HSTS_SECONDS = 3600
    

CSRF_COOKIE_SECURE = SSL_CONTEXT
SECURE_SSL_REDIRECT = SSL_CONTEXT
SESSION_COOKIE_SECURE = SSL_CONTEXT
CSP_UPGRADE_INSECURE_REQUESTS = SSL_CONTEXT
"""

FIREBASE_APP = initialize_app()

CODIGO_APLICACION=1 

IPWARE_META_PRECEDENCE_ORDER = (
     'HTTP_X_FORWARDED_FOR', 'X_FORWARDED_FOR',  # <client>, <proxy1>, <proxy2>
     'HTTP_CLIENT_IP',
     'HTTP_X_REAL_IP',
     'HTTP_X_FORWARDED',
     'HTTP_X_CLUSTER_CLIENT_IP',
     'HTTP_FORWARDED_FOR',
     'HTTP_FORWARDED',
     'HTTP_VIA',
     'REMOTE_ADDR',
 )

RECAPTCHA_SECRET_KEY = '6Lc_Cu8ZAAAAALtUDe5rEFaHEXP3hv7inPl0tmgU'





# Application definition

INSTALLED_APPS = [
    #'material.admin',
    'django.contrib.admin',
    'django_extensions',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'jquery',
    'jquery_ui',
    'django_static_jquery_ui',
    'django.contrib.sites',
    #'material',
    #'materializeform',
    'rest_framework',
    'rest_framework.authtoken',
    "qr_code",
    #'apipago',
    'main',
    'main.templatetags',
    'stellar_sdk',
    'corsheaders',
    'private_storage',
    'django_user_agents',
    #"sslserver",
    


    #2fa
    'django_otp',
    'django_otp.plugins.otp_static',
    'django_otp.plugins.otp_hotp',
    'django_otp.plugins.otp_totp',
    
    'fcm_django',

    'pwa',

    #ALLAUTH
    #'allauth',
    #'allauth.account',
    #'allauth.socialaccount',

    #providers
    #'allauth.socialaccount.providers.facebook',
    #'allauth.socialaccount.providers.google',
]

FCM_DJANGO_SETTINGS = {
     # default: _('FCM Django')
    "APP_VERBOSE_NAME": "[Prontopagos]",
     # true if you want to have only one active device per registered user at a time
     # default: False
    "ONE_DEVICE_PER_USER": False,
     # devices to which notifications cannot be sent,
     # are deleted upon receiving error response from FCM
     # default: False
    "DELETE_INACTIVE_DEVICES": False,
    # Transform create of an existing Device (based on registration id) into
                # an update. See the section
    # "Update of device with duplicate registration ID" for more details.
    "UPDATE_ON_DUPLICATE_REG_ID": True,
}

AUTHENTICATION_BACKENDS=(
    'papaya_pago.CustomAuth.StellarBackend',
    'django.contrib.auth.backends.ModelBackend',
    #'allauth.account.auth_backends.AuthenticationBackend',
)
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django_session_timeout.middleware.SessionTimeoutMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'ipaddr.middleware.IPAddrMiddleware',
    'geoip2_extras.middleware.GeoIP2Middleware',
    'django_user_agents.middleware.UserAgentMiddleware',
    #'csp.middleware.CSPMiddleware',
]

"""CSP_DEFAULT_SRC = ("'none'",'www.google.com')
CSP_STYLE_SRC = ("'self'","'unsafe-inline'",'www.google.com',
                'fonts.googleapis.com','use.fontawesome.com',
                'www.gstatic.com','cdnjs.cloudflare.com')
CSP_SCRIPT_SRC = ("'self'","'unsafe-inline'","'unsafe-eval'",'cdnjs.cloudflare.com','localhost:8000','www.google.com','maps.googleapis.com','fonts.gstatic.com','www.gstatic.com')

CSP_FONT_SRC = ("'self'","'unsafe-inline'","'unsafe-eval'",'www.google.com',
                'fonts.googleapis.com','use.fontawesome.com','fonts.gstatic.com','www.gstatic.com')
CSP_IMG_SRC = ("'self'",'www.google.com')
CSP_CONNECT_SRC =("'self'","'unsafe-inline'","'unsafe-eval'",'www.google.com')
CSP_EXCLUDE_URL_PREFIXES=("'self'")
CSP_INCLUDE_NONCE_IN=("'script-src'")"""

SESSION_EXPIRE_SECONDS = 9999999
SESSION_EXPIRE_AFTER_LAST_ACTIVITY = True
SESSION_EXPIRE_AFTER_LAST_ACTIVITY_GRACE_PERIOD = 99999999

ROOT_URLCONF = 'papaya_pago.urls'

#CORS_ALLOW_ALL_ORIGINS=True

CORS_URLS_REGEX = r'^/api/.*$'

CORS_ORIGIN_WHITELIST= (

    #"http://localhost:8080",
    #"https://127.0.0.1:8000",
    #"https://localhost:8000",
    "https://pay.gobelx.io",
    "https://pay.prontopagos.io",
    "https://anchor.gobelx.io",
    "https://dev.prontopagos.io",
    "http://pay.prontopagos.io",
    "http://gobelx.io",
    #"http://186.167.242.142"
    )


if DEBUG:
    INSTALLED_APPS += ['debug_toolbar',]
    MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware',]
    INTERNAL_IPS = [
        '127.0.0.1',
    ]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],
        },
    },
]
MATERIAL_ADMIN_SITE = 'mymodule.admin.admin_site'
WSGI_APPLICATION = 'papaya_pago.wsgi.application'

SESSION_ENGINE = "django.contrib.sessions.backends.db"
# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases
"""     'NAME': 'papaya',
        'USER': 'postgres',
        'PASSWORD': '123456',
        'HOST': 'localhost',

        'NAME': 'papayaprueba',
        'USER': 'postgres',
        'PASSWORD': '123456',
        'HOST': '190.124.30.68',

        'NAME': 'prontopago2',
        'USER': 'postgres',
        'PASSWORD': 'gobelxgv96',
        'HOST': '107.152.37.161',

        'NAME': 'prontopagos',
        'USER': 'gobelx',
        'PASSWORD': 'gosdbenfiaulx33',
        'HOST': '107.152.32.51',
        
        
        """
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'prontopagos',
        'USER': 'gobelx',
        'PASSWORD': 'gosdbenfiaulx33',
        'HOST': '107.152.41.158',
    }
}
#copy
SOCIAL_AUTH_GOOGLE_OATH2_KEY='Client ID'
SOCIAL_AUTH_GOOGLE_OATH2_KEY_SECRET='Client Secret'

SOCIAL_AUTH_KEY='ID'
SOCIAL_AUTH_SECRET='SECRET'
# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'America/Caracas'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/
OTP_TOTP_ISSUER="prontopagos.io"

LOGIN_URL = 'login'


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")

STATICFILES_DIRS = [
    os.path.join(BASE_DIR,'static','img'),
    #('node_modules', os.path.join(BASE_DIR, 'node_modules/')),
]

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'static', 'media')


PRIVATE_STORAGE_ROOT  =  os.path.join(BASE_DIR, 'static', 'mediap')
PRIVATE_STORAGE_AUTH_FUNCTION  =  'papaya_pago.CustomPrivateStorage.allow_owner_user'
#PRIVATE_STORAGE_AUTH_FUNCTION  =  'private_storage.permissions.allow_owner_user'

AUTH_USER_MODEL='main.tb001usuario'

ACCOUNT_USER_MODEL_USERNAME_FIELD = 'username'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_VERIFICATION = 'mandatory' 

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
#EMAIL_USE_TLS = True
EMAIL_USE_SSL=True
#EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_HOST = 'mail.prontopagos.com'
EMAIL_HOST = 'mail.gobelx.com'
#EMAIL_HOST_USER = 'papayagroup20@gmail.com'
# EMAIL_HOST_USER = 'noreply@prontopagos.com'
EMAIL_HOST_USER = 'noreply@gobelx.com'
#EMAIL_HOST_PASSWORD = 'noreply@prontopago.com'
# EMAIL_HOST_PASSWORD = 'noreply2020***'
EMAIL_HOST_PASSWORD = 'gobelx2022***'
#EMAIL_PORT = 587
EMAIL_PORT = 465
#EMAIL_PORT = 995
SITE_ID=1

LOGIN_REDIRECT_URL='/inicio'
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        #'apipago.authentication.ExpiringTokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated', ),
    #'DATETIME_INPUT_FORMATS':[("%d-%B-%Y-%I-%p")],
    'DATETIME_FORMAT':("%d/%m/%Y %I:%M:%S %p"),
}
TOKEN_EXPIRED_AFTER_SECONDS=604800

SOCIALACCOUNT_PROVIDERS = {
    'google': {
        # For each OAuth based provider, either add a ``SocialApp``
        # (``socialaccount`` app) containing the required client
        # credentials, or list them here:
        'APP': {
            'client_id': '827264116214-skjns2v8hqsjjm2og6fdomp8egpj4906.apps.googleusercontent.com',
            'secret': 'J6C8noua3OF2b9Mo-VysXj2W',
            'key': ''
        }
    }
}

LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "verbose": {"format": "{asctime} - {levelname}: {message}", "style": "{",},
    },
    "handlers": {
        'logfile': {
            'class': 'logging.FileHandler',
            'filename': 'server.log',
        },
    },
    "loggers": {
        "django": {"handlers": ["logfile"]},
    },
}


#----------PWA CONFIGURACION----------
PWA_APP_NAME = 'Prontopagos'
PWA_APP_DESCRIPTION = "Billetera stellar desentralizada papaya"
PWA_APP_THEME_COLOR = '#f9a825'
PWA_APP_BACKGROUND_COLOR = '#212121'
PWA_APP_DISPLAY = 'standalone'
PWA_APP_SCOPE = '/'
PWA_APP_ORIENTATION = 'any'
PWA_APP_START_URL = '/'
PWA_APP_STATUS_BAR_COLOR = 'default'

PWA_APP_ICONS = [
    {
        'src': '/static/img/android-chrome-160x160.png',
        'sizes': '160x160'
    }
]

PWA_APP_ICONS_APPLE = [
    {
        'src': '/static/img/android-chrome-160x160.png',
        'sizes': '160x160'
    }
]
PWA_APP_SPLASH_SCREEN = [
    {
        'src': '/static/img/android-chrome-512x512.png',
        'media': '(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)'
    }
]


PWA_APP_DIR = 'ltr'
PWA_APP_LANG = 'es-Es'

PWA_SERVICE_WORKER_PATH = os.path.join(BASE_DIR,'serviceworker.js')


