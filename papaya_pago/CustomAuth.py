#from django.contrib.auth.backends import BaseBackend
from main.models import *
import requests as peticion
from hashlib import sha256
from django.db.models import Prefetch
from rest_framework.authtoken.models import Token
from rest_framework.permissions import BasePermission
#from wallet.views.general import getHeaderFederacion

class InConfirmado(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and request.user.in_confirmado)
    
class StellarBackend:
    """
    Authenticate against the settings ADMIN_LOGIN and ADMIN_PASSWORD.

    Use the login name and a hash of the password. For example:

    ADMIN_LOGIN = 'admin'
    ADMIN_PASSWORD = 'pbkdf2_sha256$30000$Vo0VlMnkR4Bk$qEvtdyZRWTcOsCnI/oQ7fVOu1XAURIZYoOZ3iq8Dr4M='
    """

    def authenticate(self, request, user:Tb001Usuario, password=None):
        keystore=user.keystore_usuario.first()
        try:
            token=user.auth_token
            token.delete()
        except:
            pass
        request.session["public_key"]=keystore.address

        return user
        
        
    def get_user(self, user_id):
        try:
            usuario=Tb001Usuario.objects.\
            select_related("auth_token").\
            get(pk=user_id)
            
            return usuario
        except Tb001Usuario.DoesNotExist:
            return None