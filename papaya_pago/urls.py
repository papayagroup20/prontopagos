"""papaya_pago URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from main.views.prontoamigo import pronto_amigo
from django.contrib import admin
from django.urls import include,path,re_path
from main.views import *
from apipago.views import *
from wallet.views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns,static
from django.conf import settings
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from django.views.generic.base import RedirectView
from django.contrib.staticfiles.storage import staticfiles_storage
from fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet
import private_storage.urls
#comentar antes de subir al servidor

from main.views.reportes import *
router = DefaultRouter()

#router.register(r'estado', getEstado, basename='estado')
router.register(r'paypal', PaypalOperacionViewSet, basename='paypal')
router.register(r'directorio', DirectorioViewSet, basename='directorio')
router.register(r'solicitud-pago', SolicitudPagoViewSet, basename='solicitud-pago')
router.register(r'registro', RegistroViewSet, basename='registro')
router.register(r'login', LoginViewSet, basename='login')
router.register(r'auth', AuthPPGPay, basename='auth')
router.register(r'clave', claveViewSet, basename='clave')
router.register(r'device', DeviceViewSet, basename='device')
router.register(r'validation-2fa', Validation2FAViewSet, basename='validation-2fa')
router.register(r'msg-stellar', MensajeStellarViewSet, basename='msg-stellar')
router.register(r'precios', PreciosViewSet, basename='precios')
router.register(r'stellar-ppg', StellarPPGViewSet, basename='stellar-ppg')
router.register(r'compra-xlm', CompraXLMViewSet, basename='compra-xlm')
router.register(r'usuario', UsuarioViewSet, basename='usuario')
router.register(r'servicios', ServiciosViewSet, basename='servicios')

router.register('devices', FCMDeviceAuthorizedViewSet)

urlpatterns = [
    path(
        "favicon.ico",
        RedirectView.as_view(url=staticfiles_storage.url("favicon.ico")),
    ),
    path('', include('pwa.urls')),  # You MUST use an empty string as the URL prefix
   
    #path('op', include(tf_urls)),
    #path('prueba2fa1',prueba2fa1),

    #comentar antes de subir al servidor
    path('apitdc/', pruebatdc),
    path('tdc/', prueba2tdc),
    
    path('pruebalo1/',pruebaLo1),
    path('pruebalink/',pruebauri),
    
    path('operation/',pruebauri),
    
    # path('libros/', librosPrueba),
    path('libros/', include([
            path('',librosPrueba,name='lib'),
            path('<str:code1>/<str:issuer1>',librosPrueba,name='libros'),
    
            path('<str:code1>/<str:issuer1>/<str:code2>/<str:issuer2>',librosPrueba,name='libros2'),
    ])),

   

    path('SEP07/',include([
        path('', Sep07Views.as_view(), name='sep007'),
        path('<str:operation>', Sep07Views.as_view(), name='sep007-2'),
    ])),

    #path('', HomePageView.as_view(), name='home'),

    path('pruebastellar/',include([
        
        path('',pruebastellar1),
        
        path('api/',include([
              path('toml',getToml), 
              path('transacionEnvelope',getTransactionEnvelope),  
              path('chequearTransaccion',getChequearTransaccion), 
              
              path('balanceSTR',getBalanceSTR),  
        ])),
    
    
    
    ])),



    #url('^api/v1',include('social_django.urls',namespace='social')),
    #path('accounts/',include('allauth.urls')),
    path('admin/', admin.site.urls),
    path('login/',include([

        path('',login3,name='login'),
        path('<int:success>/<int:codigo>',login3,name='logins'),
        path('<str:operation>',login3,name='login-sep07'),
        path('cambio-clave/',cambioClave,name='cambio-clave'),
        path('password/',reclave,name='clave'),
        path('backup2fa/',backup2FA,name='salvar2fa'),
    
    ])),

    path('logingestion/',logingestion,name='logingestion'),#login para la gestion prueba
    path('registro/telegram',registroTelegram,name='registroTelegram'),
    path('contactos',contactos,name='contactos'),
    path('bandeja/',include([

    path('',bandejaSolicitud,name='bandejaInicio'),
     
    path('success/<int:success>/<int:codigo>',bandejaSolicitudSuccess,name='bandejaInicioSuccess'),


    ])),

    

    

    path("",login3,name='login'),
    path('login-2fa',login2fa,name='logingoogle'),
    path('qr_billetera',Qr_billetera,name='qr_billetera'),
    path('pronto_amigo',pronto_amigo,name='pronto_amigo'),
    path('qr_prontoamigo',imagen_prontoamigo,name='qr_prontoamigo'),
    path('reporte_prontoamigo',reporte_prontoamigo,name='reporte_prontoamigo'),
    #path('api/',include(router.urls)),
    path('api/',include([

        path('',include(router.urls)),
        path('sep10',SEP10Auth.as_view(),name="sepp10"),
        path('soliprueba',getDetalleSolicitud),
        path('agregarcontacto/',ApiAgregarContacto),
        path('editarcontacto/',ApiEditarContacto),
        path('eliminarcontacto/',ApiEliminarContacto),

        path('afiliado/',include([
            path('transaccion/',AfiliacionApiView.as_view(),name="afiliado_transa"),
            #path('recompensa/',AfiliacionRecompensaApiView.as_view(),name="afiliado_reco"),
        ])),

        path('stellar/',include([
            path('account/',include([
                path('user',AccountUser.as_view()),
                path('balance/',include([
                    path('',AccountBalancePPG.as_view()),
                    path('detalle',AccountBalanceDetalle.as_view()),
                    path('detalle-referencia',AccountBalanceDetalleReferencia.as_view()),
                    path('detalle-referencia-ppg',AccountBalanceDetalleReferenciaPPG.as_view()),
                ])),
                path('trades',AccountTrades.as_view()),
                path('ofertas',AccountOfertas.as_view()),
                path('historico/',include([
                    path('',AccountHistorico.as_view()),
                    path('detalle',AccountHistoricoDetalle.as_view()),
                ])),
                
            ]))
        ])),


        


        path('v3/',include([
            path('get-trades/',getTradesOfertasAccount),
            path('get-asset-account/',getActivoAccount),
            path('confirmar-frase/',ShowPrivateKey),
            path('generar-frase/',GenerateFrase),  
            path('validar-frase/',ValidarFrase),
            

            path('seguridad/',include([
                path('get-link/',getTemplateLink),
                path('get-qr/',getTemplatePaso2),
                path('get-backup/',getTemplatePaso3),
            ])),

            
            path('comprobar2FA/',comprobar2FA),
            path('balance/',GetBalance),
            path('get-asset/',getActivo),


            path('usuario/',include([

                path('getPublic/',validarUsuarioPublic),
                
                path('get-public/',getUsuarioPublic),
                
                path('newDirect/',newdirectorio),
                
                path('validar-clave/',validarClaveFederacion),
                path('notificaciones/',notificaciones),
                path('notif_intercambio/',notif_intercambio),
                path('cifradotdc/', cifradotdc),

            ])),
           

        ])),

        path('v1/signin', signin),
        
        path('v1/logintele', TeleSignin),
        
        path('v1/getusertoken', TeleGetUserToken),
        
        path('solicitud/',apiSolicitudController,name='apiController'),
        

        
        path('pais/',getPais,name='pais'),
        path('estado/',getEstado,name='estado'),
        path('ciudad/',getCiudad,name='ciudad'),
        path('cuenta/',getCuenta,name='cuenta'),
        path('banco/',getbanco,name='banco'),
        path('stel/',getstel,name='stel'),
        path('tipo-servicios/',getTipoServiciosP,name='tiposerviciosp'),
        path('requisitos-servicios/',getRequisitosServicios,name='requisitosservicios'),
        path('balancetoken/',getBalanceTokenDisp,name='balancetoken'),
        path('data-servicios/',getDataServicios,name='dataservicios'),
        path('bancoretiro/', getbancoRetiro,name='bancoretiro'),
        path('cuentaretiro/', getcuentaRetiro, name='cuentaretiro'),
        path('retiroAprobar/',getRetiro, name='retiroAprobar'),
        path('cuentaRegistro/',getcuentaRegistro, name='cuentaRegistro'),
        path('banco-moneda/',getBancoMoneda),
        
        
        path('usuario/',include([
            
            path('autenticar/',getUsuario),
            path('getconfirmado/',getConfirmadoUsuario),
            path('pushUsuarioJuridico/',GuardarUsuarioJuridico),
            path('pushUsuarioNatural/',GuardarUsuarioNatural),
            path('validar-usuario-email/',validarUsuarioMailReClave),
            path('verificar3/',verificarCodigo3,name='verificarCodigo3'),
            
            path('cambiar-clave/',CambiarClave,name='cambiarClave'),
            path('recuperar-clave/',RecuperarClave,name='recuperarClave'),
        ])),


        path('configuracion/',include([
            path('rol/',getRol,name='apirol'),
            path('instituto/',getInstituto,name='apiinstituto'),
            path('tipo-solicitud/',getTipoSolicitud,name='apitipo-solicitud'),
            path('delete-ruta/',deletRuta,name='apidelete-ruta'),
            path('proceso/',getProceso,name='apiproceso'),
            path('menu/',getMenu,name='apimenu'),
            path('usuario/',getUsuario,name='apiusuario'),
            path('rol-solicitud/',getTipoSolicitudRol,name='rol-solicitud'),
        ])),
    
        
        path('operaciones/',include([
            path('getoferta/',getOferta,name='getoferta'),
            path('getofertaBSS/',getOfertaBSS,name='getofertaBSS'),
            path('resultLumens/',resultLumens,name='resultLumens'),
            path('resultBSS/',resultBSS,name='resultBSS'),
            path('transaccion/',getTransaccion,name='apitransaccion'),
            path('total_usdv/',total_usdv,name='total_usdv'),
            path('paypal_solicitud/',paypal_solicitud,name='paypal_solicitud'),

            path('transaccion2/',getTransaccion2,name='apitransaccion2'),

            path('stellar/',include([

                path('validar-activo',validarActivo,name='apivalidaractivo'),

            ])),
        ])),



    ])),

    
    path('billetera/',include([
        
        path('',billetera,name='billeteraa'),
        path('success/<int:success>/<int:codigo>',billeteraSuccess,name='billeterasu'),
    ])),
    
    path('servicio/',include([
        path('bandeja/',bandejaServicios,name='bandejaser'),
        
        path('bandeja/success/<int:success>/<int:codigo>',bandejaServiciosSuccess,name='bandejasersuccess'),
    ])),
    
    path('agente/',include([
        
        path('bandeja/',bandejaAgente,name='agentebandeja'),
        path('bandeja/success/<int:success>/<int:codigo>',bandejaAgenteSuccess,name='agentebandejasuccess'),
        path('envio/',agentesSolicitudEnvio,name='agentesenvio'),
        path('lista-solicitudes/',agentesSolicitudLista,name='lista-solicitudes'),
        path('desafiliar/',agenteDesafiliar,name='desafiliar'),
    ])),
    
    path('usuario/',include([ 
        
        path('telegram',VincularTelegramViews.as_view(),name='telegramUser'),
        path('validarUsuario/',validarUsuario,name='validarUsuario'),
        path('validarEmail/',validarEmail,name='validarEmail'),
        path('registrar/',registrar,name='registroUsuario'),
        path('prueba/',pruebaIP, name='aprobar-retiros'),
        path('seguridad_IP/', ConfirmacionIP, name='confirmacion_IP' ),
        path('activarIP/<int:co_registro_ip>/<int:co_usuario>', activacionIP),
        path('sesiones/', Usuario_sesiones, name='sesiones'),
        
        path('pre-registro/',preRegistro),
        
        path('get-registrar/',getRegistrar,name='getRegistrarUsuario'),
        
        #path('registrar-success/',registrar,name='registroUsuario'),
        path('verificar/',verificarCodigo,name='verificarCodigo'),
        
        path('verificar2/',verificarCodigo2,name='verificarCodigo2'),

        path('seguridad/',SeguridadUsuario,name='seguridad'),
        path('seguridad/success/<int:success>/<int:codigo>',SeguridadUsuarioSuccess,name='seguridadsuccess'),
        path('seguridad2/',SeguridadUsuario2,name='seguridad2'),
        path('perfil/',perfilUsuario,name="perfilusuario"),
        path('editar/',editarUsuario,name="editarusuario"),#el viejo
        path('editarusuario/',editarUsuario2Fa,name="editarusuario2fa"),#el que estoy haciendo ahora
        path('registrar-cuentas/',registrar_cuentas_usuarios, name='registrar-cuentas'),
        
        path('validar-2fa/',valCorreo,name='validar2fa'),
        path('verificar-pin/',verificarPin2fa,name='verificar-pin'),
        path('Notificaciones/',DesactivarNotificacion, name='Notificaciones'),
        
        
        path('verificar/indentificacion/',include([
            path('',IndentificacionUsuario,name="indentificacionusuario"),
            path('indentidades/',VerificarIndUsuario,name="bandejaVeriInd"),
            path('get-indentidad/',getIndentificacion,name="getindentidad"),
            path('validar-indentidad/',validarIndentificacion,name="validarindentidad")
            ])
        ),
        path('cambio-clave',cambioClaveUsuario,name='cambio-clave-usuario'),

    ])),
    path('solicitud/',include([
        
        path('new-solicitud/',pushSolicitud,name='nuevaSolicitud'),
        path('new-edit/<int:co_tipo_solicitud>',NewEditSolicitud,name='nesoli'),
        path('espera/',emptyDefault,name='espera'),
        path('<int:co_solicitud>/',solicitudController,name='controller'),
        
        path('<int:co_solicitud>/<int:co_solicitud_formulario>',solicitudControllerEspecifico,name='controllerEspecifico'),
        path('especifico/<int:co_tipo_solicitud>/<int:co_solicitud_formulario>',preSolicitudControllerEspecifico,name='preSolicitudEspecifico'),
        
        path('nueva/<int:co_tipo_solicitud>',preSolicitudController,name='preSolicitud'),
    ])),
    

    path('configuracion/',include([
        #path('rol/',rolConfig,name='rol'),
        
        path('rolsolicitud/',rolSolicitud,name='rolsolicitud'),
        path('pais/',paisConfig,name='cpais'),
        path('rol/',rolConfig,name='rol'),
        path('instituto/',institutoConfig,name='instituto'),
        path('tipo-solicitud/',tipoSolicitudConfig,name='tipo-solicitud'),
        path('proceso/',procesoConfig,name='proceso'),
        path('menu/',menuConfig,name='menu'),
        path('ruta/',rutaConfig,name='ruta'),
        
        path('usuario/',usuarioConfig,name='config-usuario'),
    ])),


    path('operaciones/',include([
        path('balance/',balance,name='balance'),
        path('balance/success/<int:success>/<int:codigo>',balanceSuccess,name='balanceSuccess'),


        path('historico/',historico,name='historico'),
        path('historico/filtros/',filtro_historico,name='filtro_historico'),
        path('transferencia/',trasferencia,name='transferencia'),
        
        path('transferencia/completado/<int:co_tipo_solicitud>',trasferenciaSuccess,name='transfsuccess'),
        path('transferencia2/',trasferencia2,name='transferencia2'),
        path('retirarfondos/',retirofondos,name='retirarfondos'),
        path('badeja-retiros/',bandejaRetiros, name='bandeja-retiros'),
        #path('aprobar-retiros/',aprobarRetiros, name='aprobar-retiros'),
        path('USDV/', comprarUsdvTdc),

        path('servicios/',include([
            path('pago/',pagoServicios,name='pagoservicios'),
            
            path('proceso/',procesoServicios,name='procesoservicios'),

            
            path('bandeja/',include([

                path('agentes/',bandejaServiciosGlobal,name='bandejaservicios'),
                
                path('pendientes/',bandejaServiciosPendientes,name='bandejaservpendientes'),
                
                path('cancelar-peticion/',ServiciosProgresoUpdate,name='servicioupdate'),

            ])),

        ])),
        
        path('bandeja',bandeja,name='bandeja'),
    ])),
    path('comercio/', include([
            path('',comercioVario,name='comerciar'),
            path('<str:code1>/<str:issuer1>',comercioVario,name='comerciare'),
    
            path('<str:code1>/<str:issuer1>/<str:code2>/<str:issuer2>',comercioVario,name='comerciare2'),
    ])),
    path('retiro/', include([
            path('',retiroVario,name='retirar'),
    ])),
    path('deposito/', include([
        path('',depositoVario,name='depositar'),
        path('recarga/',include([
            path('BSS/',recargaBSS,name='recargaBSS'),
            path('validarBSS/',validarBSS,name='validarBSS'),
    ])),
    ])),

    path('reporte/',include([
            
        path('reporte-transferencia/<int:co_solicitud>',reporteTransferencia,name='reporte-transferencia'),
        path('qr-pdf/',Qr_pdf,name='qr-pdf'),
            
        ])),


    path('inicio',inicio,name="inicio"),
    
    path('registro/',include([
        path('',RegistroUserViews.as_view(),name='registroUser'),
        path('promotor/',registroPromotor,name='registroUsu'),

    ])),
    #url(r'^inicio',inicio,name='inicio'),
    path('logout/', logout,name='logout')
]

urlpatterns+=staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += [path('mediap/', include(private_storage.urls)),]



if settings.DEBUG:
   import debug_toolbar
   urlpatterns += [
       path('__debug__/', include(debug_toolbar.urls)),
   ]
#urlpatterns += [path('mediap/', MyStorageView.as_view()),]
#urlpatterns += [url('^mediap/(?P<path>.*)$', MyStorageView.as_view())],
#urlpatterns+=router.urls